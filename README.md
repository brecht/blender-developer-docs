# Blender Developer Documentation

Demo: [developer.blender.org/docs](https://developer.blender.org/docs)

This is a new (experimental) repository for Markdown based developer
documentation, using
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

__The official platform for developer documentation is still
[wiki.blender.org](https://wiki.blender.org/).__

## Building this Documentation Offline

The following assumes [Python 3](https://www.python.org/),
[pip](https://pypi.org/project/pip/), [Git](https://git-scm.com/) and
[Git LFS](https://git-lfs.com/) are installed.

**Set up Git LFS for the current user:**
```sh
git lfs install
```

**Clone the documentation sources:**
```sh
git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
```
This will clone the sources into a `developer-docs` directory inside the current
one.

**Install all dependencies, such as
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/):**
```sh
python -m pip install -r requirements.txt
```

**Build this documentation with live reloading:**
```sh
mkdocs serve
```

Alternatively `mkdocs build` will generate the documentation as HTML into a
`site/` directory. Simply open `site/index.html` in a browser.
