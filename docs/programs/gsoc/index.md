# Google Summer of Code

**Google Summer of Code (GSoC) is a Google program focused on bringing new
contributors into open source software development.**

![Google Summer of Code logo](../../images/GSoC-logo.png){width=120 align=left}

GSoC Contributors work with an open source organization on a 12+ week
programming project under the guidance of mentors.

Blender is happy to participate in this initiative since its beginning
in 2005!

<https://summerofcode.withgoogle.com>

## GSoC 2023

Blender Foundation participates in the 2023 GSoC edition with five
projects. Read more about them on the [2023](2023.md) page.

Community Bonding Period: **May 04 - May 28**.

## Projects from Previous years

[2022](2022.md) - [2021](2021.md) - [2020](2020.md) - [2019](2019.md) -
<span class="plainlinks">[2018](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2018)</span> -
<span class="plainlinks">[2017](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2017)</span> -
<span class="plainlinks">[2016](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2016)</span> -
<span class="plainlinks">[2015](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2015)</span> -
<span class="plainlinks">[2014](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2014)</span> -
<span class="plainlinks">[2013](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2013)</span> -
<span class="plainlinks">[2012](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2012)</span> -
<span class="plainlinks">[2011](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2011)</span> -
<span class="plainlinks">[2010](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2010)</span> -
<span class="plainlinks">[2009](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2009)</span> -
<span class="plainlinks">[2008](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2008)</span> -
<span class="plainlinks">[2007](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2007)</span> -
<span class="plainlinks">[2006](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2006)</span> -
<span class="plainlinks">[2005](https://en.blender.org/index.php/Dev:Ref/GoogleSummerOfCode/2005)</span>
