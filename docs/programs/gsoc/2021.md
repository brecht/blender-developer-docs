# Google Summer of Code - 2021 Projects

**Progress updates for 2021**


## Current State

**1<sup>st</sup> evaluation -** 12 - 16 July  
**Final submission -** 23 - 30 August

## Communication

### Development Forum

All Blender and code related topics will go to the devtalk forum. Here
students will also post their weekly reports, and can ask for feedback
on topics or share intermediate results with everyone.

<https://devtalk.blender.org/c/blender/summer-of-code>

### blender.chat

For real-time discussions between students and mentors. Weekly meetings
will be scheduled here as well.

<https://blender.chat>

## Projects

Google has granted 8 projects for the Blender Foundation.

------------------------------------------------------------------------

### Adaptive Cloth Simulator

by **Ish Bosamiya**  
*Mentors:* Sebastian Parborg, Sebastián Barschkis

- [Proposal](https://wiki.blender.org/wiki/User:Ishbosamiya/GSoC2021/Proposal)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2021-continued-development-on-adaptive-cloth-simulator-for-blender-weekly-reports/19205?u=ish_bosamiya)
- [Feedback](https://devtalk.blender.org/t/gsoc-2021-continued-development-on-adaptive-cloth-simulator-for-blender-feedback/19206?u=ish_bosamiya)
- [Final Report](https://wiki.blender.org/wiki/User:Ishbosamiya/GSoC2021/FinalReport)

------------------------------------------------------------------------

### Curve Improvements

by **Dilith Jayakody**  
*Mentors:* Hans Goudey, Falk David

- [Proposal](https://wiki.blender.org/wiki/User:Dilithjay/GSoC_2021/Curve_Improvements)
- [Feedback](https://devtalk.blender.org/t/gsoc-2021-curve-improvements-feedback/19109)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2021-curve-improvements-weekly-reports/19108)
- [Final
  Report](https://wiki.blender.org/wiki/User:Dilithjay/GSoC_2021/Final_Report)

------------------------------------------------------------------------

### Display simulation data for rigid bodies and cloth

by **Soumya Pochiraju**  
*Mentors:* Sebastián Barschkis, Sebastian Parborg

- Proposal
- [Weekly
  Report](https://devtalk.blender.org/t/GSoC-2021-displaying-simulation-data-for-rigid-bodies-and-cloth-weekly-reports/19061)
- [Final report](https://wiki.blender.org/wiki/User:Forest/Final_Report)

------------------------------------------------------------------------

### Knife Tool Improvements

by **Cian Jinks**  
*Mentors:* Howard Trickey

- [Proposal](https://wiki.blender.org/wiki/User:HobbesOS/GSOC2021/Proposal)
- [Feedback](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-feedback/19047)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2021-knife-tool-improvements-weekly-reports/)
- [Final
  Report](https://wiki.blender.org/wiki/User:HobbesOS/GSOC2021/FinalReport)

------------------------------------------------------------------------

### Porting popular modifiers to Geometry Nodes

by **Fabian Schempp**  
*Mentors:* Jacques Lucke, Hans Goudey

- [Proposal](https://wiki.blender.org/wiki/User:FabianSchempp/Proposal)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2021-porting-popular-modifiers-to-geometry-nodes-weekly-reports/)
- [Feedback](https://devtalk.blender.org/t/gsoc-2021-porting-popular-modifiers-to-geometry-nodes-feedback/)
- [Final Report](https://wiki.blender.org/wiki/User:Fabian_Schempp/FinalReport)

------------------------------------------------------------------------

### Regression Testing of Geometry Nodes

by **Himanshi Kalra**  
*Mentors:* Habib Gahbiche, Jacques Lucke

- [Proposal](https://wiki.blender.org/wiki/User:HimanshiKalra/GSoC21/Proposal)
- [Weekly
  Reports](https://devtalk.blender.org/t/gsoc-2021-regression-testing-of-geometry-nodes-weekly-reports/18954)
- [Daily Log](https://wiki.blender.org/wiki/User:HimanshiKalra/GSoC21/log)
- [Final Report](https://wiki.blender.org/wiki/User:HimanshiKalra/GSoC21/FinalReport)

------------------------------------------------------------------------

### UV Editor Improvements

by **Siddhartha Jejurkar**  
*Mentors:* Campbell Barton

- [Proposal](https://wiki.blender.org/wiki/User:Sidd017/Proposal)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2021-uv-editor-improvements-weekly-reports/19060)
- [Final Report](https://wiki.blender.org/wiki/User:Sidd017/FinalReport)

------------------------------------------------------------------------

### Video Sequence Editor strip previews and modification indicators

by **Aditya Jeppu**  
*Mentors:* Richard Antalík

- [Proposal](https://wiki.blender.org/wiki/User:Quantimoney/GSoC2021/Proposal)
- [Feedback](https://devtalk.blender.org/t/gsoc-2021-video-sequence-editor-strip-previews-and-modification-indicators-feedback/19096)
- [Weekly
  Report](https://wiki.blender.org/wiki/User:Quantimoney/GSoC2021/Weekly_Report)
- [Final
  Report](https://wiki.blender.org/wiki/User:Quantimoney/GSoC2021/Final_Report)
