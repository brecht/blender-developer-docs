# Blender Summer of Code 2023 - Ideas

**This page collects potential ideas and mentors for contributor projects
in the summer of 2023.**

## Contacting us

You can contact us via our [developers
forum](https://devtalk.blender.org/c/blender) or on
[blender.chat](https://blender.chat/channel/gsoc-2023).  
For matters you prefer to discuss in private, mail Thomas Dinges: thomas
at blender.org.

## General information for contributors and (potential) mentors

The list below provides ideas for projects that:

- Have a expected project size of ~ 175 working hours or ~350 working
  hours.
- Implements a feature that developers have agreed would be a great
  improvement for Blender.

Choosing an idea from this list is *not* mandatory. Contributors are
encouraged to submit their own proposals or modified versions of the
ideas from this page. Another source of inspiration can be the TODO and
Design tasks listed in the Workboards of [Blender modules](https://projects.blender.org/).

See also <https://summerofcode.withgoogle.com/how-it-works/>

### Mentors

A mentor should be someone with Blender coding experience, who can guide
a contributor during all stages of the work, from design to
implementation.

### Contributors

Contributors who wish to apply should first carefully read the [Getting
Started page](getting_started.md) and check if all the
conditions to participate are met.

If by some reason you can not participate, your contribution outside of
the program is still welcome! Feel free to develop an idea from the page
as long as no contributor chose it.

We especially invite contributors to contribute based on their past
experience and competencies, or based on their current research.

Nearly all project ideas will require a strong working knowledge of C
and/or C++. In addition, we use Python for much of our interface code
(anyone skilled in C/C++ can trivially learn enough Python to make the
needed interface code changes). An exception to this are projects where
only Python is required.


<div class="grid cards" markdown>

- **Get Started**

  [:octicons-arrow-right-24: Contributors' Getting Started Guide and Application Template](getting_started.md)

</div>

## Inspiration

To get an idea of what sort of project can be done for Blender and of
the current development direction, see:

- [Release Notes](../../release_notes/index.md)
- [Code Blog](https://code.blender.org/)
- [GSoC projects for 2022](2022.md)
- [GSoC projects for 2021](2021.md)
- [GSoC projects for 2020](2020.md)
- [GSoC projects for 2019](2019.md)

# Ideas

## Asset Browser / Asset System

### Files as Assets

- *Description:*
  - Support XMP [sidecar
    files](https://en.wikipedia.org/wiki/Sidecar_file) for storing asset
    metadata for files.
  - "Import Files" operator to select files from the file browser to
    display in (and possibly copy to) the asset library. Queries which
    file types are supported (e.g. through add-ons).
  - Basic usages, e.g. drag 'n drop for image files into the 3D view
    behaving just like drag & drop of Blender image data-blocks.
- *Expected outcomes:* Enable core code and/or scripts to display any
  file type in the asset browser. Simplify asset management of
  non-Blender data within Blender.
- *Skills required:* Proficient in C/C++, basic Python
- *Possible mentors:* Julian Eisel, Sybren Stüvel
- *Expected project size:* 350 hours
- *Difficulty:* medium

## Automated Testing

### Core Library Tests

- *Description:* There are multiple libraries in Blender which could
  have improved test automation. These include:
  - BlenLib (BLI) low level libraries for data structures, math and
    filepath handling (some areas are already well tested).
  - BMesh: Mesh editing operations (subdivide, split edges, recalculate
    normals.. etc).
  - ImBuf: 2D image library (scaling, rotation, color operations).
  - *Developers note, feel free to other areas.*
- *Expected outcomes:* Improve development process so changes to
  Blender's core libraries so further development can be validated.
- *Skills required:* Proficient in C programming.
- *Possible mentors:* Campbell Barton, Bastien Montagne, Howard Trickey
- *Expected project size:* 350 hours
- *Difficulty:* easy/medium/hard *(depends a lot on the areas tested)*

### Regression Tests

- *Description:* This would involve using Python to automate detecting
  changes to Blenders behavior, by comparing the result from operations
  in new Blender versions and a reference version. Some areas that would
  be good to test:
  - Object Bone/Constraint:
    - A similar framework to
      [MeshTest](https://developer.blender.org/diffusion/B/browse/master/tests/python/modules/mesh_test.py)
      can be written to automate constrains for different types of
      objects.
  - Shape keys: Extend existing frameworks to support Shape Keys
  - Compositor:
    - Node correctness testing, (e.g. blur node makes image less sharp,
      bright/contrast node makes image brighter regardless of exact
      absolute values etc…)
  - Operators: Simulate user input in unit tests to cover much more
    operators than currently possible.
  - Modifiers: Extend existing framework to support Volume and Grease
    Pencil objects
  - General Mesh: add test cases using existing frameworks to improve
    test coverage, e.g more extensively test different
    modifiers/operators or a combination of both (see
    [T84999](https://developer.blender.org/T84999)).

Any suggestion for other areas is welcome, especially new parts of
Blender or where changes are planned soon.

- *Expected outcomes:* Automated regression testing improves quality of
  Blender by quickly detecting if new features break existing ones, and
  therefore catch bugs way before users do.
- *Skills required:* Proficient in Python programming.
- *Possible mentors:* Campbell Barton, Bastien Montagne, Habib Gahbiche
- *Expected project size:* 175 hours
- *Difficulty:* easy/medium (depends a lot on the areas tested)

## Cycles

### Improve Hydra Render Delegate

- *Benefits*: Make Cycles work better in applications other than
  Blender, and prepare it for native Hydra support in Blender.
- *Description*: There is an [initial implementation of a Hydra render
  delegate](https://projects.blender.org/blender/blender/issues/96731)
  that allows Cycles to render USD files directly, and integrate in
  other applications. However more work is needed to make it feature
  complete and performant.
  - Improve compatibility for lights and cameras, to ensure they match
    other applications and the Blender USD exporter
  - Test USD example scenes like [Alab](https://animallogic.com/alab/)
    and [ASWF
    assets](https://wiki.aswf.io/display/ARW/Links+to+Open+Assets), and
    document and fix problems found
  - Test Cycles as a Hydra render delegate in Blender 4.0, and fix
    issues found
  - Improve UsdPreviewSurface shader loading
  - Support reading of MaterialX shaders as OSL shader nodes
  - Add more AOVs
  - Add device selection and other global render settings in other
    applications
  - Add graphics interop support Linux and macOS for efficient viewport
    rendering in other applications
- *Difficulty*: medium
- *Expected project size*: 175 or 350 depending on the amount of tasks
  chosen
- *Possible mentors*: Brecht Van Lommel

## Flamenco

### Improve Distributed Rendering & Task Execution

- *Benefits*: Give users a simple way to distribute tasks, such as
  rendering, over multiple computers on their network.
- *Description*: Last year Flamenco had a major release. It is more
  popular than ever, but still missing certain key features. Now that
  the basics are there, with some guidance, it is relatively simple to
  add new functionality.
  - Convert the build toolchain to [Mage](https://magefile.org) to
    simplify development and packaging on Windows.
    ([\#102633](https://projects.blender.org/studio/flamenco/issues/102633)
    and
    [\#102671](https://projects.blender.org/studio/flamenco/issues/102671)).
  - Improve stability and predictability
    ([\#99549](https://projects.blender.org/studio/flamenco/issues/99549)
    and
    [\#99417](https://projects.blender.org/studio/flamenco/issues/99417)).
  - Create *job types* for various tasks, such as distributed rendering
    of single images, and more powerful control over parameters for
    studios.
  - Introduce per-Worker logs on the Manager, for introspection and
    debugging.
  - Introduce RNA Overrides
    ([\#101569](https://projects.blender.org/studio/flamenco/issues/101569)).
  - Create a web interface for Flamenco Manager configuration
    ([\#99426](https://projects.blender.org/studio/flamenco/issues/99426)).
- *Requirements*: Familiarity with Go. Depending on the exact tasks that
  will be performed, also knowledge of Blender/Python and web languages
  (HTML/CSS/JavaScript).
- *Difficulty*: medium
- *Expected project size*: 175 hours
- *Possible mentors*: Sybren Stüvel

## Geometry Nodes

### Volume Nodes

- *Description:* OpenVDB comes with many tools for volumes and signed
  distance fields (level sets). Exposing these to geometry nodes in a
  consistent way would be a useful change. The way volume grids
  correspond to fields means that implementing this fully would involve
  supporting existing field operations on voxel values. However, a
  smaller version of the project would involve accessing "built-in"
  grids on volume objects without implementing fields.
- *Expected outcomes:* Provide access to powerful existing volumes
  tools, iterate on how to best map them to geometry nodes and other
  Blender concepts.
- *Skills required:* Proficient in C/C++
- *Possible mentors:* Jacques Lucke, Hans Goudey
- *Expected project size:* 350 hours
- *Difficulty:* advanced

## Modeling

### UV Editor Improvements

- *Description:* A selection of some of the smaller user requests for UV
  editing tools would help users make better UV maps more easily. Some
  example ideas:
  - A tool to straighten selected edges into a line. [This
    request](https://blender.community/c/rightclickselect/mpfbbc/) shows
    some examples.
  - Adapting some more Mesh Edit tools to the 2d UV editing case. For
    example, [Vertex and Edge
    Slide](https://blender.community/c/rightclickselect/66dbbc/).
- *Expected outcomes:* More tools to improve editing of UVs.
- *Skills required:* Proficient in C/C++, familiarity with vector math
- *Possible mentors:* Howard Trickey, Campbell Barton
- *Expected project size:* 350 hours
- *Difficulty:* medium

### UV Stitch Tool Improvements

- *Description:* Support stitching between UV islands resulting in
  gap-less joins at boundaries. This would require fitting one UV island
  to another by deforming it, other tasks may include:
  - Parameters for fitting boundaries, to adjust the tolerance for the
    extents of the boundaries which are merged.
  - Parameters for UV island distortion to adjust how the surrounding
    UV's are adjusted for minimal distortion.
  - Potentially multiple methods to fit islands.
- *Expected outcomes:* Faster stitching of UV islands which are not
  exactly aligned, especially for organic models.
- *Skills required:* Proficient in C/C++, familiarity with vector math
- *Possible mentors:* Campbell Barton
- *Expected project size:* 350 hours
- *Difficulty:* medium

## User Interface

The UI module only suggests refactoring projects here (without user
visible changes) due to limited resources on design topics.

### Continued Outliner Code Refactor

- *Description*: As part of ongoing efforts to improve code quality in
  the UI code, a rewrite of the Outliner code was started, using C++ and
  basic object-oriented programming. Main architectural changes are
  done, but plenty of work is left to port all code to it. Part of the
  deliverables would be some basic technical documentation.
- *Task*: [\#96713](http://developer.blender.org/T96713)
- *Expected outcomes*: More maintainable, testable and scalable code for
  the Outliner. Improved documentation.
- *Skills required*: Proficient in C/C++ and object oriented
  programming.
- *Possible mentors*: Julian Eisel
- *Expected project size:* 175 hours
- *Difficulty*: easy/medium

## Video Sequence Editor

### Performance Improvements

- *Description:* There are areas, that aren't performing as well as they
  could. Examples are seeking in video files, disk cache IO /
  compression speed and single threaded image processing.
- *Expected outcomes:* Updating preview quickly is important for video
  editing experience
- *Skills required:* Proficient in C, FFmpeg knowledge welcome
- *Possible mentors:* Richard Antalík
- *Expected project size:* 350 hours
- *Difficulty:* medium

### Waveform Drawing Improvements

- *Description:* Waveforms are processed by single thread and progress
  bar is used to indicate this process is running. This task can be
  parallelized to read waveforms faster. Instead of a progress bar, the
  available portion of the waveform can be drawn as it loads. Making the
  waveforms visible on strips allows to work with immediately available
  data.
- *Expected outcomes:* Reduce time to draw wavevorms.
- *Skills required:* Proficient in C
- *Possible mentors:* Richard Antalík
- *Expected project size:* 175 hours
- *Difficulty:* medium

### Editing Tools

- *Description:* Currently editing tools are designed to be fast, but if
  you are not familiar with what they do, it is hard to memorize them.
  functionality can be improved with drawing gizmos on top of strips
  that are edited, drawing previews of frames when these are useful for
  operation. Some editing tools use playhead as editing cursor, which
  sometimes is useful but it can be counter productive and more
  interactive approach can be used.
- *Expected outcomes:* More interactive tools that fits tool system make
  it easy to discover functionality
- *Skills required:* Proficient in C
- *Possible mentors:* Richard Antalík
- *Expected project size:* 175 hours
- *Difficulty:* medium

### Preview Tool Improvements

- *Description:* Current interactive tools in preview require selection.
  Non-interactive tools can use selection in preview to perform
  operations that were originally intended to be accessible only from
  timeline. This usually requires simple changes to switch context of
  tool, but in other cases principle of operation may have to be
  adjusted to make this possible.
- *Expected outcomes:* Non-interactive tools can now be used in preview
  context further optimizing workflows.
- *Skills required:* Proficient in C
- *Possible mentors:* Richard Antalík
- *Expected project size:* 175 hours
- *Difficulty:* easy/medium

## Virtual Reality

### VR Windows

- *Description:* Some users have expressed interest in accessing
  Blender's traditional 2D interface during a VR session for adjusting
  scene shading/lighting, positioning objects, and even modeling in VR
  using a mouse and keyboard - all without having to remove one's
  headset. While there are various external solutions (desktop
  mirroring, pass-through), it would be nice to have this functionality
  included in Blender natively to provide a standard, maintainable
  experience for users. In addition to viewing windows in VR and
  interacting via mouse and keyboard, interacting with windows using VR
  controllers would be useful.
- *Expected outcomes:* Access to Blender's 2D interface from within VR
  without relying on external solutions such as desktop mirroring.
- *Skills required:* Proficient in C/C++. Must have an OpenXR compatible
  headset and system setup/OS.
- *Possible mentors:* Peter Kim, Julian Eisel
- *Expected project size:* 350 hours
- *Difficulty:* medium
