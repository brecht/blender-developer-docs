# Google Summer of Code - 2023 Projects

**Progress updates for 2023**

## Current State

The information in this page still to be populated through the duration
of the Google Summer of Code.

**1<sup>st</sup> evaluation -** 10 - 14 July  
**Final submission -** 28 August - 4 September

## Communication

### Development Forum

All Blender and code related topics will go to the devtalk forum. Here
contributors will also post their weekly reports, and can ask for
feedback on topics or share intermediate results with everyone.

<https://devtalk.blender.org/c/contributing-to-blender/summer-of-code/15>

### blender.chat

For real-time discussions between contributors and mentors. Weekly
meetings will be scheduled here as well.

<https://blender.chat/channel/gsoc-2023>

## Projects

Google has granted 5 projects for the Blender Foundation.

------------------------------------------------------------------------

### Flamenco Improvements

by **Eveline Anderson**  
*Mentors:* Sybren Stüvel  
*Code:* <https://projects.blender.org/Evelinealy>

- [Proposal and
  Reports](https://devtalk.blender.org/t/gsoc-2023-flamenco-improvements/29318)

### VSE Waveform Drawing Performance

by **Lucas Tadeu Teixeira**  
*Mentors:* Richard Antalik  
*Code:* <https://projects.blender.org/Yup_Lucas>

- [Proposal and
  Reports](https://devtalk.blender.org/t/gsoc-2023-improve-waveform-drawing-speed/29408)

### UV Editor Improvements

by **Melissa Goon**  
*Mentors:* Campbell Barton  
*Code:* <https://projects.blender.org/Melissa-Goon>

- [Proposal and
  Reports](https://devtalk.blender.org/t/gsoc-2023-uv-editor-improvements/29329)

### Shader Editor Node Preview

by **Colin Marmond**  
*Mentors:* Brecht Van Lommel  
*Code:* <https://projects.blender.org/Kdaf>

- [Proposal and
  Reports](https://devtalk.blender.org/t/gsoc-2023-shader-editor-node-preview-weekly-reports/29359)

### Outliner Tree Refactoring

by **Almaz Shinbay**  
*Mentors:* Julian Eisel  
*Code:* <https://projects.blender.org/Almaz-Shinbay>

- [Proposal and
  Reports](https://devtalk.blender.org/t/gsoc-2023-outliner-tree-refactoring/29355)
