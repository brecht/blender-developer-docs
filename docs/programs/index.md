# Contributor Programs


<div class="grid cards" markdown>

- [**Google Summer of Code (GSoC)**](gsoc/index.md)

  ![Google Summer of Code logo](../images/GSoC-logo.png){width=50 align=left}

  A Google program focused on bringing new contributors into open source
  software development.

  [:octicons-arrow-right-24: GSoC](gsoc/index.md)

</div>
