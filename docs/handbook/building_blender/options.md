# Build Options

By default Blender builds will be similar to official releases. Many
build options are available for debugging, faster builds, and to enable
or disable various features.

## Build Targets

There are a few build targets to choose the overall type of build.

- <b>`make`</b> - build with all features enabled, except Cycles GPU
  binaries
- <b>`make lite`</b> - build with the absolute minimum amount of
  features enabled, either for faster builds or to avoid installing many
  dependencies
- <b>`make release`</b> - complete build with all options enabled
  including [Cycles GPU binaries](cycles_gpu_binaries.md), matching the
  releases on blender.org
- <b>`make headless`</b> - build designed to run from the command line
  without an interface (for renderfarm or server automation)
- <b>`make bpy`</b> - build [as a python module](python_module.md) which can
  be loaded from python directly

For a full list of targets, run <b>`make help`</b>.

## Setup For Developers

### Debug Builds

On Windows in Visual Studio or Xcode on macOS, a single build folder can
contain both release and debug builds, and you can switch between them
in the IDE.

For other platforms, the easiest way to set up a debug build is to build
the debug target. This will create a separate build in
`../build_<platform>_debug`.

``` text
make debug
```

The build type of an existing build can also be changed by setting
`CMAKE_BUILD_TYPE` in the CMake configuration to either `Debug` or
`RelWithDebInfo`.

### Developer Options

We recommend developers to configure CMake to enable [address
sanitizer](../tooling/asan.md), automated
tests and options for faster builds. More details about [tools for
Blender development are here](../tooling/index.md).

The most common options can be enabled by using the `developer` target,
which can be combined with other targets. For example:

``` text
make developer
```

Or to combine it with a debug build as you usually would:

``` text
make debug developer
```

### Ninja

For faster builds, the [Ninja build system](https://ninja-build.org/)
can be used. If ninja is installed and available in the path, it can be
added to the `make` command when setting up the build folder. If there
already exists a build folder with a different build system, the folder
must be removed first. Example command to set up ninja build:

``` text
make debug developer ninja
```

### Caching

Caching helps make rebuilds faster, especially when switching between
git revisions and branches.

#### Linux and macOS

If [ccache](https://ccache.dev/) is installed, it can be used as
follows:

    make ccache

It can be combined with other options, for example:

    make debug developer ccache ninja

The equivalent of the above for Xcode generator would be:

    cmake -S . -B ../build_xcode -C build_files/cmake/config/blender_developer.cmake -DWITH_COMPILER_CCACHE=ON -G Xcode

#### Windows

If [sccache](https://github.com/mozilla/sccache) is installed and
available in the path it can be used, however it will only function with
the ninja generator.

``` text
make developer ninja sccache 
```

## Editing CMake Options

By default, the `CMakeCache.txt` configuration file will be in
`../build_<platform>`. There are a multiple ways to edit it.

- Editing `CMakeCache.txt` in a text editor.
- Opening `CMakeCache.txt` with the CMake GUI, to easily change options
  and re-configure. For example on Linux:

  
``` bash
cmake-gui ../blender
```

- Using **ccmake**, for a command line text interface to easily change
  options and re-configure.

  
``` bash
ccmake ../blender
```

- cmake parameters can also be set on the command line, for example:

:

``` bash
cmake ../blender \
    -DCMAKE_INSTALL_PREFIX=/opt/blender \
    -DWITH_INSTALL_PORTABLE=OFF \
    -DWITH_BUILDINFO=OFF
```

  
These commands are exactly those found in `CMakeCache.txt` so you can
copy commands from there and use them in the command line without
running ccmake.

|CMake GUI|ccmake text interface|
|-|-|
|![](../../images/Cmake-gui.jpg)|![](../../images/Ccmake.jpg)|

## Reproducible Builds

Blender's release builds use the option `WITH_BUILDINFO`

This includes build date and time, which means each build will be
slightly different from the previous.

Typically you can just disable the `WITH_BUILDINFO` option, if you don't
want this, however projects may want to be able to create reproducible
builds. See: <https://wiki.debian.org/> To support this we have 2
optional CMake variables:

- `BUILDINFO_OVERRIDE_DATE`, formatted `YYYY-MM-DD` (year:month:day). e.g.
  "2016-07-11"
- `BUILDINFO_OVERRIDE_TIME`, formatted `HH:MM:SS` (hour:min:seconds). eg
  "23:54:05"

When defined, these will override the current date and time.
