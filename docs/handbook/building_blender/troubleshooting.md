# Resolving Build Failures

## In New Builds

When building the `main` or release branches without modifications,
most building problems are not actually errors in Blender's source code.
Although you can never fully rule out that possibility.

Below are common causes for failing to build.

### Missing Library Dependencies

Missing dependencies cause two types of compiler errors. `No such file`
errors mean a header (.h) file is missing, while `unresolved symbol`
errors when linking mean a library is missing. This is usually because
either a path to the dependency was not set correctly in the build
system, the dependency was not installed, or a wrong version of the
dependency was used.

The recommended way is build Blender is using precompiled libraries,
which provide the required library dependencies. If the build still
fails after following the instructions:

* Check that the `lib` folder is located in the same directory as the
  `blender` source code folder.
* Run `make update` to ensure the libraries match the branch, especially
  when building a release branch or tag.
* Try removing the build folder and compiling from scratch. This may
  be needed after some bigger updates to Blender or switching branches.

### Local Changes

Some complaints of Blender failing to build end up being caused by
developers forgetting they have made changes to their code or are
on a different branch than expected.

Running `git status` shows if you have any changes, and which branch
you are on.

To stash any changes and switch to the main branch:
```bash
git stash
git checkout main
```

Stashed changes can be restored later if desired.

### Unsupported Environment

While Blender is portable, if you compile on a less common operating
system, like NetBSD for example, it may need some minor edits to
compile. The same goes for compilers, less common versions may need some
adjustments if no active developers are currently using them.

Unless you want to spend time supporting less common development
environments, normally it is best to use the default/standard
development tools for your platform.

## Reporting Build Problems

- Use the [Building Blender forum](https://devtalk.blender.org/c/blender/building-blender) or the
  [\#blender-builds channel](https://blender.chat/channel/blender-builds) on blender.chat
  to report your problem.

  Don't paste long messages directly in chat, instead attach them as text files.

- Always include the full build output from start to end and the `CMakeCache.txt`.
  Often the real cause is early in the build log, and the last error may not provide
  enough context.

  To create a full build log you will want to redirect output into a
  file, see ([Linux/macOS](http://stackoverflow.com/a/13591423/432509)
  and [Windows](http://stackoverflow.com/a/1420981/432509)).

- Always mention the operating system, compiler version and git branch.

## After Editing Code

When editing the Blender source code, here are some common errors that
you may run into.

### DNA compile errors

Usually this means you added something to a struct in a header file and
forgot to adjust the amount of padding in the struct. Blender requires
that the structs be multiples of 8, see the
[SDNA Notes](https://archive.blender.org/wiki/index.php/Dev:Source/Architecture/SDNA_Notes/)
for more detail.

### RNA compile errors

This usually means the wrong DNA struct type or property name was
specified.

### Missing Interface Buttons and Menus

Usually this means you added something to the user interface python code
and made a mistake. Check the console for errors.

### Menu or checkbox defaults to the wrong value

Make sure you added a default value in the function that creates the
struct or `DNA_*_defaults.h` file, and that you created a version patch
in the versioning code for initializing structs from existing .blend files.
