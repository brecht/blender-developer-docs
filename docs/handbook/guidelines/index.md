# Guidelines

Here you can find a collection of conventions for various development
activities.

## Style

- [C and C++ Style Guide](c_cpp.md)
- [C and C++ Best Practice](best_practice_c_cpp.md)
- [Python Style Guide](python.md)
- [GLSL Style Guide](glsl.md)
- [Commit Message Guidelines](commit_messages.md)
- [Release Notes Guide](release_notes.md)

- [IDE Configuration](../tooling/clangformat.md#ide-configuration)

## Implementation

- [Blend File Compatibility](compatibility_handling_for_blend_files.md)

## Process

- [New Committers](new_committers.md)
- [Reverting Commits](revert_a_commit.md)
- [Code Quality Day](code_quality_day.md)
