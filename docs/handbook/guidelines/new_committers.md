# New Committers

You've been added as Blender project member with Git write access, welcome!
You can use this access now to work on Blender. You're also welcome to work
on other features or fixes as discussed with the team online.
Please always verify with the [owner of the module](https://projects.blender.org/blender/blender/wiki),
or with one of the [Blender admins](../organization/index.md#admins) before committing.
You can also talk with us on one of the [blender.chat channels](../communication/chat.md) if in doubt.

Here's the standard checklist & guidelines all new devs need to read:

1.  Always ensure that what you commit is either on your "own code", or
    you've verified the patch with the owner of the module. If you want
    to change things in other parts of the code, check with one of the
    developers who owns/maintains that.  

2.  Always carefully check which files you have included in your local
    Git commit before pushing it to the main repository. Use 'git
    status', 'git show' and similar commands to inspect your commit.
    Check carefully if the files marked as modified are actually what
    you want to commit. Also remember to pull in recent changes to
    ensure your own changes are still working.  
    If needed, use 'git rebase --interactive' to reorder, edit, fixup
    (join), or split the **not yet pushed** commits so they keep a
    comprehensible history and do not break 'git bisect' or other tools
    like automatic builds.  

3.  Verify that the licensing and copyright information is correct in
    files you commit (especially for new files).  

4.  We have adopted [Clang Format](../tooling/clangformat.md) as the
    styling tool. You should ensure your coding environment is set up
    for this tool. Before committing or contributing code ensure the
    tools has been run on the files you modify.  

5.  Blender is strictly cross-platform and we only accept code that
    compiles correctly for all current platforms. When in doubt, use the
    [buildbot integration in pull requests](../contributing/pull_requests.md#tests).

6.  Document new features and changes in the [release notes](../../release_notes/index.md),
    either before or directly after committing a feature.
      
7.  Blender has stages in its development cycle called BCon levels.
    Patches may be delayed or refused (even if the patch is acceptable)
    if a new Blender version is being prepared for release. To avoid
    this, make sure to check on the current BCon stage. See the
    [Release Cycle](../release_process/release_cycle.md) docs.

## Best Practice

### Etiquette

- When making very large changes to blender's source, keep yourself
  available for some time after committing them (1-3 hrs) in case your
  changes break blender in some way.
- Don't make large changes just before a release  
  See [Release Cycle](../release_process/release_cycle.md) docs.
- Developers may reply to your commit logs. Be sure to reply comments on
  your commit.
- When committing contributions from others, make sure the log includes
  their name and task / pull request number.

### Code Style

- Follow the [C/C++](c_cpp.md) and [Python](python.md) guidelines.

- When making changes to an area that doesn't follow the guidelines,
  following the existing style may be preferred. Do not mix functional
  and style changes in a single commit.

- Avoid having trailing whitespace in new code.

### Commits

- **Always** double check your commits before pushing
  *(just as a quick verification to make sure other changes aren't
  accidentally being included)*.
- Add [tests](../testing/index.md) when possible.
- Observe the [commit message guidelines](commit_messages.md).

### Branch Usage

Most features should be developed in branches in forks, and then
contributed as pull requests.

For collaboration between multiple developers, branches in the main
Blender repository can be created.

- Use clear names, not cryptic abbreviation.
- Use lowercase and dashes, for example `brush-assets-ui`.
- Avoid committing large binary files. Branches are fetched by
  all developers and increases the repository size for everyone.
- When the feature is merged or the branch no longer serves a
  purposes, delete the branch.
- Inactive branches are periodically moved to the
  [blender-archive repository](https://projects.blender.org/archive/blender-archive).
