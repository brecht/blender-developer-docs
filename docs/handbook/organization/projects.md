# Projects

Projects gather multiple developers and contributors to work on specific
topics. They have a start and end date, while module work is always ongoing.

The main purpose of projects is to allocate time from developers funded
by the Blender Foundation to do focused work on important topics. Other
developers and contributors are welcome to be involved too.

Not all major Blender development needs to be organized as projects, it
can be tackled within modules as well. But projects help clarify the targets
for developers funded by the Blender Foundation.

## Roles

The roles of owners and members are the same as in [modules](modules.md).

## Starting a Project

Admins and the Blender Foundation chairman review and approve proposals
for projects and fit them in the overall planning. Both a design and
engineering plan should be presented.

Projects are announced in articles on the [code blog](https://code.blender.org).

## Running a Project

Once started, a project owner is accountable for the project organization
and outcome. If the project owner is not an admin, one go-to admin will be
consulted about the project progress.

The project page should be as simple and to the point as possible.
Design and engineer plans can have dedicated tasks and documentation,
however a single-paragraph or bullet point version of it is important
to keep the main project task compact.

## Completion

After the project completes the outcome becomes part of regular module
maintenance.

## Examples

- [Virtual Reality - Milestone 1 - Scene Inspection](https://developer.blender.org/T71347)
- [Virtual Reality - Milestone 2 - Continuous Immersive Drawing](https://developer.blender.org/T71348)
- [Library Overrides](https://developer.blender.org/T73318)
- [Multires](https://developer.blender.org/T73317)
- [Particle Nodes](https://developer.blender.org/T73324)
- [Volume Object Type](https://developer.blender.org/T73201)
- [Hair Object Type](https://developer.blender.org/T68981)
- [Faster Animation Playback](https://developer.blender.org/T68908)
- [Scene Editing in Object Mode](https://developer.blender.org/T73359)
- [Fast Highpoly Mesh Editing](https://developer.blender.org/T73360)
- [Alembic / USD](https://developer.blender.org/T73363)
- [Asset Manager “Basics”](https://developer.blender.org/T73366)
