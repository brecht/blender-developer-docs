# Debugging with GDB

There are a couple ways to go about debugging crashes in Blender. Today
I had to learn one of them. The steps are pretty easy to follow. These
instructions **should** work for any platform with Bash and GDB,
typically Unix system or with MinGW/Cygwin Windows.

### Compile Debug Build

How to make a debug build depends on the build system used:

- For CMake, set CMAKE_BUILD_TYPE=Debug
- In Visual Studio, set the Release Configuration to Debug

### Run GDB

Start GDB by changing the working directory to the location of your new
debug build and typing one of the following, depending on the platform:

``` bash
gdb blender.exe
```

``` bash
gdb ./blender
```

Then to start Blender, type:

``` bash
run
```

Now make Blender crash. Blender will freeze, so switch to the GDB
prompt. Get a backtrace by typing:

``` bash
bt
```

A more complete backtrace can be printed using:

``` bash
thread apply all bt full
```

For more information, see this guide: [How to Get a
Backtrace](http://wiki.debian.org/HowToGetABacktrace).

### Tip: Happy AMD graphic card owners

If you are using proprietary AMD drivers under Linux (fglrx), **before
doing any debugging**, run (in a root terminal) :

``` bash
# aticonfig --sb off
```

And reboot your PC (you can also, if you prefer, directly edit your
xorg.conf file, and add, in the Device section, the line
`Option "BlockSignalsOnLock" "off"`)

Else, you may get some (really unfriendly) random freezes, deadlocks, as
fglrx blocks signals in certain situations, and gdb waits for those
signals…

### Copy Crash Information

If you would like to make this information available to a developer you
can copy and paste it from the command prompt.

On Windows, selecting and copying text from the command prompt works
different than you might expect: by right clicking on the windows
header, select Edit \> Mark from the menu. Then highlight your
selection. Then again right click on the header and select Edit \> Copy.
