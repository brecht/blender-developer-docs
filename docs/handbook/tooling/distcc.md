# distcc

distcc is a tool to distribute building C/C++ software across multiple
Unix like systems.

This project has its own documentation, so this is just some notes on
using it with Blender.

- For reference, the web-page for distcc can be found here:  
  <https://github.com/distcc/distcc>
- For community maintained docs, See the arch wiki:  
  <https://wiki.archlinux.org/index.php/Distcc>

## Use Case

For incremental rebuilds and general development, there aren't
significant advantages to distributing the process.

This is mainly useful when you need to rebuild the entire source tree,
many times.

- Using **`git bisect`**.
- Changing compiler flags.
- Changes to heavily used headers.

## Configuration

- Install `distcc` using your systems package manager on 2 or more
  systems.

In the following example, `192.168.1.100` is the master, and
`192.168.1.200`, `192.168.1.201` are the slaves.

### Master

Edit **`/etc/distcc/hosts`**

Example configuration:

` 192.168.1.200/16,lzo,cpp 192.168.1.201/16,lzo,cpp localhost/8,cpp`

Note that the values 16 and 8 represent the number of jobs you want each
system to be given.

- **`cpp`** means the pre-processing is done on build the slaves too.
- **`lzo`** uses lzo compression for file transfer.

### Slaves

Edit **`/etc/default/distcc`**

Example configuration:

` STARTDISTCC=true`  
` ALLOWEDNETS="192.168.1.100"`

## Build System

First start the distcc service on master and slave systems.

``` bash
systemctl start distccd
```

*... replace 'start' with 'enable' if you want this to be stated each
reboot.*

In this example we'll assume your source directory is called
**`blender/`** and create a new build directory called
**`build_distcc/`**.

The following commands assume you're in an existing Blender checkout.

*(Authors note, in CMake 3.4x you should not need to change the
compiler, instead use
[CMAKE_<LANG>_COMPILER_LAUNCHER](https://stackoverflow.com/a/34317588/432509).
This needs to be tested though before updating the docs!
[Ideasman42](https://wiki.blender.org/wiki/User:ideasman42) 10:02, 21 July 2017 (UTC))*

``` bash
mkdir ../build_distcc
cd ../build_distcc
CC="distcc gcc" CXX="distcc g++" cmake ../blender
```

This should configure the new build without any errors.

Now compile the source.

``` bash
pump make -j32
```

In this example the number of jobs is 32, jobs will be split across all
systems. To keep all cores busy you can set the job count to slightly
more than the sum of all cores - since there will be network latency
where cores wait for new jobs.

## Notes

- Building with the instructions above you may get the warning
  **`-Wmissing-include-dirs`** which can be safely ignored.
- When the **`distcc`** daemon isn't running, it will print a warning
  and fall-back to building locally.
- If you try to modify an existing build to use **`distcc`**, CMake will
  detect the compiler as being changed and remove your existing
  **`CMakeCache.txt`** and create a new one (loosing any manual edits!).
