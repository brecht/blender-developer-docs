# ClangFormat

ClangFormat is a tool for automatically formatting code to follow the
Blender code style.

## Policy

Most of the Blender source code is formatted automatically, with only a
few exceptions for externally maintained code.

All developers that commit code to the Blender repository **must use
clang-format before committing**. Otherwise, another developer running
clang-format will end up changing unrelated code.

We highly recommend integrating clang-format into your IDE to
automatically format code on file save. For all other cases, like
committing patches from other developers, run `make format` to format
the code.

## Installation

We provide clang-format as part of our precompiled libraries for
Windows, macOS and Linux.

On Linux, clang-format is also installed by the `install_deps.sh`
script. It can also be installed through the package manager, and must
be version 12.0.0 or higher.

## Basic Usage

To apply code formatting to the entire source code, simply run this from
the blender root directory:

``` bash
make format
```

This is a wrapper around `clang-format` that checks the appropriate
version is available.

## IDE Integration

We highly recommend integrating clang-format into your IDE to
automatically format code, as this makes formatting convenient and hard
to forget.

Most development environments have native support or extensions to
integrate clang-format. Here are some pointers for commonly used IDEs,
more information is easily found online.

- [Visual Studio](../development_environments/visual_studio.md#clangformat)
- [Visual Studio Code](../development_environments/vscode.md#clang-format)
- [Vim](../development_environments/vim.md#clangformat)
- [Emacs](../development_environments/emacs.md#clangformat)
- [QTCreator](../development_environments/qtcreator.md#clangformat)
