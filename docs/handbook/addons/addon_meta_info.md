# Add-on Meta Info

You must include a python dictionary named "bl_info" at the top of your
addon .py file or __init__.py if your Addon is a Python module.

``` python
bl_info = {
    "name": "My Script",
    "description": "Single line explaining what this script exactly does.",
    "author": "John Doe, Jane Doe",
    "version": (1, 0),
    "blender": (2, 65, 0),
    "location": "View3D > Add > Mesh",
    "warning": "", # used for warning icon and text in addons panel
    "doc_url": "http://wiki.blender.org/index.php/Extensions:2.6/Py/"
                "Scripts/My_Script",
    "tracker_url": "https://developer.blender.org/maniphest/task/edit/form/2/",
    "support": "COMMUNITY",
    "category": "Add Mesh",
}
```

Below you find a brief description of the meta data elements:

------------------------------------------------------------------------

##### name

  
*(string)*

Name of the script. This will be displayed in the add-ons menu as the
main entry.

------------------------------------------------------------------------

##### description

  
*(string)*

This short text helps the user to decide if he needs the addon when he
reads the addons list. Any further help text should be put in the help
page (see [\#doc_url](#doc-url)).

------------------------------------------------------------------------

##### author

  
*(string)*

Author name(s).

------------------------------------------------------------------------

##### version

  
*(tuple of integers)*

Script version. Please put any important notes into the warning field,
don't use text in the version number.

------------------------------------------------------------------------

##### blender

  
*(tuple of 3 integers)*

The minimum Blender version required to run the script. In the add-ons
panel this is used to check if the user has a Blender version that is
new enough to enable the script. e.g.
**Blender2.65** -\> `(2, 65, 0)`, note that the last value should
normally be kept to zero, this is blenders sub-versions which scripts
wont normally need to use.

When the version number ends in zero, you need to supply that 0 too, as
in `(2, 50, 0)` and not `(2, 5, 0)`.

------------------------------------------------------------------------

##### location

  
*(string)*

Explains where the new functionality can be found. For example: "View3D
\> Properties \> Measure"

------------------------------------------------------------------------

##### warning

  
*(string)*

Used for warning icon and text in addons panel. If this is empty you
will see nothing; if it is non-empty, you will see a warning icon and
the text you put here alerts the user about a bug or a problem to be
aware of.

------------------------------------------------------------------------

##### support

  
*(string)*

Display support level (*COMMUNITY* by default)

- *OFFICIAL* - Officially supported
- *COMMUNITY* - Maintained by community developers
- *TESTING* - Newly contributed scripts (excluded from release builds)

------------------------------------------------------------------------

##### doc_url

  
*(string)*

Link to the documentation page of the script: here you must put the
script manual.

<!-- -->

  
This url is mandatory for the add-on to be accepted in the Blender Addon
repositories (see
[below](#publishing-a-script-in-blender))'''.

  

------------------------------------------------------------------------

##### tracker_url

  
*(string)*

Optional field to specify a bug tracker other than the default
<https://developer.blender.org>

------------------------------------------------------------------------

##### category

  
*(string)*

Defines the group to which the script belongs. Only one value is allowed
in this field, multiple categories are not supported. The category is
used for filtering in the add-ons panel.

Below is the list of categories currently in use by official add-ons.
Please pay attention to their correct spelling and capitalization, as
you can also type your own custom categories and they will show in the
add-ons panel filtering options.

<table>
<caption><strong>List of categories</strong></caption>
<tbody>
<tr class="odd">
<td><ul>
<li>3D View</li>
<li>Add Mesh</li>
<li>Add Curve</li>
<li>Animation</li>
<li>Compositing</li>
<li>Development</li>
</ul></td>
<td><ul>
<li>Game Engine</li>
<li>Import-Export</li>
<li>Lighting</li>
<li>Material</li>
<li>Mesh</li>
<li>Node</li>
</ul></td>
<td><ul>
<li>Object</li>
<li>Paint</li>
<li>Physics</li>
<li>Render</li>
<li>Rigging</li>
<li>Scene</li>
</ul></td>
<td><ul>
<li>Sequencer</li>
<li>System</li>
<li>Text Editor</li>
<li>UV</li>
<li>User Interface</li>
</ul></td>
</tr>
</tbody>
</table>

**List of categories**

  
