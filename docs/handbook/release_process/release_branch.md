# Stabilizing Branch

## Overview

Towards the end of the [release cycle](release_cycle.md)
a stabilizing branch is created. Below the `blender-v4.0-release`
branch is used as an example.

### Before a release

Bug fixes for the upcoming release must be committed in the stabilizing
branch.

- Commit and push bug fixes to the `blender-v4.XX-release` stabilizing
  branch. This goes for Blender, add-ons, the manual and tests.
- Merge the stabilizing branch into `main`, where the following
  release is being developed.

### After a release

Bug fixes for a corrective release (for example 4.0.1, after 4.0.0 has
been released) must be committed to main and added to a task on
projects.blender.org. Don't commit fixes to the stabilizing branch
yourself!

- Commit and push bug fixes to the `main` branch.
- Check the corresponding
  [milestone](https://projects.blender.org/milestones) for a link to the
  backport list. Put your commit into the *To Be Backported* column. The
  release team will take care of the backport. If you don't have
  permissions to edit the task directly please add a comment instead.
- In special cases when a fix should go directly to the branch (and is
  not suitable for main), open a pull request and put that into the
  backport list instead.

## Details - Before a release

### Committing to the Stabilizing Branch

Develop the commit in a local branch based on the stabilizing branch:

```sh
git checkout -b my-fix blender-v4.0-release
make update
```

`make update` will set all submodules and tests to their corresponding
stabilizing branches. If you have local changes to Blender or
submodules, it will skip updating those repositories. You'll need to
commit (or remove) the local changes first.

Following the [pull request](../contributing/pull_requests.md) workflow,
commit changes to your local branch, push to your fork and create a pull
request. Choose `blender/blender-v4.0-release` as target instead of
`blender/main`. Then merge the pull request as usual.

If you accidentally targeted `main`, you can change it following
[these steps](../contributing/pull_requests.md#rebasing-a-pull-request).


??? Info "Alternative: No pull request"

    If you want to commit directly without a pull request, ensure you are on
    the correct stabilizing branch and update to the latest changes.

    ```sh
    git checkout blender-v4.0-release
    make update
    ```

    Once you have your local `blender-v4.0-release` up-to-date you can
    make your fix with the care you would do in normal `main` development.

    Once done, necessary commits to fix have been made, and you’ve checked
    everything is fine, you can push your changes. As a reminder: always
    commit _only_ that what _you_ have actually changed. If you see
    unrelated changes, just don’t stage nor commit.

### Merge to Main

As soon as you’ve successfully pushed your changes to
`blender-v4.0-release` it is time to also merge that change to
`main`.

```sh
git checkout main
make update
```

You should have latest `main` now. You can now fetch all the latest
change into the release branch, and then merge them into main:

```sh
git fetch origin blender-v4.0-release:blender-v4.0-release
git merge blender-v4.0-release
```

Pay attention to the output. Resolve conflicts if necessary.
Double-check changes with `git log`. If everything looks fine go ahead
and push.

```sh
git push origin main
```

### Submodules

If you need to make fixes in a submodule, then do similar steps as above
and handle pushes and merges with similar care. The only difference is
that `make update` is not needed.

## Subversion

The libraries in the [Subversion](../contributing/subversion.md)
repository also gets a tag for the release. Note that any library
changes made in bcon 3-5 should be committed to that tag as well (as
there is no difference in Subversion between tags and branches).

### Merging Tests and Libraries

Most of the time tests and libraries are unchanged, but changes in them
need to be merged from the release branch. The steps for this are:

- When working on the git release branch, ensure you have run `make
  update` so the svn branches match the git branch.
- Commit test or library changes to the subversion release branch that
  has been checked out.
- For merging, check out the main git branch and again ensure you have
  run `make update` so that the trunk svn branch is checked out.
- Subversion merging is then done as follows.

```sh
cd lib/tests
svn merge ^/tags/blender-4.0-release/lib/tests
svn commit
```
