# User Feedback

Blender has magnitudes of more users than people who are involved with
making it. That makes handling user feedback and feature requests - or
even tracking them sanely - nearly impossible.

Instead we try to organize ourselves in ways that development and users
can cooperate closely. For each module in Blender, there's a small and
competent team working on it - including users helping out.
Feel welcome to use our [channels](chat.md) and get involved with making
Blender better.

## Feature Requests

[Right-Click Select](https://rightclickselect.com/p/) is the official website to
gather feature requests. The content is community maintained, and developers
regularly read it to find out what the most requested features and improvements
are.

Getting feedback on a public forum like blenderartists.org can help you find
out if there is an existing feature requests, alternative solution, or even
if the feature is already under development or available.

Important in feature requests is to motivate why the feature is needed, as
there are usually multiple possible solutions. Understanding the request
well is important to help developers find the right design for Blender.

## Feedback on Ongoing Development

Users are encouraged to test [daily builds](https://builder.blender.org/download/daily/).
If you find something that is broken, please
[Report a Bug](https://projects.blender.org/blender/blender/issues/new?template=.gitea%2fissue_template%2fbug.yaml).
Bug reports are essential to making Blender stable.

Developers are encouraged to start topics in the
[Feature & Design Feedback](https://devtalk.blender.org/c/feature-design-feedback/30)
section of the forum. Here users can test features and changes under
development, and provide feedback before the final release.
