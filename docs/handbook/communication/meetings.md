# Meetings

Most modules have regular meetings. Their announcements and final notes
are posted on [devtalk](https://devtalk.blender.org/tag/meeting), along with
instructions on how to join.

Blender admins have monthly meetings, and their reports are also published
on the forum.
