# Navigating the Code

There a few different strategies for navigating the code. Each may be the
most efficient depending on what you are looking for.

* Browse the code.
* Search through the entire source code with relevant search terms.
* Find the operator or property name connected to a button.

## Code Layout

The [code layout](../../features/code_layout.md) page explains which
folders contain which code.

The `source/blender/editors` folder defines most operators, and is a
good place to start browsing when looking for what a button or tool
does.

Data structures saved to .blend files are mostly defined in
`source/blender/makesdna`, and finding the relevant struct and members
names there can lead you to the code manipulating it.

Many of the menus, panels and buttons are defined in Python in
`release/scripts/startup/bl_ui`. Browsing this code may help you
find the name of the relevant operator or property.

## Search Tools

Using an IDE can make searching through the code convenient and fast.
Simple command line tools like grep can also be used. Learning to use
search tools efficiently is an important skill.

The key to making search efficient is finding keywords that are unlikely
to be used in many other places, so that a small number of relevant
results is returned.

## From the Interface

When enabling the Developer Extras under Preferences > Interface, tooltips
show additional information about the operators and properties connected
to a button.

These names can then be used to search through the code.

- For operators, a name like `bpy.ops.mesh.spin()` might show up.
  The corresponding operator in C/C++ would be called `MESH_OT_spin`
  by convention, so this is what you would search for.

- For properties, most are defined in `source/blender/makesrna`.
  Searching for the string there can help you find the relevant
  [RNA](../../features/core/rna.md) and
  [DNA](../../features/core/dna.md) code.

- Exact tooltip text is also usually unique enough to find the
  corresponding code.

## Videos

Short:

- [Campbell's Tips for Code Navigation](https://www.youtube.com/watch?v=_kniMTbcCko&t=38s)
- [Campbell's Code Navigation and Code Fix](https://www.youtube.com/watch?v=5Ymoav0nNWQ)

Longer:

- [Dalai's Code Dive Video](https://www.youtube.com/watch?v=tCdx7gzp0Ac)
- [Dalai's Bug Fix](https://www.youtube.com/watch?v=tQzKjf2_Hmk&t=157s)

