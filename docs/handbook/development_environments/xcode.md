# Xcode

Xcode is the integrated development environment provided by Apple on
macOS. It can be installed for free from the app store.

## Create an Xcode project

To get started, make sure you have first completed the steps to
[build Blender on macOS](../building_blender/mac.md). Installing Xcode,
installing CMake and downloading the libraries.

Then execute the following instructions in a terminal:

``` bash
cd ~/blender-git/blender
cmake . -B ../build_xcode/ -G "Xcode"
```

This will generate the CMake cache files and an Xcode project file in
`~/blender-git/build_xcode`.

## Building Blender in Xcode

Go to the folder with the generated the project file, and double click
`Blender.xcodeproj` to open it in Xcode.

Then follow these steps to setup your project:

1\. Choose **Automatically Create Schemes** if you are being asked after
opening the project file.  
![](../../images/Dev-Doc-Building-Blender-Xcode11-automatically-create-popup.png)

2\. Change the **Active Scheme** popup in the upper left corner of the
XCode project window to **install**.  
![](../../images/Dev-Doc-Building-Blender-Xcode11-active-scheme-popup.png)

3\. Select menu **Product-\>Scheme-\>Edit Scheme**

  
Edit Scheme is located all the way at the bottom of the list of targets.
Or just Press Command-<.

4\. Select the **Run** item from the left view and you will see
something like this:
![](../../images/Dev-Doc-Building-Blender-Xcode11-Default-SchemeEditPanel.png){width=720}

5\. Select the **Blender App** from the **Executable** popup menu.
![](../../images/Dev-Doc-Building-Blender-Xcode11-Select-Executable-Blender.png){width=720}

6\. Click **Close** to save changes.

Now clicking the **Run** triangle next to the **Active Scheme** popup
should build the application and launch Blender in the debugger.

## Troubleshooting

If you have both the minimal command line tools (in
`/Library/Developer/CommandLineTools/`) and Xcode (usually in
`/Applications`) installed, `xcode-select --print-path` shows the
path to the C/C++ compiler toolchain which will be used by CMake. It can
be reset to a good default by running `sudo xcode-select -r`.
