# Render Tests

## Inspection

Render and OpenGL drawing tests produce a HTML file to show the result
of running regression tests, comparing renders and screenshots against a
reference image. These can be found at:

``` text
~/blender-git/build/tests/report.html
```

## Adding or Updating Tests

When adding a new test, or when there are benign test failures due to
intentional changes, the reference images can be created or updated as
follows:

``` bash
cd ~/blender-git/build
# Update reference images for cycles tests
BLENDER_TEST_UPDATE=1 ctest -R cycles
```

After this all the tests should pass, and .blend files and references
images can be committed.

## Cycles GPU

For testing Cycles GPU rendering, `CYCLES_TEST_DEVICES` in the CMake
configuration is a list of devices to test. By default it is only
`CPU`, but `CUDA`, `OPTIX`, `ONEAPI` and `HIP` can be added.

## EEVEE and Workbench

`WITH_GPU_RENDER_TESTS` enables EEVEE and Workbench tests, which
are currently disabled by default due to different outputs of different
GPUs. They render the same .blend files as Cycles.
