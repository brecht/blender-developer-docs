# Materials Bundle

The material bundle has procedural materials which don't need any
texture files. Their functionality is encapsulated inside a node group
per material. This way only high-level parameters are exposed in the
properties editor.

![](../../../images/Materials_asset_bundle.jpg){style="width:800px;"}

## Catalogs

- Metal
- Wood
- Bricks
- Tiles
- Organic
- Fabric
- Concrete
- Glass
- Water
- Misc
- NPR
