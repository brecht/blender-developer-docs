# Cycles Output and Display Drivers

## Overview

Output of render buffers is done through the implementation of a driver.
There are two types of drivers:

- Output driver: for offline rendering, getting pixels tile by tile.
- Display driver: for efficient display of interactive renders.

## Output Driver

Host applications implement this interface for outputting render buffers
for offline rendering. Drivers can be used to copy the buffers into the
host application or write them directly to disk. This interface may also
be used for interactive display, however the display driver is more
efficient for that purpose.

Cycles standalone has a simple OpenImageIO output driver for saving
renders to disk. Blender has a more advanced output driver for filling
Blender render result buffers.

For details see `output_driver.h`.

## Display Driver

Display driver for efficient interactive display of renders.

Host applications implement this interface for viewport rendering. For
best performance, we recommend:

- Allocating a texture on the GPU to be interactively updated
- Using the graphics interop mechanism to avoid CPU-GPU copying overhead
- Using a dedicated or thread-safe graphics API context for updates, to
  avoid blocking the host application.

Note that the texture is updated from a render thread, which is
different than the thread used for interacting with the `Session`. The
implementation is responsible for mutex locking and graphics API context
activation when required.

For details see `display_driver.h`.
