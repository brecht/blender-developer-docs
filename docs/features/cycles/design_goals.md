# Cycles Design Goals

## Summary

We intend to make a render engine that is interactive and easy to use,
while still supporting many production features.

We do not intend to make a physically correct render engine per se, but
we do think that a physically based shading system is much easier to
use. With a flexible node system and shading language support, various
production tricks are also possible. Not all rendering algorithms must
be physically correct, but we prefer to avoid approximations where we
can.

We do not intend to make a Renderman / micropolygon render engine, but
we do think handling large amounts of geometry for smooth surfaces,
displacement and hair is important. However we also think that rendering
algorithms based on long preprocessing and baking, which are often used
in such Renderman engines, are not sufficiently interactive and user
friendly for most users.

As a result, we intend to improve handling of complex scenes within our
architecture and we believe much progress can be made still, but not at
the cost of (too much) interactivity and ease of use.
