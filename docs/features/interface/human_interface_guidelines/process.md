# Process

The UI module overlaps with all other modules. The communication during
UI development has to be particularly good to avoid conflicts.  
Most points here are good practice for other areas as well.

The page here describes the process formalities. For guidelines on how
to approach projects with design focus, refer to the page on
[User-Centered
Design](user_centered_design.md).

## General Process and Communication

- For non-trivial changes, create a design task as explained
  [below](#design-task).
- For bigger projects (rule of thumb: takes longer than one release
  cycle to develop), create an [own
  project](https://projects.blender.org/blender/blender/projects). The UI
  project should link to it in the navigation menu and the roadmap.
- When committing a change, link to all design tasks and patches in the
  commit description.  
  For every change the rationales should be explicit and easy to reach
  via the commit description, no matter how obvious the rationale seems
  to be.
- Immediately after committing and pushing a change, add it to the
  [release notes](../../../release_notes/index.md) if needed.
  For bigger changes this should already be prepared prior to pushing.

## Submitting Patches

For patches that need UI team involvement:

- **Always** add the *User Interface* project tag.
- If you want the patch to be reviewed for a certain release:
  - Add a TODO task and add the patch revision(s) as "Related Object".
    For example: [\#80338](http://developer.blender.org/T80338),
    [\#77824](http://developer.blender.org/T77824).
  - Notify the UI team about this. They have to discuss if this is a
    reasonable target and will assign it to the release column on the
    workboard if so.
  - As long as the task is not in the release column, it's **not** a
    release target for the UI team. Check with the team again if it
    appears like they didn't act on your request.

## Design Task

- A design task must always mention the motivation or rationale for a
  proposed change.
- A design task for non-trivial changes must always contain a definition
  of done. That is, it must be clear at which point it's fine for the
  developers to move on to other projects as opposed to doing further
  iterations.

## Definition of Done

User interface development typically happens in an iterative fashion.
There never is a clear finish line, but at some point it has to be
drawn.

## Controversial Changes

## Dealing with Feedback
