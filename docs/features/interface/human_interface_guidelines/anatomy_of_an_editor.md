# Anatomy of an Editor

Editors in Blender should all follow the same design patterns. In a way,
editors are own modes with own shortcuts and tools - almost like
different applications. It is crucial that these present themselves in a
familiar way so users don't have to learn additional UI patterns and to
avoid user confusion when switching between editors.

Note however, that since editors are effectively modes, it is important
that each editor is clearly distinguishable from others - even while
following editor design patterns and having as many consistent keymap
items as possible. Check the page on [Modal
Interfaces](modal_interfaces.md) for
more information.

## Main Region

## Headers

Each regular editor has a header. The header is a well known region for
users, that helps them tell apart the multiple editors visible on
screen. It may be hidden in some default layouts, but only in rare, well
reasoned instances.

!!! Info "Regular and Irregular Editors"
    Irregular editors are global ones (top- and status-bar) as well as
    editors opened in temporary windows (Preferences, File Browser, Render
    View, Info Log, Drivers Editor). Note that the editors opened in
    temporary windows are also available to open in the regular split screen
    layout. If they are used this way they count as regular editors, and all
    the usual guidelines for regular editors apply.

### Mode Toggle

If the editor has multiple modes or a similar high-level option (display
types, view mode, shader types for Shader Node Editors, ...) the toggle
for that should be displayed on the left of the header, right after the
editor switcher.

### Editor Pulldowns

All operators related to some editor should be available in the editor's
pulldown section. This is located on the left of the header, after the
Mode toggle (or a similar toggle) if any, otherwise after the editor
switcher.

### Display Options

Display options (e.g. overlays, drawing modes, X-Ray, sorting, ...)
should go to the right. Popovers can be used to group them together and
avoid clutter.

## Editor Level Search Buttons

If the search applies to the main window or the entire editor, it should
be placed in the header, ideally centered. This isn't always the best
solution though, because there already are options there, or the search
button is better kept together with other filtering and display options
on the right of the header. In irregular editors there may also not be a
header. So the center of the header should be the first choice, but use
common sense to make the decision for a particular case.

## Common Regions

There's a variety of regions used by multiple editors, but don't always
make sense.

### Toolbar

Toolbars are on the left by default. They can be toggled with the
<span class="hotkeybg"><span class="hotkey">N</span></span> key
(exception: see [Text Input
Editors](anatomy_of_an_editor.md#text-input-editors)
for exceptions).

### Tool Settings

### Sidebar

Sidebars are on the right by default. They can be toggled with the
<span class="hotkeybg"><span class="hotkey">N</span></span> key
(exception: see [Text Input
Editors](anatomy_of_an_editor.md#text-input-editors)
for exceptions).

### Adjust Last Operation Popup

### Execution Region

### Navigation Region

### Channels Region

### Other regions

In some rare instances an editor might want to use additional regions,
that are not commonly used by other editors. For example the File
Browser has a region at the top showing the file path, navigation
controls, display settings, filter settings and general file operators.

## Text Input Editors

Some editors in Blender receive all text input by default. Issue with
that is that makes it impossible to use common single-letter shortcuts
like <span class="hotkeybg"><span class="hotkey">N</span></span> to
toggle the sidebar.

## What Goes Where?

This is basically an index of what kind of UI elements should go where
in an editor.

**Display Options**
: Display options should go into the header, see the dedicated [Display
  Options](anatomy_of_an_editor.md#display-options) section.

**Editor-Global Options**
: Options that affect multiple tools and/or operators (e.g. transform
  options, snapping, ) or the behavior of the editor itself (e.g. *Sync
  With Outliner* for Properties Editors) go to right side of headers or
  Tool Settings regions. It may make sense to put them into an *Options*
  popup. In some occasions, there quite a few such options at which point
  it may make sense to put the most relevant ones in the center instead
  (if there is space), like done for some 3D View options.

**Main Data Selectors**
: Some editors require selecting the data, typically the data-block to
  operate on. Examples are: The UV/Image Editor (select image), Movie Clip
  Editor (select Clip), Action Editor (select action), Node Editor (select
  Node Tree), ... This option should be in the center of the header.
