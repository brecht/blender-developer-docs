# Line Art Code Structure

## File structure

Here's current file structure for Line Art module.

**`blender/editors/lineart/`**
: - `lineart_cpu.c`: CPU Intersection and occlusion engine. Also handles some grease pencil stroke generations.  
  - `lineart_chain.c`: Chaining operation for occlusion result.  
  - `lineart_ops.c`: Operator registering.  
  - `lineart_util.c`: Utility functions. Including a dedicated memory tool.

**`blender/editors/include/`**
: - `ED_lineart.h`: Definitions of Line Art internal data cache structures, globals and ED_ functions.

**`blender/makesdna/`**
: - `DNA_lineart_types.h`: Definitions of Line Art exposed flags.  
  - `DNA_scene_types.h`: Definitions of SceneLineart.  
  - `DNA_collection_types.h`: Definitions of Collection Line Art flags.  
  - `DNA_object_types.h`: Definitions of Object Line Art flags.

**`blender/gpencil_modifiers/intern/`**
: - `MOD_gpencillineart`: Line Art gpencil modifier. Calls functions from lineart_cpu.c to get data.
