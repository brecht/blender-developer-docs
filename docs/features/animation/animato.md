# Animato – An Improved Animation System for Blender

Animato is Blender's animation system, which was introduced in 2009 for
Blender 2.50.

This page has been copied verbatim from [the old
wiki](https://archive.blender.org/wiki/index.php/Dev:2.5/Source/Animation/Animato/).

------------------------------------------------------------------------

Joshua Leung <aligorith@gmail.com>, January 2009

Blender’s Animation System has been showing its age and deficiencies
recently, as the overall complexity and flexibility brought about during
the last few years as a result of its Open-Sourcing, the growth spurts
from developments necessary for the successful completion of the Open
Movie projects (the ‘Orange’ and ‘Peach’ projects) and the Open Game
project (‘Apricot’), have exceeded the simplistic system that was more
than sufficient for an in-house software for NeoGeo back in 1994.

In addition to the increased stresses on the basic architecture – which
were only made worse by the somewhat hackish implementation of the
Action/NLA/Constraints systems based on the system used by Hash’s
Animation:Master – the increasing complexity of Blender has led to a
snowball effect in the complexity of features demanded by the user-base.
The most notable of these, the “everything is animateable” idiom, was
not feasible to be able to have in the old system, which was based on a
set of fixed defines divided into ID-datablock and ‘fake’ datablock
groupings. Although this system works quite well when there are only a
few ‘static’ settings available (which were grouped directly in
ID-datablocks), the inadequacies of such a system become apparent when
the number of settings increases, and dynamic data (i.e. ID-Properties,
Constraints, Modifiers, Nodes, and also Bones/Pose-Channels) becomes
more widely used.

Furthermore, there were some legacy hacks used to solve some specific
production bottleneck at the time, which have been causing various
usability issues. For instance, rotation curves (for objects) had their
units divided by 10 in order to fit on the same axis scale as other
animation curves, however, this resulted in many inconsistencies in the
means for user interaction to occur. Another problem was the way that
bitflag/boolean values (such as Object_Layers or Material_Mode) were
exposed, with the raw shorts/ints that such values were stored in being
directly exposed to the user, presenting a confusing interface for the
animator. This was due to more of a technical limitation than a

The “Animato” system aims to rectify these issues, by addressing several
of the main issues here in the following ways:

- Data access is performed through the RNA (i.e. Data-API) system, which
  provides a method for accessing all user-editable settings in the same
  way as the UI and PyAPI allow, thus allowing “everything is
  animateable” (avoiding problems previous with accessing dynamic data).
  A number of benefits can be derived immediately from such a step, the
  details of which will be explored further in this document.
- The complexity in the data layout of the old system is simplified
  through the introduction of a new container (AnimData) which gets
  stored in each major ID-block which can be animated, and the
  unification of IPO’s and Actions into a single type for the reuse of
  animation.
- The evaluation code has been streamlined to reduce the need for
  various data to be calculated manually and multiple times by
  individual modules. Part of this stems from the decision to enforce
  strict separate of animation data from drivers, which allows rigs in a
  production environment to be more ‘animator proof’ and also more
  flexible.
- Animation editors have been converted to use a data abstraction
  system, initially designed for use by the Action Editor only, which
  makes it easier to write tools which modify animation data (i.e.
  keyframes) without worrying about where the data comes from. This
  removes the restrictions regarding editing animation from multiple
  sources simultaneously, which was a serious limitation for animators
  working in older versions of Blender. In a framework where “everything
  is animateable”, it is certainly advanteous to be able to animate
  multiple settings at once.

[animato-design.doc](../../images/animato-design.doc)
