# Animation & Rigging: Code Review

Before submitting a patch, read through [Contributing Code](../../../handbook/contributing/index.md).

When a patch is to be reviewed by the Animation & Rigging module, do the
following:

- Make sure the patch description follows the
  **[Ingredients of a Patch](../../../handbook/contributing/index.md#ingredients-of-a-patch)**
  guideline.
- Add developers and artists from the Animation & Rigging module as
  reviewers. Be sure to pick at least one developer (when in doubt, add
  Sybren) as **blocking reviewer**. That way artists are free to approve
  any changes in functionality and user interface, without the patch
  immediately going to "Approved" state.
- **Tag** the patch with the 'Animation & Rigging' project. This can
  only be done after the patch has been created, so you'll have to edit
  it after creation.
