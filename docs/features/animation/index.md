# Animation & Rigging

This section describes the code design and functional requirements of
Animation & Rigging related features.

## Core Concepts

- [Active Keyframe](active_keyframe.md)
- [Parenting](parenting.md)
- [B-Bone Vertex Mapping](b-bone_vertex_mapping.md)
- [IK: iTaSC algorithm](ik.md)

To be documented:

- Animation Channel Filtering,
  used for obtaining animation channels for drawing in UIs, performing
  operations, and evaluating animation.
- Animation Data, how animation data is attached to ID datablocks.
- Keying Sets and Auto-Keying
- Armature Skeletal Animation, how pose data is stored, evaluated, and applied.
- Armature Skeleton Editing, how armatures are created and edited,
  including bone naming conventions, mirroring, symmetrizing.
- Constraints
- Evaluation Pipeline, how animation data is evaluated and applied.
- Interpolation / Blending
- Motion Paths
- Quaternions

## Tools

- [Pose Library](tools/pose_library.md)

To be documented:

- In-Between tools, like the Pose Breakdowner, Relax To Rest Pose, etc.

## Animation & Rigging Related Editors

- [Non-Linear Animation](nla.md)

To be documented:

- Dope Sheet and Action Editor
- Graph Editor
- Timeline

## Historical Design

[Animato](animato.md) is the animation system
introduced by Joshua Leung in 2009. Many of its parts are still in use
in Blender. The design document is spread between the Wiki, a
Word-document, and screenshotted text in that document. It's probably
best to extract the still-current information from it, and move it into
the pages linked above.
