# Skin System

Skin rigs are based on the idea of control point nodes that are
automatically merged between all skin rigs based on their position. This
page documents modules classes involved in this merging system.

The design of the system from the power user stand point is explained in
the [Blender Manual](https://docs.blender.org/manual/en/dev/addons/rigging/rigify/rig_types/skin.html).

# Skin Nodes (`rigify.rigs.skin.skin_nodes`)

This module implements the classes of the control nodes themselves.
There are two types of nodes:

Control Nodes  
:   These nodes are created by deformation skin rigs and used to generate
    the actual controls. When multiple nodes are created at the same
    location, one of them is chosen as the master and generates the control
    bone, while others are redirected to using that control.

Query Nodes  
:   These nodes are used to extract information about control nodes at that
    location. They are grouped by the merge system like other nodes, but
    never become master or generate control bones themselves, are invisible
    to control node code, and it is an error if a control doesn't exist at
    their location.

Control nodes are normally used by instantiating the same provided
class, while query nodes are expected to be subclassed when necessary.

Master control nodes and all query nodes are
[sub-components](rig_class.md#staged-sub-components)
of the [generator plugin](generator.md#generatorplugin) that
implements node merging. Non-master control nodes become sub-components
of their master node.

All nodes must be created during the `initialize` stage and will be
merged at its end.

## ControlNodeLayer Enum

Specifies the role of the control based on the amount of automation
within its owner rig. This is used as part of the merge master selection
heuristics.

`FREE`  
:   The control has no internal automation.

`MIDDLE_PIVOT`  
:   The control depends on the FREE controls, but is above TWEAK.

`TWEAK`  
:   The control depends on others, and has no other controls depending on
    it.

## ControlNodeIcon Enum

Specifies the type of the widget to generate.

`TWEAK`  
:   Generate the tweak widget (sphere).

`MIDDLE_PIVOT`  
:   Generate the middle pivot widget (cube).

`FREE`  
:   Generate the top level control widget (cube).

`CUSTOM`  
:   Invoke a callback on the owner rig to generate the widget.

## ControlNodeEnd Enum

Specifies where in the chain the control is located.

`START`  
:   First control of the chain.

`MIDDLE`  
:   One of the middle controls of the chain.

`END`  
:   Last control of the chain.

## Common Node Attributes

Properties and methods common to both control and query nodes.

`rig`  
:   The rig that created the node.

`obj`  
:   The armature object being generated.

`name`  
:   Name of the node, which can be used to create control and mechanism
    bones.

`point`  
:   Vector location of the node.

`merged_master`  
:   Reference to the node chosen as the representative of all merged with it.

`is_master_node`  
:   True if this node is the master.

`control_bone`  
:   Name of the generated master control bone.

`node_parent`  
:   Parent mechanism generator object for this node if `build_parent` was
    called.

`reparent_bone`  
:   If generated, bone that is parented to this node's parent mechanism and
    copies the control world transform in order to include parent motion
    difference into the local transformation.

`node.build_parent(use=True, reparent=False)`  
:   Create and intern the parent mechanism generator for this node. Returns
    `node_parent`.

If `use` is true, register it as being used (activate the generator).

If `reparent` is true, request a reparent bone to be generated.

## ControlBoneNode

This class is the node that is used to generate actual controls.

Configuration attributes:

`org`  
:   The org bone used as the template for generated bones.

`name_split`  
:   [NameSides](utils/naming.md#name-mirroring) tuple decomposition of the bone name.

`size`  
:   Size of the bone, used to size the control.

`layer`  
:   `ControlNodeLayer` enum value of this node.

`icon`  
:   `ControlNodeIcon` enum value of this node.

`chain_end`  
:   `ControlNodeEnd` enum value of this node.

`rotation`  
:   Orientation Quaternion specifying intended control rotation for this
    control node.

`node_needs_parent`  
:   Specifies that the parent mechanism should be created even if not
    master.

`node_needs_reparent`  
:   Specifies that the reparent mechanism should be created.

`hide_control`  
:   Specifies that the control bone should be generated as MCH to hide it
    from the user.

`allow_scale`  
:   Specifies if scale channels of the control should be unlocked.

`index`  
:   For nodes belonging to chains, index in the chain.

`chain_end_neighbor`  
:   For chain ends nodes, points to the adjacent node.

`__init__(rig, org, name, *, point=None, size=None, needs_parent=False, needs_reparent=False, allow_scale=False, chain_end=MIDDLE, layer=FREE, icon=TWEAK, index=None)`  
:   Initializes the node, assigning some of the above fields from parameters.

`node.get_rotation()`  
:   Returns `rotation`, computing it if necessary.

Merge results:

`merged_into`  
:   Points to the master, or None if master.

`merged`  
:   List of nodes merged into this one. Empty unless master, and doesn't
    include query nodes.

`mirror_siblings`  
:   Mapping from `name_split` to the node for all merged siblings with
    names that are mirrored versions of this node's name.

`mirror_sides_x`, `mirror_sides_z`  
:   Set of all available side enum values among mirror siblings.

`name_merged`, `name_merged_split`  
:   Name with side components cleared if merging with a node of the opposite
    mirror side, and its `NameSides` decomposition.

`node.get_merged_siblings()`  
:   Returns a list containing the master and all nodes merged into it.

## ControlQueryNode

This is the base class for query nodes.

`matched_nodes`  
:   Other nodes matched with this one.

# Skin Parents (`rigify.rigs.skin.skin_parents`)

In order to optimize rig structure and avoid duplication, it is
necessary to be able to compare parent mechanisms for nodes before
actually generating them. To achieve that, the mechanisms are expressed
to nodes via equality-comparable
[LazyRigComponent](rig_class.md#staged-sub-components)
subclasses.

Nodes build their parent mechanism generators from the `build_parent`
method in the following way:

1.  Call `build_own_control_node_parent` of their owner rig.
2.  Call `extend_control_node_parent` of their owner rig and its
    parents, starting from topmost parent.
3.  Call `extend_control_node_parent_post` of their owner rig and its
    parents, starting from the owner rig.

This module provides classes useful both directly and as bases for
generators.

## ControlBoneParentBase

This subclass of LazyRigComponent provides a useful base class for
parent generators. It is however not mandatory to inherit from it.

Lazy component attributes:

`rig`  
:   Points to the rig that created this generator.

`owner`, `node`  
:   Points to the node that owns this generator.

`__init__(rig, node)`  
:   Initializes the lazy component.

Required generator attributes (if not inheriting):

`is_parent_frozen`  
:   This field is set to true if the generator cannot be modified further
    e.g. because it is used by multiple parent chains.

`output_bone`  
:   This field or property returns the generated parent bone.

`parent.enable_component()`  
:   Can be called to enable the lazy component.

`parent.__eq__(other)`  
:   Compares with other parent mechanisms (returns NotImplementedError in
    this class)

## ControlBoneParentOrg

A generator that simply returns a fixed bone (not necessarily an ORG
bone). Does not inherit from ControlBoneParentBase due to its
simplicity.

`output_bone`  
:   Returns the wrapped bone.

`__init__(org)`  
:   Initializes the wrapper. The `org` value may be a lazy closure.

## ControlBoneParentArmature

Generator that interpolates between multiple bones using the Armature
constraint. If the bone list contains only one bone automatically steps
down to simple parenting.

`bones`  
:   Optionally lazy list of targets for the constraint.

`orientation`  
:   Optional Quaternion overriding the mechanism bone orientation (if None
    uses the control node orientation).

`copy_scale`  
:   Optional bone to copy world scale from after the armature constraint.

`copy_rotation`  
:   Optional bone to copy world rotation from after the armature constraint.

`__init__(rig, node, *, bones, orientation=None, copy_scale=None, copy_rotation=None)`  
:   Initializes the generator.

## ControlBoneParentMix

Generator that interpolates between multiple parent mechanisms using the
Armature constraint. If the parent contains only one mechanism
automatically steps down to simple parenting.

This is used internally for mixing parent motion between mirror siblings
of a merged control.

`parents`  
:   List of parent mechanism generators.

`parent_weights`  
:   Weight values for the parent mechanism generators.

`__init__(rig, node, parents, *, suffix=None)`  
:   Initializes the generator. The suffix is used when naming the mechanism bone.

## ControlBoneParentLayer

Base class for generators that build on top of another generator.

`parent`  
:   The generator this one builds on top of.

`__init__(rig, node, parent)`  
:   Initializes the generator.

## ControlBoneWeakParentLayer

Base class for a layered parent generator that is only used for the
reparent source. I.e. it doesn't affect the control for its owner rig,
but only for other rigs that have controls merged into this one and use
reparent.

The special weak behavior is only applied to the final generator
layer(s) in the chain.

## ControlBoneParentOffset

Layered generator that offsets the control's location.

Supports Copy Transforms (Local) constraints and location drivers.
Multiple offsets can be accumulated in the same generator, which will
automatically create as many bones as needed.

Multiple consecutive ControlBoneParentOffset layers will try to combine
themselves to optimize bone count.

`parent.add_copy_local_location(target, *, influence=1, influence_expr=None, influence_vars={})`  
:   Add a Copy Location (Local, Owner Orientation) constraint.

    `target` is the optionally lazy target bone.
    
    `influence` specifies optionally lazy constant influence.
    
    `influence_expr`, `influence_vars` specify an influence expression
    driver (use `$var` syntax like below)

`parent.add_location_driver(orientation, index, expression, variables)`  
:   Add a driver offsetting along the specified axis in the given Quaternion
    orientation.
    
    The keys in the `variables` dictionary may have to be renamed due to
    conflicts between multiple add requests, so the expression should use
    the `$var` syntax of Template to reference them.

`parent.add_limit_distance(target, *, ensure_order=False, ...)`  
:   Add a Limit Distance constraint with the given target and keyword
    arguments.

    `ensure_order` specifies that the limit cannot be reordered to
    optimize multiple ControlBoneParentOffset layers into one.

# Skin Rigs (`rigify.rigs.skin.skin_rigs`)

Rigs using the skin system must inherit from one of the classes from
this module.

The classes implement many overridable callbacks that are invoked by the
node code on the owner rig and its parents in order to provide extension
points into the generation process.

## BaseSkinRig

Base type for all rigs involved in the skin system. This includes both
deformation chain rigs and the parent controller rigs.

Utilities:

`rig_parent_bone`  
:   References the parent bone of the rig.

`rig.get_parent_skin_rig()`  
:   Returns the closest parent that is a BaseSkinRig subclass.

`rig.get_all_parent_skin_rigs()`  
:   Returns a list of all parents that are a BaseSkinRig subclass.

`rig.get_child_chain_parent_next(child_rig)`  
:   If the direct parent is a BaseSkinRig, calls `get_child_chain_parent`;
    otherwise returns `rig_parent_bone`.

`rig.build_control_node_parent_next(node)`  
:   If the direct parent is a BaseSkinRig, calls
    `build_control_node_parent`; otherwise returns `rig_parent_bone`.

Callbacks to override:

`rig.get_child_chain_parent(child_rig, child_parent_bone)`  
:   Returns the (lazy) parent bone to use for the given child chain rig.

    The `child_parent_bone` argument specifies the actual parent bone of
    the direct child rig that called the method.

`rig.build_control_node_parent(node, child_parent_bone)`  
:   Returns the parent mechanism generator for the given control node that
    belongs to a child rig. Called during the `initialize` stage.
    
    The `child_parent_bone` argument specifies the actual parent bone of
    the direct child rig that called the method.

`rig.build_own_control_node_parent(node)`  
:   Returns the parent mechanism generator for nodes directly owned by this
    rig. Called during the `initialize` stage.

`rig.extend_control_node_parent(parent, node)`  
:   Called for all BaseSkinRig parents of the node in parent to child order
    during the `initialize` stage to allow them to adjust the parent.

`rig.extend_control_node_parent_post(parent, node)`  
:   Called for all BaseSkinRig parents of the node in child to parent order
    during the `initialize` stage after `extend_control_node_parent` to
    allow them to adjust the parent.

`rig.extend_control_node_rig(node)`  
:   Called for all BaseSkinRig parents of the node in parent to child order
    during the `rig` stage to allow direct adjustment to the control
    constraints.

## BaseSkinChainRig

Subclass of BaseSkinRig required for all skin rigs that can own control
nodes, rather than only modifying nodes of their children or other rigs.

`chain_priority`  
:   Priority of this chain. If the static value is None, initialized
    dynamically from the `skin_chain_priority` parameter, which is also
    exposed in the rig properties panel.

`rig_parent_bone`  
:   At the start of the `parent_bones` stage re-initialized from
    `get_child_chain_parent_next(self)`.

Callbacks to override:

`rig.get_control_node_rotation(node)`  
:   Called from `node.get_rotation()` to compute the rig-specific
    orientation to use for the given control node of this rig, if not
    overridden by the Orientation Bone option.

`rig.get_control_node_layers(node)`  
:   Called to retrieve the list of bone collections to use for the given
    control node owned by this rig. The final control uses a combination of
    layers for all of merged control nodes.

`rig.make_control_node_widget(node)`  
:   Called to generate the widget if the node uses `ControlNodeIcon.CUSTOM`.

## BaseSkinChainRigWithRotationOption

Subclass of BaseSkinChainRig that supports the Orientation Bone option
that allows overriding the orientation to use for controls via
specifying an arbitrary template bone.

`use_skin_control_orientation_bone` *static*  
:   Can be set to False in a subclass to disable the option.
