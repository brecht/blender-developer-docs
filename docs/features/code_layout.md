# Source Code Layout

This page lists the directories in Blender's code and their use.

- [Diagram showing code layout of modules](https://download.blender.org/ftp/ideasman42/pics/code_layout.webp).
- [Browse the code online](https://projects.blender.org/blender/blender).

**Note:** this documents paths can be validated against Blender's source
using:
[check_wiki_file_structure](https://projects.blender.org/blender/blender/src/branch/main/tools/check_wiki/check_wiki_file_structure.py).

## Directory Layout

### Support Files

<table markdown>
<colgroup>
<col style="width: 43%" />
<col style="width: 57%" />
</colgroup>
<thead>
<tr class="header">
<th scope="col" width="43%"><p>DIRECTORY / FILE</p></th>
<th scope="col"><p>USE</p></th>
</tr>
</thead>
<tbody markdown>
<tr markdown>
<td markdown><p>/<strong>doc/</strong></p></td>
<td markdown><p>Document generation (doxygen and sphinx), licenses, man page and
file format info.<br />
Note that documentation may be comments in the code, or on our developer
wiki. So this isn't the best place to look for introductory
materials.</p></td>
</tr>
<tr markdown>
<td markdown><p>/<strong>locale/</strong></p></td>
<td markdown><p>Translation files for Blender's interface and scripts.</p></td>
</tr>
<tr markdown>
<td markdown><p>/<strong>release/</strong></p></td>
<td markdown><p>Contains data-files included with Blender releases, such as
icons, fonts and platform specific files.</p>
<p>This also includes all Python scripts: add-ons, user-interface
layout, utility modules.</p>
<p><em>Even though in a sense this _is_ source-code, its simply copied
into a directory for releases.</em></p></td>
</tr>
<tr markdown>
<td markdown><p>/release/<strong>bin/</strong></p></td>
<td markdown><p>Contains the "`.blender`" folder, with fonts and such files.
These are copied in place the installation dir.</p></td>
</tr>
<tr markdown>
<td markdown><p>/release/<strong>darwin</strong></p></td>
<td markdown><p>Files to be included for macOS installation.</p></td>
</tr>
<tr markdown>
<td markdown><p>/release/<strong>datafiles</strong></p></td>
<td markdown><p>Fonts, icons, mat-cap images and other files included in Blender
builds.</p></td>
</tr>
<tr markdown>
<td markdown><p>/release/<strong>freedesktop</strong></p></td>
<td markdown><p>Desktop and icon files for the Linux desktop
(X11/Wayland).</p></td>
</tr>
<tr markdown>
<td markdown><p>/release/<strong>license</strong></p></td>
<td markdown><p>The full text for all licenses used by Blender and the libraries
it's distributed with.</p></td>
</tr>
<tr markdown>
<td markdown><p>/release/<strong>lts</strong></p></td>
<td markdown><p>Utilities to assist with the long term support (LTS)
releases.</p></td>
</tr>
<tr markdown>
<td markdown><p>/release/<strong>text</strong></p></td>
<td markdown><p>Readme and license files and text included with Blender
installations.</p></td>
</tr>
<tr markdown>
<td markdown><p>/release/<strong>windows</strong></p></td>
<td markdown><p>Files for MS-Windows installation.</p></td>
</tr>
<tr markdown>
<td markdown><p>/<strong>scripts/</strong></p></td>
<td markdown><p>Python scripts that will be placed in Blender's installation
directories</p></td>
</tr>
<tr markdown>
<td markdown><p>/<strong>tests/</strong></p></td>
<td markdown><p>Files for testing.</p></td>
</tr>
<tr markdown>
<td markdown><p>/tests/<strong>blender_as_python_module/</strong></p></td>
<td markdown><p>Tests for Blender when compiled as a Python module. See: <a
href="https://docs.blender.org/api/current/info_advanced_blender_as_bpy.html">Blender
as a Python Module</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/tests/<strong>gtests/</strong></p></td>
<td markdown><p>C &amp; C++ tests using the gtest framework. See: <a
href="http://wiki.blender.org/index.php/Dev:Doc/Tools/Tests/GTest">Blender's
gtest docs</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/tests/<strong>performance/</strong></p></td>
<td markdown><p>Benchmarks.</p></td>
</tr>
<tr markdown>
<td markdown><p>/tests/<strong>python/</strong></p></td>
<td markdown><p>Various tests for Blender's Python API, as well as other tests
written in Python. See: <a
href="http://wiki.blender.org/index.php/Dev:Doc/Tools/Tests/Python">Blender's
gtest docs</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/<strong>tools/</strong></p></td>
<td markdown><p>Tools and utilities to assist with development (not required for
building). See: <a
href="https://wiki.blender.org/index.php/Dev:Doc/Tools/Blender_Tools_Repo">Blender's
Developer Tools docs</a>.</p></td>
</tr>
</tbody>
</table>

### Build System

<table markdown>
<colgroup>
<col style="width: 43%" />
<col style="width: 57%" />
</colgroup>
<thead>
<tr class="header">
<th scope="col" width="43%"><p>DIRECTORY / FILE</p></th>
<th scope="col"><p>USE</p></th>
</tr>
</thead>
<tbody markdown>
<tr markdown>
<td markdown><p>/<strong>build_files/</strong></p></td>
<td markdown><p>Files needed when building the source</p></td>
</tr>
<tr markdown>
<td markdown><p>/build_files/<strong>build_environment/</strong></p></td>
<td markdown><p>Scripts for setting up a build environment (currently only used
for Linux).</p></td>
</tr>
<tr markdown>
<td markdown><p>/build_files/<strong>buildbot/</strong></p></td>
<td markdown><p>Configuration for automated builds. Used for automated builds on
<a href="http://builder.blender.org">builder.blender.org</a></p>
<p>See: <a href="http://buildbot.net">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/build_files/<strong>cmake/</strong></p></td>
<td markdown><p>CMake build-system modules.</p></td>
</tr>
<tr markdown>
<td markdown><p>/build_files/<strong>config/</strong></p></td>
<td markdown><p>Buildbot configuration, used to create daily builds on the <a
href="https://builder.blender.org">builder.blender.org</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/build_files/<strong>package_spec/</strong></p></td>
<td markdown><p>Specifications for Blender packages (currently supporting various
popular Linux package formats). Note: these aren't currently used for
official releases which use a portable archive, building package files
can be useful still.</p></td>
</tr>
<tr markdown>
<td markdown><p>/build_files/<strong>utils/</strong></p></td>
<td markdown><p>Utility scripts to help with releases &amp; building.</p></td>
</tr>
<tr markdown>
<td markdown><p>/build_files/<strong>windows/</strong></p></td>
<td markdown><p>Files associated with convenience targets on MS-Windows. Commands
such as `make update` and `make icons` are implemented here.</p></td>
</tr>
<tr markdown>
<td markdown><p>/<strong>CMakeLists.txt</strong></p></td>
<td markdown><p>CMake build-system file. See: <a href="http://cmake.org">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/<strong>GNUmakefile</strong></p></td>
<td markdown><p>Make-file convenience wrapper for the CMake build-system, to
quickly setup CMake builds without manual (configure, build, install)
steps. Used on Linux/BSD/maxOS.<br />
There are various useful targets unrelated to building, see: `make help`
for details.</p></td>
</tr>
<tr markdown>
<td markdown><p>/<strong>make.bat</strong></p></td>
<td markdown><p>Batch-file convenience wrapper for the CMake build-system, to
quickly setup CMake builds without manual (configure, build, install)
steps. Used on MS-Windows.<br />
There are various useful targets unrelated to building, see: `make help`
for details.</p></td>
</tr>
</tbody>
</table>

### Application Code (source/)

<table markdown>
<colgroup>
<col style="width: 43%" />
<col style="width: 57%" />
</colgroup>
<thead>
<tr class="header">
<th scope="col" width="43%"><p>DIRECTORY / FILE</p></th>
<th scope="col"><p>USE</p></th>
</tr>
</thead>
<tbody markdown>
<tr markdown>
<td markdown><p>/<strong>source/</strong></p></td>
<td markdown><p>Main source code directory for code maintained by Blender
developers.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/<strong>blender/</strong></p></td>
<td markdown><p>Source code directory for Blender's application code.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>asset_system/</strong></p></td>
<td markdown><p><a href="Source/Architecture/Asset_System/Back_End">Asset system back-end</a> (asset representations, asset
libraries, catalogs, etc.).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>blendthumb/</strong></p></td>
<td markdown><p>Stand-alone utility for extracting preview thumbnails from
`.blend` files so thumbnails can be shown by file-managers.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>blenfont/</strong></p></td>
<td markdown><p>Blender's internal font system</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>blenkernel/</strong></p></td>
<td markdown><p>Kernel functions (data structure manipulation, allocation, free.
No tools or UI stuff, very low level).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>blenlib/</strong></p></td>
<td markdown><p>Internal misc libraries: math functions, lists, random, noise,
memory pools, file operations (platform agnostic).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>blenloader/</strong></p></td>
<td markdown><p>Blend file loading and writing as well as in memory undo file
system.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>blentranslation/</strong></p></td>
<td markdown><p>Internal support for non-English translations (uses <a
href="http://www.gnu.org/software/gettext">gettext</a>). Used by the
user interface for displaying text labels and tool-tips.</p>
<p>See: <a href="Dev:Doc/Process/Translate_Blender">Blender's translations docs</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>bmesh/</strong></p></td>
<td markdown><p>Mesh editing API, with support for n-gons and comprehensive
editing operations. Used in <em>Edit Mode</em> and by modifiers that
perform advanced editing operations (such as bevel).</p>
<p>See: <a href="Source/Modeling/BMesh/Design">BMesh
Design</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>compositor/</strong></p></td>
<td markdown><p>Compositor nodes, operations and execution logic.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>datatoc/</strong></p></td>
<td markdown><p>Utility to convert data files (images, fonts &amp; shaders) into
C source code files containing that data. Used by the build system to
convert data into symbols that can be compiled into the
executable.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>depsgraph/</strong></p></td>
<td markdown><p>Dependency graph API. Used to track relations between various
pieces of data in a Blender file. The evaluation engine is responsible
for updating and scheduling the operation nodes according to their
constraints and relations.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>io/</strong></p></td>
<td markdown><p>Input/output libraries for various format (including 2D &amp; 3D
file formats).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/io/<strong>avi/</strong></p></td>
<td markdown><p>AVI loading and writing code (Only supporting primitive AVI raw,
AVI jpeg and windows codec, not ffmpeg)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/io/<strong>collada/</strong></p></td>
<td markdown><p>Collada interface between blender and the external collada
library</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>draw/</strong></p></td>
<td markdown><p>Viewport drawing and EEVEE rendering engine.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>editors/</strong></p></td>
<td markdown><p>Graphical editor code, tools, UI ... (most of the interesting
code is in there)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>animation/</strong></p></td>
<td markdown><p>F-Curves, keyframes utilities and UI drawing for NLA, Grease
Pencil and Graph Editor.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>armature/</strong></p></td>
<td markdown><p>API and related functions and operations for bones, poses and
armatures in edit mode.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>asset/</strong></p></td>
<td markdown><p>Asset editor: operators and asset manager implementation (uses
the `asset_system`).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>curve/</strong></p></td>
<td markdown><p>Curves and fonts edit operators (selection, copy, extrude...).
Note that this is considered legacy, new development is happening in
`/source/blender/editors/curves/`.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>curves/</strong></p></td>
<td markdown><p>Curves editing functions and operators (editing, selection,
conversion &amp; undo).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>datafiles/</strong></p></td>
<td markdown><p>Single `CMakeLists.txt` that aggregates various editor related
datafiles (brushes, blend files, images...).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>geometry/</strong></p></td>
<td markdown><p>Generic operators for managing geometry attributes which may
apply to meshes, curves &amp; point clouds.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>gizmo_library/</strong></p></td>
<td markdown><p>3D and 2D shapes such as arrows &amp; that can be used to drag
gizmos (the transform gizmo in the 3D view-port for example).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>gpencil/</strong></p></td>
<td markdown><p>Grease pencil editing functions and operators (drawing, armature,
selection...).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>include/</strong></p></td>
<td markdown><p>Include directory for editor related header files.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>interface/</strong></p></td>
<td markdown><p>Widget drawing, widget handling, widget layout generation, 2D
editor scrolling, interface operators, icon API, theme
management.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>io/</strong></p></td>
<td markdown><p>input/Output operators for Alembic, Collada, USD and Grease
Pencil. Note that the implementation is located under
`/source/blender/io/`.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>lattice/</strong></p></td>
<td markdown><p>Lattice related editor functions (selection, duplicate,
undo...).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>mask/</strong></p></td>
<td markdown><p>2D mask editing &amp; operators for creating, selecting,
modifying &amp; animating masks which can used by the compositor and
sequencer.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>mesh/</strong></p></td>
<td markdown><p>Mesh edit operators (bevel, extrude, knife...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>metaball/</strong></p></td>
<td markdown><p>Meta-Ball related editor functions (selection, duplicate,
undo...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>object/</strong></p></td>
<td markdown><p>Object level operators including Add, Transform, Modifiers,
Selection, Bake...</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>physics/</strong></p></td>
<td markdown><p>Physics related operators for editing particles, fluids or
dynamic paint.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>render/</strong></p></td>
<td markdown><p>3D viewport and render engine operators in the editor (view
layers, freestyle, material slots, render...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>scene/</strong></p></td>
<td markdown><p>Scene operators (new &amp; delete).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>screen/</strong></p></td>
<td markdown><p>Screen layout operators for modifying and editing panels, menus
and windows.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>sculpt_paint/</strong></p></td>
<td markdown><p>Sculpt and paint operators (weight paint, texture paint and
sculpt).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>sound/</strong></p></td>
<td markdown><p>Sound operators (open, bake animation, pack, unpack...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_action/</strong></p></td>
<td markdown><p>Action editor: between the low-level F-Curves, and the high-level
NLA editor.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_api/</strong></p></td>
<td markdown><p>Registering space types, space macros and keymaps. Includes a
templace for implementing a custom space editor.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_buttons/</strong></p></td>
<td markdown><p>Buttons utilities (property search, context menu, file
browse...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_clip/</strong></p></td>
<td markdown><p>Movie clip and tracking operators in the editor (solving,
tracking, clip graph...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_console/</strong></p></td>
<td markdown><p>Python console UI and operators.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_file/</strong></p></td>
<td markdown><p>File browser UI and operators (selection, folders, bookmarks...
)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_graph/</strong></p></td>
<td markdown><p>Graph related UI and operators for dealing with keyframes and
FCurve modifiers.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_image/</strong></p></td>
<td markdown><p>Image viewer UI and operators (zooming, panning, render
slots...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_info/</strong></p></td>
<td markdown><p>Scene statistics and info.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_nla/</strong></p></td>
<td markdown><p>Non Linear Action editor UI and operators.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_node/</strong></p></td>
<td markdown><p>Node editor drawing and operators (hide, selection,
link...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_outliner/</strong></p></td>
<td markdown><p>Outliner UI and operators for moving, displaying and editing the
tree.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_script/</strong></p></td>
<td markdown><p>Python script editor (deprecated)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_sequencer/</strong></p></td>
<td markdown><p>Sequencer UI and operators for interacting with strips, modifiers
and proxies.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_spreadsheet/</strong></p></td>
<td markdown><p>Spread-sheet UI to explore geometry data in a table.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_statusbar/</strong></p></td>
<td markdown><p>Interface code for the status-bar (shown at the bottom of the
window).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_text/</strong></p></td>
<td markdown><p>Text editor UI and operators. Used as a basic text editor for raw
text and Python scripts.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_topbar/</strong></p></td>
<td markdown><p>Interface code for the top-bar (shown at the top of the window)
including the file menu and workspace tabs.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_userpref/</strong></p></td>
<td markdown><p>User preference operators (auto execution, asset
library)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>space_view3d/</strong></p></td>
<td markdown><p>3D view operators (rotate, pan, move, select...) and gizmos
(lights, forcefields, ruler, camera...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>transform/</strong></p></td>
<td markdown><p>Transform operators across different spaces including object,
graph, curve or greasepencil.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>undo/</strong></p></td>
<td markdown><p>High level undo logic (undo, redo operators &amp; access to the
undo history). The undo system is defined in `blenkernel`.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>util/</strong></p></td>
<td markdown><p>Utility operators for UI data or for the UI to use.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/editors/<strong>uvedit/</strong></p></td>
<td markdown><p>UV editor operators (selection, align, pack islands...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>functions/</strong></p></td>
<td markdown><p>Run-time type system that implements lazy functions,
multi-function network &amp; generic functions. Used by geometry
nodes.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>geometry/</strong></p></td>
<td markdown><p>Geometry (Mesh) functions such as merging vertices, converting
mesh to curves and other geometry manipulation operations.</p>
<p>Used by modifiers and geometry nodes.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>gpencil_modifiers/</strong></p></td>
<td markdown><p>Grease pencil modifiers.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>gpu/</strong></p></td>
<td markdown><p>GLSL shaders, buffers and other <a
href="Dev:Doc/Blender_Source/Files_Structure#GPU">GPU</a> related functions</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/io/<strong>alembic/</strong></p></td>
<td markdown><p>Support for alembic, a computer graphics interchange framework.
See: <a href="http://www.alembic.io">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/io/<strong>common/</strong></p></td>
<td markdown><p>Shared functionality for importers and exporters.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>freestyle/</strong></p></td>
<td markdown><p>Freestyle NPR rendering engine (scene graph, Python code, stroke,
geometry.. etc).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/io/<strong>gpencil/</strong></p></td>
<td markdown><p>Grease-pencil support for 2D PDF &amp; SVG export and SVG
import.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/io/<strong>stl/</strong></p></td>
<td markdown><p>Import/export support for the STL 3D file format. See: <a
href="https://en.wikipedia.org/wiki/STL_(file_format)">Wikipedia</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/io/<strong>usd/</strong></p></td>
<td markdown><p>Support for Universal Scene Description (USD), an open and
extensible framework and ecosystem for describing, composing, simulating
and collaborating within 3D worlds. See: <a
href="https://graphics.pixar.com/usd">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/io/<strong>wavefront_obj/</strong></p></td>
<td markdown><p>Import/export support for the Wavefront OBJ file format. See: <a
href="https://en.wikipedia.org/wiki/Wavefront_.obj_file">Wikipedia</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>ikplugin/</strong></p></td>
<td markdown><p>Inverse kinematics API, abstracting the itasc and iksolver
modules.<br />
Note that while this system supports multiple inverse-kinematics solvers
there is no <em>plug-in</em> system exposed from a user
perspective.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>imbuf/</strong></p></td>
<td markdown><p>Image buffer API and functions (loading, writing and manipulating
different image formats); seq just has it's own buffer structure, but
the loading is still done by imbuf; imbuf also has code for loading
textures into openGL for display and all that</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>makesdna/</strong></p></td>
<td markdown><p>DNA structures definition: All data structures that are saved
into files are here</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>makesrna/</strong></p></td>
<td markdown><p>RNA definitions and functions. Sits on top of DNA to provide a
low level data access and definition API</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>modifiers/</strong></p></td>
<td markdown><p>Object modifiers (used for meshes, curves and lattices).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>nodes/</strong></p></td>
<td markdown><p>Nodes code: `CMP`: composite, `GEO`: geometry, `SHD`: material,
`TEX`: texture.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>python/</strong></p></td>
<td markdown><p>Python API, source files that embed the <a
href="http://www.python.org">Python</a> scripting language into Blender,
as well as the API's Blender exposes which are written in C. See: <a
href="http://www.blender.org/api/blender_python_api_current">Python API
Reference</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/python/<strong>bmesh/</strong></p></td>
<td markdown><p>The Python API for editing mesh data.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/python/<strong>generic/</strong></p></td>
<td markdown><p>The Python API generic functions (font, opengl, noise,
...)</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/python/<strong>gpu/</strong></p></td>
<td markdown><p>The Python API for drawing using the GPU (geometry, shaders,
frame-buffers &amp; selection).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/python/<strong>intern/</strong></p></td>
<td markdown><p>The primary parts of the API (bpy module, wraps
<em>Operators</em> and <em>RNA</em>).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/python/<strong>mathutils/</strong></p></td>
<td markdown><p>The Python API 3D math module (access to Euler, Vector, Matrix,
Quaternion, Color types).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>render/</strong></p></td>
<td markdown><p>The rendering API and pipeline</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>sequencer/</strong></p></td>
<td markdown><p>Blender's video sequencer editor (sequencer).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>shader_fx/</strong></p></td>
<td markdown><p>Real-time effects (currently used for grease-pencil).</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>simulation/</strong></p></td>
<td markdown><p>Solver for cloth simulation, used by the `BKE_cloth`
API.</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/blender/<strong>windowmanager/</strong></p></td>
<td markdown><p>Blender's internal window manager, event system, operators,
thread jobs, ...</p></td>
</tr>
<tr markdown>
<td markdown><p>/source/<strong>creator/</strong></p></td>
<td markdown><p>Contains the `main(...)` entry point function for the Blender
executable. Handles initialization and command line argument
parsing.</p></td>
</tr>
</tbody>
</table>

### Internal Library Code (intern/)

<table markdown>
<colgroup>
<col style="width: 43%" />
<col style="width: 57%" />
</colgroup>
<thead>
<tr class="header">
<th scope="col" width="43%"><p>DIRECTORY / FILE</p></th>
<th scope="col"><p>USE</p></th>
</tr>
</thead>
<tbody markdown>
<tr markdown>
<td markdown><p>/<strong>intern/</strong></p></td>
<td markdown><p>Abbreviation for <em>internal</em>. This directory contains
source-code maintained by Blender developers but kept as isolated
modules/libraries. Some of these modules are C-API's to libraries
written in C++, since much of Blender's core is written in C and can't
call into C++ code directly.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>atomic/</strong></p></td>
<td markdown><p>Low level atomic operations, for thread-safe atomic updates of
variables.</p>
<p>See jemalloc's <a href="http://www.canonware.com/jemalloc">home
page</a> for the original source.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>audaspace/</strong></p></td>
<td markdown><p>An internal C API for ``audaspace``, see:
``/extern/audaspace``.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>clog/</strong></p></td>
<td markdown><p>C-Logging library, API (similar to glog).</p>
<p>Use for logging output in Blender's C code.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>cycles/</strong></p></td>
<td markdown><p>Cycles rendering engine.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>dualcon/</strong></p></td>
<td markdown><p>This library is used to re-create geometry. Used by the remesh
modifier.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>eigen/</strong></p></td>
<td markdown><p>Library written in C used to wrap the C++ Eigen3
library.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>ffmpeg/</strong></p></td>
<td markdown><p>Library used for video encoding and decoding. Used for reading
and writing many different video formats (movie clip editor, sequencer,
render output... etc).</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>ghost/</strong></p></td>
<td markdown><p>Stands for <em>General Handy Operating System Toolkit</em>.</p>
<p>This library abstracts platform specific operations. So we can avoid
using macOS/Wayland/WIN32/X11 API calls in the rest of Blender's
code.</p>
<p>It handles window management actions, the OpenGL context and reading
events from the mouse and keyboard, it also supports less common input
devices such as a tablet or <a href="#NDOF">NDOF</a>
device.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>guardedalloc/</strong></p></td>
<td markdown><p>This Blender primary memory allocator which is used in place of
the system allocator. It has facilities for (optionally) guarding
against common memory errors. Enabled when running with the `--debug`
command-line argument. Used throuought nearly all of Blender's own
code.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>iksolver/</strong></p></td>
<td markdown><p>Inverse kinematics solver, to constraint a chain of bone to a
target. Used by the IK armature constraint.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>itasc/</strong></p></td>
<td markdown><p>A library used for the <a href="#IK">IK</a>
constraint solver <em>iTaSC</em>, an alternate solver useful for
robotics. Used by the <em>IK plug-in system</em>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>libc_compat/</strong></p></td>
<td markdown><p>Improved GLIBC compatibility on Linux.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>libmv/</strong></p></td>
<td markdown><p>A motion tracking library. Used for motion-tracking in the
movie-clip editor.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>locale/</strong></p></td>
<td markdown><p>Wrapper for `gettext` translation library, not intended for
standalone use. Used by `blentranslation`.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>mantaflow/</strong></p></td>
<td markdown><p>Fluid &amp; smoke simulation library (internal API to wrap
`extern/mantaflow`).</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>memutil/</strong></p></td>
<td markdown><p>Utility library with C++ memory management classes. Generic
cache-limiter and reference-counted implimentations and their
C-API's.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>mikktspace/</strong></p></td>
<td markdown><p>Utility module for calculating tangents from mesh geometry
(triangles and quads) and their UV coordinates. Used by the DerivedMesh
system and Blender Internal Render Engine.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>opencolorio/</strong></p></td>
<td markdown><p>A C-API to the OpenColorIO Library (often abbreviated to OCIO).
Used by image loading and saving code to support color-management, as
well as rendering.</p>
<p>See <a href="http://opencolorio.org">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>opensubdiv/</strong></p></td>
<td markdown><p>Subdivision surface library. Used by Blender's subdivision
surface modifier.</p>
<p>See: <a href="http://graphics.pixar.com/opensubdiv">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>openvdb/</strong></p></td>
<td markdown><p>Library used for efficiently store volumetric data. Used for
storing smoke simulation cache.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>quadriflow/</strong></p></td>
<td markdown><p>A C-API for the quadreflow remeshing library (see
`extern/quadriflow`).</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>renderdoc_dynload/</strong></p></td>
<td markdown><p>RenderDoc is a graphic debugger for multiple platforms and output
systems.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>rigidbody/</strong></p></td>
<td markdown><p>A C-API to the Bullet physics library. Used by the rigid-body
physics system.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>sky/</strong></p></td>
<td markdown><p>Parametric sky texture (used by Cycles and EEVEE).</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>utfconv/</strong></p></td>
<td markdown><p>Utility functions for converting between unicode `utf-8` and
`utf-16` (`wchar_t`). Used for file operations on MS-Windows, which
require `utf-16`.</p></td>
</tr>
<tr markdown>
<td markdown><p>/intern/<strong>wayland_dynload/</strong></p></td>
<td markdown><p>Support for dynamically loading WAYLAND and related libraries on
Linux. Allowing WAYLAND to be supported on Linux without it being a
requirement.</p></td>
</tr>
</tbody>
</table>

### External Library Code (extern/)

<table markdown>
<colgroup>
<col style="width: 43%" />
<col style="width: 57%" />
</colgroup>
<thead>
<tr class="header">
<th scope="col" width="43%"><p>DIRECTORY / FILE</p></th>
<th scope="col"><p>USE</p></th>
</tr>
</thead>
<tbody markdown>
<tr markdown>
<td markdown><p>/<strong>extern/</strong></p></td>
<td markdown><p>Abbreviation for <em>external</em>, This directory contains
source-code imported from other projects and not apart of Blender
(strictly speaking).</p>
<p>Each library is stand-alone and typically included because they
aren't common system libraries we can rely on the system providing (such
as `libjpeg`, `zlib` and `libc`).</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>audaspace/</strong></p></td>
<td markdown><p>An audio library which abstracts playback across different back
ends, as well as handling 3D audio. Used for audio playback in the
sequencer, speaker objects and by the <em>Game Engine</em>.</p>
<p>See: <a href="http://github.com/neXyon/audaspace">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>binreloc/</strong></p></td>
<td markdown><p>Utility to find the absolute path of the currently running
executable on Linux. Used for launching animation playback and some
scripts which launch a Blender sub-process.</p>
<p>See: <a
href="http://web.archive.org/web/20090125113506/http://autopackage.org/docs/binreloc">archived
home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>bullet2/</strong></p></td>
<td markdown><p>A real-time physics library. Used for animating rigid-body
physics and in the <em>Game Engine</em>.</p>
<p>See: <a href="http://bulletphysics.org">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>ceres/</strong></p></td>
<td markdown><p>Ceres Solver is a C++ library for modeling and solving large,
complicated optimization problems. Used for motion tracking.</p>
<p>See: <a href="http://ceres-solver.org">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>clew/</strong></p></td>
<td markdown><p>An <a href="#extension_wrangler">extension
wrangler</a> library for OpenCL, a library used for executing code both
on the CPU and GPU with the ability to take advantage of many <a
href="#GPU">GPU</a> cores.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>cuew/</strong></p></td>
<td markdown><p>CUDA Extension Wrangler.</p>
<p>See: <a href="http://github.com/CudaWrangler/cuew">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>curve_fit_nd/</strong></p></td>
<td markdown><p>Library for calculating bezier (cubic) curve paths from input
points. Used for freehand curve drawing.</p>
<p>See: <a href="https://github.com/ideasman42/curve-fit-nd">home
page</a></p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>draco/</strong></p></td>
<td markdown><p>An open-source library for compressing and decompressing 3D
geometric meshes and point clouds. Used by the Python `draco` module
which is used by the `io_scene_gltf2` add-ons (GLTF support).</p>
<p>See: <a href="https://google.github.io/draco">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>Eigen3/</strong></p></td>
<td markdown><p>A linear algebra header-only library to perform vector, matrix
math operations and a sparse matrix solver. Used by motion-tracking,
ITASC <a href="#IK">IK</a> solver and BLI math
library.</p>
<p>See: <a href="http://eigen.tuxfamily.org/index.php">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>fast_float/</strong></p></td>
<td markdown><p>Fast conversion of text to floating point values. Used by the OBJ
and STL importers.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>fmtlib/</strong></p></td>
<td markdown><p>Formatting library providing a fast and safe alternative to C
stdio and C++ iostreams. Used by the wavefront OBJ exporter.</p>
<p>See: <a href="https://github.com/fmtlib/fmt">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>gflags/</strong></p></td>
<td markdown><p>A C++ command line flag parsing library. Not used directly, only
included as a dependency for glog.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>glew-es/</strong></p></td>
<td markdown><p>An <a href="#extension_wrangler">extension
wrangler</a> library for OpenGL-ES. A limited version of OpenGL,
supported on mobile devices. Note that OpenGL-ES is currently not
supported.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>glog/</strong></p></td>
<td markdown><p>A C++ logging framework. Used by `/extern/ceres`, `/intern/libmv`
&amp; `/tests/gtests`.</p>
<p>See: <a href="http://github.com/google/glog">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>gmock/</strong></p></td>
<td markdown><p>A mocking library for C++. Used by `gtest`.</p>
<p>See: <a
href="https://google.github.io/googletest/gmock_for_dummies.html">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>gtest/</strong></p></td>
<td markdown><p>A C++ testing framework. Used for testing C and C++ code in
``tests/``</p>
<p>See: <a href="http://github.com/google/googletest">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>hipew/</strong></p></td>
<td markdown><p>The HIP Extension Wrangler Library (HIPEW) is a cross-platform
open-source C/C++ library to dynamically load the HIP library. HIP
(Heterogeneous-Compute Interface for Portability) is an API for C++
programming on AMD GPUs. Used by cycles.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>json/</strong></p></td>
<td markdown><p>JavaScript Object Notation (JSON) library for modern C++. Used by
`tinygltf` and `BLI_serialize`.</p>
<p>See: <a href="https://github.com/nlohmann/json">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>lzma/</strong></p></td>
<td markdown><p>A compression library focusing on best compression at the expense
of processing time. Used for compressing physics cache.</p>
<p>See: <a href="http://www.7-zip.org/sdk.html">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>lzo/</strong></p></td>
<td markdown><p>A compression library focusing on fast compression at the expense
of final file-size. Used for compressing physics cache.</p>
<p>See: <a href="http://www.oberhumer.com/opensource/lzo">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>mantaflow/</strong></p></td>
<td markdown><p>Fluid &amp; smoke simulation library. See: <a
href="http://mantaflow.com">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>nanosvg/</strong></p></td>
<td markdown><p>Support for reading scalable-vector-graphics (SVG), used by
grease-pencil SVG import. See: <a
href="https://github.com/memononen/nanosvg">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>quadriflow/</strong></p></td>
<td markdown><p>Quad re-meshing library, used for sculpt-mode remeshing. See: <a
href="https://github.com/hjwdzh/QuadriFlow">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>rangetree/</strong></p></td>
<td markdown><p>Basic library for storing non-overlapping scalar ranges. Used by
dynamic-topology sculpting, to store unique ID's for undo data. See: <a
href="https://github.com/ideasman42/rangetree-c">home page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>renderdoc/</strong></p></td>
<td markdown><p>Graphics debugger</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>sdlew/</strong></p></td>
<td markdown><p>An <a href="#extension_wrangler">extension
wrangler</a> library for the SDL library. Used for audio and joystick
support in the <em>Game Engine</em>.</p>
<p>See: <a href="http://github.com/SDLWrangler/sdlew">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>smaa_areatex/</strong></p></td>
<td markdown><p>A C++ implementation of Enhanced Subpixel Morphological
Antialiasing (SMAA). Used by the viewport and GPU compositor.</p>
<p>See: <a href="https://github.com/iRi-E/smaa-cpp">home
page.</a></p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>tinygltf/</strong></p></td>
<td markdown><p>A C++ library for loading 3D models in the GLTF file format. Used
by Blender's virtual reality support (`GHOST_XrControllerModel`).</p>
<p>See: <a href="https://github.com/syoyo/tinygltf">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>vulkan_memory_allocator/</strong></p></td>
<td markdown><p>Easy to integrate Vulkan memory allocation library. Used by
Blender's Vulkan GPU back-end.</p>
<p>See: <a
href="https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html">home
page</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>wcwidth/</strong></p></td>
<td markdown><p>Support for fixed width unicode characters, where some characters
use multiple <em>cells</em>. Used for the text editor and Python
console.</p>
<p>See: <a href="http://www.cl.cam.ac.uk/~mgk25/ucs/wcwidth.c">online
source</a>.</p></td>
</tr>
<tr markdown>
<td markdown><p>/extern/<strong>xdnd/</strong></p></td>
<td markdown><p>Utility to support drag and drop on Unix/X11. Used to support
dropping files onto Blender's window.</p></td>
</tr>
</tbody>
</table>


