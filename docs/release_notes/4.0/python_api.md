# Blender 4.0: Python API

## Breaking changes

### Asset System

- `context.asset_file_handle` is removed. Use `context.asset`
  instead.  
  Note that it returns a `AssetRepresentation`, not a
  `FileSelectEntry`, but should provide access to the same
  information. When not (e.g. because an asset's preview icon-ID is
  needed), `context.active_file` may be available instead.
  (blender/blender@f6a6b27ac1)
- `context.selected_asset_files` is removed, use
  `context.selected_assets` instead. The same note as for
  `context.asset_file_handle` applies.
- `FileSelectEntry`:
  - `.id_type` was removed. For assets, use
    `AssetRepresentation.id_type` instead.
    (blender/blender@d973cc7992)
  - `.local_id` was removed. For assets, use
    `AssetRepresentation.local_id` instead.
    (blender/blender@d973cc7992)
- `AssetHandle`:
  - `get_full_library_path()` was removed. Use
    `AssetRepresentation.full_library_path` instead.
    (blender/blender@d973cc7992)
  - `local_id` was removed. Use `AssetRepresentation.local_id`
    instead.
    (blender/blender@d973cc7992)
- Occurrences of the name `asset_library_ref` were replaced by
  `asset_library_reference` throughout the API
  (blender/blender@974d70918b582cff41b12b29105a0f85786b27fe).
- `FileAssetSelectParams.import_type` was renamed to
  `FileAssetSelectParams.import_method`
  (blender/blender@ca2a8be15fef453bacc03be3eba66d7ca837f15d).

Note: `AssetHandle` should now only be used to provide a collection
custom property for `UILayout.template_asset_view()`.

### Window Manager

- `Operator.bl_property` no longer defaults to "type"
  (blender/blender@7bc34283eacce03fee8506ee5744ff07dd1e6820)

### Mesh

- The mesh format changes [from previous
  versions](../3.6/python_api.md#internal-mesh-format)
  are now included in the Blender file format, reducing overhead when
  saving files and decreasing file sizes
  (blender/blender@1b63a290c68636211b16c5e212a699e6b63031b9).
  - Blender 3.6 can read files from 4.0, but earlier versions cannot.
  - Blender 3.6 can be used to save files from 4.0 in a format that is
    compatible with older versions.
- Face Maps have been removed, with their values converted automatically
  to an integer attribute
  (blender/blender@46cf09327001331c77bcd54ceab73404a1733172).
- Bevel weights have been moved to generic attributes with the names
  `bevel_weight_vert` and `bevel_weight_edge`
  (blender/blender@2a56403cb0dbcbc1dfb19a9bf7e6434517cbdca9).
  - Existing access to bevel weights (`MeshEdge.bevel_weight`) has
    been removed in favor of the attribute API.
  - Forward compatibility is not preserved, though old versions will
    read the new generic attribute.
- Vertex and edge crease have been moved to generic attributes as well,
  with the names `crease_vert` and `crease_edge`
  (blender/blender@e5ec04d73c7873498f4052cbb9f58acfdaf4b7b0).
  - The `MeshEdge.crease` property has been removed.
  - The mesh properties `vertex_creases` and `edge_creases` now
    return attributes directly.
  - New functions `{vertex/edge}_creases_ensure` and
    `{vertex/edge}_creases_remove` create and remove the attributes
    with the proper domain and type.
- The `sculpt_vertex_colors` API has been removed, replaced by
  `mesh.color_attributes`
  (blender/blender@6805657a39b7eeb18b642c769900c6fac46d8367).
- Other mesh custom data functions redundant with the attribute API have
  been removed: `vertex_layers_float`, `vertex_layers_int`,
  `vertex_layers_string`, `polygon_layers_float`,
  `polygon_layers_int`, `polygon_layers_string`
  (blender/blender@d41021a7d4112bdc54fdc93c452348e01a57b5a2).
- `Mesh.calc_normals` is no longer needed and has been removed
  (blender/blender@ab5fc46872b9960b5bb50d98147bea0d677028b9).
- `bmesh_from_object` and `BVHTree` `FromObject` now require an
  evaluated dependency graph to retrieve the final mesh
  (blender/blender@e64b3c821252fc8707b4dcd9f8d1a0dee45c7d3f).
- A deprecated argument has been removed from the `split_faces`
  function
  (blender/blender@efbcfd870301b9d3d257c74fb13020575a0b6216).

### Curves

- The `curves.select_end` operator is now named
  `curves.select_ends`. It has a `amount_start` and `amount_end`
  property to select points from both the front and back.

### Nodes

- The unused `width_hidden` property has been removed
  (blender/blender@30a20b4def3350cced29eb72e7e586ccbe7f1821).
- `NodeItem` and `NodeCategory` definitions have been removed for
  shader and compositor nodes
  (blender/blender@15f5dfd45d14655f715617802f371f7f6b7a26ae).
  - The add menus can be extended the [standard
    way](https://docs.blender.org/api/4.0/bpy.types.Menu.html#extending-menus)
    from the Python API, with the ID names
    `NODE_MT_shader_node_add_all` and
    `NODE_MT_compositor_node_add_all`.
- `node.inputs\[xxx\]` and `node.outputs\[xxx\]` take socket
  identifiers and availability status into account
  (blender/blender@e4ad58114b9d56fe838396a97fe09aff32c79c6a).

### Shader Nodes

- The Glossy BSDF and Anisotropic BSDF nodes have been merged
  (blender/blender@8cde7d8f8a5a07661ad614aa7716ddc1cc7c182e).
  In the Python API, the resulting node is referred to as
  `ShaderNodeBsdfAnisotropic`. When creating nodes,
  `ShaderNodeBsdfGlossy` is still accepted as an alias, but it will
  result in a `ShaderNodeBsdfAnisotropic` being created. Add-ons and
  scripts should replace usage of `ShaderNodeBsdfGlossy` with
  `ShaderNodeBsdfAnisotropic`, which will also work with previous
  versions all the way to pre-2.80.
- The [Principled BSDF node has been
  revamped](shading.md#Principled_BSDF)
  and now aligns more closely to Standard Surface and OpenPBR. Various
  sockets were renamed:
  - `Subsurface` -\> `Subsurface Weight`
  - `Subsurface Color` removed, use `Base Color` instead
  - `Specular` -\> `Specular IOR Level`
  - `Specular Tint` changed from float to color
  - `Transmission` -\> `Transmission Weight`
  - `Coat` -\> `Coat Weight`
  - `Sheen` -\> `Sheen Weight`
  - `Emission` -\> `Emission Color`

### Lights

- `Light` properties for falloff, attenuation and shadow buffers were
  removed. These were not used by Cycles or EEVEE.
  (blender/blender@57d85b32a9aaa71b50c15d19f432462fa9fc4830)

### Particle System

- `ParticleSystem.child_nbr` has been renamed to `child_percent`
  blender/blender@e82ca9b5ffb6480ec41f01e2bebf20326187d104.

### Blender Application (`bpy.app`)

- Remove deprecated `bpy.app.version_char`
  (blender/blender@345fc2b1f6de7624a09cd91048476e2a932f83fc)

### Blender Operators (`bpy.ops`)

- Remove the context override argument to `bpy.ops` in favor of
  `context.temp_override(..)`
  (blender/blender@ac263a9bce53e190d07d679a058a230e91e722be)

### Font Module (`blf`)

- `blf.size()` `dpi` argument has been removed
  (blender/blender@9357f7b606262aa42cee0fc97dd4487b74ca9a29).

### GPU Module (`gpu`)

\- Remove deprecated 2D_ / 3D_ prefix for built-in shader names
(blender/blender@9a8fd2f1ddb491892297315a4f76b6ed2b0c1b94).

### Animation

- `FCurve.update()` now also deduplicates the F-Curve's keys
  (blender/blender@6452eccc800ff11cad4051ec24bdf6f7de451295).
- `Action.frame_range` now accurately reports zero-length frame ranges
  (blender/blender@49eab72141ee7e2df8d276156566f2cf9b660316).
  Previously, it was special-cased to construct and report a
  1-frame-length range if and only if the action was exactly zero length
  (not e.g. 0.001 length). This was confusing and easily misinterpreted
  as being a minimum 1-frame-length range or as being an
  exclusive-on-the-right range, neither of which have ever been the
  case. The new behavior simply reports the actual frame range as-is in
  all cases.

### Armatures

- Layers and bone groups have been removed with all associated
  properties and methods. Bone collections have been added instead.
  (blender/blender@998136f7a7b520ef10c998c5af744eb0d464fdd3
  and many subsequent commits). See [Bone Collections & Colors:
  Upgrading](upgrading/bone_collections.md)
  for an overview of how to change Python code to the new API.
- Now `edit_bones.new()` behaves similar to object creation, and does
  not automatically add the created bone to any collections. To emulate
  the behavior of the add bone operator, manually assign the bone to
  `arm.collections.active` if it is not None.
  (blender/blender@ab67d410a90d5d2d07f5c34d3dae197331b41dc8)
- The deprecated `use_inherit_scale` bone property has been removed.
  (blender/blender@2abd026cfedaecbb0be039c02f2aa142cef3fc09)

### Node Groups

- Node groups API moves from
  [`NodeTree`](https://docs.blender.org/api/3.6/bpy.types.NodeTree.html)
  to
  [`NodeTree.interface`](https://docs.blender.org/api/4.0/bpy.types.NodeTreeInterface.html).

=== "Old"

        # Make a socket
        tree.inputs.new(name="My Input")
        tree.outputs.new(name="My Output")
        
        


        
        
        
        
        # Remove a socket
        tree.inputs.remove(socket)
        tree.outputs.remove(socket)
        
        # Move a socket up or down
        tree.inputs.move(from_index=4, to_index=2)
        
            
        
        # Iterate over sockets
        for socket in tree.inputs:
            ...
        for socket in tree.outputs:
            ...
            
=== "New"
            
        # Make a socket
        # Note: socket_type accepts only base socket type names,
        # e.g. `NodeSocketFloat` but not `NodeSocketFloatFactor`.
        tree.interface.new_socket(name="My Input", in_out='INPUT')
        tree.interface.new_socket(name="My Output", in_out='OUTPUT')
        # Make node panel
        tree.interface.new_panel(name="My Panel")
        
        # Copy an existing socket or panel
        tree.interface.copy(socket)
        
        # Remove a socket or panel
        tree.interface.remove(socket)
        tree.interface.remove(panel)
        
        # Move a socket up or down
        tree.interface.move(socket, to_index=2)
        # Move a socket into a panel
        tree.interface.move_to_parent(socket, new_panel, to_index=2)
        
        # Iterate over sockets
        for item in tree.interface.items_tree:
            if item.item_type == 'SOCKET':
                if item.in_out == 'INPUT':
                    ...
                elif item.in_out == 'OUTPUT':
                    ...
        # Iterate over panels
        for item in tree.interface.items_tree:
            if item.item_type == 'PANEL':
                ...
        

<table markdown>
<tbody markdown>
<tr markdown>
<td markdown><div class="note_title">
<p><strong>Modifying items</strong></p>
</div>
<div class="note_content">
<p>The previous node group API in some cases allowed modifying
inputs/outputs collections while iterating over them. This was
incidental and generally should be avoided. See <a
href="https://docs.blender.org/api/4.0/info_gotcha.html#help-my-script-crashes-blender">"Gotchas"</a>
for more details</p>
</div></td>
</tr>
</tbody>
</table>

- Custom nodes: `NodeSocket.draw_color_simple` callback becomes the
  preferred color function for custom socket types.
    
  It does not take a context or node instance but that is sufficient for
  most use cases. The new callback is used in cases where concrete node
  instance exists, for example drawing node group interfaces.

=== "Old"

        class MyCustomSocket(NodeSocket):
            def draw_color(self, context, node):
                return (1, 1, 1, 1)

=== "New"

        class MyCustomSocket(NodeSocket):
            @classmethod
            def draw_color_simple(cls):
                return (1, 1, 1, 1)

### Sequencer

- `Sequence.speed_factor` was replaced with more complex retiming
  system
  (blender/blender@86a0d0015ac7bb114cbc44a03dd6b422a5ac709f).

### Import/Export

- The Python based OBJ importer/exporter has now been removed. Addons
  using `bpy.ops.import_scene.obj` and `bpy.ops.export_scene.obj`
  APIs should to switch to `bpy.ops.wm.obj_import` and
  `bpy.ops.wm.obj_export`
- The Python based PLY importer/exporter has now been removed. Addons
  using `bpy.ops.import_mesh.ply` and `bpy.ops.import_mesh.ply` APIs
  should to switch to `bpy.ops.wm.ply_import` and
  `bpy.ops.wm.ply_export`

### Misc

- Rename `filename` to `filepath` for RNA API calls
  (blender/blender@efa4179982d9ea7007b88b1a60c3168a19b59305).
  - `Depsgraph.debug_relations_graphviz`,
    `Depsgraph.debug_stats_gnuplot`.
  - `RenderLayer.load_from_file`, `RenderResult.load_from_file`.
  - `bpy.app.icons.new_triangles_from_file`
  - Some built-in color space names were renamed. Forward compatibility
    is preserved via do-versions, but assignment of hard-coded names
    could break

## Hydra Render Delegates

Renderer add-ons can now be implemented as [USD
Hydra](https://openusd.org/release/glossary.html#hydra) render
delegates. We recommend add-on developers to use this, and get involved
in Blender development to add any missing functionality.
(blender/blender@04bb5f999501d97ab142eb2bce8f3a696bb06fa5)

Using Hydra provides two major benefits:

- The same Hydra render delegate works across multiple 3D apps, and
  therefore much of the implementation can be shared.
- Performance with heavy scenes is much better, because Hydra is a C++
  API instead of Python.

Blender shader nodes can be automatically converted to a
[MaterialX](https://materialx.org/) node graph for renderers that
support it. Only a subset of shader nodes are supported currently, see
the [compatibility list](https://projects.blender.org/blender/blender/issues/112864).

The [`HydraRenderEngine` API docs](https://docs.blender.org/api/4.0/bpy.types.HydraRenderEngine.html)
have more for details.

## Additions

- `bpy.context.property`, for property under the mouse cursor
  (blender/blender@6ba0346797f43cd73eb1003ce65a62b8409203fb).
- `UILayout.progress(..)` widget type added to display progress from
  scripts
  (blender/blender@c6adafd8ef325de6e925e30504882057d8455a8f).
- `PoseBone.bbone_segment_index()` for accessing the internal math
  mapping vertices (their positions) to B-Bone segments that deform them
  (blender/blender@36c6bcca1a5aa2553d98a9b9e9af422b7458ad40)
- `Curves.add_curves(..)` for adding new curves to a curves data-block
  (blender/blender@07f01b5fc2732406ab2183c831c3f1361d55375a).
