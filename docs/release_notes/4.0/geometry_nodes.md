# Blender 4.0: Geometry Nodes

## Node-Based Tools

[Node
tools](https://docs.blender.org/manual/en/4.0/modeling/geometry_nodes/tools.html)
are an accessible way to expand Blender and customize tools without
requiring Python.

<figure>
<video src="../../../videos/Edge_to_mesh_node_tools_demo.mp4" title="Edge to mesh node tools demo by Tim Greenberg (passivestar)." width="500" controls="" />
<figcaption>Edge to mesh node tools demo by Tim Greenberg (passivestar).</figcaption>
</figure>

- Geometry node node groups can now be used as operators from the 3D
  view menus.
- Specific nodes are available for controlling tool-specific data:
  - Edit mode selection is accessible with the **Selection** and
    controllable with the **Set Selection** node.
  - Sculpt face sets are accessible as well with the **Face Set** and
    **Set Face Set** nodes.
  - The **3D Cursor** node gives access to the 3D cursor location and
    rotation.
- Documentation is available in the [user manual](https://docs.blender.org/manual/en/4.0/modeling/geometry_nodes/tools.html).
- See the [overview task](https://projects.blender.org/blender/blender/issues/101778) for
  more information.
- Read the [announcement blog post](https://code.blender.org/2023/10/node-tools/).
- Watch the [live demo video](https://www.youtube.com/watch?v=rU_j1Jw3ev4).

## General

- The new **Repeat** zone allows repeating nodes a dynamic number of
  times. [See Manual](https://docs.blender.org/manual/en/4.0/modeling/geometry_nodes/utilities/repeat_zone.html).
  (blender/blender@3d73b71a97,
  blender/blender@c8cc169d6fee3)

![](../../images/Modeling_geometry-nodes_repeat_zone.png){style="width:800px;"}

- The mesh sharp edge status is accessible in builtin nodes.
  (blender/blender@4e97def8a32147b245759f04ceceeefd937b43f5)

![A new node and a modification to the "Set Shade Smooth" node to control the sharpness of mesh edges.](../../images/Sharp_Edge_Nodes.png){style="width:500px;"}

- The *Mesh to Volume* node now generates a proper fog volume rather
  than a converted SDF.
  (blender/blender@700d168a5c7)
  - The "Exterior Band Width" and "Fill Interior" options have been
    removed.
- The **Points to Curves** node groups point cloud points into curves
  and sorts each curve's points based on a weight.
  (blender/blender@48b08199d5dde4efb7c4d08342017a6c77df056c)
- A new debug utility helps to check procedural systems for bad index
  dependence.
  (blender/blender@cc7da09c1b820b91b8cfc7639893284e2bfc7ac2)
- Rotation sockets and eight new rotation nodes are available for
  simpler processing. [See
  Manual](https://docs.blender.org/manual/en/4.0/modeling/geometry_nodes/utilities/rotation/index.html).
  (blender/blender@34e4bedcd8a79ddff1a9e7d77ea3f5a99071b61f).
  - **Axis Angle to Rotation**
  - **Rotation to Axis Angle**
  - **Quaternion to Rotation**
  - **Rotation to Quaternion**
  - **Euler to Rotation**
  - **Rotation to Euler**
  - **Rotate Vector**
  - **Invert Rotation**
- Simulation zones can now be baked individually.
  (blender/blender@ad169ba67a)
- The *Simulation Output* node has a new Skip input.
  (blender/blender@dd515ebc1df)

## Modifiers

- The "Add Modifier" menu has been changed to a standard menu and is
  extended with custom modifiers from geometry node group assets.
  (blender/blender@6da4b87661f0fbbe7fc567b3cc3b6eb320bf87a2)
  - When geometry node groups are marked as assets and the "Is Modifier"
    property is enabled, the asset catalog path is used to dynamically
    create the modifier menu.
  - Assets unassigned to any node group are displayed in a separate
    "Unassigned" category.
    (blender/blender@d2d4de8c710c4e8e648a86f904e6436133664836)
- Two new options help to clean up the modifier interface
  - The node group selector can be disabled, which happens by default
    for node group asset modifiers.
    (blender/blender@0783debda88779c6ee56281d08ac0b2d3b3d4dd9)
  - Inputs can be forced to be single values to remove the attribute
    toggle.
    (blender/blender@6875925efa2acdd25eec5165915b2c79533746d8)
    - This also removes the attribute toggle in the node tool redo
      panel.

![The node group toggle and the attribute toggle are visible in the modifier](../../images/Modifier_Interface_Cleanup_Before.png){style="width:250px;"}

![No attribute toggle or node group selector](../../images/Modifier_Interface_Cleanup_After.png){style="width:250px;"}

## Performance

- The *Curve to Mesh* node is significantly faster when the profile
  input is a single point.
  - When the input point is at the origin, tangent, normal, and radii
    calculations are skipped, giving a 2x improvement.
    (blender/blender@6e9f54cbdadb529a30b38cc5e1c87d24efbcda88)
  - Handling with many point-domain attributes is improved, with an
    observed 4x improvement.
    (blender/blender@48fad9cd0c9693e4dbb31d94c349260e486d45d9)
  - Further improvements avoiding copying attributes entirely are
    possible when all curves are poly curves.
    (blender/blender@bef20cd3f1e77105b67fb90f0cd4d576ed851545)
