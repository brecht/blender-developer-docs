# Blender 4.0: Keymap

## General

- Markers: it is now possible to rename markers with double left-click,
  for consistency with other areas in Blender.
  (blender/blender@26dcc961)
- Tweak Tool: can now click-drag multiple objects at once in the
  3D-viewport & UV-editor
  (blender/blender@618f39fca23ca7734a09a8f08efced8379efd2b3)
- The Preferences window can now be opened with `Ctrl+Comma`.
  (blender/blender@86f659ad96)
- Pose Mode: **`1..0`** for switching between scene collections has
  been removed
  (blender/blender@adef3a4556ac3448392f68bb6f9ae9dd3d4406df)

## Sculpt, Paint, Grease Pencil

For Blender 4.0 various updates were made to the keymap. This will allow
us to better support current and future features and only affects
sculpting, painting and grease pencil related modes.

This is an overview of all changes that may affect users.

### Left Click Select

Better support for selecting individual faces/vertices while a brush is
used (vertex/weight/texture painting).
(blender/blender@fb54d3f865928ebe4042eb1a3a12eb02709332b8)

- **`Alt LMB`** = Select
- **`Shift Alt LMB`** = Select Extend/Toggle
- Face Loop selection shortcuts
  (blender/blender@98d48c16a3)
  are only available if any selection tool is used.

### 3 Mouse Button Emulation

- Selecting individual faces/verts is now supported and available from
  the selection tools (not while a brush is used).
  (blender/blender@fb54d3f865928ebe4042eb1a3a12eb02709332b8)
- Selecting single bones is possible using **`Ctrl-Shift-LMB`**.
  (blender/blender@69e290d2fef33550d2e8e9777abff38fcaf177e7)

### Right Click Select

More Mesh Sculpt Mode shortcuts are now properly supported:

- **`W`** = Context Menu
- **`Shift Ctrl LMB`** = Lasso Masking (blender/blender!110960)
- **`Shift Alt Ctrl LMB`** = Lasso Unmasking/Selecting (blender/blender!110960)

### Removed Shortcuts

More space on the keyboard is now freed for user customization. If
needed, any of these shortcuts can be added back by right clicking on
the related UI button and using “Assign Shortcut”.

- **Some brush shortcuts are removed in Sculpt Mode and Grease Pencil.**
  (blender/blender@a7dd864dcf92e01e68e5b24564995dcd78098e50)
  (blender/blender@fa27e6561e6f34463ee7ecc11f1fb8b182ff0494)
  - **`D`** = Gpencil: Draw brush
  - **`L`** = Mesh Sculpt Mode: Layer brush

<!-- -->

- **Various unused or niche shortcuts have been removed.**
  (blender/blender@a7dd864dcf92e01e68e5b24564995dcd78098e50)
  - **`Ctrl M`** = Mesh Sculpt Mode: Mask Overlay
  - **`Ctrl D`** = Mesh Sculpt Mode: Dyntopo toggle
  - **`Ctrl Alt R`** = Mesh Sculpt Mode: Quadriflow remesher on
  - **`Shift L`** = Gpencil: Alternate selection
  - Gpencil Draw Mode: 13 individual Guides shortcuts on Draw brush

### Common Shortcuts

Various shortcuts are now more consistent and shortcut conflicts are
fixed. More features are now better supported.

- **`1,2,3` = Selection Modes (if available)**
  (blender/blender@827918c925d3ab21dbba27016cf1218cb70c6551)
- **Full brush support in vertex/weight paint
  modes**(blender/blender@6de6d7267f3d0eb913ce08675c9d73e27b8785e4)
  - **`Shift LMB`** = Smooth stroke
  - **`Ctrl LMB`** = Inverted Stroke
- **Color & Weight painting
  shortcuts**(blender/blender@6de6d7267f3d0eb913ce08675c9d73e27b8785e4)
  - **`X`** = Flip Color (Not supported yet in Grease Pencil Draw
    Mode)
  - **`Shift X`** = Sample color/weight
  - **`Ctrl X`** = Fill color/weight
  - **`Shift Ctrl X`** = Weight Paint Mode: Sample Vertex Group
- **Other**
  (blender/blender@6de6d7267f3d0eb913ce08675c9d73e27b8785e4)
  - **`Alt E`** = Stroke Type Menu

### Mesh Sculpt Mode

- Changed some brush shortcuts to more commonly used brushes and to
  avoid shortcut conflicts
  (blender/blender@fa27e6561e6f34463ee7ecc11f1fb8b182ff0494)
  - **`V`** = Draw Brush (previously on **`X`**)
  - **`S`** = Smooth Brush (previously on **`Shift S`**)
  - **`C`** = Clay Strips Brush (previously Clay)
  - **`Shift T`** = Scrape Brush (previously Flatten)
- **Other shortcuts**
  (blender/blender@6de6d7267f3d0eb913ce08675c9d73e27b8785e4)
  - **`Alt W`** = Face Set Edit pie menu
  - **`H`** = Hide Active Face Set
  - **`Shift H`** = Hide Inactive Face Sets / Show All
  - **`Alt 1`** / **`Alt 2`** = Increase/decrease Multires Levels
  - **`Ctrl R`** = Dyntopo flood fill if Dyntopo is enabled
  - **`Shift RMB`** = Set the Pivot Point for Transform Gizmos

### Weight Paint Mode

- **`Ctrl-Shift-LMB`** = Selecting bones
  (blender/blender@69e290d2fef33550d2e8e9777abff38fcaf177e7)
- **`Shift A`** & **`Shift Alt A`** = Weight Paint Gradients
  (blender/blender@6de6d7267f3d0eb913ce08675c9d73e27b8785e4)

### Grease Pencil

- **`Alt I`** = Grease Pencil Delete Active Frame
  (blender/blender@6de6d7267f3d0eb913ce08675c9d73e27b8785e4)
- **Gpencil Draw Mode: Box Delete and Lasso Delete shortcuts are easier
  to
  access.**(blender/blender@a7dd864dcf92e01e68e5b24564995dcd78098e50)
  - The shortcuts on **`B`** and **`Ctrl Alt RMB`** were previously
    only available on the Draw brush. Now they are are accessible in the
    entire Draw Mode.
- Grease Pencil shortcuts to select entire strokes are now consistent
  with Select Linked shortcuts on **`L`** & **`Shift L`** (blender/blender!110960)
  - It’s also better supported to change to stroke selection mode and
    select stroke any time with mouse clicks.

## Industry Compatible Keymap

Many shortcuts of this keymap have been added and fixed for modes
related to painting, sculpting and grease pencil.
(blender/blender@8814101a2067a1139f6d4eb984a86bd0a084bd2c)

This will will help maintain and further support the keymap in the
future. If any shortcut isn't mentioned, that means it is unchanged.

### Removed Shortcuts

- All brush shortcuts have been removed for free custom mapping
- Redundant shortcuts have been removed such as
  - Line Session drawing (In GP Draw Tool)
  - Select Alternate in GP
  - Quadriflow Remesh (In Mesh Sculpt Mode)
  - Box Hide (In Mesh Sculpt Mode)
  - Dynamic Topology Toggle (In Mesh Sculpt Mode)
  - Mask Overlay (In Mesh Sculpt Mode)
- Removed Lasso Mask/Select shortcuts (This keymap is mainly using the
  Tools instead. Easy to add back if needed.)

### Mode Switching

Easier mode switching access for any workflow. The previous shortcuts
heavily favored modeling.
(blender/blender@a3d0006ac1b8daa075943bfd4db2e8a99a7784ef)

- **`123`** = Switch selection/mask modes for the **current mode**. If
  in object mode, it will first switch to edit mode (Previously this
  would always switch to edit mode)
- **`5`** = Mode switching pie menu (previously `5-7` for individual
  modes)
- **`Tilde`** = Transfer Mode (Previously not mapped. Useful for
  instant object switching while keeping the current mode)
- **`6-0`** = Unassigned

### Brush & Selection

Multiple Modes were missing these shortcuts.

- **`Shift LMB`** = Smooth Brush
- **`Ctrl LMB`** = Inverted Brush
- `**Shift Alt LMB`** = Set Selection
- **`Shift Alt Ctrl LMB`** = Toggle-Extend Selection

### Brush Size & Select Linked

These were previously in direct conflict. Now they are both distinct and
based on other DCC software.

- **`\[`** = Decrease Brush Size
- **`\]`** = Increase Brush Size
- **`Alt dbl LMB`** = Select Linked Pick
- **`Ctrl Alt dbl LMB`** = Deselect Linked Pick
- **`Ctrl L`** = Select Linked

### Select & Mask

- **`Q`** = Cycle Masking/Selection Tools
- **`Ctrl A`** = Mask Pie Menu (Mesh Sculpt Mode)
- **`Alt A`** = Auto-Masking Pie Menu (Mesh & GP Sculpt Mode)
- **`Shift A`** = Expand Mask (Mesh Sculpt Mode) / Grow Selection
  (Curve Sculpt Mode)
- **`Shift Alt A`** = Expand Mask by Normals (Mesh Sculpt Mode)

### Face Sets & Visibility

Previously missing shortcuts.

- **`Shift W`** = Face Set Expand
- **`Shift Alt W`** = Active Face Set Expand
- **`Ctrl W`** = Face Set Edit Pie Menu
- **`Page Up`** = Grow Active Face Set
- **`Page Down`** = Shrink Active Face Set
- **`H`** = Hide Active Face Set
- **`Shift H`** = Hide Inactive Face Sets / Show everything
- **`Ctrl H`** = Hide Masked Geometry

### Transforming

For Sculpt and Weight Painting modes. Previously missing

- **`C`** = Cursor Tool
- **`W`** = Move Tool
- **`E`** = Rotate Tool
- **`R`** = Scale Tool
- **`T`** = Transform Tool
- **`Shift RMB`** = Set Cursor / Set Transform Pivot in Sculpt Mode

### Sculpting

Consistent and easier to access shortcuts for remeshing and subdiv level
.

- **`Shift D`** = Subdivision level Down (Previously on **`Pg
  Down`**)
- **`D`** = Subdivision level Up (Previously on **`Pg Up`**)
- **`Shift Ctrl D`** = Voxel Size / Dyntopo Size
- **`Ctrl D`** = Voxel Remesh / Dyntopo Flood Fill

Adding missing shortcuts

### Painting

Fixing various missing or inconsistent shortcuts.

- **`Backspace`** = Set Color/Weight
- **`I`** = Sample Color/Weight
- **`Alt I`** = Sample Vertex Group
- **`Alt Ctrl I`** = Vertex Group Lock Pie
- **`X`** = Swap Colors (Previously missing in Sculpt Mode)

### Grease Pencil

Adding many missing shortcuts.

- **`Shift S`** = Animation Menu (Previously on **`I`** and missing
  outside of Draw Mode)
- **`Shift Alt S`** = Insert Blank Keyframe
- **`Alt H`** = Unhide Layers
- **`Ctrl H`** = Hide Active Layer
- **`Shift H`** = Hide Inactive Layers
- **`Y`** = Active Layer Menu
- **`Shift Y`** = Merge Layer Down
- **`Alt Y`** = Material Menu
- **`Backspace`/`Delete`** = Delete Menu (Now in all modes)
- **`Shift Backspace`/`Delete`** = Delete All Keyframes (Previously
  on **`D Backspace`/`Delete`**)

### Mesh Edit Mode

- **`Ctrl Shift Alt LMB`** = Region Selection
  (blender/blender@a3d0006ac1b8daa075943bfd4db2e8a99a7784ef)

### Other

Adding missing shortcuts.

- **`MMB`** for panning in the User Interface (Highly requested)
- **`Shift F`** = Center View to Mouse (Replacing Frame Selected(All
  Regions) shortcut)
- **`D`** = Annotate
- **`L`** = Toggle Stabilize Stroke (Previously on **`Shift S`**)
- **`Ctrl F`** = Weight radial control in weight painting modes

Generally improved consistency for region shortcuts and linked selection
in all editors
(blender/blender@a0e5b0312610fab1107982121b654834560de5bc)

- **`Ctrl L`** = Select Linked
- **`Ctrl [`** = Toggle Toolbar/Channels
- **`Ctrl ]`** = Toggle Sidebar
- **`Shift Spacebar`** = Toggle Asset Shelf
