# Blender 4.0 Release Notes

Blender 4.0 was released on November 14, 2023.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/4-0/).

- [Animation & Rigging](animation_rigging.md)
- [Geometry Nodes](geometry_nodes.md)
- [Modeling](modeling.md)
- [Sculpt & Paint](sculpt.md)
- [Shading & Texturing](shading.md)
- [Cycles](cycles.md)
- [Color Management](color_management.md)
- [Compositor & Sequencer](vfx.md)
- [Import & Export](import_export.md)
- [Core](core.md)
- [User Interface](user_interface.md)
- [3D Viewport](viewport.md)
- [Node Editor](node_editor.md)
- [Keymap](keymap.md)
- [Add-ons](add_ons.md)
- [Python API](python_api.md)
- [Asset Bundles](asset_bundles.md)

## Compatibility

### Blend Files

- The mesh format changes from previous versions are now included in the
  Blender file format.
  - Blender 3.6 LTS can read files saved with 4.0, but earlier versions
    cannot.
  - Blender 3.6 LTS will save meshes in a format compatible with older
    versions of Blender, so it can be used as conversion tool if needed.
  - See the section in the [Python
    API](python_api.md#breaking-changes)
    section for more details.
- The [new blendfile compatibility policy](../../handbook/guidelines/compatibility_handling_for_blend_files.md)
  (blender/blender#109151)
  has been implemented (see blender/blender!110109).
- Unused linked data is not kept anymore when saving and re-opening .blend files. See [the core page](core.md) for details.

### Graphics Cards

- The minimum required OpenGL version has been increased to 4.3 for Linux and Windows.
  (blender/blender@3478093183f91ac49988a0f1e077bde590d62a82)
- On macOS only Metal is supported now, and the OpenGL backend has been
  removed. A GPU with Metal 2.2 support is required. Intel Macs without
  an AMD GPU require a Skylake processor or higher.
  (blender/blender@cdb8a8929cecd09735092e9ad1fc3adf15501615)
- Due to driver issues, support for Intel HD4000 series GPUs has been
  dropped.

## [Corrective Releases](corrective_releases.md)
