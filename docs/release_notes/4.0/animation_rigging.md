# Blender 4.0: Animation & Rigging

## Pose Library

The pose library uses the newly introduced asset shelf to make pose
assets available in the 3D viewport.

![Pose library in the asset shelf](../../images/Asset_browser-pose_library-use_from_viewport.png){ width="720" }

## Bone Collections and Colors

New bone collections replace both legacy numbered layers and bone
groups. Bone collections can be named and are no longer limited to a
fixed number. Colors are now specified individually for each bone,
replacing bone group colors. (Design
blender/blender#108941,
blender/blender@998136f7a7b520ef10c998c5af744eb0d464fdd3)

- Bone colors can be defined on the armature (edit mode) bones, as well
  as on the pose bones (also see [Gotchas: edit bones, pose bones, bone
  bones](https://docs.blender.org/api/current/info_gotcha.html#edit-bones-pose-bones-bone-bones)).
  The "edit mode" colors are shared between all users of the armature,
  and are available in edit mode as well as pose mode. The "pose mode"
  colors can be different per armature object (in the same way that each
  armature object has its own pose), and thus allow per-character
  customization of colors. The pose mode colors work as override on the
  edit mode colors; setting the pose color to "default" will show the
  edit mode color.
  (blender/blender@042c5347f4b8e69dee76285e3bf085b8933427b0)
- The Select Grouped operator (Shift+G) can now select by bone color and
  by bone collection. This works based on the active bone; it selects
  all the other bones with the same color resp. sharing one or more bone
  collections. This is slightly different than before, where all
  selected bones were taken into account.
  (blender/blender@e41fbfd6e93,
  blender/blender@c230c526261,
  blender/blender@d5329eeea23,
  blender/blender@e59944dba4e,
  blender/blender@62639080bce)
- The Bone Layer operators (M, Shift+M) have been replaced by bone
  collection operators.
  (blender/blender@b48031df8e7)
- Bone collections can be added to linked armatures via library
  overrides.
- The Armature property `show_group_colors` has been renamed to
  `show_bone_colors`.
  (blender/blender@f78ed7b900e99302d3bd0dd828167ec7fc09f370)
- The "Skeleton" panel in the Armature properties has been renamed to
  "Pose".
  (blender/blender@007cea46537)

See [Bone Collections & Colors:
Upgrading](upgrading/bone_collections.md)
for an overview of how to change Python code from armature layers & bone
groups to bone collections.

## Bendy Bones

A new method of mapping vertices to B-Bone segments for deformation that
takes the rest pose curvature into account has been added.
(blender/blender@0055ae01abbf0784eb3a64a6c50be7a5183b9524,
[Wiki](../../features/animation/b-bone_vertex_mapping.md#curved-mapping))

The new mapping is slower to compute, but produces better deformation
when the B-Bone is strongly curved in the rest pose, e.g. in a corner of
the mouth:

|Straight Mapping|Curved Mapping|Deformation|
|-|-|-|
|![](../../images/Bbone-mapping-straight.png)|![](../../images/Bbone-mapping-curved.png)|![](../../images/Bbone-mapping-comparison.png)|

## Preserve Volume

The long standing issue with Preserve Volume causing distortion when
rotation is combined with scaling has been fixed.
(blender/blender@f12e9f32b56ec7ca8cc3251dd46e1ffc56d9d102,
blender/blender@dee29f4e81d22f0d721e2d8010b06637ef7a5882)

Example deformation of a rotated and uniformly scaled down joint.

|Before|After|
|-|-|
|![](../../images/Demo-preserve-volume-fix-before.png)|![](../../images/Demo-preserve-volume-fix-after.png)|

## NLA Editor

- NLA Strips can now be vertically re-ordered. Strips can vertically be
  dragged through existing strips, and locked tracks.
  (blender/blender@4268ac0ed96bfacf0ad093be2e593208d64c8fcb)
- Vertically clamp scrolling to view. Preventing infinite scrolling.
  (blender/blender@71463719646d686ec1f9341f64e93f42b5158dc5)

## Graph Editor

### Editing

- Interactive sliders for Match Slope, Blend To Ease, Blend Offset,
  Shear Keys, Scale Average, Time Offset, Push/Pull. (blender/blender!110567,
  blender/blender!110566,
  blender/blender!110544,
  blender/blender!111735,
  blender/blender!111744,
  blender/blender!110540,
  blender/blender!112388)

<figure>
<video src="../../../videos/Animaide_tools.mp4" title="New graph editor operators" width="720" controls="" />
<figcaption>New graph editor operators</figcaption>
</figure>

- Butterworth Smoothing filter. (blender/blender!106952)

<figure>
<video src="../../../videos/Butterworth_Smoothing_Filter.mp4" title="Butterworth Smoothing Filter" width="720" controls="" />
<figcaption>Butterworth Smoothing Filter</figcaption>
</figure>

- Select/deselect handles from current selection. (blender/blender!111143)

### Performance

The performance of the Graph Editor has been greatly enhanced for when
it comes to dense key data. (blender/blender!110301,
blender/blender!110764,
blender/blender!110788,
blender/blender!112126,

In the following test setup with 6000 keys per channel on 62 bones,
performance was improved between 5x and 10x depending on the zoom level.

<figure>
<video src="../../../videos/Graph_Editor_speedup_comparison.mp4" title="Performance comparison between 3.6 and 4.0" controls="" />
<figcaption>Performance comparison between 3.6 and 4.0</figcaption>
</figure>

Animation playback timing was improved, to avoid jitter between frames.
(blender/blender#111579)

![FPS Jitter comparison](../../images/FPS_Jitter_Compare.png)

## Weight Paint

- Add loop select for faces. (blender/blender!107653)

## User Interface

- Channel list background colors have moved from covering the entire
  background (making text hard to read) to a small rectangle. The
  channel background color is also more subtle and blends in better. As
  the feature now no longer causes hard to read text, the "Channel Group
  Colors" option in the preferences has been enabled by default.
  (blender/blender@c524fbe62393bdaba6503e1317c009e66be0df66)
- Improved drawing of locked FCurves. (blender/blender!106052)
- Multi Editing for FCurve modifiers. (blender/blender!112419)
- Support adjusting the number of samples used for FPS playback display
  in the preferences.
  (blender/blender@b150b477202dc6d2a595b51be8cc34f8d6269731)
- Rename "Bake Curve" to "Keys to Samples". (blender/blender!111049)
- Rename "Sample Keyframes" to "Bake Keyframes". (blender/blender!112148,
  blender/blender!112151)
- Keep icons aligned when curves are baked. (blender/blender!108518)

|Channel Drawing in Blender 3.6|Blender 4.0|
|-|-|
|![](../../images/blender-4.0-channel-group-colors-before.png){style="width:500px;"}|![](../../images/blender-4.0-channel-group-colors-after.png){style="width:500px;"}|

## Breaking Changes

- 1-9 hotkeys for switching collection visibility have been removed from
  Pose Mode. (blender/blender!105120)
- Certain Graph Editor Slider tools had their range changed from 0-1 to
  -1/1: Blend to Neighbor, Breakdown, Ease. (blender/blender!107173)
- Making Make Vertex Weight Edit modifier Group Add / Remove threshold
  inclusive.
  blender/blender@1837bda030651582766ca5f8ebda216739285185
- The "Push Rest Pose" and "Relax Rest Pose" operators have been merge
  into "Blend Pose with Rest Pose". (blender/blender!108309)
- Update NLA "Duplicate hotkey" to be "Duplicate Linked" by default.
  (blender/blender@8183f21258cd15d3aab1dfc5b1344f42e9560f78)
- The "Tab" shortcut to enter tweak mode in the NLA now defaults to
  "full stack evaluation".
  (blender/blender@b95cf5ff0f3da57738303b653c2c01326d663e46)
- The Preserve Volume deformation fix may change deformation in existing
  rigs (see above).
- Snapping for the Graph Editor, NLA and Dope Sheet have moved to the
  scene. (blender/blender!109015)
- Armature Layers and Bone Groups have been removed, and replaced with
  Bone Collections and per-bone colors (see above).
- Bones influenced by an Inverse Kinematics (IK) constraint can now be
  keyed visually. This means that when the 'Use Visual Keying'
  preferenced is enabled, visual keying is used on such bones (where in
  Blender 3.6 and older still would create regular keys).
  (blender/blender@eab95fa2aaa9d1b29b460ab5c52fe8daf1da2d1b)
