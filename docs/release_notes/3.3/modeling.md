# Modeling

## General

- New property in the *Shade Smooth* operator to enable *Auto Smooth*.
  Exposed as **Shade Auto Smooth** in the 3D Viewport's *Object* menu
  and *Object Context Menu*.
  (blender/blender@eef98e66)
- New snapping method that snaps transformed geometry to *Nearest Face*.
  Expanded snapping options to give more control over possible snapping
  targets.
  (blender/blender@011327224ece)

## Modifiers

- Surface deform modifier can be used when target mesh increases number
  of vertices.
  (blender/blender@5f8f436dca9)

## UV

![Note the flipped and overlapping UVs around the nose, and the shear and non-uniform scale for the ears and eyes.](../../images/Uv_before.png){style="width:600px;"}

![After minimize stretch, the flipped faces are smoothed away. After Average Island Scale with Shear and Scale UV, the ears and eyes now have a nice even balance between the U coordinate and V coordinate.](../../images/Uv_after.png){style="width:600px;"}

Four changes help with workflow and improve final UV quality.

- When using UV Unwrap in the UV editor, only the selected UVs will be
  unwrapped.
  (blender/blender@c0e453233132)
- Minimize Stretch will now unflip faces.
  (blender/blender@135e530356d0)
- Upgrade Average Island Scale with new options, Scale UV and Shear.
  (blender/blender@931779197a9c)
- Add new operator, "UV Select Similar".
  (blender/blender@1154b4552679)

In addition, many smaller bugs have been fixed or resolved including:

- Snapping to UV vertices is now done if the cursor reaches a minimum
  distance.
  (blender/blender@d2271cf93926)
- Improved constrain-to-image-bounds when scaling UVs.
  (blender/blender@bc256a450770,
  blender/blender@4d7c99018008)
- Use per-face aspect correction when applying Cube Projection,
  Spherical Projection, Cylinder Projection and Project From View.
  (blender/blender@1c1e8428791f)
- Update size of hitbox for UV axis gizmo so it matches hitbox size in
  the 3D viewport.
  (blender/blender@65e7d4993933)
- During UV unwrap, pinned vertices will not be merged.
  (blender/blender@e6e9f1ac5a2d)
- Improved UDIM support.
