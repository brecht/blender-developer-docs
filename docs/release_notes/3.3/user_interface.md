# User Interface

## General

- Windows: support for precision touchpad gestures. Navigation is now
  consistent with middle mouse button, and different from the previous
  default behavior. Panning with two fingers orbits the 3D viewport and
  pans in 2D viewports. Hold down shift to pan, and control to zoom. The
  pinch gesture can also be used to zoom in or out.
  (blender/blender@e58b18888c0e)  
  Check if your touchpad supports precision gestures by going to Windows
  Settings \> Devices \> Touchpad and looking for the "Your PC has a
  precision touchpad" label.  
  Trackpad input can be customized in the keymap editor, by filtering by
  Key-Binding "trackpad".

<video src="../../../videos/Windows_Precision_touchpad_gestures_demo.mp4" width="800" controls="" />

- Windows: title Bar will respect operating system "Dark Mode" setting.
  (blender/blender@ddbac88c08ef)

<!-- -->

- Support for text fields to show candidates in more situations
  including operator, menu & panel identifiers in the key-map editor and
  context data-paths for operators that use data-paths
  (blender/blender@3f3d82cfe9cefe4bfd9da3d283dec4a1923ec22d)

![](../../images/UI_Text_Field_Search.png){style="width:600px;"}

- Scrollbars always visible, but gain prominence as your mouse
  approaches.
  (blender/blender@624397231928)
- Avoid redundant words in checkbox labels for custom properties: *Is
  Library Overridable* -\> *Library Overridable*, *Use Soft Limits* -\>
  *Soft Limits*
  (blender/blender@18960c08fde7,
  blender/blender@18960c08fde7)
- A *Pin Scene* option was introduced to workspaces. Workspaces with the
  scene pinned will remember the last used scene, and restore that when
  the workspace gets re-enabled
  (blender/blender@e0cc86978c0f).
  An icon in the scene switcher allows toggling it
  (blender/blender@b8605ee458e3).
- Improved layout of File Browser UI settings in the Preferences: *Show
  Recent Locations* and *Show System Locations* now show up as *Recent*
  and *System* under a *Show Locations* heading.
  (blender/blender@1ef686bd26c)

## Outliner

- In the Library Overrides view mode for Properties, display overridden
  properties in hierarchy with proper UI names and icons, instead of as
  a list of RNA paths to the properties. This is a more user friendly
  way of visualizing this data.

<table markdown>
<thead>
<tr class="header">
<th><p>Old</p></th>
<th><p>Blender 3.3</p></th>
</tr>
</thead>
<tbody markdown>
<tr markdown>
<td markdown>
![](../../images/Outliner-library-overrides-properties-pre-3.3.png)
</td>
<td markdown>
![](../../images/Outliner-library-overrides-properties-post-3.3.png)
</td>
</tr>
</tbody>
</table>

- Ability to directly edit library paths in the Outliner has been
  removed. This could lead to corruptions of advanced library data like
  overrides. Modern process using relocate operation should be used
  instead. Direct library file path editing remains possible from direct
  RNA access (python console or Data API view of the Outliner for
  example).
- Improved performance of *View Layer* and *Library Overrides* display
  mode with complex object parent/child hierarchies.
  (blender/blender@afde12e066e2)

## Asset Browser

- Don't show catalog path in the tooltip when dragging assets over root
  level catalogs. The path and catalog name would match then, so the
  name would just be repeated twice in the tooltip.
  (blender/blender@2e33172719c9)

## File Browser

- <span class="hotkeybg"><span class="hotkey">Ctrl l</span></span>
  (<span class="hotkeybg"><span class="hotkey">Cmd l</span></span> on
  macOS) to edit the directory path button.
  (blender/blender@209bf7780e7c)
- Theme settings: Use *Widget* font style instead of *Widget Label* for
  File Browser text.
  (blender/blender@6f7171525b4a)

## Gizmos

- Transform gizmos now display while being dragged
  (blender/blender@648350e456490f8d6258e7de9bf94d3a6a34dbb2).
