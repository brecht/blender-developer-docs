# Hair System

- A new hair workflow is possible with the new curves object
  - [The Future of Hair
    Grooming](https://code.blender.org/2022/07/the-future-of-hair-grooming)
  - [User Manual: Hair
    Primitive](https://docs.blender.org/manual/en/3.3/modeling/curves/primitives.html#empty-hair)
  - [User Manual: Curves
    Sculpting](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/introduction.html)

![](../../images/Einar_hair_video_thumbnail.jpg){style="width:700px;"}

### Sculpt Mode

![](../../images/Curves_Sculpt_Brush_Icons_3.3.png){align=right}

The new curves object type has a sculpt mode meant mostly for hair
grooming

- Sculpting can be done on top of curves deformed with geometry nodes
  (blender/blender@1f94b56d774).
- Curves sculpt mode supports selecting control points or curves to
  limit the effect of actions
  (blender/blender@a1830859fa9).
  - An overlay dims any deselected curves or points
    (blender/blender@e3ef56ef914).
  - A few new selection operators adjust the soft selection in sculpt
    mode: *Select Random*, *Select Endpoints*, and *Grow/Shrink*
    (blender/blender@e3ef56ef914).
  - The spreadsheet editor supports filtering by selected curves or
    points in sculpt mode
    (blender/blender@396b7a6ec8f).
- Sculpt mode supports symmetry on the X, Y and Z axes
  (blender/blender@7dc94155f62).
- Several sculpt brushes are available
  - The [Selection
    Paint](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/selection_paint.html)
    brush edits the selection mask
    (blender/blender@a1830859fa9).
  - The
    [Add](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/add_curves.html)
    brush adds new curves, interpolating the length, shape, and point
    count from neighbors
    (blender/blender@ac45540a348,
    blender/blender@8852191b779,
    blender/blender@dbba5c4df97).
  - The
    [Delete](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/delete_curves.html)
    brush removes curves
    (blender/blender@3f91e7d7792,
    blender/blender@f0f44fd92f1).
  - The
    [Density](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/density_curves.html)
    brush adds or removes curves based on a density
    (blender/blender@416aef4e13c).
  - The
    [Comb](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/comb_curves.html)
    brush moves points, preserving segment lengths
    (blender/blender@10c11bb8973).
  - The [Snake
    Hook](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/snake_hook_curves.html)
    brush grabs points and moves them without preserving length
    (blender/blender@fced604acfc).
  - The
    [Grow/Shrink](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/grow_shrink_curves.html)
    brush makes curves longer or shorter
    (blender/blender@190334b47df46f).
  - The
    [Pinch](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/pinch_curves.html)
    brush moves control points closer together
    (blender/blender@416aef4e13c).
  - The
    [Puff](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/puff_curves.html)
    brush rotates curves to match the surface normal
    (blender/blender@416aef4e13c).
  - The
    [Smooth](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/smooth_curves.html)
    brush makes curves straighter
    (blender/blender@416aef4e13c).
  - The
    [Slide](https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/slide_curves.html)
    brush moves curves along the surface
    (blender/blender@416aef4e13c,
    blender/blender@1f94b56d774).

### Surface Attachment

- Curves can be attached to a mesh surface with UV coordinates and a UV
  map
  (blender/blender@899ec8b6b80,
  blender/blender@12722bd3545).
  - Operators are available in sculpt mode to snap curves to the nearest
    or deformed mesh surface
    (blender/blender@50869b408b20f6).
  - *Note: Many tools require a UV map to function. The quality of the
    map does not matter, but it must not have overlapping faces.*
- *Add Empty Hair* in the add menu creates curves with the active object
  as a surface and its active UV map for attachment
  (blender/blender@434521a9e2cc41,
  blender/blender@2232855b50e).

### Nodes

- The curves object supports the geometry nodes modifier, which can be
  applied. All existing curve nodes are supported
  (blender/blender@96bdd65e740).
  - A new geometry node deforms curves based on the deformation of the
    surface mesh they are attached to
    (blender/blender@05b38ecc783).
    - *Note: The node will be generalized in the future by exposing more
      inputs or turning it into a node group.*

### Rendering

- Curves are rendered like hair particles, with support for Workbench,
  EEVEE, and Cycles.
- The object mode selection outline works for curves objects
  (blender/blender@a8f7d41d389).
- Curves can be converted to and from a hair particle system, and from
  the old curve type
  (blender/blender@94a54ab5549,
  blender/blender@3656e66ac24d1c,
  blender/blender@edc92f779ec102).
- *Note: Rendering the new curves object is not fully supported yet. For
  example, curves are always rendered as Catmull Rom curves. More
  information can be found
  [here](https://developer.blender.org/T96455).*

# Geometry Nodes

### Performance Improvements

- Curve nodes have been ported to the new curves type, with performance
  improvements anywhere from 3x to over 10x.
  - The *Set Spline Type* node can also skip processing when all curves
    have the correct type already
    (blender/blender@9e393fc2f125).
  - *Fillet Curve*
    (blender/blender@2551cf908732ba).
  - The *Subdivide Curve* node supports Catmull Rom curves now as well
    (blender/blender@9435ee8c65193e).
- The *Separate XYZ* and *Separate Color* nodes can be over 20% faster
  when not all outputs are used
  (blender/blender@b8bd304bd453,
  blender/blender@04734622415521).
- The *UV Sphere* primitive is parallelized, with a 3.6x improvement
  observed for high resolutions
  (blender/blender@7808ee9bd73).
- Evaluating the input field in the *Capture Attribute* is skipped if
  the output is not needed
  (blender/blender@c980ed27f09).

### General

- The **Interpolate Domain** node allows evaluating a field on a
  different domain *inside* of another field, avoiding the need for a
  separate "Capture Attribute" node
  (blender/blender@ba1e97f1c6cd0c,
  blender/blender@ea4b1d027d97).

### Meshes

- The **UV Unwrap** and **Pack UV Islands** nodes open the possibility
  of creating and adjusting UV maps directly inside of geometry nodes
  (blender/blender@4593fb52cf809d).
- The **Mesh To Volume** node acts like the existing modifier of the
  same name
  (blender/blender@1516f7dcde34a4,
  blender/blender@0ea282f7462070).
- Three new nodes can be used for path-finding across mesh edges.
  - The **Shortest Edge Paths** node finds the best path along edges
    from every vertex to a set of end vertices
    (blender/blender@c8ae1fce6024).
  - The **Edge Paths to Curves** node generates a separate curve for
    every edge path from a set of start vertices. The path information
    usually generated by the "Shortest Edge Paths" node
    (blender/blender@c8ae1fce6024).
  - The **Edge Paths to Selection** node generates an edge selection
    that includes every edge that is part of an edge path
    (blender/blender@c8ae1fce6024).
- The *Mesh Boolean* node now has an output field that gives just the
  intersecting edges
  (blender/blender@e2975cb70169).

### Instances

- New **Instance Scale** and **Instance Rotation** nodes provide access
  to instance transformations
  (blender/blender@3a57f5a9cff5,
  blender/blender@14fc89f38f0e).

### Primitive Nodes

- The **Volume Cube** primitive node allows sampling a field in a dense
  bounding box to create an arbitrary volume grid
  (blender/blender@838c4a97f).
- The **Points** node creates any number of point cloud points, with
  position and radius defined by fields
  (blender/blender@9a0a4b0c0d452d).

# Physics

- Effector forces on rigid bodies are independent of simulation substeps
  again
  (blender/blender@f4d8382c86ba760c5fccf7096ca50d6572b208bd).

  
This has been a regression since 2.91, so the fix may affect user files
with combinations of effectors and rigid bodies. Forces can be scaled
down inversely to substeps to maintain the effect. If the scene uses 10
substeps for rigid body simulation (the default), effector force should
be scaled by 0.1.
