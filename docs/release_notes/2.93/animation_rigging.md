# Blender 2.93: Animation & Rigging

- Update layouts for graph editor and NLA editor FModifiers
  (blender/blender@1f5647c07d15d2).

- Armature bone axes can now be shown anywhere on the bone (previously
  only on the tip),
  (blender/blender@74d5a93b2bf7).
  Existing files still have the old behaviour, while new armatures will
  default to showing the axes at the root (head) of the bone.

![](../../images/release-notes-293-animation-bone-axes.png){style="width:400px;"}

- Weight paint symmetry options has now been tweaked so you can't have
  vertex group mirroring and brush mirroring on at the same time:

![Only X axis mirroring is supported with vertex groups mirroring](../../images/Weight_paint_symmetry_options.png)

- The Cycle-Aware Keying option was moved back to the Keying popover
  since it's not limited to auto-keying.
  (blender/blender@caf92a647f76)

<!-- -->

- The "follow path constraint" and "follow parented curve" are no longer
  clamped and can extend beyond the start/end point of the curve
  (blender/blender@cf2baa585cc8
  blender/blender@6a0906c09a26).

<!-- -->

- It is now possible to alt click to get a list of bones under the
  cursor.
  (blender/blender@5e77ff79ccda)

![](../../images/Bone_menu_selection.png){style="width:400px;"}
