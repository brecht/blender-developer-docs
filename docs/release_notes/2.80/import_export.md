# Blender 2.80: Import / Export

## glTF

The Khronos glTF 2.0 importer and exporter is now bundled with Blender.
This file format is designed for efficient loading of 3D scenes, often
used for game engines and 3D content on the web.

It includes support for objects, meshes, cameras, PBR materials and
animation. For more details, see the [glTF add-on
documentation](https://docs.blender.org/manual/en/dev/addons/io_gltf2.html)
in the manual.

## COLLADA

- Reorganized user interface.
- Material I/O is now based on the Principled BSDF.
- More complete animation import and export, with options to export
  keyframes or baked animation.
- Exact preservation of material, bone and object names, encoded with
  XML entities.
- Object and Bone Hierarchies are now exported correctly based on
  parenting in Blender.

## Alembic

- Metaball animation can now be exported.
- Curve and NURBS object animation can be exported as a mesh with the
  "Curves as Mesh" option.
- Imported Alembic files can now be offset by a number of frames. The
  offset is subtracted from the current frame number to obtain the frame
  retrieved from the Alembic file.

## Images and Video

### Metadata

- Video file output now includes metadata like images, and reading of
  metadata is supported as well.
- Added a new metadata field 'Frame Range', which writes the scene's
  frame range "start:end", for example "32:88". This allows mapping back
  from a video's frame to the Blender frame that produced it.
- The hostname of the machine running Blender can now be embedded in the
  output file metadata. This helps identifying which machine rendered
  which frame in a multi-machine setup (i.e. render farm).

### WebM

WebM support has been added, via FFmpeg's VP9 encoder.

WebM/VP9 and h.264 both have options to control the file size versus
compression time (e.g. fast but big, or slow and small, for the same
output quality). Since WebM/VP9 only has three choices, we've chosen to
map those to 3 of the 9 possible choices of h.264. The choices now are:

- Good: the default and recommended for most applications.
- Slower: recommended if you have lots of time and want the best
  compression efficiency.
- Realtime: recommended for fast encoding.
