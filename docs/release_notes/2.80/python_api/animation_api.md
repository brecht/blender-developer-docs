# Blender 2.80: Animation & Rigging API

## Fix in PoseBone.bone Behavior

The `PoseBone.bone` property now correctly sets `id_data` of its
result to the Armature ID, instead of keeping it pointing at the
armature Object
(blender/blender@1c106e18).

**2.7x**

``` Python
>>> bpy.data.objects[0].pose.bones[0].bone
bpy.data.objects['Armature'].pose.bones["Bone"].bone
```

**2.8x**

``` Python
>>> bpy.data.objects[0].pose.bones[0].bone
bpy.data.armatures['Armature'].bones["Bone"]
```

The main consequence is that now
`object.pose.bones\[...\].bone.driver_add()` correctly adds the driver
to `object.data.animation_data` instead of `object.animation_data`.

## Bone Matrix Utilities

- Bone and PoseBone have new utility methods for bone roll math and
  B-Bone shape.
  (blender/blender@a58f0eea)
  - `Bone.MatrixFromAxisRoll()`
  - `Bone.AxisRollFromMatrix()`
  - `pose_bone.bbone_segment_matrix()`
  - `pose_bone.compute_bbone_handles()`

<!-- -->

- Bone provides access to the parent-child transformation math with
  arbitrary matrices.
  (blender/blender@48a3f97b)
  - `bone.convert_local_to_pose()`

## Keyframing Utilities

- Added method for conversion between strip and scene time for NLA tweak
  mode.
  (blender/blender@27d097e9)
  - `anim_data.nla_tweak_strip_time_to_scene()`
- Added more flags for use in `object.keyframe_insert()`.
  (blender/blender@ebc44aae)
  - `INSERTKEY_REPLACE`, `INSERTKEY_AVAILABLE`,
    `INSERTKEY_CYCLE_AWARE`
- Added a new `fcurve.is_empty` property for easily detecting when the
  curve contributes no animation due to lack of keyframes or useful
  modifiers, and should be deleted.
  (blender/blender@b05038fe)
  - Such curves are now ignored by animation evaluation code, so this
    cleanup can be safely postponed until the end of a complex editing
    operator, even if it changes the current frame or otherwise triggers
    evaluation.

## Misc

- Hook Modifier now provides direct access to the bound vertex index
  array.
  (blender/blender@3f788eac)
  - `hook.vertex_indices`, `hook.vertex_indices_set()`

<!-- -->

- New method to delete all Shape Keys of an object at once.
  (blender/blender@c7ec6bb7)
  - `object.shape_key_clear()`

<!-- -->

- New utility for creating a custom property with default value, limits
  and other UI settings in one call.
  (blender/blender@40dd9156)
  - `from rna_prop_ui import rna_idprop_ui_create`
