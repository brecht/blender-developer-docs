# Blender 2.83 Release Notes

Blender 2.83 was released on June 3, 2020.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-83/).

This release includes long-term support, see the [LTS
page](https://www.blender.org/download/lts/) for a list of bugfixes
included in the latest version.

## [Grease Pencil](grease_pencil.md)

Completely rewritten from scratch, the new Grease Pencil is much faster
and better integrated with Blender

## [Volumes](volumes.md)

OpenVDB files can now be imported and rendered with the new volume
object.

## [Virtual Reality](virtual_reality.md)

Initial native virtual reality, focused on scene inspection.

## [Cycles](cycles.md)

Adaptive sampling, OptiX viewport denoising, improved shader nodes, and
more.

## [EEVEE](eevee.md)

New render passes, improved light cache and high-quality hair
transparency.

## [Sculpt](sculpt.md)

Face sets to control visibility, cloth brush, and many more
improvements.

## [Modeling](modeling.md)

## [Physics](physics.md)

## [User Interface](user_interface.md)

## [Animation & Rigging](animation.md)

## [More Features](more_features.md)

## [Python API](python_api.md)

## [Add-ons](add_ons.md)

Major updates to Collection Manager.
