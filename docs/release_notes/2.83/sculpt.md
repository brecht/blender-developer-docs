# Blender 2.83: Sculpting

## Brushes

- Clay Thumb Brush: new brush to simulate deforming clay with your
  fingers, accumulating material during the stroke.
  blender/blender@015d5eda884d
- Cloth Brush: new brush with a simple physics solver that helps when
  sculpting cloth.
  blender/blender@4a373afa5fb8
  - The mass and the damping properties of the simulation are properties
    of the brush.
  - It has two additional radius control to limit the influence and
    falloff of the simulation.
  - Masked vertices are pinned in the simulation, and it applies the
    sculpt gravity directly in the solver.
  - The Cloth Brush has 7 deformation modes with 2 falloff types (radial
    and plane).

<figure>
<video src="../../../videos/Clothbrushdemo.mp4" title="Cloth Brush" width="800" controls="" />
<figcaption>Cloth Brush</figcaption>
</figure>

- Pose Brush: option to disable the IK anchor
  point.blender/blender@0ab7e321585f
- Clay Strips Brush: option to change the brush tip shape between a
  square and a circle.
  blender/blender@6ee6a42d10e3
- All brushes have a hardness property which controls where the falloff
  starts affecting the brush in relation to the radius.
  blender/blender@ff0124418f42

<figure>
<video src="../../../videos/Clay_brush_2.80_-_2.83.mp4" title="Clay brush 2.80 - 2.83" width="800" controls="" />
<figcaption>Clay brush 2.80 - 2.83</figcaption>
</figure>

- Layer Brush: the Layer Brush was completely
  redesigned:blender/blender@47f46637be1b
  - No artifacts when alternating between adding and subtracting or when
    modifying the same area multiple times
  - Layer height preview in the cursor
  - Multires support for Persistent Base
  - Better masking support

<figure>
<video src="../../../videos/Layer_Brush_2.82_-_2.83.mp4" title="Layer Brush 2.82 - 2.83" width="800" controls="" />
<figcaption>Layer Brush 2.82 - 2.83</figcaption>
</figure>

- Brushes now have separate normal and area radius, enabled by default
  for Scrape brush. It achieves better behavior when working on curved
  surfaces and controls how much volume you want to trim.
  blender/blender@df45257ec53c
- Surface Smooth mode in the Smooth brush. This mode smooths the high
  frequency detail while preserving the volume.
  blender/blender@a218be308032

<video src="../../../videos/Surface_Smooth_Brush.mp4" width="800" controls="" />

- Mesh boundary Automasking Option to protect the boundaries of the mesh
  when sculpting cloth. This option has an Propagation Steps property to
  control the smoothness of the falloff near the edges of the Sculpt.
  blender/blender@84b94f9e7b8b

## Face Sets

New system to control the visibility state of the mesh in sculpt and
paint modes. They are designed to work in modes where brushes are the
primary way of interaction and they provide much more control when
working with meshes with complex shapes and overlapping surfaces.
blender/blender@38d6533f212b

- Remesher reprojection support. The visibility state of the mesh is
  also preserved when remeshing.
- Draw face sets tool to create and edit the face sets. Start a new
  stroke to create a new Face Set. Press Ctrl to modify the Face Set
  under the cursor and Shift to relax the boundary of a Face Set
- W key pie menu with the most common Face Set visibility operations
- Mask expand operator support. User Shift + W to expand a new Face Set
  by topology.
- H to toggle Face sets visibility, Alt + H to show all Face Sets,
  Shift + H to hide the Face Set under the cursor.
- Pressing Ctrl when expanding Face Sets or Masks in the mask expand
  operator flood fills a topology connected area.

Face set compatibility was also added to some of the existing tools:

- The Mesh Filter can apply a filter to an individual face set with the
  option "use Face Sets\*
- There is a Relax Face Sets filter in the Mesh Filter, which smooths
  the boundary of all Face Sets
  blender/blender@0dfb4ac1ff9a

<video src="../../../videos/Relax_Face_Sets_Mesh_Filter.mp4" width="800" controls="" />

- The Pose Brush can snap the rotation origin points to the boundaries
  of the Face Sets
  blender/blender@9120191fe2a2

<video src="../../../videos/Pose_Brush_Face_Sets_origin_mode.mp4" width="800" controls="" />

- Face Set automasking option to affect only the Face Set were the
  stroke started
- Face Set boundary automasking option to avoid affecting the boundaries
  of the Face Sets
  blender/blender@7c88968c8954

## Voxel Remesh

- Voxel Size edit operator to change the voxel size of the voxel
  remesher with a real time preview in the viewport by pressing Shift +
  R
  blender/blender@1c1a14dcdfff
- Voxel mode in the remesh modifier which produces the same result as
  the Voxel Remesher
  blender/blender@6c036a65c974

## Other

- Automasking options can now be enabled globally instead of per brush.
  blender/blender@1f745e2c72b2
- Delay Viewport Updates option to improve the performance when
  navigating in high poly meshes. When this option is enabled only the
  data will be updated in the viewport when the view navigation stops,
  avoiding the lag.
  blender/blender@b8d9b5e33152

<video src="../../../videos/Delay_Viewport_Updates.mp4" width="800" controls="" />

- Surface Smooth mode in the Mesh filter to remove high frequency detail
  while preserving the volume
- Sharpen mode in the Mesh Filter which pinches the edges and smooths
  flat surfaces automatically

<video src="../../../videos/Sharpen_Mesh_Filter.mp4" width="800" controls="" />
