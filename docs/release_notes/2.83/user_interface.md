# Blender 2.83: User Interface

## Animation Editors

- Improved graph editor channels drawing
  (blender/blender@4aa0e21)
- Improved graph and NLA editor sidebar panel layout
  (blender/blender@04e318de3a4)
- Rename 'View Frame' to 'Go to Current Frame'
  (blender/blender@dee01cad19f)
- Change 'Lock Time to Other Windows' \> 'Sync Visible Range' and add to
  Sequencer.
  (blender/blender@fe772bf8186)
- Changed dopesheet & timeline filter names
  (blender/blender@9ccc73ade8a25db5bb):
  - Only Selected → Only Show Selected
  - Show Errors → Only Show Errors
  - Display Hidden → Show Hidden
- Show animation cancel button in all status-bars
  (blender/blender@25cb12dc71e)

## File Browser

- Support for file attributes and hidden files.
  (blender/blender@1fb62d1272db)
- Show icons for special folder locations.
  (blender/blender@84c9a99cca90)
  (blender/blender@1af8e0cc6c47)
  (blender/blender@a622e29a2541)
- Spport for Ctrl+F shortcut to start filtering files.
  (blender/blender@e8ab0137f876)
- Recognizes .fountain files as text files.
  (blender/blender@aa4579c35f93)

## Miscellaneous

- Improve toolbar width snapping
  (blender/blender@ac7eb710890)
- Consolidate masking-related brush controls
  (blender/blender@c01246f6c0f)
- Change Area Duplicate Icon
  (blender/blender@452834f1e3a)
- On Windows, restore app from minimized when closing from taskbar
  (blender/blender@22ca8b8aee99)
- Dynamically enable and disable Edit Menu items based on whether
  currently applicable
  (blender/blender@b7075049732a)
- Visual changes to Info Editor
  (blender/blender@aa919f3e8202)
- Improved keyboard symbols for menus and status bar, following platform
  conventions.
  (blender/blender@dc3f073d1c52)
  (blender/blender@f051d47cdbce)
- Overflowing text lines now use ellipsis character to indicate line
  continuation.
  (blender/blender@63d5b974ccfa)
- Larger Alert icons for dialogs
  (blender/blender@a210b8297f5a)
- Language Selection on "Quick Start" Splash screen
  (blender/blender@a210b8297f5a)
- Improved UI Layout for 3D Mouse Settings, all settings accessible from
  Preferences
  (blender/blender@ce0db0d329985e4)
- Keep outliner selection in sync for more operators
  (blender/blender@5d14463e1aee)
  (blender/blender@c06a40006d6c)
  (blender/blender@f772a4b8fa87)
- Display and allow creation of drivers and keyframes for bone
  visibility in the outliner
  (blender/blender@6f8d99322cf6)
