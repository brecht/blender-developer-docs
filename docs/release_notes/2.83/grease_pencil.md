# Blender 2.83: Grease Pencil

Completely rewritten from scratch, the new Grease Pencil is much faster
and better integrated with Blender.
(blender/blender@29f3af9527)

## Drawing Engine

- FPS improvement for files with high amount of strokes.
- Blender Lights now affect Grease Pencil objects materials.
- VFX results are zoom independent and look the same in viewport or
  render.
- Better management of alpha avoiding all known issues.
- Update Viewport shading modes for better consistency with meshes.
- Mask system refactor.
- Smoother Drawing of Fast Strokes.
- Better Stroke Quality when drawing hard angles.

![Smooth lines even in tightest strokes](../../images/Better_fast_strokes.png){style="width:800px;"}

- Grease Pencil shader Updates:
  - UI and names updates.
  - Fill checkerboard was removed.
  - Add Alpha control to stroke and Fill Base colors.

## User Interface

- Updated 2D Animation template

![2D Animation Template](../../images/2D_Animation_Template.png){style="width:800px;"}

- Full review of Grease Pencil Brushes UI (topbar and Properties Editor)
  for consistency with other Blender brushes.
- Review of Brush presets to improve drawing experience.
- Control to activate the use of Blender lights in Grease Pencil
  objects.
- New Materials preset.
- Modifiers, Updates UI to be more consistent with other Blender
  Modifiers.
- New context menus.
  - Animation (I key).
  - Change the Active Layer (Y key).
- New Active Layer selector in Brushes context menu.
- New Toggle to scale the stroke thickness while editing strokes.
- New Masks List view.
- Dopesheet, Remove Active Only control, now it shows only Grease Pencil
  that are in the current scene.
- Overlays updates.
  - New Material Name option.
  - New Vertex Paint opacity.
  - New Show Stroke Directions (Moved from Viewport Diplay in Properties
    panel).
  - Fade objects and Grease Pencil objetcs updates.
  - Grid now is occluded by 3D geometry if the geometry is in front.

## Operators

- New Operators for Materials and Vertext color.
- New Merge Similar Materials operator.
- New Reset fill transform.
- Updates in Paint operators to better result when drawing fast strokes.

## Tools

- New complete Vertex Paint Mode Tools.

![Vertex Paint Tools](../../images/Vertex_Paint_Tools.png){style="width:800px;"}

- Sculpt and Weight Paint brushes and are now real Blender Brushes.
- Sculpt, Weight and Vertex Paint brushes now support all types of
  falloff curves.
- Eyedropper tool now support the creation of Materials or Palettes
  color.
- New Transform Fill tool to adjust UV.

<figure>
<video src="../../../videos/Transform_Fill_tool.mp4" title="Transform Fill tool" width="800" controls="" />
<figcaption>Transform Fill tool</figcaption>
</figure>

- New Tint tool for adding vertex color while in Draw Mode.

<figure>
<video src="../../../videos/Tint_tool.mp4" title="Tint tool" width="800" controls="" />
<figcaption>Tint tool</figcaption>
</figure>

- Draw brushes, color toggle to use Base color material or Vertex Color.

## Modifiers

- Updated Modifiers UI to be more consistent with other Modifiers in
  Blender.
- Influence Filters updates.
  - Panel was unified and more compact for all modifiers.
  - New Custom Curves to shape the modifier effect along the strokes.
- Tint Modifier, add color Gradient functionality.

![Tint Modifier using an Empty to control the new gradient mode](../../images/Tint_Modifier.png){style="width:800px;"}

- Array Modifier, removed Keep on top option.
- Multiple Strokes modifier, Removed Angle Split from.
- Tint, Opacity and HSV modifiers, Removed Create Material option.
- Now modifiers support Link operator to duplicate between objects.
  (blender/blender@d8c3aad8004c)

## VFX

- Effects only visible in Render mode.
- Blur VFX, Camera Depth of field improvements and add blur rotation.
- Colorize VFX, add Mix factor for all the modes.
- Glow VFX, add Blend modes, size and rotation.
- Swirl VFX, Removed Transparent option.
- Removed Light VFX (now replaced with the use of Blender Lights).
