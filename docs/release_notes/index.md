# Blender Release Notes

What's new in each Blender version.
Visit [blender.org](https://www.blender.org/download/releases/) for the full list.

## Currently Active

- [Blender 4.1](4.1/index.md) *(alpha)*
- **[Blender 4.0](4.0/index.md)** *(current stable release)*
- [Blender 3.6 LTS](3.6/index.md) *(support until June 2025)*
- [Blender 3.3 LTS](3.3/index.md) *(support until September 2024)*

## Older Versions

- [Blender 3.5](3.5/index.md)
- [Blender 3.4](3.4/index.md)
- [Blender 3.2](3.2/index.md)
- [Blender 3.1](3.1/index.md)
- [Blender 3.0](3.0/index.md)

- [Blender 2.93 LTS](2.93/index.md)
- [Blender 2.92](2.92/index.md)
- [Blender 2.91](2.91/index.md)
- [Blender 2.90](2.90/index.md)

- [Blender 2.83 LTS](2.83/index.md)
- [Blender 2.82](2.82/index.md)
- [Blender 2.81](2.81/index.md)
- [Blender 2.80](2.80/index.md)
- [Blender 2.79](2.79/index.md)
- [Archived Release Notes](https://en.blender.org/index.php/Dev:Ref/Release_Notes)
