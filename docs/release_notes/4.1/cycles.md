# Blender 4.1: Cycles

## Other

- Added an option to disable bump map correction
  (blender/blender@36e603c430ca90a4a19574a5071db54a7cebcb39)
- Support for AMD GPU rendering with RDNA3 generation APUs
  (blender/blender@d19ad12b45c14dee9d17272cbc1d1b22d3a725aa)
