# Blender 4.1: Python API & Text Editor

## Enum ID Properties

Support for enum items has been added to integer properties.
blender/blender@92cf9dd2f2d7f4d8d380d4b1dcc17f6c2b5b175d

There is no support yet for editing enum items through the UI (see
commit message for technical reasons).

Enum items can be added to an integer property using the
[id_properties_ui](https://docs.blender.org/api/main/bpy.types.bpy_struct.html#bpy.types.bpy_struct.id_properties_ui)
python method. The `as_dict` method can be used to retrieve a list of
enum items of a property.

    my_object["my_prop"] = 2
    ui_data = my_object.id_properties_ui("my_prop")
    ui_data.update(items=[
        ("TOMATOES", "Tomatoes", "Solanum lycopersicum"),
        ("CUCUMBERS", "Cucumbers", "Cucumis sativus"),
        ("RADISHES", "Radishes", "Raphanus raphanistrum"),
    ])
    print(ui_data.as_dict())

Int properties with enum items are shown as a dropdown button in the UI.

![](../../images/Id_property_enums.png)

## Breaking changes

### Light Probes

- The Lightprobe type items have been renamed. It affects
  `bpy.types.LightProbe.type`,` bpy.types.BlendDataProbes.new()` and
  `bpy.ops.object.lightprobe_add()`.
  - `CUBEMAP` -\> `SPHERE`
  - `PLANAR` -\> `PLANE`
  - `GRID` -\> `VOLUME`

<!-- -->

- `show_data` has been deprecated. Use `use_data_display` instead.
- Each `LightProbe` now has its own `data_display_size` property.

### Mesh

- Sculpt mask values are stored in a generic attribute
  (blender/blender@f2bcd73bd25625d6b5c1194a4199e96fc5fafa2e).
  - The name is ".sculpt_mask", with the `FLOAT` type.
  - Accessing, adding, and removing masks is done in a simpler way:
    - The `Mesh.vertex_paint_mask` property returns the attribute
      directly, rather than a collection.
    - The `Mesh.vertex_paint_mask_ensure()` and
      `Mesh.vertex_paint_mask_remove()` functions add and remove the
      attribute.
- The `Mesh` `auto_smooth` property has been replaced by a modifier
  node group asset
  (blender/blender@89e3ba4e25c9ff921b2584c294cbc38c3d344c34).
  - `use_auto_smooth` is removed. Face corner normals are now used
    automatically if there are mixed smooth vs. not smooth tags. Meshes
    now always use custom normals if they exist.
  - `auto_smooth_angle` is removed. Replaced by a modifier (or
    operator) controlling the `"sharp_edge"` attribute. This means the
    mesh itself (without an object) doesn't know anything about
    automatically smoothing by angle anymore.
  - `create_normals_split`, `calc_normals_split`, and
    `free_normals_split` are removed, and are replaced by the simpler
    `Mesh.corner_normals` collection property. Since it gives access
    to the normals cache, it is automatically updated when relevant data
    changes.
  - `MeshLoop.normal` is now a read-only property. Custom normals
    should be created by `normals_split_custom_set` or
    `normals_split_custom_set_from_vertices`.

### Material

- The `displacement_method` property has moved from
  `cycles.properties.CyclesMaterialSettings` to `bpy.types.Material`
  blender/blender@a001cf9f2b08a67aa3b20f1857eac5c915f5ef33
