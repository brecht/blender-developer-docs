# Blender 4.1: Modeling & UV

- The "Auto Smooth" option has been replaced by a modifier node group
  asset
  (blender/blender@89e3ba4e25c9ff921b2584c294cbc38c3d344c34).
  - The base state of meshes is now the same as having "Auto Smooth" on
    with an angle of 180 degrees in older versions:
    - Face corner "split" normals are calculated when there is a mix of
      sharp and smooth elements.
    - Custom normals are used if available.
  - The modifier node group asset sets sharpness by the angle between
    faces.
  - Blender automatically chooses whether to calculate vertex, face, or
    face corner normals given the presence of custom normals and the mix
    of sharp/smooth edges and faces.
  - In edit mode, "Select Sharp Edges" and "Mark Sharp" can be used to
    set sharpness, or "Shade Smooth by Angle" in other modes.
  - For more information on the procedural smoothing modifier, see the
    [Nodes &
    Physics](nodes_physics.md)
    section.
  - For more information on the Python API changes, see the [Python
    API](python_api.md#breaking-changes)
    section.
- A render "simplify" setting adds the ability to turn off calculation
  of face corner and custom normals in the viewport
  (blender/blender@912c4c60d8b3bdf741d9cdcb6d04d17029b02b69).
