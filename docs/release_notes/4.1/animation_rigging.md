# Blender 4.1: Animation & Rigging

## NLA

- Add new channel options to Action bake
  (blender/blender@dd5b870d15287e8322c799edef9823e3c9a8bda2).

![](../../images/Action_bake_options.png)

- Rename "NLA Channels" to "NLA Tracks"
  (blender/blender@661e7e451a0eadd7955a6074a79d570ac4ce2927).

## Weight Paint

### Bone Selection

The bone selection mode is made explicit when you enter weight paint
mode with an armature. It now has an icon and can be accessed with the
hotkey `3`. The following selection tools have been added to make bone
selection easier:

- Tweak

Commits:
blender/blender@edcac1f48b92aee693f01a9bb4d23c03870a8f29

## Keying

- Pressing I in the viewport will no longer pop up a menu of keying
  sets. Instead it will read the User Preferences which has a new
  setting to define which channels get keyed.
  (blender/blender@a99e419b6e841d673a3542c372d5faec82c0d138)

## Graph Editor

- Add option to right click menu on animated properties to view the
  FCurve that animates it.
  (blender/blender@a91a8f3fed8c54574b73b9187f004cf0632691da)

<figure>
<video src="../../../videos/View_fcurve.mp4" title="Demo of viewing the F-Curve of the animated property" width="720" controls="" />
<figcaption>Demo of viewing the F-Curve of the animated property</figcaption>
</figure>

### F-Curve Baking

A new operator "Bake Channels" has been added to the Graph Editor. It
allows you to:

- Specify a range to bake.
- Define the distance between baked keys, e.g. 2s or 3s.
- Remove keys outside the baked range.
- Define a interpolation type for new keyframes.
- Bake modifiers to keyframes.

It is found in the Graph Editor under
<span class="literal">Channel</span> » <span class="literal">Bake Channels</span>.

## Dope Sheet

- Speed up Dope Sheet by only calculating keyframes that are visible in
  the current view.
  (blender/blender@f06fd85d970f5b37ceb50a2eca6dc766a4b9de8e)
