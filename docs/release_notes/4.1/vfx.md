# Blender 4.1: VFX & Video

## Compositor

- The *Keying Screen* node was changed to use a *Gaussian Radial Basis
Function Interpolation*, which produces smoother temporally stable
keying screens. The node now also have smoothness parameter.

![](../../images/KeyingScreenTriangulationVsRBF.png)

- The Kuwahara node now allows variable sizing by providing a Size
input.

## Sequencer

- Timeline user interface repaints 3x-4x faster for complex timelines.
  (blender/blender@df16f4931e00)
- Luma Waveform display calculation is 8x-15x faster.
  (blender/blender@93ca2788ff01f)
- Various Sequencer effects and transitions were optimized:
  - Glow is 6x-10x faster.
    (blender/blender@fc64f4868230)
  - Wipe is 6x-20x faster.
    (blender/blender@06370b5fd685)
  - Gamma Cross is 4x faster.
    (blender/blender@9cbc96194ea)
  - Gaussian Blur is 1.5x faster.
    (blender/blender@5cac8e2bb4b)
