# Blender 4.1: Core

### Wayland on Linux

- Input Method Editors (IME) now supported
  (blender/blender@a38a49b073f582a0f6ddcca392f2760afdc4d5ed).
