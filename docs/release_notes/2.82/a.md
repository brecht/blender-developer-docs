## Blender 2.82a: Bug Fixes

### Blender

- Fix [\#74003](http://developer.blender.org/T74003): Autocomplete bug
  with mesh.loop_triangles. in Blender Python Console.
  (blender/blender@69c587888)
- Fix [\#73898](http://developer.blender.org/T73898): UDIM crash
  changing form tiled to single
  (blender/blender@d6977b5)
- Fix [\#73862](http://developer.blender.org/T73862): Fluid Sim file
  handle resource leak
  (blender/blender@60890cc)
- Fix [\#74182](http://developer.blender.org/T74182): Crash saving
  images from non-image spaces
  (blender/blender@096936fe1)
- Fix [\#72903](http://developer.blender.org/T72903): Bone envelope &
  bone size tool functionality swapped
  (blender/blender@dd2cdd436)
- Fix [\#74278](http://developer.blender.org/T74278): camera border
  gizmo size
  (blender/blender@9cac5fa681,
  blender/blender@360443a48)
- Fix [\#54270](http://developer.blender.org/T54270): Reset last_hit and
  last_location when reading the file
  (blender/blender@2df040ed58fb)
- Fix [\#74431](http://developer.blender.org/T74431): crash when
  compiling renderpass shader on some AMD drivers
  (blender/blender@9c4523b1fde4 +
  blender/blender@e5f98c79b0ed)
- Fix [\#74295](http://developer.blender.org/T74295): Cloth + Internal
  springs crashes on a non-polygonal geometry
  (blender/blender@1648a7903672)
- Fix Fix (unreported) Separate bones creates empty armature
  (blender/blender@498397f7bd8)
- Revert "Constraints: remove special meaning of Local Space for
  parentless Objects."
  (blender/blender@f881162f81e)
- Fix [\#73932](http://developer.blender.org/T73932): modifying
  keyframes in nodes fails when there is an image sequence
  (blender/blender@f0a22f5)
- Fix crash loading .blend file saved with Blender 2.25
  (blender/blender@ed8aa15)
- Fix potential crash with Eevee render of missing image textures
  (blender/blender@ab18dbb)
- Fix [\#72253](http://developer.blender.org/T72253): Mantaflow adaptive
  domain cuts off
  (blender/blender@32fc22db5679)
- Keymap: Add front/back Alt-MMB absolute view axis switching
  (blender/blender@a200986273)
- Fix [\#72028](http://developer.blender.org/T72028): Crash switching to
  vertex paint
  (blender/blender@31aefdeec5)
- Fix bone envelopes displaying wrong when armature is scaled
  (blender/blender@ee7034949fd)
- Fix Vertex weight gradient tool show wrong weight/strength values in
  the UI
  (blender/blender@f38c54d56e)
- Fix [\#74225](http://developer.blender.org/T74225): Image (from
  sequence) cannot be loaded
  (blender/blender@9dbfc7ca8bf)
- Fix [\#63892](http://developer.blender.org/T63892): Tools cannot be
  registered into some contexts (e.g. PAINT_TEXTURE)
  (blender/blender@d95e9c7cf8)
- Fix [\#73369](http://developer.blender.org/T73369): corner pin &
  sun-beam nodes gizmos are too big
  (blender/blender@212660f467)
- Fix [\#74425](http://developer.blender.org/T74425): Cannot texture
  paint an images sequence anymore
  (blender/blender@ca717f0489)

### Add-ons

- glTF: Fix some strange reference error in blender api when exporting
  shapekeys / ApplyModifier
  (blender/blender-addons@659c121a68)
- glTF: Fix crash using compositor rendering for image generation
  (blender/blender-addons@af687f5a041ee)
