# Blender 2.82 Release Notes

Blender 2.82 was released on 14 February, 2020.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-82/).

## [2.82a Corrective Release](a.md)

This corrective release does not include new features and only fixes a
few serious regressions introduced in 2.82 release.

## [Physics](physics.md)

Mantaflow fluid simulation, cloth internal air pressure, cloth internal
springs.

## [Cycles](cycles.md)

OptiX denoiser, improvements to shader nodes, custom render passes, and
more.

## [EEVEE](eevee.md)

Viewport render passes, and a lot of important fixes.

## [Textures](textures.md)

UDIM textures for texture painting and rendering in Eevee and Cycles.

## [Sculpt & Paint](sculpt.md)

Slide/relax tool, multiplane scrape tool, operator improvements, pose
brush and brush updates, paint improvements.

## [Grease Pencil](grease_pencil.md)

![](../../images/Multiple_Stroke_Modifier.png){style="width:400px;"}

User interface, improved tools, multiple strokes modifier.

  

## [Modeling](modeling.md)

![](../../images/Custom_Bevel_Profile_Release_Notes.png){style="width:400px;"}

Custom profiles for bevel, cutoff vertex mesh, weld modifier and
solidify modifier.

  

## [User Interface](ui.md)

![](../../images/Bevel_gizmo.png){style="width:400px;"}

Many tools now have a gizmo, like bevel and the UV editor.

  

## [Animation & Rigging](rigging.md)

Improvements to bones, constraints and F-curves.

## [Import & Export](import_&_export.md)

![](../../images/Blender-2.82-USD-usdview-spring.png){style="width:400px;"}

New USD exporter, many glTF improvements, Alembic mesh normals export,
VFX platform 2020 compatibility.

  

## [Python API](python_api.md)

Python API changes, Custom curve profile and widget, changes to handling
of Python environment.

## [Add-ons](add_ons.md)

Updates to Rigify, Add Mesh Extra objects, BlenderKit, and new add-ons
Import Palettes, PDT, Add Node Presets, Collection manager, Sun position
and Amaranth toolkit.
