## Cloth Simulation

- Internal air pressure simulation, for example to simulation balloons.
  (blender/blender@f6cefbef22c8)
- Internal cloth springs for making cloth behave more like a soft body
  (blender/blender@62ef59aa0cca)

<figure>
<video src="../../../videos/Cloth-high-res.mp4" width="480" controls="" />
<figcaption>Cloth pressure example (Zuggamasta)</figcaption>
</figure>

<figure>
<video src="../../../videos/03_int_springs.mp4" width="480" controls="" />
<figcaption>Internal cloth springs example. Left
torus has no internal springs while the top one has. The right one has
internal springs that are influenced by a vertex group.</figcaption>
</figure>

## Fluid Simulation

[Mantaflow](http://mantaflow.com/) is the new physically-based fluid
simulation framework in Blender for gas (smoke & fire) and liquid
simulations. It completely replaces the existing fluid simulation
systems and changes the way you work with fluids.

![Fire with Wavelet noise in new Mantaflow fluids system<br/>(Crossmind Studio)](../../images/Blender2.82_mantaflow_fire_crossmind.png)

### Compatibility

Loading older files is only partly possible: The existing Fluid and
Smoke modifiers have been deprecated. When loading older files that use
such modifiers they will be converted to the new "Fluid" modifier which
handles both smoke and liquids. All values in these converted modifiers
will be set to the default value and need to be readjusted manually. It
is recommended to keep a backup of old fluid & smoke simulation files
since saving those files with Blender 2.82 will irreversibly override
the modifier information.

### General Changes

Simulation settings that apply to both gas and liquid simulations have
been unified. Both fluid types now use the same UI for them.

#### Domain Settings

- Adaptive Time Stepping: Simulations can now also run in between frames
  (and not just per frame). Especially fluid-obstacle interactions
  become accurate through this setup. Users can specify how many
  simulation steps should run in between frames.
- More flexible control over domain collisions; each wall can now be
  enabled/disabled individually.

#### Flow Settings

- Flow behavior: "Type" and "Behavior" of fluids flows are now two
  separate fields.
- Sampling sub-steps: This feature has been synced with the adaptive
  time stepping setting from the domain options. I.e. sampling takes
  sub-simulations steps into account too.
- Initial velocities: It's now possible to set velocities based on
  normals *and/or* XYZ coordinates.

#### Effector Settings

Collision and the newly introduced "Guiding" objects both belong to the
"Effector" fluid type. The objects are meant to affect the flow of the
simulation, i.e. their presence in the fluid domain has an effect on the
global velocity field.

- Effector type "Guide": Objects with a fluid modifier of this type are
  used to draw a velocity field. This will be used during the actual
  bake and affect the flow direction of the fluid.

### Gas Simulations

While smoke & fire simulations have received a new simulation back-end
the settings in the UI have remained mostly the same. Compared to the
system from Blender 2.81 and before changes include:

- Split up low- and high-resolution simulation loops. The up scaled
  version of a smoke / fire simulation can now be baked separately in
  the "Noise" panel.
- Settings from the old smoke simulations might need differently scaled
  values in the new smoke system.

### Liquid Simulations

Liquid simulation settings are completely new. The new (FLIP) solver
does not make use of any of the older simulation settings.

- There are now three components that can be configured and baked
  individually in a liquid simulation: liquid particles, the mesh, and
  secondary particles.
- Each liquid simulation component has its own resolution. The liquid
  mesh, for example, can have twice the resolution as the underlying
  FLIP particle simulation.
- There are now three different types of secondary particles for liquid
  simulations: Spray, Foam, and Bubble.
