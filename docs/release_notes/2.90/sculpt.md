# Blender 2.90: Sculpting

## Multires Modifier

Multiresolution sculpting is now fully supported. It is now possible to
select the subdivision level to sculpt on and switch between levels,
with displacement information smoothly propagated between them.
(blender/blender@d4c547b7bd8)

![Multires Modifier UI](../../images/Multires_UI.png){style="width:600px;"}

### Unsubdivide and Rebuild subdivisions

The Multires Modifier can now rebuild lower subdivisions levels and
extract its displacement. This can be used to import models from any
subdivision based sculting software and rebuild all subdivisions levels
to be editable inside the modifier.
(blender/blender@f28875a998d)

<figure>
<video src="../../../videos/Unsubdivide_demo.mp4" title="Unsubdivide different meshes with triangles, ngons and non-manifold geometry" width="600" controls="" />
<figcaption>Unsubdivide different meshes with triangles, ngons and
non-manifold geometry</figcaption>
</figure>

### Subdivision Modes

Multires can now create smooth, linear and simple subdivisions. This
means that any subdivision type can be created at any time without
changing the subdivision type of the modifier. Changing the subdivision
type of the modifier is now and advance option only used to support
legacy files.
(blender/blender@134619fabbc)

![All subdivision modes available in Multires](../../images/Subdivs.png){style="width:600px;"}

## Tools

- The Cloth filter runs a cloth simulation in the entire mesh. It has 3
  force modes and the area were the force is applied can be defined by a
  Face Set.
  (blender/blender@1d4bae85669)

<figure>
<video src="../../../videos/Cloth_filter_demo.mp4" title="Cloth filter applying forces to different Face Sets" width="600" controls="" />
<figcaption>Cloth filter applying forces to different Face Sets</figcaption>
</figure>

- The Face Set edit operator allows to modify a Face Set. The supported
  operations are Grow (Ctrl + W) and Shrink (Ctrl + Alt + W).
  (blender/blender@cb9de95d61b)

## Brushes

- The Pose Brush includes two more deformation modes: Scale/translate
  and Squash/stretch. They can be selected using the deformation
  property. Pressing Ctrl before starting the stroke selects the
  secondary deformation mode.
  (blender/blender@77789a19049)

<!-- -->

- The Pose Brush has a Face Set FK mode, which deforms the mesh using
  the Face Sets to simulate and FK rig.
  (blender/blender@3778f168f68)

<figure>
<video src="../../../videos/Pose_fk_290_demo.mp4" title="Pose Brush with FK/IK modes" width="600" controls="" />
<figcaption>Pose Brush with FK/IK modes</figcaption>
</figure>

- The Pose Brush has a Connected only property. By disabling it, the
  brush can affect multiple disconnected parts of the same mesh. The
  distance to consider a part as connected can be tweaked with the Max
  Element Distance property.
  blender/blender@438bd823714

<!-- -->

- Clay strips now has more predictable pressure/size and
  pressure/strength curves. Its deformation algorithm was modified to
  prevent geometry artifacts in large deformations.
  (blender/blender@560a73610b7)
  (blender/blender@0a1fbfee2ba)

<figure>
<video src="../../../videos/Demo_clay_strips_290.mp4" title="Clay strips 2.83/2.90 artifacts comparison" width="600" controls="" />
<figcaption>Clay strips 2.83/2.90 artifacts comparison</figcaption>
</figure>

- The Topology Slide/Relax mode now has two more deformation modes for
  Slide: Pinch and Expand. They can be selected using the Deformation
  property.
  (blender/blender@878d191baee)

## Miscellaneous

- The Automasking system was redesigned, so brush will not have and
  start delay when automasking options are enabled in a high poly mesh.
  (blender/blender@e06a346458f)

<!-- -->

- Boundary mesh detection was redesigned. Now mesh boundaries are
  properly detected by the automasking system in both meshes and
  Multires levels. This means that the smooth brush does not longer
  automasks boundary vertices by default and its meshes and multires
  behavior now matches.
  (blender/blender@e06a346458f)

<!-- -->

- Pen pressure modulation is now supported for the hardness property.
  The modulation can be inverted for achieving higher hardness with
  lower pressure.
  (blender/blender@69afdf69702)
