# Blender 2.90: Cycles

## CPU Rendering

[Intel Embree](https://www.embree.org/) is now used for ray tracing on
the CPU. This significantly improves performance in scenes with motion
blur. Other scenes with high geometric complexity also benefit on
average, depending on the scene contents.

|                         | Old            | Embree        |
|-------------------------|----------------|---------------|
| Barbershop              | 7:07 (2189MB)  | 7:12 (2600MB) |
| Victor                  | 11:27 (8012MB) | 8:51 (7060MB) |
| Agent 327 (motion blur) | 54:15 (7987MB) | 5:00 (6778MB) |
| BMW                     | 1:42 (142MB)   | 1:48 (145MB)  |
| Classroom               | 5:08 (289MB)   | 4:57 (321MB)  |
| Fishy Cat               | 2:37 (795MB)   | 2:20 (558MB)  |
| Koro                    | 4:21 (523MB)   | 3:17 (390MB)  |
| Pavillon                | 5:30 (146MB)   | 5:11 (156MB)  |

## GPU Rendering

- OptiX is now available on all NVIDIA GPUs that support it, which is
  Maxwell or higher (GeForce 700, 800, 900, 1000 series).
- NVLink support for CUDA and OptiX. When enabled in the Cycles device
  preferences, GPUs connected with an NVLink bridge will share memory to
  support rendering bigger scenes.
- NVIDIA GPU rendering on macOS is no longer supported. Apple dropped
  support for CUDA drivers in macOS 10.14, and no recent Apple hardware
  uses NVIDIA graphics cards.

## Hair

Hair curves are rendered differently due to the switch to Embree. GPU
rendering was made to match Embree. There are now two shapes to choose
from:

- Rounded Ribbon: render hair as flat ribbon with (fake) rounded
  normals, for fast rendering. Hair curves are subdivided with a fixed
  number of user specified subdivisions. This gives relatively accurate
  results, especially when used with the Principled Hair BSDF and hair
  viewed from a typical distance. There may be artifacts when viewed
  closed up.
- 3D Curve: render hair as 3D curve, for accurate results when viewing
  hair close up. This automatically subdivides the curve until it is
  smooth. This gives higher quality than any of the previous primitives,
  but does come at a performance cost. Especially on the GPU this is
  slower than previous primitives.

## Sky Texture

The new Sky texture is an improved version of the
[Nishita](https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/simulating-sky/simulating-colors-of-the-sky)
single scattering sky model, and it accounts for altitude change as well
as ozone absorption.

<figure>
<video src="../../../videos/Barcelona.mp4" title="Nishita Sky Texture" width="600" controls="" />
<figcaption>Nishita Sky Texture</figcaption>
</figure>

It includes a sun component with limb darkening effect.

![Limb Darkening](../../images/Limb_Darkening.png){style="width:200px;"}

Comparison between the old Hosek/Wilkie (left) and new Nishita (right)
sky models:

<figure>
<video src="../../../videos/Hosek_-_Nishita.mp4" title="Hosek / Nishita comparison" width="600" controls="" />
<figcaption>Hosek / Nishita comparison</figcaption>
</figure>

Lighting from the Sky texture may be quite bright or dark depending on
the time of day. Adjust the Exposure in the Film panel to compensate for
this.

## Denoising

[Intel OpenImageDenoise](https://openimagedenoise.github.io/) is now
supported for interactive denoising in the 3D viewport and for final
renders. This works on any recent Intel/AMD CPU (with SSE 4.1).

Previously viewport denoising was only supported for NVIDIA GPUs using
OptiX, and final render denoising with OpenImageDenoise was only
possible in the compositor.

Performance of tiled denoising in final renders is still poor, we will
investigate optimizing this.

## Shadow Terminator

- Per-object shadow terminator offset setting, to avoid artifacts with
  smooth normals on low-poly meshes.
  ([c7280ce65b85](https://developer.blender.org/rBc7280ce65b856d26d6807c1b7a081ffc6311bfdc))

## Changes

- Depth render pass for perspective cameras was changed to be more
  standard, matching OpenGL, Eevee and other renderers.
- Error messages are now propagated to the viewport status overlay
  (instead of it only showing "Cancel").
- Denoising is now enabled in the Render Properties for all view layers,
  in the Sampling \> Denoising panel. Settings can still be fine-tuned
  per view layer, including disabling denoising for specific layers.
