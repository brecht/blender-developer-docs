# Sculpt, Paint, Texture

## Color Attributes

- Vertex colors have been refactored into generic color attributes
  (blender/blender@eae36be372a6b16ee3e76eff0485a47da4f3c230).

![Color Attributes in UI](../../images/ColorAttributes.png){style="width:600px;"}

- Color attributes support both 8-bit byte and 32-bit floating point
  colors and can be stored in Vertex or Face Corner domains. The default
  is using Vertex -\> Color setting, to reduce file sizes, RAM usage and
  improve color quality.

![Creating a new Color Attribute](../../images/CreateAttribute.png){style="width:600px;"}

- "Vertex color" was renamed to "Color Attribute" in various places
  (blender/blender@eae36be372a6b16ee3e76eff0485a47da4f3c230,blender/blender@07b10638aa0a6a1d91740c2aed749a69b878b9cd).

## Painting in Sculpt Mode

- Color attributes can be painted inside vertex paint mode or in sculpt
  mode using the new sculpt paint tools
  (blender/blender@eae36be372a6b16ee3e76eff0485a47da4f3c230).
  - Painting tools in Sculpt mode have advantages like masking options,
    face sets/masks support and faster performance. They will serve as a
    base for future painting improvements and modes.
  - Vertex paint mode has been ported to support painting color
    attributes.
    (blender/blender@575ade22d4de472ccf9e7d2dc1ffca37416c58f6).
- Using painting tools in solid view switches viewport shading color to
  “Attribute”
  (blender/blender@90042b7d796608cf680620041785bfa432975d48).
  - This ensures that the color attributes are visible when painting,
    without making the visualisation mandatory in sculpt mode.
- Paint Brush - default brush for painting color attributes.
  - Holding Shift key switches Paint brush to Blur.
  - Can modulate wet paint effect for color smudging.
  - Includes extra sliders to control the tip roundness and width
  - Flow and Density also control how much color is applied during the
    stroke

<video src="../../../videos/PaintBrush.mp4" title="Showcase of different brush presets from the demo file" width="600" controls="" />

- Smear Brush - brush for smearing color attributes
  - Has various deformation modes.

<video src="../../../videos/SmearBrush.mp4" title="Smear brush deformation modes" width="600" controls="" />

- Color Filter - tool for modifying existing colors. Filter operations
include:

  - Fill
  - Hue
  - Saturation
  - Value
  - Brightness
  - Contrast
  - Smooth
  - Red
  - Green
  - Blue

<video src="../../../videos/ColorFilterVideo.mp4" title="Color filter modes" width="600" controls="" />

- Mask By Color - tool for creating masks from the active color attribute

<video src="../../../videos/MaskByColorVideo.mp4" title="Making a mask from a color" width="600" controls="" />

## Voxel Remesher

- Edit Voxel Size operator now displays units properly
  (blender/blender@b226236b017f5f84c5ce029689b0707f639299dc).
- Voxel Remesh default settings have been changed
  (blender/blender@d1418dd151b1b446fd97108959ef0ba5c0404424).
  - Voxel Remesher supports color attributes.
  - Color attributes, face sets and masks are now being preserved.
  - 'Fix poles' is for now disabled to drastically increase remeshing
    speed.
