# Blender 3.2: Cycles

## AMD GPU Rendering

AMD GPU Rendering for Linux has been enabled. Like Windows, this is
supported for RDNA and RDNA2 generation discrete graphics cards, which
includes Radeon RX 5000 and RX 6000 series, and Radeon Pro W6000 series
GPUs.

This currently requires the 22.10 Linux driver from the [AMD Radeon
repository](https://repo.radeon.com/amdgpu-install/22.10/).

For RDNA1 cards, the drivers have a known bug rendering image textures
with certain resolutions, where the width is not a multiple of 128. We
recommend using typical texture resolutions like 128, 256, 512, 1024,
2048, 4096 as a workaround, until the driver bug is fixed.

## Color Management

- Support different color management settings for render output than
  viewport display. Animation rendering, compositing file output nodes
  and image saving now have options to override the scene color
  management settings. For linear file formats like OpenEXR, a different
  linear color space can be chosen, for example to export ACES OpenEXR
  files. For other file formats a different display, view transform and
  look can be provided, for example to create a compositing nodes to
  output images for multiple display targets.
  (blender/blender@7aab508e)
- OpenColorIO color space aliases support, to create backwards
  compatible configurations.
  (blender/blender@5ac4e6c)
- Linear ACEScg color space added to the configuration, for more easily
  handling OpenEXR files with the ACEScg color space.
  (blender/blender@f991f5a)

## Light Groups

Support for Light Groups, a type of pass that only contains lighting
from a subset of light sources
(blender/blender@ad35453).

This can be used in order to e.g. modify the color and/or intensity of
light sources in the compositor without re-rendering.

![An example of using lightgroups to separate light sources in a scene](../../images/Lightgroups.png){style="width:800px;"}

## Shadow Caustics

Support for selective rendering of caustics in shadows of refractive
objects
(blender/blender@1fb0247).

Example uses are rendering of underwater caustics and eye caustics.

![Eye caustics using Manifold Next Event Estimation](../../images/Cycles_3.2_eye_caustics.png){style="width:800px;"}

This is based on "Manifold Next Event Estimation", a method developed
for production rendering. The idea is to selectively enable shadow
caustics on a few objects in the scene where they have a big visual
impact, without impacting render performance for the rest of the scene.

Note this method has a number of limitations:

- Only caustics in shadows of refractive objects work, which means no
  caustics from reflection or caustics that fall outside shadows. Only
  up to 4 refractive caustic bounces are supported.
- Caustic caster objects should have smooth normals. Bump and normal
  maps are ignored.
- Not currently supported for Metal GPU rendering.

In the future this method may be extended for more general caustics.

## Volume Motion Blur

Motion blur for gas simulations and imported VDB volumes is now
supported
(blender/blender@2890c11cd7b).
For this, the volumes must have a velocity field, defined either as a
vector grid, or as a triplet of scalar grids for the X, Y, and Z
components of the vector field. For volume objects, a parameter is
available to set the velocity grid's name.

If there is no vector grid matching the velocity grid name, the name
will be used to try and find grids with the name ending either with
`x|y|z`, or `.x|.y.z`, or `_x|_y|_z`. For example, if
the base name is `velocity`, then any group of 3 scalar grids named
either `velocityx`, `velocityy`, and `velocityz`, or
`velocity.x`, `velocity.y`, and `velocity.z`, or `velocity_x`,
`velocity_y`, and `velocity_z` will be merged into a vector grid at
render time and used for motion blur.

For imported volumes, an option to set the unit of time in which the
velocity vectors are expressed is available. Some software may export
velocities scaled to seconds (`SECOND`) or scaled to the simulation
frame time (`FRAME`). If it is not known what the unit of time is, and
if motion blur appears too strong, then `FRAME` might need to be used,
or if it does not seem to have any effect, then this might need to set
to `SECOND`.

<video src="../../../videos/Comparison_between_with_and_without_volume_motion_blur.mp4" width="800" controls="" />

## Changes

- Support using Adaptive Sampling together with Scrambling Distance.
  (blender/blender@630d2b6)
- Shader Nodes: add Alpha output to Object Info node
  (blender/blender@5b4ab89)
- Automatically unpause viewport when switching to rendered mode
  (blender/blender@ae217295578)
- The Alembic procedural is now enabled for final renders
  (blender/blender@e81d7bfcc9f4)
- Baking to UDIM tiles is now supported
  (blender/blender@6787cc13d4ef)
