# Blender 3.0: Nodes & Physics

## Geometry Nodes

In Blender 3.0, geometry nodes has been extended with a re-imagined
method for designing node groups, a new attribute system, many new nodes
for interaction with curves, text data, instances, and much more.

In **bold** you can find the names of about 100 newly added nodes.

### Fields

Geometry nodes makes use of a new concept for passing around data and
functions. Operations can be built from basic nodes and simply connected
together, removing the need for named attributes as a way to store
intermediate data, and removing the need for special "Attribute" nodes
for simple operations. With fields it's much easier to build node groups
for higher level concepts that work together better than before.

See the page in the manual for more detail:
<https://docs.blender.org/manual/en/3.0/modeling/geometry_nodes/fields.html>

- Dashed lines are used to represent the flow of functions while
  data-flow node links look the same as before
  (blender/blender@ff7e67afd5de2d).
- Socket shapes are used to visualize whether inputs and outputs can be
  fields
  (blender/blender@61f3d4eb7c7db7).
  - Circle sockets only support single values
  - Diamond sockets support fields. If they have a dot in the middle,
    then they are only a single value currently though.
- The **Set Position** node simply sets a geometry's position attribute
  to its input
  (blender/blender@bf47fb40fd6f0e).
- The **Position** node outputs the position of the geometry it is
  evaluated for
  (blender/blender@bf47fb40fd6f0e).
  - Other built-in attributes can be accessed and set by specific nodes
    (blender/blender@c1b4abf527f102).
- Most nodes have been updated to make use of new concepts like
  selections and field inputs:
  - The *Geometry Proximity* node replaces the attribute proximity node
    (blender/blender@9f0a3a99ab664b).
- The *Align Euler to Vector* and *Rotate Euler* node are now function
  nodes that can affect any rotation
  (blender/blender@79425ed3267663,
  blender/blender@c5c94e3eae74a7).
- The *Edge Split* node now has a selection input, allowing any edges to
  be selected procedurally
  (blender/blender@366cea95c5d95d).
- The ability to work on data without attributes means texture nodes can
  be ported from the shader editor:
  - **Noise Texture**
    (blender/blender@fc4f82d2004c84)
  - **Gradient Texture**
    (blender/blender@d4f1bc5f39b)
  - **White Noise Texture**
    (blender/blender@2341ca990c17)
  - **Voronoi Texture**
    (blender/blender@104887800c0f,
    blender/blender@4468c343787471)
  - **Checker Texture**
    (blender/blender@2537b3239240)
  - **Magic Texture**
    (blender/blender@56bf34aa174f)
  - **Wave Texture**
    (blender/blender@67dbb42236da)
  - **Musgrave Texture**
    (blender/blender@729b2d026d13)

### Improved Attribute Workflow

The attribute workflow in geometry nodes has been completely redesigned.
While the old system relied on addressing attributes by name, attribute
data can now be passed around with node links just like data in the node
tree.
(blender/blender@bf47fb40fd6f0e).

- As an example, the **Random Value** node replaces the existing
  *Attribute Randomize* node, and can simply be connected to any input
  to randomize it.
- The **Capture Attribute** node stores data from its input on a
  geometry so it is carried forward to other nodes, allowing the use of
  the same data in a different context
  (blender/blender@bf47fb40fd6f0e).
  - For example, this node makes it possible to use data only available
    on curves, like the output of the *Curve Parameter* node, after
    converting a curve to a mesh.
- The modifier interface can now interact directly with named attributes
  - A new attribute/value toggle on exposed inputs allows quick
    switching between attribute and single value inputs
    (blender/blender@8e21d528cab984,
    blender/blender@797064544ea3e1).
  - Named attributes can be created by connecting them to the group
    output node and choosing a name in the modifier
    (blender/blender@3d2ce25afd7e8e).
  - Attribute search is supported in the modifier
    (blender/blender@1d96a482675d).
- A new **Transfer Attribute** node can copy attribute values between
  geometries with various methods
  (blender/blender@a022cffb72f,
  blender/blender@76f386a37a9c).
  - The node includes an "Index" method to retrieve a value from the
    target geometry at a certain index
    (blender/blender@76f386a37a9cf1).
- The Attributes Panel in the property editor shows all generic
  attributes available on a mesh
  (blender/blender@6ce4d39e6bd585).
- Vertex group names are now stored on meshes rather than objects, which
  makes working with them in geometry nodes more predictable
  (blender/blender@3b6ee8cee708).
- The **Attribute Statistic** node outputs values like average, min, and
  max for an attribute
  (blender/blender@05f3f11d553e97).
- There is now a special domain interpolation method for selections
  (boolean attributes) to make the conversions work like they do in edit
  mode
  (blender/blender@5841f8656d9580).
- The `id` attribute is now built-in, but optional, instead of just a
  reserved name
  (blender/blender@40c3b8836b7).
  - The **ID** input node outputs the `id` attribute if it exists,
    otherwise the index
    (blender/blender@7d3d09b69c0af67,
    blender/blender@9beb5e38a9ca69b3).

### Instances

![The nodes in the instances category in 3.0](../../images/Instance_Nodes_3.0.png){style="width:600px;"}

The way geometry nodes works with instances has been improved, to make
it more intuitive and faster. ([Blender
Manual](https://docs.blender.org/manual/en/3.1/modeling/geometry_nodes/instances.html))

- Geometry can now be instanced directly, without a separate object
  (blender/blender@5a9a16334c573c).
- When the same geometry is instanced many times, nodes can often only
  do the calculation once on the original geometry, rather than
  realizing instances and doing it many times
  (blender/blender@44e4f077a9d7b5).
  - This can result in a large performance improvement in many cases.
    For example, each "A" character created by the *String to Curves*
    node in a long paragraph of text only has to be filled a single time
    by the *Curve Fill* node.
- Mesh modifiers after geometry nodes no longer implicitly realize
  instances or convert point clouds to meshes
  (blender/blender@219058c213d).
- Instances are now exposed properly to nodes, and aren't converted to
  real geometry implicitly.
- The **Realize Instances** node can explicitly convert instances into
  real geometry
  (blender/blender@7da9da2b27dddd).
- Because instances each have their own transformation, there are
  special nodes for transforming instances.
  - The **Rotate Instances** node rotates instances in their local space
    or global space
    (blender/blender@78445ebd5fd994).
  - The **Scale Instances** node scales an instance from a center point,
    in local or global space
    (blender/blender@6c11b320c41025).
  - The **Translate Instances** node moves instances along the three
    axes, also relative to their current transform, or globally.
    (blender/blender@9d49fc2ba0ecd0).
- The **Instances to Points** node converts the origins of a geometry's
  instances into a point cloud
  (blender/blender@3af597d16b1).
- A selection boolean input is added to to *Instance on Points*
  (blender/blender@d0a4a41b5d2).
- The *Object Info* node can now optionally output an instance instead
  of real geometry
  (blender/blender@19bab2a5360).
- The instances in the output of the *Collection Info* node are sorted
  alphabetically
  (blender/blender@5f59bf00444).
  - *Known Issue: Currently updating an object name does not cause a
    reevaluation of the node tree.*

### Curve Nodes

![All of the curve nodes added in 3.0, including the nodes for accessing built-in attributes](../../images/3.0_Curve_Nodes.png){style="width:800px;"}

Curve data is now supported in the node tree
(blender/blender@8216b759e955,
blender/blender@7c1bb239bebd).

- The **Curve to Mesh** node extrudes a profile curve along another
  curve
  (blender/blender@8216b759e955).
  - A "Fill Caps" option allows generating an N-gon at the ends of the
    profile, producing a manifold mesh
    (blender/blender@bc2f4dd8b408ee).
  - Custom attributes are automatically transferred from the curve to
    the mesh output
    (blender/blender@f81bacd6f0fc8d).
- The **Resample Curve** node distributes evenly spaced segments along a
  curve
  (blender/blender@3185084efbe4,
  blender/blender@d475f994606e38).
- The **Fill Curve** node creates a filled 2D mesh from an input curve
  (blender/blender@a71d2b26017090,
  blender/blender@6d162d35e2c85e)
- The **Subdivide Curve** node adds new control points between existing
  control points
  (blender/blender@ed4222258ea6).
- The **Sample Curve** node outputs values from the curve at a given
  factor or length along it
  (blender/blender@17021adceaee28).
- The **Trim Curve** node makes a curve shorter by discarding sections
  at the start and end
  (blender/blender@e7a800c52f07).
- The **Fillet Curve** node rounds corners of a curve with Bezier or
  Poly control points
  (blender/blender@0d350e0193f1af).
- The **Mesh to Curve** node creates curve poly splines from mesh edges
  (blender/blender@11e32332ddfd,
  blender/blender@17b8da719606).
- The **Endpoint Selection** node outputs a selection for a number of
  points at the start and end of each spline
  (blender/blender@1f510376764d).
- The **Curve to Points** node creates a point cloud with data necessary
  for instancing along the curve
  (blender/blender@fcbb20286a31,
  blender/blender@334a8d9b3eeb).

![Mandala patterns by Abir Dutta](../../images/Mandala.png){style="width:500px;"}

- There are field inputs specifically for curve data:
  - The **Curve Parameter** node outputs how far along the spline each
    point is as a 0-1 factor
    (blender/blender@4d881d9dad764d).
  - The **Curve Tangent** node outputs the direction of the curve at
    each control point
    (blender/blender@4d51af68adb273).
  - The **Normal** node outputs curve normals at each control point
    (blender/blender@05ce5276db7be4).
  - The **Handle Type Selection** outputs which handles have a certain
    type
    (blender/blender@1f8485ae8222d4).
- The **Set Handle Type** node changes the left or right handles of
  Bezier control points
  (blender/blender@0e8d1c6bcfcf).
- The **Curve Length** node outputs the total length of all splines in
  the curve
  (blender/blender@ddd4b2b78558).
- The **Reverse Curve** node reverses the order of all of a curve's
  control points
  (blender/blender@4a540b9b48cf).
- The type (Bezier, Poly, or NURBS) of each spline can be changed with
  the **Set Spline Type** node
  (blender/blender@0f455765907528).
- Curves fully support the existing attribute system.
  - Splines have `resolution` and `cyclic` builtin attributes.
  - Control points have `position`, `radius`, `tilt`,
    `handle_left` and `handle_right` built-in attributes
    (blender/blender@1892b131edc7,
    blender/blender@81f552e9ad1ab5).
  - Attributes with any name or data type can be created or removed on
    splines or control points
    (blender/blender@627f3571271e).
  - Attributes can be interpolated between the two curve domains
    (blender/blender@c97b6215a37e).
- Curve primitive nodes have been added for parametric addition of curve
  data directly.
  - **Bezier Segment**
    (blender/blender@8884d2d61b3d).
  - **Quadratic Bezier Segment**
    (blender/blender@d3788207aae6).
  - **Quadrilateral**
    (blender/blender@2a41ab5e6ca4).
  - **Circle**
    (blender/blender@c1fc18086118).
  - **Spiral**
    (blender/blender@21ebee258027).
  - **Star**
    (blender/blender@86c6769e2033).
  - **Line**
    (blender/blender@29d6750134c4e4).
- The geometry nodes modifier is now supported on curve (and text)
  object types
  (blender/blender@b9febb54a492ac).
  - Curve data from curve objects without a geometry nodes modifier will
    also display in the spreadsheet.
    - *Known Issue: This does not work in the "Original" mode of the
      spreadsheet.*

### General

- **Raycast** node projects lines to a mesh, returning information about
  the hit location to attributes
  (blender/blender@4b673ebb9906,
  blender/blender@0a6cf3ed0c64).
- **Delete Geometry** node removes parts of geometry, depending on the
  domain of the selection attribute
  (blender/blender@464797078d44,
  blender/blender@e5a1cadb2f7a)
- **Switch** node can efficiently select between two inputs of any type
  (blender/blender@799f532f4698,
  blender/blender@653bbaa246e3).
- **Convex Hull** node outputs a convex mesh containing all of the input
  points
  (blender/blender@df2a19eac7da).
- **Separate Components** node splits a geometry set into an output for
  each data type
  (blender/blender@ed4b2ba75a47).
- **RGB Curves** and **Vector Curves** nodes from shader nodes are
  supported in geometry node trees
  (blender/blender@8cd506639a78).
- Float to integer conversion can be done more explicitly with the
  **Float to Integer** node
  (blender/blender@fd0370acc2129f).
- Intermediate geometry data can be viewed in the spreadsheet with the
  **Viewer** node
  (blender/blender@9009ac2c3d62e).
  - This also replaces the screen icon in the header of every node.
- Some mesh primitive nodes have been improved:
  - The *Cube* node has been updated to allow a separate size and vertex
    count in each dimension
    (blender/blender@b44406f9634a35).
  - The *Cone* and *Cylinder* nodes have been updated with options for
    the amount of side and fill segments
    (blender/blender@54927caf4fcf21).
- The **Index** node outputs the index of each element when evaluating a
  field
  (blender/blender@bf47fb40fd6f0e).
- The **Normal** node outputs mesh normals, normalized on every domain,
  including faces and vertices
  (blender/blender@bf47fb40fd6f0e).
- **Mesh to Points** and **Points to Vertices** nodes allow converting
  directly between point clouds and mesh elements
  (blender/blender@262b2118565826).
- The old *Point Separate* node was updated to become more general as
  the **Separate Geometry** node
  (blender/blender@9c004864511f80).
- There are more basic input nodes
  - A new **Color** input node is an easier way to directly input a
    color
    (blender/blender@f7ef68514bc2ee).
  - There are now **Boolean** and **Integer** input nodes
    (blender/blender@781289e31fb).

### Text Nodes

![The new text manipulation nodes and the curve creation node](../../images/3.0_Text_Nodes.png){style="width:600px;"}

- The **String to Curves** node generates curve instances for a text
  paragraph, like the text object type but procedural
  (blender/blender@be16794ba17246).
  - Because the node creates curve instances, performance can be much
    better than the existing text object.
- String manipulation nodes allow creating text for the string to curve
  node procedurally
  (blender/blender@29e35451948044).
  - **String Length** Outputs length of a string
  - **Slice String** Outputs part of a string
  - **Value to String** Converts a value to a string
  - **Join Strings** Concatenates multiple strings with a delimiter
  - **Replace String** A node-based find and replace operation
    (blender/blender@a83b405a452)
- The **Special Characters** node can be used to build a paragraph with
  multiple lines
  (blender/blender@5c0017e85a75ad).

### Performance

- Geometry nodes has a new, faster evaluation system
  (blender/blender@b084b57fbf6d).
  - Multiple branches of the node tree are now executed in parallel
    where possible.
  - Lazy evaluation is supported, so where possible, only the inputs and
    outputs that are required are computed.
    - For example, the *Separate Geometry* node can be twice as fast if
      only one output is used and the *Switch* node only computes the
      input that is actually used.
- All field evaluation is parallelized, so computations for many
  geometry elements will always be multi-threaded
  (blender/blender@e6ca0545904fe4).
- The *Instance on Points* node now executes faster, especially when
  multiple CPU threads are available
  (blender/blender@518c5ce4cd75,
  blender/blender@617954c1438096).
- Transforming meshes with the *Transform* node is now much faster in
  some situations
  (blender/blender@17b09b509c06).

### Materials

![All of the new material nodes in 3.0](../../images/3.0_Material_Nodes.png){style="width:600px;"}

- An object's material slots can change during evaluation of geometry
  nodes
  (blender/blender@1a81d268a19f).
- Joining geometries from other objects will now properly add their
  materials to the modifier object
  (blender/blender@5e6f3b85646f).
- The **Set Material** node sets the material for a selection of faces
  (blender/blender@f41a753e7514,
  blender/blender@09f14b38f2d0c2).
  - Though volumes only support a single material, it can also be
    changed
    (blender/blender@348d7c35a9f).
- The **Replace Material** node replaces an existing material on a
  geometry with another
  (blender/blender@c154b078b529).
- The **Material Selection** node creates a boolean mask of the parts of
  a geometry with a certain material
  (blender/blender@08b0de45f323,
  blender/blender@09f14b38f2d0c2).
- The **Material Input** node allows using the same material in multiple
  places
  (blender/blender@3e3ecc329caa).
- The built-in `material_index` attribute on meshes can be accessed
  with the **Material Index** input node
  (blender/blender@24cc552cf48694).

### Volumes

- The geometry nodes modifier is now supported on volume objects
  (blender/blender@8c0f7d17725d).
- *Known Issue: Currently EEVEE cannot display procedurally generated
  volumes, converting the volume to a mesh and using a volume shader
  does work though*

## User Interface

### Node Editor

- The node editor now has an "Overlays" popover, with options for
  displaying wire colors and annotations
  (blender/blender@9b1b4b9e32c).
- The editor's view now pans automatically when links or nodes are
  dragged to the edges
  (blender/blender@a1cc7042a745,
  blender/blender@19da434e9cc020).
- Node editor UI style changes
  - There were many changes to the style of nodes themselves, to
    increase clarity and make them look nicer
    (blender/blender@4db4a97355672e).
  - The background grid is now displayed with dots instead of lines
    (blender/blender@e463d2c16f72e9).
  - The breadcrumbs to show the path of the node tree are now drawn
    differently, on the top left
    (blender/blender@3371a4c472eef0).
- On node frames, the label text is only displayed when there is a label
  set
  (blender/blender@8f04ddbbc626).
- Node links between different types with no possible implicit
  conversion now turn red to indicate the error (Geometry Nodes)
  (blender/blender@65244ac1c3f1).
- Socket labels on nodes like "Object Info" are hidden to give more
  space to data-block names
  (blender/blender@ec98bb318b93).
- Node links with sockets outside the view are dimmed, to remove visual
  noise from long links
  (blender/blender@c27ef1e9e8e6).
- Parent node trees are no longer displayed behind a green background
  when editing a nested node group
  (blender/blender@919e513fa8f9fb).
- Wire colors overlay to match the socket's color they are connected to.
  (blender/blender@9b1b4b9e32c8).

### Spreadsheet Editor

- The spreadsheet now has a region on the left to quickly switch between
  geometry components or domains
  (blender/blender@ae085e301c2aac).
- Row filters were added to a right property region, to allow removing
  rows from the view
  (blender/blender@f9aea19d9890).

### Modifier

- Node warnings are also displayed in the modifier, so they don't get
  lost in the node tree
  (blender/blender@49e68f15f204).
