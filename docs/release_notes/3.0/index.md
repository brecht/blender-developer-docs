# Blender 3.0 Release Notes

Blender 3.0 was released on December 3, 2021.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-0).

## [Animation & Rigging](animation_rigging.md)

## [Asset Browser](asset_browser.md)

## [Core](core.md)

## [EEVEE & Viewport](eevee.md)

## [Grease Pencil](grease_pencil.md)

## [Modeling](modeling.md)

## [Nodes & Physics](nodes_physics.md)

## [Pipeline, Assets & I/O](pipeline_assets_io.md)

## [Python API & Text Editor](python_api.md)

## [Render & Cycles](cycles.md)

## [Sculpt, Paint, Texture](sculpt.md)

## [User Interface](user_interface.md)

## [VFX & Video](vfx.md)

## [Virtual Reality](virtual_reality.md)

## [Add-ons](add_ons.md)

## Compatibility

See the sections
[Nodes](nodes_physics.md),
[Add-ons](add_ons.md),
[Cycles](cycles.md),
[Core](core.md),
[Animation_Rigging](animation_rigging.md)
and [Python API](python_api.md).

## [Corrective Releases](corrective_releases.md)
