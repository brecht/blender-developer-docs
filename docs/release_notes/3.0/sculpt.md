# Blender 3.0: Sculpt

## Transfer Mode

Transfer mode tool now has a nicer flash animation and a new hotkey,
ALT-Q. It also now works in more modes.

<figure>
<video src="../../../videos/Vokoscreen-2021-12-07_10-56-51.mp4" title="Demo of transferring sculpt mode between separate objects. Recorded by Tonatiuh de San Julián" controls="" />
<figcaption>Demo of transferring sculpt mode between separate objects.
Recorded by Tonatiuh de San Julián</figcaption>
</figure>

See also:

- Commit
  blender/blender@7bc5246156e0eba3992317f8ca1b4642de324689
- Commit
  blender/blender@c614eadb479e43258586af16deef6bd7d873ac59

## Other Changes

- The Voxel Remesher shading mode is now automatic depending on the
  object's flat/smooth shading option.
  blender/blender@ef7fcaf8e6c6.
- Texture Paint: better syncing between texture slots and editors.
  (blender/blender@56005ef499894bcd48313e3df7ec965fe516c1dc,
  blender/blender@3b2a01edf698fb27d9984fe12eb210df5246f16c,
  blender/blender@b0cb0a78547582848f5f18f5d5cc85c69f3dbe30)
- Texture Paint: Stencil now uses the assigned UV Layer properly.
  blender/blender@f11bcb5a80eb106560678c8c60be32ad997d6641.
- Weight Paint Gradient tool now has its falloff exposed.
  blender/blender@f8a0e102cf5e33f92447fe6cd6db7838f6051aab.
- Improved Multires performance
  blender/blender@482465e18aa7c9f74fcb90ec1002f157a580e240.
