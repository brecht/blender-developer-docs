## Fluid

- APIC simulation method:
  - There is now basic support for APIC simulations. They can be enabled
    from the simulation method menu which before only supported FLIP
    simulations.
    (blender/blender@8bdf191461a6)

<figure>
<video src="../../../videos/FLIP_Mantaflow.mp4" title="Simulation with FLIP" width="500" controls="" />
<figcaption>Simulation with FLIP</figcaption>
</figure>

<figure>
<video src="../../../videos/APIC_Mantaflow.mp4" title="Same simulation with APIC" width="500" controls="" />
<figcaption>Simulation with APIC</figcaption>
</figure>

  - Up until now, the FLIP method was used to create very splashy scenes
    with lots of particles dispersed in the air. For simulations on a
    smaller scale this behavior was not always desirable.
  - In these cases, and for simulations where achieving a more stable
    look is important, APIC can be used.
  - In general, users will note that APIC preserves vortices better than
    FLIP while remaining less noisy at the same time.
  - Except for FLIP specific properties (e.g. FLIP ratio), all existing
    liquid simulation settings (e.g. control particle narrow-band
    surface) can be used with APIC too.
- Improved particle sampling:
  - Liquid and secondary particles are sampled more predictably. With
    all parameters being equal, baked particles will be computed at the
    exact same position during every bake. Before, this was not
    guaranteed.
    (blender/blender@e09d0c0d077c)
- Sparse grid caching:
  - While saving grid data to disk, grids will from now on be saved in a
    sparse structure whenever possible (e.g. density, flame but not
    levelsets).
    (blender/blender@e09d0c0d077c)
  - With the sparse optimization grid cells with a value under the
    "Empty Space" value (already present in domain settings) will not be
    cached.
  - The main benefits of this optimization are:
    - Smaller cache sizes
    - Faster playback of simulation data in the viewport
  - This optimization "works out of the box". There is no option in the
    UI to enable it.
  - For now, only smoke simulation grids will take advantage of this
    optimization.
- Viscosity:
  - A new method for viscosity simulations has been added.
    (blender/blender@635694c0ff8f,
    [rBM7510](https://developer.blender.org/rBM7510))
  - The new viscosity algorithm makes it possible to simulate buckling,
    coiling and rotating liquids. (Underlying method: [Viscous Fluids](http://www.cs.ubc.ca/labs/imager/tr/2008/Batty_ViscousFluids/))
  - All new UI viscosity properties (i.e. enable/disable viscosity
    solver, change viscosity strength) can be animated with keyframes.
  - Both FLIP and the new APIC simulation method can be used with the
    viscosity solver. The more stable behavior makes APIC the
    recommended simulation method though.

## Collision

- Cloth now supports excluding faces from object collision using a
  vertex group, similar to an already existing feature for
  self-collision.
  (blender/blender@e44e0e4e7)
- Colliders can now be disabled without removing the Collision modifier
  via an animatable toggle button.
  (blender/blender@ac290bfbe)
