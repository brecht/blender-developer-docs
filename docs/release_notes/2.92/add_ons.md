# Blender 2.92: Add-ons

## Rigify

- Support for 5 bone chains in the `limbs.paw` rig type.
  (blender/blender-addons@46590bb780)
- New `limbs.front_paw` and `limbs.rear_paw` rig subtypes with extra
  IK automation designed for front and rear limbs of animals.
  (blender/blender-addons@46590bb780)
- The horse metarig updated using the new paw features to represent the
  horse anatomy better.
  (blender/blender-addons@46590bb780)
- The `limbs.super_palm` rig can now optionally generate a separate FK
  control for every palm bone.
  (blender/blender-addons@287f8443007)
- The `basic.super_copy` rig will now use the widget assigned to the
  metarig bone as is, and it's possible to choose which built-in widget
  to generate from a list. Human metarigs have better shoulder widgets
  now.
  (blender/blender-addons@b0361dcaac)
- Rigify will now create a separate widget collection for each generated
  rig, using the `WGTS_...` naming.
  (blender/blender-addons@f8d6489fb6)

## FBX I/O

- Exporter has a new option, "Use Space Transform", that allows to write
  a different axis orientation system that default Blender one, without
  actually trying to apply this global transformation to objects. It
  seems to allow Unity to better deal with orientations on import
  (blender/blender-addons@1a9a9d008dea).

## BlenderKit

- Texture Resolutions - All textured assets with higher original
  resolutions now have lower resolutions generated.
  - This saves memory, speeds up all interactions, and also helps to
    render bigger scenes.
  - Replace asset resolution command in the Selected model panel or in
    the Asset bar can be used to swap asset resolution.

<figure>
<video src="../../../videos/Resolutions_feature_in_BlenderKit.mp4" title="The resolution is set before download in import settings and can be changed after downlod from menu." width="500" controls="" />
<figcaption>The resolution is set before download in import settings and can be changed after downlod from menu.</figcaption>
</figure>

- Model thumbnailer was improved, default lighting and angle now look
  much cleaner. Uses a new HDR provided by Mike Radjabov.
- HDR asset type
- Scene asset type - BlenderKit scenes are templates to finish certain
  tasks, or usable as environment setups for your models.
- Particle system support inside Model asset type
  - It's possible to drag-drop particle systems and these will
    automatically be transferred on the target mesh. The count of
    particles is recalculated according to the target area, so the
    particle system looks similar as result.

<figure>
<video src="../../../videos/Particlesystem.mp4" title="Drag-drop of a compatible particle system on a mesh." width="500" controls="" />
<figcaption>Drag-drop of a compatible particle system on a mesh.</figcaption>
</figure>

- Upload was refactored
  - Updating only metadata is now much faster since it happens in the
    background thread.
  - If only the thumbnail is re-uploaded, background blender instance
    doesn't start.
- New fast metadata editing is possible from the right-click menu in
  asset bar.
- Snapping was improved and fixed.
  - Snapping ignores objects that are only drawn as bound boxes or are
    part of a particle system(like grass). This is especially useful if
    your scene has a lot of boolean modifier objects for purpose of
    cutting holes.
  - Snapping also works on inverted normals objects by always snapping
    on the visible side of the mesh.
  - Snapping works from inside of orthographic cameras
- Several improvements have been made to the rating UI, showing ratings
  more prominently and allowing users to rate.

## Collection Manager

### New Features

- An override checkbox was added to the preferences to override the
  object hiding hotkeys with object disabling hotkeys. Menu items were
  added to the Object-\>Show/Hide menu for the disable object operators.
  (blender/blender-addons@7cc2e4b1)

<!-- -->

- Selected objects' collections can now be either isolated or disabled
  from both the Collection Manager popup and the Quick View Toggles
  menu.
  (blender/blender-addons@47820f66)
- Quick View Toggle hotkeys were updated to be more consistent.
  (blender/blender-addons@6fcfb869)
  (blender/blender-addons@03fd6c5a)

### Bug Fixes

- Fixed a bug where QCD hotkeys would get registered twice when toggling
  the QCD checkbox in the preferences off and then on
  again.(blender/blender-addons@2b1d58fe)
