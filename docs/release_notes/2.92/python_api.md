# Blender 2.92: Python API

## Mirror operator

The operator `bpy.ops.transform.mirror` dropped support for
proportional editing to allow for correct object mirroring using scale
and rotation
(blender/blender@ec39d8de4adf).
Instead, use `bpy.ops.transform.resize` with negative scale, as this
is equivalent to the prior mirror operator. Except for the
`constraint_axis` option, it has the same the options as the prior
mirror operator.

``` python
# This mirror call in 2.91:
bpy.ops.transform.mirror(constraint_axis=(False, True, False), <other options>)
# ought to be written as this in 2.92:
bpy.ops.transform.resize(value=(1.0, -1.0, 1.0), <same other options>)
```

## Other changes

- Add `bpy.utils.unescape_identifier` a utility that performs the
  reverse of `bpy.utils.escape_identifier`
  (blender/blender@b5bc9d80a11dc99a296d0621bf3fdd156b70b754).
- In the Outliner, operators can get a list of the selected data-blocks,
  via `bpy.context.selected_ids`
  (blender/blender@af008f553293).
  This may become available in more editors in future.
- A button that represents a data-block may make the data-block
  available to operators via `bpy.context.id`
  (blender/blender@0c1d4769235c).
  This may however only work for operators placed in context menus of
  such buttons.
