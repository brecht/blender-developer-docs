# EEVEE & Viewport

## Transparent Render Pass

The new **Transparent** pass contain alpha blended surfaces, so they can
be adjusted in the compositor and later mixed with opaque passes that do
not contain them.

This pass only supports monochromatic opacity. Colored opacity will show
differently than in combined pass.

![From top-left in clock-wise order: Combined Pass, Diffuse Light Pass, Diffuse Color Pass, Transparent Pass](../../images/eevee_transparent_renderpass.png)

## Viewport Compositor

The following nodes are now supported in the Viewport Compositor:

- Convert Color Space
- Plane Track Deform
- Denoise
- Stabilize 2D
- Mask
- Corner Pin
- Texture
- ID Mask
- Map UV
- Fog Glow Glare
- Displace
- Z Combine
- Anti-Aliasing

Additionally, the following features are now supported:

- Bicubic Interpolation
- Repetition along a single axis
- Multi-layer EXR images

Finally, the following changes were implemented:

- Viewer nodes now take priority over Composite nodes as the active
  output if multiple output nodes exist.

## Edit Mesh Overlay

New Retopology option replaces the Hidden Wire option in mesh edit mode.
It allows the edit mesh to be drawn with a custom depth offset.
blender/blender@dcad51dfc3c5ead718bfe744669effdc304f79fb
