# Blender 3.6 LTS Release Notes

Blender 3.6 was released on June 27, 2023.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-6/).

This release includes long-term support, see the [LTS
page](https://www.blender.org/download/lts/) for a list of bugfixes
included in the latest version.

## [Animation & Rigging](animation_rigging.md)

## [Core](core.md)

## [EEVEE & Viewport](eevee.md)

## [Grease Pencil](grease_pencil.md)

## [Modeling & UV](modeling.md)

## [Nodes & Physics](nodes_physics.md)

## [Pipeline, Assets & I/O](pipeline_assets_io.md)

## [Python API & Text Editor](python_api.md)

## [Render & Cycles](cycles.md)

## [Sculpt, Paint, Texture](sculpt.md)

## [User Interface](user_interface.md)

## [Add-ons](add_ons.md)

## [Asset Bundles](asset_bundles.md)

## Compatibility
