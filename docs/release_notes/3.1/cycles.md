# Blender 3.1: Cycles

## Apple Metal

Cycles now has a Metal GPU backend, contributed by Apple. Metal GPU
rendering is currently supported on:

- Apple M1 computers running macOS 12.2 or newer
- Apple computers with AMD graphics card running macOS 12.3 or newer

The implementation is in an early state. Performance optimizations and
support for Intel GPUs are under development.

![Render time per sample on an Apple MacBook Air with M1 processor](../../images/Cycles_3.1_M1_CPU_vs_GPU.png){style="width:800px;"}

## Ray Tracing Precision

Many artifacts from rendering small, large and far away objects have
been eliminated. There will always be object scales where numerical
precision becomes a problem, but it's further out now.

There can still be artifacts with rendering overlapping geometry, in
some cases more severe than before. Such overlapping geometry should be
removed, or have a small distance added in between.

## Point Clouds

Direct rendering of point cloud objects as spheres is now supported.
This is much more efficient in memory usage and render time, compared to
instancing an object on every point.
(blender/blender@35b1e9f)

Point clouds can be generated with geometry nodes or imported from other
software. Some example uses are sand, water splashes, particles or
motion graphics.

The new Point Info shader node can be used to get the point center
position and radius, as well as a random value for point.

![Mandelbulb3D fractal by Michael A Prostka](../../images/Cycles3.1_mandelbrot_pointcloud.jpeg){style="width:600px;"}

## Features

- Camera: new Fisheye Lens Polynomial model. This allows real world
  cameras to be modeled by specifying the coordinates of a 4th degree
  polynomial that relates a pixels distance (in mm) from the optical
  center on the sensor to the angle (in radians) of the world ray that
  is projected onto that pixel. This is available as part of the
  panoramic lens type, however it can also be used to model lens
  distortions in projective cameras for example.
  (blender/blender@24e0165)
- Baking: new "Adjacent Faces" method to generate margin, based on
  adjacent faces across UV seams. This significantly reduces
  discontinuities on UV seams. The old method is called "Extend", and
  extends border pixels outwards.
  (blender/blender@449db0a)
- The map range node can now work directly on vectors.
  (blender/blender@5b61737a8f4168)
- Make Embree compact BVH optional. Disabling this can improve CPU
  render time, at the cost of higher memory usage.
  (blender/blender@e2a36a6e459f)
- Denoise operator in the Python API now supports OptiX temporal
  denoising. This requires enabling the Denoising Data and Vector
  passes. There is currently no user interface for this feature, only a
  Python API.
  (blender/blender@8393ccd07634)
- Merge operator in the Python API now supports merging OpenEXR renders
  that used adaptive sampling.
  (blender/blender@dbd64a5)
