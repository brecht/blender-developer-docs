# Geometry Nodes

## Viewer Node

- The *Viewer Node* now has the ability to preview geometry and
  attributes in the viewport as well as the spreadsheet
  (blender/blender@c55d38f00b8c).
  - When a field is linked to the second input of the viewer node it is
    displayed as an overlay in the viewport.
  - A viewer node is activated by clicking on it.
  - The attribute overlay opacity can be controlled with the "Viewer
    Node" setting in the overlays popover.
  - A viewport can be configured not to show intermediate
    viewer-geometry by disabling the "Viewer Node" option in the "View"
    menu.
  - Viewer nodes now have a domain dropdown. It determines which domain
    the field will be evaluated on.

<video src="../../../videos/2022-09-28_13-06-14.mp4" width="800" controls="" />

### General

- The **Self Object** retrieves the current modifier object for
  retrieving transforms
  (blender/blender@dd5131bd700c7e).
- The *Transfer Attribute* node has been removed and split into multiple
  more specific nodes
  (blender/blender@dedc679ecabb43).
  - The **Sample Index** node retrieves data from specific geometry
    elements by index.
  - The **Sample Nearest** node retrieves the indices from the closest
    geometry elements
  - The **Sample Nearest Surface** node interpolates a field input to
    the closest location on a mesh surface.

### Meshes

- The new **Face Set Boundaries** node finds the edges between different
  patches of faces
  (blender/blender@3ff15a9e23bd8a).
- Access to mesh topology information has been improved, with new nodes
  and other changes .
  - **Corners of Face** Retrieves corners that make up a face
    (blender/blender@482d431bb673).
  - **Corners of Vertex** Retrieves face corners connected to vertices
    (blender/blender@482d431bb673).
  - **Edges of Corner** Retrieves the edges on boths sides of a face
    corner
    (blender/blender@482d431bb673).
  - **Edges of Vertex** Retrieves the edges connected to each vertex
    (blender/blender@482d431bb673).
  - **Face of Corner** Retrieves the face each face corner is part of
    (blender/blender@482d431bb673).
  - **Offset Corner in Face** Retrieves neighboring corners within a
    face
    (blender/blender@482d431bb673).
  - **Vertex of Corner** Retrieves the vertex each face corner is
    attached to
    (blender/blender@482d431bb673).
  - The *Edge Vertices* node now has an index input so it can be more
    easily combined with the other nodes
    (blender/blender@4ddc5a936e07).
- The new **Sample UV Surface** node allows getting an attribute value
  based on a UV coordinate
  (blender/blender@e65598b4fa2059).

### Curves

- The *Trim Curves* node now supports cyclic curves
  (blender/blender@eaf416693dcb43).
- New nodes give access to topology information about the mapping
  between curves and points.
  - **Curve of Point** Retrieves the curve a control point is part of
    (blender/blender@482d431bb673).
  - **Points of Curve** Retrieves a point index within a curve
    (blender/blender@482d431bb673).
  - **Offset Point in Curve** Offset a control point index within its
    curve
    (blender/blender@8a6dc0fac71c,
    (blender/blender@248def7e4806)).
- The **Set Curve Normal** allows choosing the normal evaluation mode
  for curves
  (blender/blender@748fda32ede8).
- The **Sample Curve** node now has inputs for the curve index and a
  custom value to sample
  (blender/blender@5f7ca5462d31).

### Volumes

- The **Distribute Points in Volume** node creates points inside of
  volume grids
  (blender/blender@b6e26a410cd29f).

### Instances

- Attributes of instances created via geometry nodes are now accessible
  from materials through the Instancer mode of the Attribute node
  (blender/blender@2f7234d3e1bbc).

### Performance

- Geometry nodes has a new evaluation system
  (blender/blender@4130f1e674f83f).
  - The evaluator can provide better performance when many complex node
    groups are used.
  - Performance can be increased when there are very many small nodes
    (blender/blender@5c81d3bd469121).
- The *Trim Curves* node has been ported to the new data-block and can
  be 3-4 times faster than in 3.2 or 3.3
  (blender/blender@eaf416693dcb43).
- Mesh and curve domain interpolation can be skipped for single values
  (blender/blender@e89b2b122135,
  blender/blender@535f50e5a6a2).

## Node Editor

### Assets

- Node group assets are visible in the add menus of the node editor
  (blender/blender@bdb57541475f20).

### User Interface

- Show data-block use count in the Context Path and Group node header
  (blender/blender@84825e4ed2e0).

![](../../images/Geometry_Nodes_Use_Count.png)

- Node links have been tweaked to make them fit better with the overall
  look of the node editor
  (blender/blender@9a86255da8dcf0).
  - Link curving has been adjusted to make vertical links look better
    (blender/blender@67308d73a4f9ec).
- Input sockets are reused when creating new node groups
  (blender/blender@14e4c96b64f9).

### Duplicate Linked

- Duplicate Linked operator and user Preference option for Node Tree
  data duplication
  (blender/blender@6beeba1ef5ae).

![](../../images/Nodetree_duplication.png)
