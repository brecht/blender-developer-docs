# Grease Pencil

## Operators

- Now SVG import allows to import several files in one operation. Also,
  the name of the .SVG is used as Blender name.
  (blender/blender@670ced97589d)
- New `Outline` operator to convert strokes to perimeter.
  (blender/blender@aa7b2f1dd9f7)
- New `Outline` option at brush level to convert to outline the last
  stroke drawn.
  (blender/blender@613b6ad9e55f)
- New `Set Start Point`operator to define the start point for cyclic
  strokes.
  (blender/blender@98c4e1e590cb)
- New `Offset` parameter in reproject operator for surface mode.
  (blender/blender@5c33704ffa2f)

## Tools

- Fill tool has been improved adding a new algorithm to close gaps.
  (blender/blender@468f2ccc0ecb,
  blender/blender@dd0ef7488705,
  blender/blender@742268c50bd4,
  blender/blender@bdbf24772a0d,
  blender/blender@392855ce5022,
  blender/blender@fe19de5fccac)

The new method determines when to close the strokes based on how close
they are using a circle radius. This new method is better suited than
the old one (now renamed Extend) when the extended strokes never cross

If the gap between strokes is greater than the actual radius, the system
shows circles. To change the size of the circle use `Wheel Mouse` or
`PageUp` / `PageDown`. Also it's possible to use `MMB` of the
mouse or pen.

![](../../images/Close_Open.png){style="width:400px;"}

As soon the radius determine that the gap is closed, the help circles
are hidden. This allows to focus in pending gaps, keeping only circles
in pending ones.

![](../../images/Close_Contact.png){style="width:400px;"}

The Advanced panel was rearranged and now has a new section for `Gap
Closure` with all the new options.

![](../../images/Panel_Fill_Tool_new.jpg){style="width:486px;"}

Note: The old gap closure method using straight lines has been renamed
as `Extend` and the `Leak Size` has been removed because new
algorithms make this parameter obsolete.

- Fill tool extend lines now are clipped when there is a collision.
  (blender/blender@5c13c7cd30d1)
- New option to check if extension collide with normal strokes (not
  extensions). If enabled, if the extension collide, the color will be
  blue. Only blue extension will be used to limit fill area.
- Use `S` key to toggle extend method from Extend-\>Radius-\>Extend.
- Use `D` key to toggle stroke collision check.

## Modifiers

- New Outline modifier to generate perimeter stroke from camera view.
  (blender/blender@5f7259a0013b)
- LineArt: New "Forced Intersection" option that allows objects to
  always produce intersections even against objects that are set as "No
  Intersection". This allows more flexible configuration of more complex
  scenes.
  (blender/blender@0bdb5239c1a5)
- New `Chain` mode in Time Offset modifier that allows run one after
  another different Offset types.
  (blender/blender@b0d70a9c8040)

![](../../images/Time_Offset.png){style="width:346px;"}

## Python API

- New Trace Frame parameter in Trace operator.
  (blender/blender@903709c4ebd7)

The default trace can only trace an image or a sequence, but it was not
possible to trace only selected frames in a sequence. This new parameter
allows to define what frame trace. If the value is 0, the trace is done
as before. The parameter is not exposed in the UI because this is only
logic if it is managed by a python API.
