# Blender 2.91: Animation & Rigging

## FCurves

- Animation curves can now become much snappier, allowing for more
  sudden changes with fewer keyframes required
  (blender/blender@da95d1d851b).

![](../../images/release-notes-291-animation-fcurve-extremes.png){style="width:500px;"}

<video src="../../../videos/release-notes-291-animation-fcurve-extremes.mp4" title="Snappier, more extreme FCurves" width="500" controls="" />

- All Keyframes types can now be inserted without changing the FCurve
  shape
  (blender/blender@8b72d9cc1530).

![Insert keyframes without changing the curve shape. The blue line in the top image is the original FCurve before the middle key was inserted.](../../images/release-notes-291-animation-fcurve-insert-keyframes.png){style="width:500px;"}

- FCurves in the keyframe now have an active keyframe, similar to the
  active vertex in object edit mode. This makes it much easier to tell
  which keyframe the "Active Keyframe" panel corresponds to
  (blender/blender@983ad4210b9e).

![The active keyframe is drawn in white](../../images/Graph_Editor_Active_Keyframe.png){style="width:500px;"}

- The Snap menu in the graph editor now can snap the full 2D Cursor and
  just the cursor value
  (blender/blender@5ebdbcafcb27).

![Graph Editor Snap Pie Menu](../../images/release-notes-291-animation-graph-editor-snap.png){style="width:500px;"}

## Keyframes

- "Copy To Selected" support for keyframes
  (blender/blender@0a66436fe5f2):

<video src="../../../videos/Keyframe_Copy_To_Selected.mp4" controls="" />

## Non-Linear Animation (NLA) Editor

- "Sync Length" now no longer moves animation keys around
  (blender/blender@50919413fe7).

<figure>
<video src="../../../videos/Sync_move_keys.mp4" title="Move keyframes then sync strip length. New version preserves global frame timing." controls="" />
<figcaption>Move keyframes then sync strip length. New version preserves global frame timing.</figcaption>
</figure>

- The NLA system now evaluates the strip based on the action bounds if
  it's flagged for syncing
  (blender/blender@89ee260ef22).
  Now the animator can freely insert and modify keys outside of the
  strip bounds. They will never have to touch the strip bounds directly.
  Changing the evaluation bounds is a simple as moving keys around.

<figure>
<video src="../../../videos/Keyframe_outside_strip_bounds.mp4" title="Ability to keyframe outside strips that have Sync Length toggle active." controls="" />
<figcaption>Ability to keyframe outside strips that have Sync Length toggle active.</figcaption>
</figure>

- Non-active strips no longer gets hidden when entering edit mode
  (blender/blender@d067c13a9d07):

![Deactivated upper strip, &quot;noise&quot;, still shown while in tweak mode.](../../images/Show_All_Strips1.png)

## Constraints

- The Child Of constraint now sets the inverse matrix when it's created
  (blender/blender@ad70d4b0956f5f).
  This means that the object no longer jumps around when the Child Of
  constraint is added.

<!-- -->

- Add an optional Evaluation Time slide to the Action Constraints. This
  makes it possible to specify the playback position of the action
  without using any objects
  (blender/blender@c9c0f8930733):

![](../../images/Action_constraint_slider.png)

## Playback

- If using the "Allow Negative Frames" option, The Playback "Set Start
  Frame" / "Set Start Frame" operators allowed for setting a negative
  rendering range, which is not supported. This was corrected in
  blender/blender@ca38bce46aa2.
  A negative range can still be used if "Use Preview Range" option is
  used.
