# Blender 2.81: User Interface

## Outliner

![Active and selected items are synced and drawn highlighted](../../images/Outliner_2.81_Selection.jpg){style="width:444px;"}

<figure>
<video src="../../../videos/Blender_Outliner_2.81_gsoc.mp4" title="A short demonstration of the outliner improvements" width="444" controls="" />
<figcaption>A short demonstration of the outliner improvements</figcaption>
</figure>

The outliner got many usability improvements.

- Selection is now synced between the 3D viewport and outliner by
  default, in both directions.
- Walk navigation with up and down keys.
- Expand and collapse with left and right keys, hold shift to apply
  recursively.
- Box select with click and drag.
- Range selection with shift click, extend selection with ctrl click,
  and range select without deselecting with ctrl shift click.
- Expand arrow click and drag to expand or collapse multiple.
- Sub elements shown as icon row can now be selected.
- Object selection with eyedropper now works in the outliner.
- Filter option to show hidden objects.
- F2 to rename active outliner element.
- Parent drag and drop for multiple objects.
- Icons for constraints, vertex groups and sequencer.

  

## File Browser

![New File Browser design in Vertical List view](../../images/2_81_file_browser_vertical_list.png){style="width:400px;"}
![Thumbnail view](../../images/2_81_file_browser_thumbnails.png){style="width:400px;"}

The file browser was redesigned.

- Opens as floating window with more standard layout.
- Popovers for view and filter options.
- File operation options display in the sidebar. Depending on the
  operation the File Browser will execute, the sidebar is opened or
  closed by default.
- Vertical list view with interactive column header.
- Reworked display of date and file size text.
- New and updated icons.
- Deleting files will now move them to the system trash.
- Some settings of the dialog are saved in the preferences, so reopening
  it will have them remembered.
- Preferences option allows using the File Browser in full-screen
  layout.

Mouse and keyboard interaction is also more consistent and powerful. For
details, see
[section](https://wiki.blender.org/wiki/Reference/Release_Notes/2.81/UI#Keymap%7Ckeymap).

  

## Batch Rename

![](../../images/2_81_batch_rename.png){style="width:400px;"}

Where you could previously rename the active item with F2, you can now
also batch rename all selected items with Ctrl F2.

- Find & replace, with regular expressions
- Set, prefix or suffix
- Strip characters
- Change capitalization

  

## Preferences

- Option to use OS-Key for emulate middle mouse button.
  (blender/blender@d596a6368)
- Option to show temporary File Browser in full-screen layout
  (<span class="literal">Interface »
  <span class="literal">Temporary Windows »
  <span class="literal">File Browser).
  (blender/blender@f5bbaf55ac3d)
- Option for the render result location was moved from the scene, to the
  Preferences (<span class="literal">Interface »
  <span class="literal">Temporary Windows »
  <span class="literal">Render In).
  (blender/blender@cac756a92aae)
- Option for view rotation sensitivity.
  (blender/blender@e82b7f15270e)
- Option for Auto Handle Smoothing mode.
  (blender/blender@aabd8701e985)
- Annotation Simplify option got removed.
  (blender/blender@317033a1be2a)
- "Enabled Add-ons Only" option
  blender/blender@078d02f55743
- Auto-saving:
  - Option is disabled when factory settings have been loaded.
  - "\*" suffix is added to Save Preferences button when changes were
    made.
- Themes:
  - File Browser Custom Folder Color

## Keymap

### Default Keymap

3D View:

- Ctrl .:
  Toggle origin editing
- Poly Build Tool:
  - LMB Drag:
    On boundary edge, extrude quad and move with cursor; on vertices
    tweak position
  - Ctrl LMB: Add geometry
  - Shift LMB: Delete elements
- Shear Tool:
  - LMB Tweak north/south: Y axis shear (else X axis shear)
- Sculpt Mode:
  - A: Mask edit pie menu
  - Shift A: Mask expand from active index
  - Shift Alt A: Mask expand from active index, curvature mode
  - Ctrl R: Voxel remesh
  - Ctrl Alt R: Quadriflow remesh

File Browser:

- N: Toggle operator options sidebar
- LMB Click: Select item, or deselect on empty space
- LMB Double Click: Open
- Ctrl LMB: Add to selection
- Shift LMB: Add to selection and fill in-between items
- RMB Press: Context Menu
- LMB Press: Box-select
- F2: Rename

Grease Pencil:

- Shift I: Add blank frame
- Shift X, Shift Del: Delete all active frames
- Ctrl Alt E: Interpolate
- Shift Ctrl E: Interpolate Sequence
- Alt H: Unhide
- H: Hide selected
- Shift H: Hide unselected
- LMB Click: Create stroke material
- Shift LMB: Create fill material
- Ctrl LMB: Create stroke and fill material

Text Editor:

- Ctrl /: Toggle comments
- Ctrl G: Find selected text

Video Sequencer:

- 2 NumPad, 4 NumPad, 8 NumPad: Zoom 1:2, 1:4, 1:8 respectively
- Ctrl 2 NumPad, Ctrl 4 NumPad, Ctrl 8 NumPad: Zoom 2:1, 4:1, 8:1 respectively
- \[, \]: Select left/right of Playhead.

### Industry Compatible Keymap

For this keymap, an issue was fixed that made element mode keys break
when customising the keymap with additional shortcuts.

## Miscellaneous

- Context Menus for Info and Console Editors.
  (blender/blender@41f8f08e5188)
- Only hide locked transform manipulator axes for matching spaces.
  (blender/blender@1857aa32bd3b)
- Changed the default drawing method of Vector sockets. They are now
  drawn expanded using a column layout.
  (blender/blender@26911ba1)
- All selected elements are now properly moved when dragging keys and
  markers in the Dopesheet & Timeline, as well as strips in the Video
  Sequence Editor & NLA Editor. Previously this only worked in the node
  editors.  
  The Graph Editor is still missing this functionality, but will be
  added in the next release.
