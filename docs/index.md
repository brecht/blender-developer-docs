---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Blender Developer Documentation

> WARNING: **Migration in Progress!**
>
> This documentation platform is in migration.
>
> __The official platform for developer documentation is still
> [wiki.blender.org](https://wiki.blender.org/).__

<br/>

<div class="grid cards" markdown>

- :octicons-book-24: [**Blender Developer Handbook**](handbook/index.md)

  ---
  Build blender yourself, get started and read up all general information needed
  to develop Blender. <br/>
  For new and experienced developers alike.

  [:octicons-arrow-right-24: Check out the handbook](handbook/index.md)

</div>

<div class="grid cards" markdown>

- **Features**

  Dive into the many (sub-)modules and features of Blender.

  [:octicons-arrow-right-24: See the features](features/index.md)

- **Release Notes**

  Every release of Blender is packed with features! Browse through the history,
  or check out the latest and greatest.

  [:octicons-arrow-right-24: Release Notes](release_notes/index.md)
</div>

---

<div class="grid cards" markdown>
- :octicons-git-pull-request-24: **Contribute Documentation**

  ---
  Help build and improve this documentation platform.

  [:octicons-arrow-right-24: Read more](contribute/index.md)

  [:octicons-arrow-right-24: Build instructions](contribute/build_instructions.md)
</div>
