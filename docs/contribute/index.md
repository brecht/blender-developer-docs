# Contribute Documentation

While the developer documentation is mostly intended to be written by and for
developers, other contributors are more than welcome to get involved. Especially
involvement from technical writers would be appreciated.

This documentation is built using [Material for
MkDocs](https://squidfunk.github.io/mkdocs-material/). This provides a simple
developer-oriented workflow with the familiar Markdown language for writing.

You can conveniently edit files via Gitea and submit the changes for review, or
[build a local version](build_instructions.md) of the full documentation for
offline editing.

## Getting Started

This documentation can be edited in two ways:

- Online, using the Gitea UI on [projects.blender.org](https://projects.blender.org/blender/blender-developer-docs)
- Offline, by building a local version of the documentation.

=== "Online"
    > TODO: Instructions on how to edit documentation online.
=== "Offline"
    1. [Build the documentation](build_instructions.md) locally.
    2. [Edit](editing.md) the documentation.
    3. [Submit](submit_changes.md) your changes.
