# Editing

This documentation uses Markdown, so you can edit it in a normal text editor.
Also have a look at the [common extensions and
plugins](common_extensions_and_plugins.md) used to make the documentation a rich
experience.

## Adding Pages

When adding pages, they may not automatically show up in the navigation. If that
is the case you may want to add them to a `navigation.md` file in the directory
containing the file, or any of its parents. This directory must be referenced by
another `navigation.md` file, or live in one of the top-level sections (the ones
visible in the header bar). In them, the navigation can be defined like this:
```md title="Example <code>navigation.md</code>"
- [Frob](index.md)
- [Baz](baz.md)
- Borgs
    - [Bar](borgs/bar.md)
    - [Foo](borgs/foo.md)
- [More Borgs](more-borgs/index.md)
    - [Bar](more-borgs/bar.md)
    - [Foo](more-borgs/foo.md)
```

This decentralized navigation definition in Markdown files is made possible
through the
[`mkdocs-literate-nav`](https://oprypin.github.io/mkdocs-literate-nav/) plugin.

A directory can be included as subsection too:
```md title="Example <code>navigation.md</code>"
- [Some Directory](some-directory/)
```
This will include all files inside this directory under a "Some Directory"
subsection. Wildcard characters (`*`) can be used to include any remaining
files. Prefer including whole directories and wildcard characters over explicit
navigation entries, since new files will automatically show up this way.

## Adding Images

Simply place images in the directory of the Markdown file(s) using them. This
way images can be referenced there by name only, rather than an actual relative
path, and they are as close as possible to the content using them.

In Markdown you can use the usual syntax to emplace images. One difference is
that images will be centered by default for convenience.

```md
![](my-image-file.png)
```

To add a caption to the image, fill in the alt text as follows:
```md
![Caption goes here](my-image-file.png)
```

Left or right align:
```md
![This is the alt text](my-image-file.png){ align=left }
```

Change width:
```md
![This is the alt text](my-image-file.png){ style="width:300px;" }
```

For side-by-side images use a table:
```md
|A|B|
|-|-|
|![](image-a.jpg)|![](image-b.jpg)|
```

## Adding Video

To add a video:
```md
<video src="test.mp4" width="600" controls />
```

To add a caption to a video:
```
<figure>
<video src="test.mp4" controls />
<figcaption>Caption goes here</figcaption>
</figure>
```

## Advanced

Only basic/common editing was covered here. The following resources contain a
lot more information on how to write rich documentation using Material for
MkDocs:

- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
- [MkDocs](https://www.mkdocs.org/)
- [MkDocs projects and plugins](https://github.com/mkdocs/catalog)
- [Python Markdown extensions](https://python-markdown.github.io/extensions/)
