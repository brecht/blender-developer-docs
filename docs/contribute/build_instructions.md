# Build Instructions

These instructions only apply if you want to build a local/offline version of
the documentation. This is convenient for more elaborate edits, using an editor
and workflow of your choice.

=== "Windows"
    **Install required software:**

    1. Download the [Python installation package](https://www.python.org/downloads/) for Windows.
    2. Install Python with the installation wizard. **Make sure to enable the "Add Python to PATH" option**.
    3. Download and install [Git for Windows](https://git-scm.com/download/win).

    **Set up Git LFS:**<br/>
    Open a command line window and run the following command:
    ```shell
    git lfs install
    ```

    **Clone the documentation sources:**
    ```shell
    git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
    cd developer-docs
    ```
    This will clone the sources into a `developer-docs` directory inside the current
    one and change into it.

    !!! tip ""
        **Recommended: Setup and activate a virtual environment where dependencies will be installed:**
        ```sh
        python3 -m venv .venv
        .venv/Scripts/activate
        ```

=== "macOS"
    Install required software:
    [PIP](https://pip.pypa.io/en/latest/installation/),
    [Git](https://git-scm.com/download/mac) and [Git LFS](https://git-lfs.com/).
    When using [Homebrew](https://brew.sh/), run the following commands in the
    terminal:
    ```sh
    python3 -m ensurepip
    brew install git git-lfs
    ```

    **Set up Git LFS:**
    ```sh
    git lfs  install
    ```

    **Clone the documentation sources:**
    ```sh
    git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
    cd developer-docs
    ```
    This will clone the sources into a `developer-docs` directory inside the
    current one and changesinto it.

    !!! tip ""
        **Recommended: Setup and activate a virtual environment where dependencies will be installed:**
        ```sh
        python3 -m venv .venv
        source .venv/bin/activate
        ```

=== "Linux"
    Install Python, PIP, Git and Git LFS using your package manager:

    === "Debian"
        ```sh
        sudo apt install python3 python3-pip git git-lfs
        git lfs install --skip-repo
        ```
    === "Redhat/Fedora"
        ```sh
        sudo yum install python python-pip git git-lfs
        git lfs install --skip-repo
        ```
    === "Arch Linux"
        ```sh
        sudo pacman -S python python-pip git git-lfs
        git lfs install --skip-repo
        ```

    **Clone the documentation sources:**
    ```sh
    git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
    cd developer-docs
    ```
    This will clone the sources into a `developer-docs` directory inside the
    current one and change into it.

    !!! tip ""
        **Recommended: Setup and activate a virtual environment where dependencies will be installed:**
        ```sh
        python3 -m venv .venv
        source .venv/bin/activate
        ```

**Install all dependencies, such as
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/):**
```sh
python3 -m pip install -r requirements.txt
```

**Build this documentation with live reloading:**
```sh
mkdocs serve
```

Alternatively `mkdocs build` will generate the documentation as HTML into a
`site/` directory. Simply open `site/index.html` in a browser.
