= Unity Builds =

No, not the game engine :)

Unity builds are a method to speed up compilation time by avoiding redundant compilation of headers. The idea is to `#include` multiple source (`.c`, `.cc`, `.cpp`, etc.) files into a single source file so that they form one translation unit instead of multiple ones. This means that headers used by multiple of these initial source files only have to be compiled once, which can speed up compilation time significantly. Especially notable improvements can be made with template heavy C++ code, that would otherwise increase the size of the separate translation units with redundant template instantiations.

In Blender, unity builds are created with the help of [https://cmake.org/cmake/help/latest/prop_tgt/UNITY_BUILD.html CMake's unity build support].

Also see: https://en.wikipedia.org/wiki/Unity_build

== Enabling/Disabling Unity Builds ==

Unity builds can be toggled via Blender's CMake option `WITH_UNITY_BUILD`.

== Implementing Unity Builds for a Module ==

TODO

== Preventing Naming Collisions ==

By merging translation units into one, there is an increased chance of naming collisions, when multiple source files define the function name/signature or type name more than once. Therefore [[Style_Guide/C_Cpp#Unity_builder_namespace|the style guidelines include a preferred way to avoid that]].