= Profiling Python Code =

== The 'profile' module ==

This example shows how to use the [https://docs.python.org/3.5/library/profile.html <tt>cProfile</tt>] Python module to show script performance.

https://projects.blender.org/blender/blender/src/commit/711e3564b808ab802c503409e7740b0ca9b6ef65/release/scripts/modules/bpy/__init__.py#L68

The function call shows:

<source lang=python>
# Initializes Python classes.
# (good place to run a profiler or trace).
utils.load_scripts()
</source>

This example shows how a the function call can be profiled.

<source lang=python>
import cProfile
cProfile.run("import bpy; bpy.utils.load_scripts()", "blender.prof")

import pstats
p = pstats.Stats("blender.prof")
p.sort_stats("cumulative").print_stats(20)
</source>

This prints the top 20 cumulative functions.

Heres an example of what the output may look like.

<source lang=text>
         45572 function calls (43669 primitive calls) in 0.243 seconds

   Ordered by: cumulative time
   List reduced from 1507 to 20 due to restriction <20>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
    120/1    0.002    0.000    0.243    0.243 {built-in method builtins.exec}
        1    0.000    0.000    0.242    0.242 <string>:1(<module>)
        1    0.000    0.000    0.242    0.242 /src/blender/release/scripts/modules/bpy/utils/__init__.py:134(load_scripts)
   126/20    0.002    0.000    0.192    0.010 <frozen importlib._bootstrap>:966(_find_and_load)
   126/20    0.001    0.000    0.190    0.010 <frozen importlib._bootstrap>:939(_find_and_load_unlocked)
   125/20    0.001    0.000    0.186    0.009 <frozen importlib._bootstrap>:659(_load_unlocked)
    97/18    0.001    0.000    0.185    0.010 {built-in method builtins.__import__}
   117/20    0.001    0.000    0.185    0.009 <frozen importlib._bootstrap_external>:656(exec_module)
   213/20    0.000    0.000    0.182    0.009 <frozen importlib._bootstrap>:214(_call_with_frames_removed)
 1231/162    0.001    0.000    0.169    0.001 <frozen importlib._bootstrap>:996(_handle_fromlist)
        1    0.000    0.000    0.143    0.143 /src/blender/release/scripts/modules/bpy/utils/__init__.py:108(modules_from_path)
        4    0.000    0.000    0.143    0.036 /src/blender/release/scripts/modules/bpy/utils/__init__.py:75(_test_import)
        1    0.000    0.000    0.099    0.099 /src/blender/release/scripts/startup/bl_ui/__init__.py:23(<module>)
     1071    0.013    0.000    0.072    0.000 {built-in method builtins.__build_class__}
        1    0.000    0.000    0.057    0.057 /src/blender/release/scripts/modules/addon_utils.py:40(_initialize)
       11    0.000    0.000    0.056    0.005 /src/blender/release/scripts/modules/addon_utils.py:258(enable)
      991    0.007    0.000    0.050    0.000 /src/blender/release/scripts/modules/bpy_types.py:537(__new__)
        4    0.000    0.000    0.041    0.010 /src/blender/release/scripts/modules/bpy/utils/__init__.py:202(test_register)
        4    0.000    0.000    0.041    0.010 /src/blender/release/scripts/modules/bpy/utils/__init__.py:166(register_module_call)
      117    0.001    0.000    0.040    0.000 <frozen importlib._bootstrap_external>:726(get_code)
</source>