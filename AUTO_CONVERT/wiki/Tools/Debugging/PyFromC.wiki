''Note that this is a very specific debugging/testing facility, if you are new to debugging its probably not what your looking for.''

== Rationale ==
This function lets you set one line in your code which calls python to execute code, modifying C/C++ variables, so you can write and test code while blenders running - renders/animates/redraws.

Python running in C is nothing new, but normally this is done with predefined API glue code which has strict entry points, where setting up a proper API would be prohibitively slow for quick tests in the middle of a C function.

== Usage ==
* Add a line into your code.
 PyC_RunQuicky("script.py", vars...)
* create a python script that edits '''values''', a list which stores each variable given.
* Run blender and modify the script leaving blender open, modifications to '''values''' are copied back into blender after each execution.

===Example===
Having C call python is just one function call.

The first argument is the file to run, the second is the number of arguments, the rest are format & pointer pairs. This formatting is passed onto struct.pack/unpack.

<source lang=c>
/* this is needed to avoid any python includes */
extern void PyC_RunQuicky(const char *filepath, int n, ...);

static void drawcentercircle(View3D *v3d, RegionView3D *rv3d, float *vec, int selstate, int special_color)
{
    float size;

    /* run a python script to modify the values passed */
    PyC_RunQuicky("/home/ideasman42/test.py", 8,
        "3f",vec, "16f",rv3d->persmat, "f",&size, "f",&rv3d->pixsize);
    ...
    ...
</source>

The script can be very simple, just modify '''values''' in place.
<source lang=python>
# get the values set by PyC_RunQuicky
vec, perspmap, size, pixelsize = values

# modify some value
mat = perspmap[0:4], perspmap[4:8], perspmap[8:12], perspmap[12:16]
size = mat[0][3]*vec[0] + mat[1][3]*vec[1] + mat[2][3]*vec[2] + mat[3][3]

print(vec, size, pixelsize)

# set the list so the changes are updated in blender.
values = vec, perspmap, size, pixelsize
</source>

== Summery ==
'''Pros'''
* immediate feedback.
* quick setup.
* no need to write api wrapper code.
* access to blender python modules like bgl and mathutils.
'''Cons'''
* not so useful when the fix involves changes in many places.
* not so useful for modifying structures or pointers.
* allows for bad practice - "trial & error, without understanding".