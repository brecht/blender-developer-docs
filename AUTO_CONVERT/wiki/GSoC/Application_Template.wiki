= Google Summer of Code - Application Template =

Students will have to submit a proposal on the GSoC website based on the template below.<br>
You can submit a draft proposal early on, get feedback and iterate on it.<br>
The proposal must be submitted as a final PDF before the end date.

Be sure to read Google's [https://google.github.io/gsocguides/student/writing-a-proposal guide to writing a proposal] and our [[GSoC/Getting_Started|getting started page]].

== Template ==

=== Project Title  ===

:Please provide the title of your proposed project.

=== Name  ===

:Please provide your full name.

=== Contact  ===

:Where can we contact you? Add here your email, Blender chat nickname, social network handles and website.<br>If you frequent our [https://blender.chat/channel/blender-coders #blender-coders channel on blender.chat], let us know your nickname.<br>If you have a web page you'd like us to know about, please include it. You can mention your Facebook, Twitter or other identities!

=== Synopsis  ===

:A short description of your project idea.

=== Benefits  ===

:Describe how your project will improve Blender.
: How will it benefit the artists using Blender for 3D content creation? Will it be an aid for future Blender development?

=== Deliverables  ===

:Provide a summary of the final output or results of your project in terms that are understandable by artists and user the community.
:How does it integrate in Blender and how does it cooperate with the rest of Blender's features?<br>Note that end-user documentation should be one of the deliverables.

=== Project Details  ===

:Elaborate your project idea. Here is the place to add technical explanations, architecture diagrams or UI/UX mockups.<br>Make sure that your vision is clear and avoid fuzzy promises that you aren't sure about. If you have doubts, research and ask for feedback and clarifications beforehand.

=== Project Schedule  ===

:Specify how long the project will take and when you can you start. 
:Add a timeline with the breakdown of the work. Include milestones with deliverables.<br>Tip! leave at least a week at the end to cope with delays.
:Please note any time that you will be unavailable during the project period. Will there be school exams or other events that conflict with the schedule? 

=== Bio  ===

:Tell us a little about yourself. What are you studying and where? What activities do you enjoy?
:What is your experience using Blender or other computer graphics programs? What code development projects have you participated in so far? What is your level of proficiency with C, C++, Python and programming in general?
:What makes you the best person to work on this project? If you have any history submitting bug fixes or patches to our tracker, please indicate what you have done.

== Examples ==

Browse the accepted [[GSoC|projects from the previous years]].