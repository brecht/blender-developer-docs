=Google Summer of Code - 2023 Projects=

<div class="bd-lead">

Progress updates for 2023
</div>


==Current State==

The information in this page still to be populated through the duration of the Google Summer of Code.

'''1<sup>st</sup> evaluation - ''' 10 - 14 July<br/>
'''Final submission - ''' 28 August - 4 September

==Communication==
===Development Forum===
All Blender and code related topics will go to the devtalk forum. Here contributors will also post their weekly reports, and can ask for feedback on topics or share intermediate results with everyone.

https://devtalk.blender.org/c/contributing-to-blender/summer-of-code/15

===blender.chat===
For real-time discussions between contributors and mentors.
Weekly meetings will be scheduled here as well. 

https://blender.chat/channel/gsoc-2023

==Projects==
Google has granted 5 projects for the Blender Foundation.

----

===Flamenco Improvements===
by '''Eveline Anderson'''<br />
''Mentors:'' Sybren Stüvel<br>
''Code:'' https://projects.blender.org/Evelinealy
 
* [https://devtalk.blender.org/t/gsoc-2023-flamenco-improvements/29318 Proposal and Reports]

===VSE Waveform Drawing Performance===
by '''Lucas Tadeu Teixeira'''<br />
''Mentors:'' Richard Antalik<br>
''Code:'' https://projects.blender.org/Yup_Lucas
 
* [https://devtalk.blender.org/t/gsoc-2023-improve-waveform-drawing-speed/29408 Proposal and Reports]

===UV Editor Improvements===
by '''Melissa Goon'''<br />
''Mentors:'' Campbell Barton<br>
''Code:'' https://projects.blender.org/Melissa-Goon
 
* [https://devtalk.blender.org/t/gsoc-2023-uv-editor-improvements/29329 Proposal and Reports]

===Shader Editor Node Preview===
by '''Colin Marmond'''<br />
''Mentors:'' Brecht Van Lommel<br>
''Code:'' https://projects.blender.org/Kdaf
 
* [https://devtalk.blender.org/t/gsoc-2023-shader-editor-node-preview-weekly-reports/29359 Proposal and Reports]

===Outliner Tree Refactoring===
by '''Almaz Shinbay'''<br />
''Mentors:'' Julian Eisel<br>
''Code:'' https://projects.blender.org/Almaz-Shinbay
 
* [https://devtalk.blender.org/t/gsoc-2023-outliner-tree-refactoring/29355 Proposal and Reports]