= Animations =

User interface animations should be used to visually guide the user and make interactions feel seamless. They should never get in the way or be used just for the sake of it.

== Duration ==

'''Try to find a good balance between a quick and easy to follow animation.''' An animation that is too fast for the user to follow isn't better, maybe worse than no animation. A too slow animation on the other hand can get in the way and annoy.

'''The duration should be calculated based on the amount of total change.''' If the animation visualizes a big transition (e.g. to scroll the view a lot), the duration should be slightly longer but it should feel rather fast paced. A small transition (e.g. to scroll the view a little) should feel a bit slower - but not too slow - so the duration should be just a bit shorter.<br/>If the amount of total change is fixed (e.g. to animate between a simple visible/not-visible state), the duration can be fixed at a well working value as well.

== Pacing ==

'''Make good use of interpolation functions to pace the animation.''' A linear interpolation transitions with an equal speed at any part of the animation. The start and end are abrupt. A simple ease in/out interpolation makes the animation feel much smoother, because it starts slowly, speeds up, and slows down until it's in the final state. Such interpolation methods are key to making animations beautiful and natural.