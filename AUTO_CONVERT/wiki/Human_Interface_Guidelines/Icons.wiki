= Icons =

* '''Do not reuse icons for different purposes.''' Slight variation of icons are fine. Composing an icon out of multiple other icons is fine as well. But generally the rule is: One icon for one thing.
* '''Do not use the Blender logo on its own, unless it is referring to Blender itself.''' An icon may consist out of multiple symbols though, including the Blender logo if the icon indicates something as Blender specific.

== Main Icon set ==

== Tool Icons ==

The colors are used to convey different information to the icons. For instance:
* Green: Added geometry
* Red: Removed geometry.
* Purple: Edited geometry.

== Default Thumbnails/Previews ==