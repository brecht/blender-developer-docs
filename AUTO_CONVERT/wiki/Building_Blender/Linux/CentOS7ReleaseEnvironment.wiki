= Cent OS 7 Release Build Environment =

The official Blender builds are done on the CentOS 7 system. This makes it possible to have a portable build which will work on [https://vfxplatform.com/ VFX reference platform] compatible systems.

This page describes how the official setup is created. This setup allows to compile Blender itself, as well as the Blender dependencies.

== Preparing CentOS 7 Environment ==

The release environment is based on the `Minimal Install` of the CentOS 7. This document does not cover the steps needed to get the OS installed: it is rather straightforward and the default installation will get to where the systems needs to be.

The [https://developer.blender.org/diffusion/B/browse/blender-v3.4-release/build_files/build_environment/linux/linux-centos7-setup.sh linux-centos7-setup.sh] script is then run as a root on the freshly installed system. It takes care of installing all dependencies.

{|class="note"
|-
|<div class="note_title">'''Note'''</div>
<div class="note_content">Some system-wide changes are performed, so it is recommended to use a dedicated virtual machine for the release build environment. </div>
|}

Congratulations, the base system is now ready!

Follow the [[Building_Blender/GPU_Binaries|Building Cycles with GPU Binaries]] for the instructions related to installing the GPU compute toolkits.

== Building Blender ==

Blender is built from sources using the precompiled CentOS 7 libraries. Follow the Download Sources and Download Libraries sections from the [[Building_Blender/Linux/Generic_Distro|Linux Quick Setup]]. Note that all packages have already been installed, so that step from the Quick Setup should be skipped. Also make sure to get precompiled libraries for CentOS 7.

The build is to be performed using gcc-9 toolchain. It is activated using the `scl` command:

<source lang="bash">
scl enable devtoolset-9 bash
cd ~/blender-git/blender
make release
</source>

== Building Blender 3.3 LTS with Python 3.9 ==

The official Blender 3.3 LTS uses Python 3.10, but it is possible to compile it using Python 3.9 for compatibility with the VFX Reference Platform 2022. Python 3.9 comes with the precompiled libraries.

Configuration of the build environment is done as described above, but using the following make command instead:

<source lang="bash">
make release BUILD_CMAKE_ARGS="-DPYTHON_VERSION=3.9"
</source>