





== Quick Setup ==

The following instructions will create a Blender build in a `blender-git` folder in your home directory.

=== Install Initial Packages ===

Ensure both Python3 and Git are installed.

=== Download Sources ===

Download the latest source code from projects.blender.org.

<source lang="bash">
mkdir ~/blender-git
cd ~/blender-git
git clone https://projects.blender.org/blender/blender.git
</source>

''For additional information on using Git with Blender's sources, see: [[Tools/Git|Tools/Git]]''

=== Install Basic Building Environment ===

You need to install all the basic dependencies manually, using packages specific to your Linux distribution:
<source lang="text">
gcc g++ make cmake
git subversion
libx11-dev libxxf86vm-dev libxcursor-dev libxi-dev libxrandr-dev libxinerama-dev libegl-dev
libwayland-dev wayland-protocols libxkbcommon-dev libdbus-1-dev linux-libc-dev
</source>

Running this command will also give you the list of required dependencies.
<source lang="bash">
cd ~/blender-git/blender/
./build_files/build_environment/install_linux_packages.py
</source>

=== Download Libraries ===

For Intel and AMD Linux systems, '''we recommend using precompiled libraries'''. These are the quickest way to get a feature complete Blender build and can be downloaded as follows.

These libraries are built on Rocky8 for [https://vfxplatform.com/ VFX reference platform] compatibility, but they work fine on other Linux distributions. 

<source lang="bash">
mkdir ~/blender-git/lib
cd ~/blender-git/lib
svn checkout https://svn.blender.org/svnroot/bf-blender/trunk/lib/linux_x86_64_glibc_228
</source>

To use system packages, see [[#Automatic_Dependency_Installation|Advanced Setup]].

=== Update and Build ===

Get latest source code and add-ons, and build. These commands can be used for the first build, and repeated whenever you want to update to the latest version.

<source lang="bash">
cd ~/blender-git/blender
make update
make
</source>

After the build finished, you will find blender ready to run in `~/blender-git/build_linux/bin`.

If building fails after an update, it sometimes helps to remove the `~/blender-git/build_linux` folder to get a completely clean build.

== Branches ==

With the quick setup instructions the `main`  branch will be built by default, which contains the latest Blender version under development.

To build another branch or a [[Tools/Pull_Requests|pull request]], first checkout the branch and then run `make update` to update the add-ons, libraries and tests to match.

<source lang="bash">
cd ~/blender-git/blender
git checkout <branch-name>
make update
make
</source>

Depending on how big the changes are compared to the `main` branch, it may be necessary to remove the build folder `~/blender-git/build_linux`. Otherwise the build folder may still refer to libraries from another branch.
= Advanced Setup =

== Automatic Dependency Installation ==

When not using precompiled libraries, the preferred way to install dependencies under Linux is to use the `install_linux_packages.py`
script in the Blender sources. It will try to install system packages for all known dependencies, required or optional.

It currently supports Debian (and derivatives like Ubuntu), Fedora, Suse and Arch distributions. It is executed as follows. 
<source lang="bash">
cd ~/blender-git/blender
./build_files/build_environment/install_linux_packages.py --all
</source>

Some commands in this script requires sudo, so you'll be likely be asked for your password.

For unrecognized distributions, it will only print a list of the dependencies.

{|class="note"
|-
|<div class="note_title">'''Building with Precompiled libraries and System libraries'''</div>
<div class="note_content">
In case both precompiled and system libraries are available, by default CMake will use the pre-compiled ones.

To force it to ignore the precompiled libraries and search for system ones, CMake `WITH_LIBS_PRECOMPILED` option must be disabled.</div>
|}



== Portable Builds ==

The above instructions install packages through the system package manager. This makes it possible to share packages between Blender and other software, however the resulting builds will generally not work on other computers.

When using the precompiled libraries, builds are portable and can be shared with others. These libraries are built from source, and this system can also be used to create your own portable libraries.

Running [[Building_Blender/Dependencies#make_deps|make deps]] will build libraries in `../lib/linux_x86_64_glibc_228`, which will be automatically picked up when building Blender itself.

Besides the libraries, the GLIBC version of the system affects portability. Builds will only run on Linuxes with the same or higher GLIBC version. Official builds are made with RHEL 8 and glibc 2.28.

== Manual CMake Setup ==

<div id="manual_cmake_setup">
  <div class="card">
    <div class="card-header" id="heading_manual_cmake_setup">
        <div class="btn btn-link" data-toggle="collapse" data-target="#collapse_manual_cmake_setup" aria-expanded="true" aria-controls="collapse_manual_cmake_setup">Instructions</div>
    </div>

    <div id="collapse_manual_cmake_setup" class="collapse" aria-labelledby="heading_manual_cmake_setup" data-parent="#manual_cmake_setup">
      <div class="card-body">
If you want to have more control over your build configuration or have multiple build directories for a single source directory.
You can follow these steps.

''Typically useful for developers, who build frequently.''

=== CMake Setup ===

Now you have to choose a location for CMake build files, currently '''in-source builds in Blender aren't supported'''.

'''Out-of-source build'''

By doing an [http://www.cmake.org/Wiki/CMake_FAQ#What_is_an_.22out-of-source.22_build.3F "out-of-source" build] you create a CMake's directory aside from `~/blender-git/blender`, for example `~/blender-git/build`:

<source lang="bash">
mkdir ~/blender-git/build
cd ~/blender-git/build
cmake ../blender
</source>

This will generate makefiles in the build directory (`~/blender-git/build`).

'''Important''' 

As said above, ''in-source-builds'' where you build blender from the source code directory are not supported.<br>
If CMake finds a `CMakeCache.txt` in the source code directory, it uses that instead of using `~/blender-git/build/CMakeCache.txt`.

If you have tried to do an in-source build, you should remove any CMakeCache.txt from the source code directory before actually running the out-of-source build:

<source lang="bash">
rm ~/blender-git/blender/CMakeCache.txt
</source>

=== Building Blender ===

After changes have been done and you have generated the makefiles, you can compile using the ''make'' command inside the build directory:

<source lang="bash">
cd ~/blender-git/build
make
make install
</source>

Also notice the '''install''' target is used, this will copy scripts and documentation into `~/blender-git/build/bin`

For future builds you can simply update the repository and re-run make.
<source lang="bash">
cd ~/blender-git/blender
make update
cd ~/blender-git/build
make
make install
</source>
</div>
    </div>
  </div>
</div>__NOEDITSECTION__

== Build Options ==

By default Blender builds will be similar to official releases. Many [[Building_Blender/Options|Build Options]] are available for debugging, faster builds, and to enable or disable various features.

== System Installation ==

Portable installation is the default where scripts and data files will be copied into the build `~/blender-git/build/bin` directory and can be moved to other systems easily.

To install Blender into the system directories, a system installation can be used instead. Disable `WITH_INSTALL_PORTABLE` to install into `CMAKE_INSTALL_PREFIX` which uses a typical Unix [http://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard FHS] layout: bin/, share/blender/, man/ etc.

== Optimize Rebuilds ==

<div id="optimize_rebuilds">
  <div class="card">
    <div class="card-header" id="heading_optimize_rebuilds">
        <div class="btn btn-link" data-toggle="collapse" data-target="#collapse_optimize_rebuilds" aria-expanded="true" aria-controls="collapse_optimize_rebuilds">Instructions</div>
    </div>

    <div id="collapse_optimize_rebuilds" class="collapse" aria-labelledby="heading_optimize_rebuilds" data-parent="#optimize_rebuilds">
      <div class="card-body">
Now that you can build Blender and run Blender, there are changes you can make which greatly increase the speed of build times.

Use one of the CMake configuration editors (ccmake or cmake-gui), see [[Building_Blender/Options#Editing_CMake_Options| Editing CMake Options]]

For really fast rebuilds you can disable Every option with the `WITH_` prefix, except for WITH_PYTHON which is needed for the UI. This speeds up linking and gives a smaller binary.

Remove the files created by `make install` since they will become stale.
However, Blender still needs to be able to find its scripts or you will have a very minimalist looking interface.
This can be solved by linking the source directory to your Blender binary path so copying isn't needed and scripts are always up to date.

<source lang="bash">
rm -r ~/blender-git/build/bin/*.*
ln -s ~/blender-git/blender/release ~/blender-git/build/bin/
</source>

For convenient access, create this symlink to run blender from the source directory:
<source lang="bash">
 ln -s ~/blender-git/build/bin/blender ~/blender-git/blender/blender.bin
</source>
You can add any files created this way to `~/blender-git/blender/.git/info/exclude` so they won't count as untracked files for git. Add your local files here, instead of in .gitignore, because .gitignore is also committed and will affect everyone else.
</div>
    </div>
  </div>
</div>__NOEDITSECTION__

== See also ==

* [[Source/Code_Style/Configuration|CMake & IDE Configuration]]


== Troubleshooting ==

=== Non-Default Compiler (CUDA Compatibility) ===

<div id="compiler_version">
  <div class="card">
    <div class="card-header" id="heading_compiler_version">
        <div class="btn btn-link" data-toggle="collapse" data-target="#collapse_compiler_version" aria-expanded="true" aria-controls="collapse_compiler_version">Instructions</div>
    </div>

    <div id="collapse_compiler_version" class="collapse" aria-labelledby="heading_compiler_version" data-parent="#compiler_version">
      <div class="card-body">
At the time of writing NVCC-12 is not compatible with GCC-13, if your distribution uses a newer GCC, and you wish to use Cycles with CUDA, you will need to compile with GCC-12 as well.

Running `make` with the following arguments can be used to specify a different compiler version:

<source lang="bash">
make CC=gcc-12 CPP=g++-12 CXX=g++-12 LD=g++-12
</source>

</div>
    </div>
  </div>
</div>__NOEDITSECTION__
