= Resolving Build Failures =

Most building problems are not actually errors in Blender's source code, although you can never fully rule out that possibility. Below are common causes for failing to build:

== Missing Dependencies ==

On Windows or OS X we provide dependencies, so before troubleshooting further, make sure you updated your local "lib/" checkout.

Missing dependencies cause two types of compiler errors. No such file errors mean a header (.h) file is missing, while unresolved symbol errors when linking mean a library is missing. This is usually because either a path to the dependency was not set correctly in the build system, the dependency was not installed, or a wrong version of the dependency was used.

Finding out which dependencies are broken may sometimes be difficult. Searching online for the missing filenames or symbols will often give a quick answer. On systems with package managers the headers and libraries are usually in a separate development package, called for example foo-dev or foo-devel.

== Local Changes ==

Some complaints of Blender failing to build end up being caused by developers forgetting they have made changes to their code (patches applied or edits when developing).

Before spending too much time investigating an error building, check that your checkout has no local changes. You can stash those away (and restore later if desired) with this command:

<source lang="bash">
git stash
</source>

== Unsupported Environment ==

While Blender is portable, if you compile on a less common operating system, like NetBSD for example, it may need some minor edits to compile. The same goes for compilers, less common versions may need some adjustments if no active developers are currently using them.

Unless you want to spend time supporting less common development environments, normally it is best to use the default/standard development tools for your platform.

== Reporting Build Problems ==
* Use the [https://devtalk.blender.org/c/blender/building-blender Building Blender forum] or the [https://blender.chat/channel/blender-coders #blender-coders channel] on blender.chat to report your problem. If using chat, use an [http://www.pasteall.org external pasting tool] instead of pasting more than 5 lines directly in the channel.
* '''ALWAYS''' include a full error log, just saying "It failed" with 1-2 lines containing the error is not very helpful.<br>To create a full error log you will want to redirect output into a file, see: ([http://stackoverflow.com/a/13591423/432509 Linux/MacOS], [http://stackoverflow.com/a/1420981/432509 MS-Windows])
* Include the operating system, compiler version and git revision of Blender.