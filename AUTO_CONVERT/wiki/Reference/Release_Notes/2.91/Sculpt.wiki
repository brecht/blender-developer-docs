= Blender 2.91: Sculpting =

== Sculpting Workflow with multiple objects ==
* XYZ symmetry is now a per mesh settings which is shared between all painting and editing modes. [https://projects.blender.org/blender/blender/commit/5502517c3c1 5502517c3c]
* Fade Inactive Geometry Overlay that fades with the background color objects that are not editable in the current mode. This makes possible to identify which object is enabled for sculpting.  [https://projects.blender.org/blender/blender/commit/ea6cd1c8f05 ea6cd1c8f0]

== Viewport ==
* The HDRI rotation can be locked to the view in LookDev mode to use EEVEE materials as Matcaps [https://projects.blender.org/blender/blender/commit/5855f317a70 5855f317a7]
[[File:Eevee lock rotation.mp4|600px|thumb|center]]
* Sculpt Mode can now renders its overlays (Face Sets and Masks) in objects with constructive modifiers enabled. [https://projects.blender.org/blender/blender/commit/6c9ec1c893f 6c9ec1c893]

== Sculpt Gestures ==
New gesture tools to manipulate different data in the sculpt mesh.
* Box and Lasso Face Set create a new Face Set in the area affected by the gesture  [https://projects.blender.org/blender/blender/commit/c05715b03fe c05715b03f].
* Box and Lasso Trim cut the mesh using a boolean operation [https://projects.blender.org/blender/blender/commit/675c9644420 675c964442]. 
* Box mask its now its own operator, separated from selection [https://projects.blender.org/blender/blender/commit/6faa765af89 6faa765af8].
* Sculpt: Line Project Gesture tool [https://projects.blender.org/blender/blender/commit/e0bfd3968c6 e0bfd3968c].
* Sculpt: Line gestures and Mask Line tool [https://projects.blender.org/blender/blender/commit/8c81b3fb8b9 8c81b3fb8b].
* Sculpt: Union and Join mode for trim tools [https://projects.blender.org/blender/blender/commit/2b72860ff44 2b72860ff4].
* Sculpt: Use cursor depth in trimming gestures [https://projects.blender.org/blender/blender/commit/da7ace00d5f da7ace00d5].

== Cloth Sculpting ==
* The Cloth Brush and Filter now support collisions with scene colliders [https://projects.blender.org/blender/blender/commit/675700d9489 675700d948].
[[File:Cloth Collisions.mp4|600px|thumb|center]]
* Cloth Brush applies gravity in the entire simulated area [https://projects.blender.org/blender/blender/commit/54a2fcc0f33 54a2fcc0f3].
* Sculpt: Cloth Brush Soft Body Plasticity property [https://projects.blender.org/blender/blender/commit/ec9edd36ca1 ec9edd36ca].
[[File:Plasticity.mov|600px|thumb|center]]

* Sculpt: Enable persistent base for the cloth brush [https://projects.blender.org/blender/blender/commit/49c1359b556 49c1359b55].
* New Cloth Grab Brush that uses constraints [https://projects.blender.org/blender/blender/commit/2e33c5ca15c 2e33c5ca15].
* New Cloth Snake Hook Brush [https://projects.blender.org/blender/blender/commit/2e33c5ca15c 2e33c5ca15].
[[File:Cloth Snake Hook.mp4|600px|thumb|center]]

* Sculpt: Cloth Brush simulation area property [https://projects.blender.org/blender/blender/commit/d693d77fed9 d693d77fed].
* Sculpt: Cloth Simulation Dynamic area mode [https://projects.blender.org/blender/blender/commit/8ef353fa506 8ef353fa50].
[[File:Dynamic simulation area.mp4|600px|thumb|center]]
* Cloth brush Pin Simulation Boundary property [https://projects.blender.org/blender/blender/commit/bf65820782a bf65820782].
* Sculpt: Option to limit the forces axis in the Cloth Filter [https://projects.blender.org/blender/blender/commit/4814836120e 4814836120].
* Sculpt: Enable Cloth Simulation Target for Pose and Boundary [https://projects.blender.org/blender/blender/commit/c2f0522760b c2f0522760].
* Sculpt: Add orientation modes to the Cloth Filter [https://projects.blender.org/blender/blender/commit/b3bd121dd4c b3bd121dd4].
* Sculpt: Scale Cloth Filter [https://projects.blender.org/blender/blender/commit/5d728d38b94 5d728d38b9].

[[File:Simulation Target.mp4|600px|thumb|center]]

== Multires ==
* Multires: Base Mesh Sculpting [https://projects.blender.org/blender/blender/commit/976f0113e00 976f0113e0].

== Tools ==

=== Boundary Brush === 
This brush includes a set of deformation modes designed to deform and control the shape of the mesh boundaries, which are really hard to do with regular sculpt brushes (and even in edit mode). This is useful for creating cloth assets and hard surface base meshes. [https://projects.blender.org/blender/blender/commit/ed9c0464bae ed9c0464ba].
[[File:Boundary Brush.mp4|600px|thumb|center]]

The boundary brush supports different modes to apply the falloff which can be used to create repeating trim patterns [https://projects.blender.org/blender/blender/commit/c77bf952210 c77bf95221].
[[File:Boundary brush falloff.mp4|600px|thumb|center]]

=== Multires Displacement Eraser === 
* This brush deletes the displacement of the Multires Modifier and moves the vertices towards the base mesh limit surface. [https://projects.blender.org/blender/blender/commit/478ea4c8988 478ea4c898].

[[File:Displacement eraser.mp4|600px|thumb|center]]


=== Sculpt Filters ===
* Sculpt: Sharpen Mesh Filter curvature smoothing and intensify details [https://projects.blender.org/blender/blender/commit/3570173d0f5 3570173d0f].
* Sculpt: Sculpt Filter Orientation Options [https://projects.blender.org/blender/blender/commit/89a374cd964 89a374cd96].
* Sculpt: Enhance Details Mesh Filter [https://projects.blender.org/blender/blender/commit/0957189d4a3 0957189d4a].
* Sculpt: Erase Displacement Mesh Filter [https://projects.blender.org/blender/blender/commit/bedd6f90ca7 bedd6f90ca].

=== Other Tools === 
* Sculpt: Add global automasking settings support in filters [https://projects.blender.org/blender/blender/commit/6fe3521481b 6fe3521481].


* Sculpt: Invert Smooth to Enhance Details [https://projects.blender.org/blender/blender/commit/3e5431fdf43 3e5431fdf4].
[[File:Enhance details.mp4|600px|thumb|center]]

* Sculpt: Option to mask front faces only using Lasso and Box Mask [https://projects.blender.org/blender/blender/commit/af77bf1f0f9 af77bf1f0f].
* Sculpt: Option to lock the rotation in the Pose Brush scale deform mode [https://projects.blender.org/blender/blender/commit/9ea77f5232c 9ea77f5232].
* Sculpt: Face Set Extract Operator [https://projects.blender.org/blender/blender/commit/afb43b881cc afb43b881c].
* Edit Face Set is now a tool with all options exposed in the UI [https://projects.blender.org/blender/blender/commit/77e40708c2c 77e40708c2] [https://projects.blender.org/blender/blender/commit/48c0a82f7a3 48c0a82f7a].
* Sculpt: Experimental Pen Tilt Support [https://projects.blender.org/blender/blender/commit/0d5ec990a98 0d5ec990a9].