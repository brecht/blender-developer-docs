= Blender 3.0: Add-ons =

== New add-on key requirements ==
Blender wants to implement fair and uniform rules for all add-ons included in official releases. Therefore [[Process/Addons#Key_requirements_for_Add-ons|new requirements]] have been added. As a result various add-ons aren't bundled with Blender anymore or have been updated. 
* '''BlenderKit''' isn't distributed with Blender anymore but can still be downloaded from the BlenderKit website. ([https://projects.blender.org/blender/blender-addons/commit/13ecbb8fee64  rBA13ecbb8]) 
* '''Archipack''' isn't distributed with Blender anymore but can still be downloaded from the Archipack website ([https://projects.blender.org/blender/blender-addons/commit/d7517a6f2a69  rBAd7517a6])
* '''Magic UV''': The online updater was removed. ([https://projects.blender.org/blender/blender-addons/commit/3c5d373fc4da  rBA3c5d373])

== Collection Manager ==
=== New Features ===
* Added selection of cumulative objects (objects that are in more than one collection) to the specials menu in the CM popup. ([https://projects.blender.org/blender/blender-addons/commit/eba3a31f  rBAeba3a31])
* Added support for creating QCD slots by clicking on the unassigned slots on the 3D Viewport header widget (the little Xs).  Unassigned slots now have a tooltip with more information and hotkeys for doing things like creating a new slot and moving the selected objects to it. ([https://projects.blender.org/blender/blender-addons/commit/f583ecf0  rBAf583ecf])
* Added the option to create all missing QCD slots from the Quick View Toggles menu so you end up with a full 20. ([https://projects.blender.org/blender/blender-addons/commit/f583ecf0  rBAf583ecf])

=== Improved Features ===
* Updated the tooltip for the Collection Manager's move objects button with the title "Send Objects to Collection" (was "Set Object Collection"). ([https://projects.blender.org/blender/blender-addons/commit/69f875e1  rBA69f875e])
* Changed the icon for the move objects button, when no objects are present in the collection, to better suggest moving objects. ([https://projects.blender.org/blender/blender-addons/commit/69f875e1  rBA69f875e])
* Updated the invert icon in the filter arrow to use the standard double arrows icon. ([https://projects.blender.org/blender/blender-addons/commit/69f875e1  rBA69f875e])
* Changed the filter collections by selected objects icon to be consistent with the move objects button's selected objects icon. ([https://projects.blender.org/blender/blender-addons/commit/69f875e1  rBA69f875e])

=== Bug Fixes ===
* Fixed a bug where attempting to deselect nested collections fails when there are no objects present in the collection you clicked on (the parent collection).  ([https://projects.blender.org/blender/blender-addons/commit/11aebc01  rBA11aebc0])

== Rigify ==

* The Rigify Legacy Mode and its preference checkbox have been removed. ([https://projects.blender.org/blender/blender-addons/commit/7412d6c3e4  rBA7412d6c])
* A new modular face rig has been implemented. An upgrade operator button is provided similar to older legacy rig types; however for now the old face is merely deprecated and can still be used. ([http://developer.blender.org/T89808 #89808], [https://projects.blender.org/blender/blender-addons/commit/2acf22b5932  rBA2acf22b], [https://projects.blender.org/blender/blender-addons/commit/985f6d8c304  rBA985f6d8])
* Some Rigify operations have been added to a new menu to make them searchable. ([https://projects.blender.org/blender/blender-addons/commit/27fe7f3a4f  rBA27fe7f3])
* If the metarig contains a bone called 'root', it is now used as the root control to allow changing its rest position or assigning a custom widget. ([https://projects.blender.org/blender/blender-addons/commit/992b0592f14  rBA992b059])
* Rigify now refreshes all drivers in the file after generation to fix drivers that got stuck due to transient errors. ([https://projects.blender.org/blender/blender-addons/commit/e4d9cf84d00  rBAe4d9cf8])
* Rigify can now execute a custom python script datablock after generation to allow arbitrary post-generation customization without losing ability to re-generate. ([https://projects.blender.org/blender/blender-addons/commit/ecf30de46c  rBAecf30de])
* Rigify now uses mirrored linked duplicates for the widgets for the left and right side by default; can be disabled with an option. ([https://projects.blender.org/blender/blender-addons/commit/eed6d6cc132  rBAeed6d6c])
* The limb IK system has been changed to improve stability of non-pole mode and pole toggle precision, which slightly changes non-pole mode output. ([https://projects.blender.org/blender/blender-addons/commit/293cc140f5  rBA293cc14])

== FBX I/O ==

* Subsurface: Support for 'Boundary Smooth' option of the Blender modifier has been added to the FBX importer and exporter ([https://projects.blender.org/blender/blender-addons/commit/9a285d80167f  rBA9a285d8]).

== X3D/WRL I/O ==

* Basic support for material import has been added ([https://projects.blender.org/blender/blender-addons/commit/0573bc7daeb1  rBA0573bc7]).