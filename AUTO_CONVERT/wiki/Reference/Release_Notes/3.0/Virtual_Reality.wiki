= Blender 3.0: Virtual Reality =
== Controller Support ==
Blender 3.0 offers a brand new set of VR controller-based functionality, including the ability to visualize controllers (VR, regular 3D viewport) and the ability to navigate (teleport/fly/grab) one's way through a scene in VR using controller inputs. This functionality is available via the updated [https://docs.blender.org/manual/en/3.0/addons/3d_view/vr_scene_inspection.html VR Scene Inspection add-on].
[[File:Blender 3.0 virtual reality.mp4|800x430px|thumb|center|"Classroom" by Christophe Seux (https://www.blender.org/download/demo-files/)]]
[[File:Xr controller support.png|800x430px|thumb|center|"Race Spaceship" by Alessandro Chiffi / ONdata Studio (https://www.blender.org/download/demo-files/)]]
== Other Changes & Additions ==
* Session uses stage reference space (user-defined tracking bounds) instead of local reference space (position at application launch), when available ([https://projects.blender.org/blender/blender/commit/36c0649d32aa 36c0649d32]). 
* New "Absolute Tracking" session option which skips eye offsets that are normally added for placing users exactly at landmarks. This allows the tracking origin to be defined independently of the headset position ([https://projects.blender.org/blender/blender/commit/36c0649d32aa 36c0649d32]).
[[File:Xr absolute tracking.png|thumb|center]]
* New `Custom Object` type for VR landmarks (replaces old `Custom Camera`), which enables any object to be used as a base pose reference for the VR viewer. In addition, a viewer reference scale can now be set for landmarks of type `Custom Object` or `Custom Pose` ([https://projects.blender.org/blender/blender-addons/commit/823910c50d1c  rBA823910c]).
[[File:Xr landmark.png|thumb|center]]
* Higher color depth for VR-displayed images when supported by runtime ([https://projects.blender.org/blender/blender/commit/eb278f5e12cf eb278f5e12]).
* Support for Varjo quad-view and foveated headsets (Varjo VR-3, XR-3) ([https://projects.blender.org/blender/blender/commit/07c6af413617 07c6af4136]).
== Fixes ==
* Fixed pink screen issue on Windows when using the SteamVR runtime with AMD graphics cards ([https://projects.blender.org/blender/blender/commit/82ab2c167844 82ab2c1678]).
* Fixed render artifacts (geometry occluded for one eye) when using VR with EEVEE and viewport denoising ([https://projects.blender.org/blender/blender/commit/c41b93bda532 c41b93bda5]).