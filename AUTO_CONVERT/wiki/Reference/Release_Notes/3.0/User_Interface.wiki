= Blender 3.0: User Interface =

== Theme and Widgets ==

* To celebrate the beginning of a new series, the default theme got a refresh. ([https://projects.blender.org/blender/blender/commit/bfec984cf82a6 bfec984cf8])
* Visual style update to panels. ([https://projects.blender.org/blender/blender/commit/93544b641bd6 93544b641b])
** New theme setting for roundness.
** Added margins to more clearly tell them apart.
** Replace triangle with chevron icon.
* ''Menu Item'' now makes use of ''Roundness'' theme setting. ([https://projects.blender.org/blender/blender/commit/518365395152 5183653951])
* ''List Item'' no longer relies on ''Regular'' buttons style when selected. ([https://projects.blender.org/blender/blender/commit/9e71a075473 9e71a07547])
* Active ''Tab'' text now use region's ''Text Highlight'' theme color when active. ([https://projects.blender.org/blender/blender/commit/af26720b2131 af26720b21])
* Adjust editor's Header color when active (instead of when inactive). ([https://projects.blender.org/blender/blender/commit/962b17b3ca14 962b17b3ca])
* Style drag-drop indicators as tooltips. ([https://projects.blender.org/blender/blender/commit/499dbb626acb 499dbb626a]) 
* Use arrow icon on context paths. ([https://projects.blender.org/blender/blender/commit/7c755293330e 7c75529333]) 
* Improve contrast on playhead. ([https://projects.blender.org/blender/blender/commit/452c78757f44 452c78757f])
* In Animation Editors and VSE, align vertical indicators to view. ([https://projects.blender.org/blender/blender/commit/3ccdee75328b 3ccdee7532]) 
* Use flat colors for NLA strips. ([https://projects.blender.org/blender/blender/commit/4758a7d35750 4758a7d357])
* Remove separator lines between rows in VSE. ([https://projects.blender.org/blender/blender/commit/b4af70563ff1 b4af70563f])

== Area Management ==
* Corner action zones allow joining any neighbors. Improved Header Context Menu. ([https://projects.blender.org/blender/blender/commit/8b049e4c2a53 8b049e4c2a], [https://projects.blender.org/blender/blender/commit/c76141e425aa c76141e425]).
[[File:AreaJoins.gif|500px|thumb|center]]
* New 'Area Close' operator. ([https://projects.blender.org/blender/blender/commit/8b049e4c2a53 8b049e4c2a], [https://projects.blender.org/blender/blender/commit/06e62adfb8f2 06e62adfb8]).
[[File:CloseOperator.png|500px|thumb|center]]
* Mouse hit size for area resizing increased ([https://projects.blender.org/blender/blender/commit/84dcf12ceb7f 84dcf12ceb]).
[[File:BorderHitSize.png|500px|thumb|center]]
* Improved mouse cursor feedback during Area Split and Join at unsupported locations. ([https://projects.blender.org/blender/blender/commit/3b1a16833b58 3b1a16833b]).
* Resizing areas now snap (with Ctrl) to more consistent locations. ([https://projects.blender.org/blender/blender/commit/e5ed9991eaf0 e5ed9991ea]).

== General ==
* The editor header and tool settings region were swapped ([https://projects.blender.org/blender/blender/commit/4cf4bb2664ebe 4cf4bb2664]).
* If saving a file using CloseSave dialog, do not close if there is an error doing so, like if read-only. ([https://projects.blender.org/blender/blender/commit/cfa20ff03bde cfa20ff03b]).
* Capture object thumbnails at an oblique angle for better preview of the shapes.  ([https://projects.blender.org/blender/blender/commit/ecc7a837982e ecc7a83798]).
[[File:PreviewOrientation.png|500px|thumb|center]]
* Do not create thumbnail previews of offline files, to avoid slow listings of Windows OneDrive folders.  ([https://projects.blender.org/blender/blender/commit/ee5ad46a0ead ee5ad46a0e]).
[[File:OfflineFiles.png|500px|thumb|center]]
* Fix console window briefly flashing when blender starts on windows.  ([https://projects.blender.org/blender/blender/commit/f3944cf50396 f3944cf503]).
* Improved positioning of menu mnemonic underlines, especially when using custom font.  ([https://projects.blender.org/blender/blender/commit/0fcc063fd99c 0fcc063fd9], [https://projects.blender.org/blender/blender/commit/aee04d496035 aee04d4960]).
* "Render" Window now top-level (not child) on Windows platform, back to behavior prior to 2.93.  ([https://projects.blender.org/blender/blender/commit/bd87ba90e639 bd87ba90e6]).
* Improved placement of child windows when using multiple monitors with any above any others (Win32).  ([https://projects.blender.org/blender/blender/commit/d75e45d10cbf d75e45d10c]).
* Windows users can now associate current installation with Blend files in Preferences.  ([https://projects.blender.org/blender/blender/commit/bcff0ef9cabc bcff0ef9ca]).
[[File:Blend File Association.png|500px|thumb|center]]
* 3DView Statistics Overlay now shows Local statistics if you are in Local View  ([https://projects.blender.org/blender/blender/commit/c8e331f45003 c8e331f450]).
* Improved scaling of File Browser preview images. ([https://projects.blender.org/blender/blender/commit/cb548329eaea cb548329ea]).
[[File:Scaling2.png|700px|thumb|center]]
* Do not resize temporary windows (Preferences, Render, etc) if they are already open. ([https://projects.blender.org/blender/blender/commit/643720f8abdb 643720f8ab]).
* Win32: Improved placement of windows when using multiple monitors that differ in DPI and scale. ([https://projects.blender.org/blender/blender/commit/999f1f75045c 999f1f7504]).
* macOS: support for Japanese, Chinese and Korean input in text fields ([https://projects.blender.org/blender/blender/commit/83e2f8c 83e2f8c], [https://projects.blender.org/blender/blender/commit/0ef794b 0ef794b])
* Ctrl+F while hovering UI lists will open the search for the list ([https://projects.blender.org/blender/blender/commit/87c1c8112fa4  rB87c1c811]).
* Text buttons are automatically scrolled into view when editing them ([https://projects.blender.org/blender/blender/commit/0c83ef567c50  rB0c83ef56]).
* Show descriptive display names for Fonts in File Browser instead of simple file names. ([https://projects.blender.org/blender/blender/commit/8aa1c0a326a8 8aa1c0a326]).
[[File:FontDisplayNames.png|500px|thumb|center]]
* Show descriptive display names for 3D Text Object fonts.  ([https://projects.blender.org/blender/blender/commit/b5bfb5f34c12 b5bfb5f34c]).
* On Windows when entering Chinese or Japanese characters, do not duplicate initial keystroke. ([https://projects.blender.org/blender/blender/commit/836aeebf7077 836aeebf70]).
* Allow use of Wingdings and Symbol fonts in VSE text strips. ([https://projects.blender.org/blender/blender/commit/ae920d789ed3 ae920d789e]).
* Substantial speed increases for interface text drawing. ([https://projects.blender.org/blender/blender/commit/d5261e973b56 d5261e973b], [https://projects.blender.org/blender/blender/commit/0d7aab2375e6 0d7aab2375])
* Less jiggling of contents when moving nodes. ([https://projects.blender.org/blender/blender/commit/400605c3a6a8 400605c3a6]).
* Increased resolution of blend file thumbnails. ([https://projects.blender.org/blender/blender/commit/bf0ac711fde2 bf0ac711fd]).
[[File:BlendThumbnailSizeComparison.png|500px|thumb|center]]
* Improved blend preview thumbs, using screen captures or rendered with current current view shading. ([https://projects.blender.org/blender/blender/commit/58632a7f3c0f 58632a7f3c], [https://projects.blender.org/blender/blender/commit/4fa0bbb5ac3d 4fa0bbb5ac]).
[[File:BlendPreviews.png|500px|thumb|center]]
* Split Output Properties Dimensions panel. ([https://projects.blender.org/blender/blender/commit/4ddad5a7ee5 4ddad5a7ee])
* File Browser "Favorites" section heading changed back to "Bookmarks" ([https://projects.blender.org/blender/blender/commit/a79c33e8f8f3 a79c33e8f8])

== Custom Properties ==
* Rework the custom property edit operator to make it faster to use and more intuitive ([https://projects.blender.org/blender/blender/commit/bf948b2cef3ba3 bf948b2cef]).
[[File:Reworked Custom Property Edit Operator 2.png|500px|thumb|center|Editing metadata from an array property with the new edit custom property operator UI.]]
* Improve the layout of custom property edit panel [https://projects.blender.org/blender/blender/commit/972677b25e1d84 972677b25e]

== 3D Viewport == 
* Display indicator in ''Text Info'' overlay when ''Clipping Region'' is enabled ([https://projects.blender.org/blender/blender/commit/9c509a721949 9c509a7219])
* Z axis lock option for the walk navigation ([https://projects.blender.org/blender/blender/commit/c749c24682159 c749c24682])
* Navigation gizmos no longer hide when using modal operators ([https://projects.blender.org/blender/blender/commit/917a972b56af10 917a972b56]).
* The different outline color for selected instances has been removed, improving readability of the active object ([https://projects.blender.org/blender/blender/commit/aa13c4b386b131 aa13c4b386]).

== Keymap ==

* Holding the `D` & dragging now opens the view menu (moved from the `Tilde` key). The "Switch to Object" operator is now mapped to the `Tilde` key. ([https://projects.blender.org/blender/blender/commit/f92f5d1ac62c66ceb7a6ac1ff69084fbd5e3614a f92f5d1ac6]).

== Outliner ==

* Filter option "All View Layers" shows all the view layers for comparing different collection values ([https://projects.blender.org/blender/blender/commit/bb2648ebf002 bb2648ebf0]).

[[File:Outliner view layers.png|500px|thumb|center]]

== Freestyle ==

A major rework of the UI layout to make the layout align with the current UI design principles ([https://projects.blender.org/blender/blender/commit/6f52ebba192 6f52ebba19]).

== Preview ==

* Sphere material preview updated to have squared UV and better poles ([https://projects.blender.org/blender/blender/commit/875f24352a76 875f24352a]).

[[File:Sphere preview - before.png|center|500px|thumb|Before]]
[[File:Sphere preview - after.png|center|500px||thumb|After]]