= Blender 2.93: EEVEE =

== Depth of field ==

EEVEE's implementation of depth of field has been rewritten from scratch to improve performance and quality. It also features a new high quality mode that matches the raytraced ground truth if using a high enough sample count. ([https://projects.blender.org/blender/blender/commit/000a340afa67 000a340afa])

<center>
{| class="transparent" style="text-align: center"

 |}
</center>

<center>
{| class="transparent" style="text-align: center"
 |colspan="2"|[[File:Depth-of-field.jpg|center|600px|thumb]]
 |-
 |valign=top|[[File:Eevee-dof-kitchen-old.jpeg|298px|center|thumb|Previous implementation]]
 |valign=top|[[File:Eevee-dof-kitchen-new.jpeg|298px|center|thumb|New depth of field]]
 |}
</center>

== Volumetrics ==
A few improvements have been made to the volumetric lighting. It is now easier to work with and has less limitations.

<center>
{| class="transparent" style="text-align: center"
 |valign=top|[[File:1_EEVEE_293_previous_implementation.png|298px|center|thumb|Previous implementation]]
 |valign=top|[[File:2_EEVEE_293_fixed_shadowing.png|298px|center|thumb|New (bug-fixed) volumetric shadowing]]
 |-
 |valign=top|[[File:3_EEVEE_293_area_light.png|298px|center|thumb|Area Light Support]]
 |valign=top|[[File:4_EEVEE_293_soft_shadows.png|298px|center|thumb|Soft Shadows Support]]
 |}
</center>


* Added a new Volume and Diffuse slider light contribution. This addition was motivated by the current lack of support of light node-tree and visibility flag. ([https://projects.blender.org/blender/blender/commit/884f934a853f 884f934a85])
* Area lights are now correctly supported inside volumetric lighting. The solution is not physically based and does not match cycles perfectly. ([https://projects.blender.org/blender/blender/commit/355f884b2f09 355f884b2f])
* Volumetric Lights Shadowing had bugs fixed that may change the look of certain scenes. ([https://projects.blender.org/blender/blender/commit/3a29c19b2bff 3a29c19b2b] [https://projects.blender.org/blender/blender/commit/54f52cac7ccd 54f52cac7c])
* Volume lighting now uses a softer attenuation function, reducing flickering of lights center. ([https://projects.blender.org/blender/blender/commit/884f934a853f 884f934a85])
* Volume light clamp was changed to fit the new volumetric lighting. ([https://projects.blender.org/blender/blender/commit/b96acd0663b5 b96acd0663])
* Volume shadowing will now be soft if using the soft shadow option in the render property panel. Light needs to have shadows enabled for this to work. ([https://projects.blender.org/blender/blender/commit/89ef0da5513a 89ef0da551])

== Ambient Occlusion ==
A complete rewrite was done to fix over-darkening artifacts and precision issues. A better specular occlusion has been implemented to reduce light leaking from light-probes. ([https://projects.blender.org/blender/blender/commit/64d96f68d6ef 64d96f68d6])

Thanks to this, the ambient occlusion node now supports variable distance and an approximation of the inverted option. ([https://projects.blender.org/blender/blender/commit/dee94afd039d dee94afd03])

[[File:EEVEE_293_ao.png|thumb|center|400px|right|left|Inverted AO node with textured distance input]]

== Improvements ==
* Glass BSDF have been updated and have less visible interpolation artifacts when IOR is less than 1. ([https://projects.blender.org/blender/blender/commit/83ac8628c490 83ac8628c4])
* Fresnel effect of glossy surfaces now follows Cycles more closely. ([https://projects.blender.org/blender/blender/commit/06492fd61984 06492fd619])
* Reflection Cubemaps probes have been improved for low roughness. Old scene now require a rebake. ([https://projects.blender.org/blender/blender/commit/aaf1650b0993 aaf1650b09])
* Normal Maps and smoothed normals do not produce strange reflections when reflection is below the surface. ([https://projects.blender.org/blender/blender/commit/1c22b551d0c1 1c22b551d0])
* Sub-Surface Scattering do not leak light onto nearby surfaces. ([https://projects.blender.org/blender/blender/commit/cd9a6a0f9389 cd9a6a0f93])
* Screen-Space Raytracing (Reflections, Refraction, Shadows) have been improved to reduce the amount of noise and convergence time. ([https://projects.blender.org/blender/blender/commit/267a9e14f5a7 267a9e14f5] [https://projects.blender.org/blender/blender/commit/bbc5e2605199 bbc5e26051] [https://projects.blender.org/blender/blender/commit/b79f20904170 b79f209041])
* Contact shadow distance fading was removed for performance and reliability reasons. ([https://projects.blender.org/blender/blender/commit/6842c549bb3f 6842c549bb])
* Faster animation rendering by reusing scene data between frames. ([https://projects.blender.org/blender/blender/commit/50782df42 50782df42])