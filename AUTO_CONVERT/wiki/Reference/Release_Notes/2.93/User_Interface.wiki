= Blender 2.93: User Interface =

== Outliner ==

* The render visibility toggles are now shown by default. ([https://projects.blender.org/blender/blender/commit/510db9512fc6 510db9512f])
* When showing folded item, more sub-items types are gathered as a single icon + counter (all bones, bone groups, and vertex groups, [https://projects.blender.org/blender/blender/commit/92dfc8f2673e  rB92dfc8f2]).

== File Browser ==

* "Frame Selected" operator to scroll to the selected files ([https://projects.blender.org/blender/blender/commit/5a67407d5aa880 5a67407d5a]). This has shortcut `Numpad .` in Blender's default keymap, and `F` in the Industry Compatible keymap.

== Window Management ==
==== All Platforms ====
* Temporary windows play nicer with others. Opening a new window can no longer take over the space of a different window. ([https://projects.blender.org/blender/blender/commit/492e64c7bcbd 492e64c7bc]).
* Window / "New Window" now opens with a simpler layout, with just a single editor (the largest of the parent). ([https://projects.blender.org/blender/blender/commit/be9842f65b85 be9842f65b]).
==== Windows Platform ====
* Child windows are shown on top of parent windows. You can now work with overlapping and floating windows. ([https://projects.blender.org/blender/blender/commit/261fa052ac42 261fa052ac]).
* Improved positioning of windows on multiple monitors. ([https://projects.blender.org/blender/blender/commit/d447bd3e4a9a d447bd3e4a]) ([https://projects.blender.org/blender/blender/commit/97fabc3c1c92 97fabc3c1c]).
* Improvements to Full-screen support ([https://projects.blender.org/blender/blender/commit/12193035ed12 12193035ed]) ([https://projects.blender.org/blender/blender/commit/9b87d3f02962 9b87d3f029]).

== General Changes ==

* Transform arrow cursor improvements in shape and in response to line width changes. ([https://projects.blender.org/blender/blender/commit/b4b02eb4ff77 b4b02eb4ff]).
[[File:TransformArrowComparison.png|500px|thumb|center]]
* Navigation Gizmo changes. Better indication of negative axes, consistent use of color and size to indicate orientation, ability to be resized. ([https://projects.blender.org/blender/blender/commit/ded9484925ed ded9484925]).
[[File:NavigateGizmoHighlight.png|500px|thumb|center]]
* Position Gizmo tooltips below their bounds so as to not obscure themselves. ([https://projects.blender.org/blender/blender/commit/cf6d17a6aa42 cf6d17a6aa]).
[[File:GizmoTooltips.png|500px|thumb|center]]
* Use ellipsis character when truncating strings no matter how narrow. ([https://projects.blender.org/blender/blender/commit/de3f369b30e5 de3f369b30]).
* Windows OS - Allow any input language, regardless of translated display language. ([https://projects.blender.org/blender/blender/commit/f2781e1c7c82 f2781e1c7c]).
* Improved contrast for Status Bar report messages.  ([https://projects.blender.org/blender/blender/commit/694bc4d040a8 694bc4d040]).
[[File:Status Bar Reports.png|500px|thumb|center]]
* WM: Add 'Confirm On Release' option for `WM_OT_radial_control` ([https://projects.blender.org/blender/blender/commit/631cc5d56ee9445ece0a9982ed58b1de1dcbbe5b 631cc5d56e]).
* Update layouts for graph editor and NLA editor FModifiers ([https://projects.blender.org/blender/blender/commit/1f5647c07d15d2 1f5647c07d]).
* Blender thumbnails now saved without "Camera View"-style Passepartout. ([https://projects.blender.org/blender/blender/commit/9c395d6275a0 9c395d6275]).
* Virtual node sockets (unconnected node group sockets) are now more visible ([https://projects.blender.org/blender/blender/commit/b54b56fcbea62b b54b56fcbe]).
* Add Add Copy Full Data Path to RMB menu ([https://projects.blender.org/blender/blender/commit/85623f6a5590 85623f6a55]).
* Dynamically increase width of data-block and search-menu name buttons for long names where there is enough space ([https://projects.blender.org/blender/blender/commit/2dd040a34953 2dd040a349]).
* Allow translation of strings even when in 'en_US' locale, useful for non-English add-ons ([https://projects.blender.org/blender/blender/commit/7a05ebf84b35 7a05ebf84b]).