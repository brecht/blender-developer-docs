
* Image: Flip image operator [https://projects.blender.org/blender/blender/commit/7d874b03433 7d874b0343].
* Sculpt: Expand Operator [https://projects.blender.org/blender/blender/commit/82e70324772 82e7032477].
* Sculpt: Mask Init operator [https://projects.blender.org/blender/blender/commit/74052a9f02a 74052a9f02].
* Sculpt: Init Face Sets by Face Sets boundaries [https://projects.blender.org/blender/blender/commit/e5c1e13ef09 e5c1e13ef0].
* The Transfer Mode operator allows to switch objects while in Sculpt Mode by pressing the D key  [https://projects.blender.org/blender/blender/commit/86915d04ee38 86915d04ee]