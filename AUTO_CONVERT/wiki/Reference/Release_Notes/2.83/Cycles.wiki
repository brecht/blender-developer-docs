= Blender 2.83: Cycles =

== Adaptive Sampling ==

With adaptive sampling Cycles automatically reduces the number of samples in areas that have little noise, for faster rendering and more even noise distribution. For example hair on a character may need many samples, but the background may need very few.

It is enabled with the Adaptive Sampling setting in the Sampling panel. By default, the threshold to stop sampling pixels is adapted to the number of AA samples. This reduces overall render time (by 10-30% in our test scenes), and particularly after denoising the result will be almost indistinguishable.

Adaptive sampling also makes it possible to render images with a target amount of noise. This is done by settings the Noise Threshold, typical values are in the range from 0.1 to 0.001. Then render samples can then be set to a high value, and the renderer will automatically choose the appropriate amount of samples.

=== Settings ===

* ''Noise Threshold'': The error threshold to decide whether to continue sampling a pixel or not. Typical values are in the range from 0.1 to 0.001, with lower values meaning less noise. Setting it to exactly 0 lets Cycles guess an automatic value for it based on the total sample count.
* ''Min Samples'': The minimum number of samples a pixel receives before adaptive sampling kicks in. When set to 0 (default), it is automatically set to the square root of the total (max) sample count.

=== GPU Rendering ===

For GPU rendering, adaptive sampling sometimes needs a bigger tile size for good performance. For combined CPU + GPU rendering, tiles rendered on the CPU and GPU may not match exactly. We will investigate automatic solutions to these problems for future releases.

== OptiX Viewport Denoising ==

Cycles now supports denoising inside the Blender viewport with the AI-Accelerated Denoiser from OptiX, in addition to final render denoising. This allows to very quickly see the rendered scene at its full visual fidelity without noise.

To enable, select the "OptiX AI-Accelerated" value in the new "Viewport Denoising" dropdown found in the render settings for Cycles.

In addition, denoising with OptiX can now be used with any render device selected (including the CPU), as long as the system has at least one compatible OptiX device. Denoising will then be executed on the first OptiX compatible GPU, while rendering runs on all the selected render devices simultaneousely.

To use this feature you need a NVIDIA RTX GPU and at least driver 441.87 (Windows) or 440.59 (Linux).

== Shader Nodes ==

* Wave Texture Node:
** Direction mode X, Y and Z to align with axes rather than diagonal or spherical as previously. X is the new default, existing files will use diagonal or spherical for compatibility.
** Phase offset to offset the wave along its direction, for purposes like animation and distortion.
* Noise and Wave Texture Node:
** Add roughness input, to blend between a smoother noise pattern, and rougher with sharper peaks.

[[File:Blender2.83_noise_roughness.mp4|600px|thumb|center|New roughness input]]

* Vector Math Node: Sin, Cos, Tan and Wrap functions.
* Math Nodes: adaptive socket labels based on operation.
* White Noise Node: new color output.
* New Vector Rotate Node: rotate a vector around a center point using either Axis-Angle, Single Axis or Euler rotation.

== Volume Rendering ==

Along with the new [[Reference/Release_Notes/2.83/Volumes|volume object]], Cycles settings for rendering volumes were changed. Volume step size is now automatically estimated based on voxel size, and can be manually set per object. Existing files may need to be modified.

Higher volume step size renders faster but loses detail and can render with more noise. Lower volume step size can capture more detail but renders slower.

* Render and Viewport Step Rate: can be increased for faster rendering. This step rate affects all objects in the scene.
* Object and World Step Size: manually set the step size rather than using the automatic estimate.
* Material Step Rate: lowered to capture detail added by shaders, for example with procedural or point density textures.

== More Features ==

* Open Shading Language was upgraded to version 1.10.9.
* Improved denoising passes for specular BSDFs.
* Subsurface scattering and diffuse transmission are now part of the diffuse render pass, to simplify compositing and baking.
* New "Progressive Multi-Jitter" sampling pattern, used for adaptive sampling.