= Blender 2.80: More Features =

== Units ==

[[File:Units panel.png|thumb|Units Panel]]

A primary unit can be selected for length, mass and time. This unit will be used everywhere to:
* Display values of that type in the user interface
* Parse unitless user input

The old behavior can be restored on a per unit basis by using the "Adaptive" mode, where a unit is adaptively chosen depending to match the value.
When "Separate Units" is activated, the primary unit is always the first of the displayed units.

== Video Sequencer ==

* Sequencer cache has been rewritten. Users can control what gets stored and what doesn't, so cache is as effective as possible. Cache usage can be visualized directly on timeline ([https://projects.blender.org/blender/blender/commit/337cac760ba9 337cac760b])
* Text strips support for selecting a font. ([https://projects.blender.org/blender/blender/commit/b3dbe17658fe b3dbe17658])
* Add many more blend modes and a new Color Mix strip. ([https://projects.blender.org/blender/blender/commit/4b4702ab8 4b4702ab8])

== Physics ==

=== Cloth ===

Angular bending springs were implemented and are now the default. Angular springs are generally much more realistic, but can cause some instability with extremely high bending stiffness at low sampling rates. The linear bending model is still available and is the default when opening existing simulations. ([https://projects.blender.org/blender/blender/commit/b6f0f8a5b5a b6f0f8a5b5])

Structural cloth forces are now separated into components, allowing individual stiffness and damping control over tension, compression, and shearing (in addition to bending). N-gons are now properly supported, having shearing springs added to them, preventing them from collapsing. ([https://projects.blender.org/blender/blender/commit/e3d31b8dfbd e3d31b8dfb])

==== Collisions ====

Cloth collisions underwent a major overhaul to improve reliability and stability ([https://projects.blender.org/blender/blender/commit/0666ece2e2f9 0666ece2e2]).

* More accurate self-collisions.
* Optimizations for multicore CPUs.
* Single-sided collision support, to prevent e.g. cloth getting stuck inside a body mesh.
* Support for different fricition per collider object.
* Simultaneous collisions and impulse clamping for more reliable collision.

=== Effectors ===

* Effectors can now have a infinite line shape, in addition to points, infinite planes and meshes. ([https://projects.blender.org/blender/blender/commit/969cbed 969cbed])

=== Rigid Bodies ===

* The Generic Spring constraint now uses the newer Bullet spring implementation with better damping behavior, but the old version is available as an option. ([https://projects.blender.org/blender/blender/commit/26a283d 26a283d], [https://projects.blender.org/blender/blender/commit/ec64051 ec64051])

== Image Objects ==

The new image objects replace the old background images in Blender, to be organized and transformed as part of the scene.

These can be added from the Add object menu, or by drag and drop into the 3D viewport.

== Drop .blend files ==

When dropping a .blend in Blender there is now the choice to load the file, append something from it or link to it.

== Datablocks ==

* Copy/paste of any kind of data-block is now possible in the outliner.
* Delete data-blocks (including purging orphaned data) can now be undone.
* Deleting many data-blocks from the outliner is now much faster.
* Scene duplication options 'Linked Objects' and 'Linked Object Data' have been removed, instead collections can be used for easy linking of objects across scenes. A new 'Linked Collections' option was added which creates a full shallow copy of the scene.

== Audio ==

[https://github.com/audaspace/audaspace Audaspace] was updated to the latest version, which includes many stability and performance improvements. Different OpenAL devices can now be chosen in the user preferences.

New features in the [http://audaspace.github.io/bindings/index.html Audaspace Python API] include:
* Play self generated sounds using numpy arrays.
* For games: Sound list, random sounds and dynamic music.
* Writing sounds to files.
* Sequencing API.
* Opening sound devices, eg. Jack.

== Sculpting ==

* Topology Rake aligns edges along the brush direction while painting. This helps to generate cleaner topology and define sharp features with dynamic topology. Best used on relatively low-poly meshes, it is not needed as much for high detail areas and has a performance impact. ([https://projects.blender.org/blender/blender/commit/b592e34 b592e34])
* Manual detail mode for dynamic topology. In this mode mesh detail does not change on each stroke, only when using flood fill. ([https://projects.blender.org/blender/blender/commit/2203b04 2203b04])

== Weight Paint ==

* A new viewport shader option to display contours formed by points with the same interpolated weight. ([https://projects.blender.org/blender/blender/commit/ba3ef44a ba3ef44a])

== Compositor ==

* The RGB Curves now supports Film Like curve mapping, used for adjusting saturation and contrast, while avoiding color shifts. It does this by balancing the color channels to preserve the hue. ([https://projects.blender.org/blender/blender/commit/4de7c0c31 4de7c0c31])

== Preferences ==

* Added two simple options that control the tablet pressure response curve globally within Blender ([https://projects.blender.org/blender/blender/commit/539b465b 539b465b]).