= Blender 3.2 Release Notes =

Blender 3.2 was released on June 8, 2022.

Check out the final [https://www.blender.org/download/releases/3-2/ release notes on blender.org].

== [[Reference/Release Notes/3.2/Animation_Rigging|Animation & Rigging]] ==

== [[Reference/Release Notes/3.2/Core|Core]] ==

== [[Reference/Release Notes/3.2/EEVEE|EEVEE & Viewport]] ==

== [[Reference/Release Notes/3.2/Grease_Pencil|Grease Pencil]] ==

== [[Reference/Release Notes/3.2/Modeling|Modeling]] ==

== [[Reference/Release Notes/3.2/Nodes_Physics|Nodes & Physics]] ==

== [[Reference/Release Notes/3.2/Pipeline_Assets_IO|Pipeline, Assets & I/O]] ==

== [[Reference/Release Notes/3.2/Platforms|Platform-Specific Changes]] ==

== [[Reference/Release Notes/3.2/Python_API|Python API & Text Editor]] ==

== [[Reference/Release Notes/3.2/Cycles|Render & Cycles]] ==

== [[Reference/Release Notes/3.2/Sculpt|Sculpt, Paint, Texture]] ==

== [[Reference/Release Notes/3.2/User_Interface|User Interface]] ==

== [[Reference/Release Notes/3.2/VFX|VFX & Video]] ==

== [[Reference/Release Notes/3.2/Virtual_Reality|Virtual Reality]] ==

== [[Reference/Release Notes/3.2/Add-ons|Add-ons]] ==

== [[Reference/Release Notes/3.2/More_Features|More Features]] ==

== Compatibility ==

=== Proxy Removal ===
The proxy system has been fully removed, see [[Reference/Release_Notes/3.2/Core|the Core release notes]] for details.

=== Legacy Geometry Nodes ===
Geometry nodes from the pre-3.0 named attribute system have been removed. See[[Reference/Release_Notes/3.2/Nodes_Physics | the geometry nodes release notes]] for details.

== [[Reference/Release Notes/3.2/Corrective_Releases|Corrective Releases]] ==