= Cycles =

== Light and Shadow Linking ==

With light linking, lights can be set to affect only specific objects in the scene. Shadow linking additionally gives control over which objects acts as shadow blockers for a light. ([https://projects.blender.org/blender/blender/commit/ba3f26fac58eb3a37b2cfac38b30d1fd6d1a4094 ba3f26fac5])

This adds more artistic control for lighting by breaking the laws of physics. For example the environment and characters in a shot might have different light setups. A character could have a dedicated linked rim light to make it stand out, and shadow linking could be used to ensure no objects from the environment block it.

See the [https://docs.blender.org/manual/en/4.0/render/cycles/object_settings/light_linking.html Light Linking in the user manual] for more details.

<center>
{| class="transparent" style="text-align: center"
|+  style="caption-side: bottom" | Light linked to give only the monkey object rim shading. On the right is the scene without light linking, with the rim light off and on.
|rowspan="2"|[[File:Cycles4.0-monkey-link-on.png|535px|center]]
|[[File:Cycles4.0-monkey-link-none.png|265px|center]]
|-
|[[File:Cycles4.0-monkey-link-all.png|265px|center]]
|}
</center>

In a future version we plan to support linking to the world lighting, and add a more convenient user interface to view and manage all light links in a scene.

=== Examples ===

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Different light per car part, to finely control shape of reflections. Left is all lights, right with light linking.<br/>Scene by Fernando Alcala using asset by LRosario.
 |valign=top|[[File:Cycles4.0-car-link-off.jpeg|400px|center|link=https://wiki.blender.org/w/images/4/46/Cycles4.0-car-link-off.jpeg]]
 |valign=top|[[File:Cycles4.0-car-link-on.jpeg|400px|center|link=https://wiki.blender.org/w/images/1/11/Cycles4.0-car-link-on.jpeg]]
 |}
</center>

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Light linking used to artificially eliminate reflections on the eyes and tree branch.<br/>Scene by Alaska Young and Daniel Bystedt.
 |valign=top|[[File:Cycles4.0-creature-link-off.jpeg|400px|center|link=https://wiki.blender.org/w/images/4/41/Cycles4.0-creature-link-off.jpeg]]
 |valign=top|[[File:Cycles4.0-creature-link-on.jpeg|400px|center|link=https://wiki.blender.org/w/images/5/55/Cycles4.0-creature-link-on.jpeg]]
 |}
</center>

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Distinct lights for character, background and volume.<br/>Scene by Fernando Alcala using Blendswap asset by adi.
 |valign=top|[[File:Cycles4.0-char-link-char.jpeg|200px|center|link=https://wiki.blender.org/w/images/c/c3/Cycles4.0-char-link-char.jpeg]]
 |valign=top|[[File:Cycles4.0-char-link-floor.jpeg|200px|center|link=https://wiki.blender.org/w/images/4/41/Cycles4.0-char-link-floor.jpeg]]
 |valign=top|[[File:Cycles4.0-char-link-volume.jpeg|200px|center|link=https://wiki.blender.org/w/images/d/d0/Cycles4.0-char-link-volume.jpeg]]
 |valign=top|[[File:Cycles4.0-char-link-on.jpeg|200px|center|link=https://wiki.blender.org/w/images/0/03/Cycles4.0-char-link-on.jpeg]]
 |}
</center>

== Path Guiding ==

Path Guiding now works on glossy surfaces in addition to diffuse surfaces. This can significantly reduce noise on glossy surfaces, and find otherwise missing glossy light paths. ([https://projects.blender.org/blender/blender/pulls/107782 PR #107782])

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Diffuse only path guiding, and new glossy path guiding finding additional light paths. Same render time.<br/>Scene by Fernando Alcala and LRosario, made using assets from Blendswap.
 |valign=top|[[File:Cycles4.0-path-guiding-diffuse.jpg|400px|center|link=https://wiki.blender.org/w/images/8/83/Cycles4.0-path-guiding-diffuse.jpg]]
 |valign=top|[[File:Cycles4.0-path-guiding-mis.jpg|400px|center|link=https://wiki.blender.org/w/images/e/e0/Cycles4.0-path-guiding-mis.jpg]]
 |}
</center>

== Multiple Scattering GGX ==

The Multiscattering GGX implementation was replaced by an approximation based on [https://blog.selfshadow.com/publications/turquin/ms_comp_final.pdf "Practical multiple scattering compensation for microfacet models" by Emmanuel Turquin]. ([https://projects.blender.org/blender/blender/commit/888bdc1419a2cd99284060a64d1df0c4397d3ac5 888bdc1419])

* This removes the performance and noise disadvantage that used to be associated with Multiscattering GGX, making it a safe default pick.
* While the directional distribution is no longer theoretically ideal, which can lead to slight differences in rendered images, the overall effect of the multiscattering correction (preserving energy, avoiding darkening at high roughness, and increasing saturation at high roughness) still applies.
* For highly anisotropic materials, the compensation might not be perfect. This is a limitation of the currently known approximation methods.

== Principled Hair BSDF ==
A new variant called "Huang" is added, based on the paper [https://doi.org/10.1111/cgf.14588 A Microfacet-based Hair Scattering Model] by Weizhen Huang, Matthias B. Hullin and Johannes Hanika ([https://projects.blender.org/blender/blender/pulls/105600 PR #105600]). The previous Principled Hair BSDF is now the "Chiang" variant.
* Supports elliptical cross-sections, adding more realism as human hairs are usually elliptical. The orientation of the cross-section is aligned with the curve normal, which can be adjusted using geometry nodes. Default is minimal twist.
* This is a far-field model, as opposed to the near-field Principled Hair BSDF model. The hair is expected to be less noisy, but lower roughness values takes longer to render due to numerical integration along the hair width. The hair also appears to be flat when viewed up-close.
* Gives nicer focused reflection when viewed against the light.
For more detailed comparisons, please refer to the original paper and the [https://docs.blender.org/manual/en/4.0/render/shader_nodes/shader/hair_principled.html#huang-model Principled Hair user manual].

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Existing Chiang and new microfacet based Huang shading models.<br/>Scene by Simon Thommes and Fernando Alcala.
 |valign=top|[[File:Cycles4.0-hair-chiang.jpg|400px|center|link=https://wiki.blender.org/w/images/4/4a/Cycles4.0-hair-chiang.jpg]]
 |valign=top|[[File:Cycles4.0-hair-huang.jpg|400px|center|link=https://wiki.blender.org/w/images/9/93/Cycles4.0-hair-huang.jpg]]
 |-
 |valign=top|[[File:Cycles4.0-hair2-chiang.jpg|400px|center|link=https://wiki.blender.org/w/images/7/71/Cycles4.0-hair2-chiang.jpg]]
 |valign=top|[[File:Cycles4.0-hair2-huang.jpg|400px|center|link=https://wiki.blender.org/w/images/a/aa/Cycles4.0-hair2-huang.jpg]]
 |}
</center>

== Open Shading Language ==

Most Cycles-specific microfacet closures were removed since they are redundant now that Cycles implements the generic `microfacet()` closure of the OSL standard. ([https://projects.blender.org/blender/blender/commit/888bdc1419a2cd99284060a64d1df0c4397d3ac5 888bdc1419])

* Specifically, the `microfacet()` closure supports the distributions `beckmann`, `sharp`, `ashikhmin_shirley`, `ggx`, `multi_ggx` and `clearcoat`. The `refract` argument supports `0` for pure reflection, `1` for pure refraction, and `2` for both (based on dielectric Fresnel according to the specified IOR).
* Additionally, the three MaterialX microfacet closures `dielectric_bsdf()`, `conductor_bsdf()` and `generalized_schlick_bsdf()` are provided. These only support `beckmann`, `ggx` and `multi_ggx` distributions.
* Two Cycles-specific closures remain, but they are for internal use and should not be relied upon by user-created OSL shaders.

== Metal Hardware Raytracing ==

The new Apple M3 processor supports hardware ray-tracing. Cycles takes advantage of this by default, with the MetalRT in the GPU device preferences set to "Auto".

For the M1 and M2 processors MetalRT is no longer an experimental feature, and fully supported now. However it is off by default, as Cycles' own intersection code still has better performance when there is no hardware ray-tracing.

== Other ==

* Performance improvements to geometry upload from [[Reference/Release_Notes/3.6/Cycles#Performance|Blender 3.6]] continued, with a 1.76x improvement measure for a large mesh ([https://projects.blender.org/blender/blender/commit/2fac2228d05e165aeb8793cbdf5f1910710ff574 2fac2228d0]).
* Metal AMD GPU rendering has a significant performance regression in scenes that use the Principled Hair BSDF, in addition to existing lack of support for the light tree and shadow caustics. Due to GPU driver limitations that are unlikely to be fixed, it is expected that AMD GPU rendering on macOS will be disabled entirely in a future release.
* Support for rendering with AMD RDNA2 and RDNA3 APUs is added.