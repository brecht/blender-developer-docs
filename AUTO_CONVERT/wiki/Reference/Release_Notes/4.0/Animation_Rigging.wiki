= Animation & Rigging =

== Pose Library ==

The pose library uses the newly introduced asset shelf to make pose assets available in the 3D viewport.

[[File:Asset browser-pose library-use from viewport.png|720px|thumb|center|Pose library in the asset shelf]]

== Bone Collections and Colors ==

New bone collections replace both legacy numbered layers and bone groups. Bone collections can be named and are no longer limited to a fixed number. Colors are now specified individually for each bone, replacing bone group colors. (Design [https://projects.blender.org/blender/blender/issues/108941 #108941], [https://projects.blender.org/blender/blender/commit/998136f7a7b520ef10c998c5af744eb0d464fdd3 998136f7a7])

* Bone colors can be defined on the armature (edit mode) bones, as well as on the pose bones (also see [https://docs.blender.org/api/current/info_gotcha.html#edit-bones-pose-bones-bone-bones Gotchas: edit bones, pose bones, bone bones]). The "edit mode" colors are shared between all users of the armature, and are available in edit mode as well as pose mode. The "pose mode" colors can be different per armature object (in the same way that each armature object has its own pose), and thus allow per-character customization of colors. The pose mode colors work as override on the edit mode colors; setting the pose color to "default" will show the edit mode color. ([https://projects.blender.org/blender/blender/commit/042c5347f4b8e69dee76285e3bf085b8933427b0 042c5347f4])
* The Select Grouped operator (Shift+G) can now select by bone color and by bone collection. This works based on the active bone; it selects all the other bones with the same color resp. sharing one or more bone collections. This is slightly different than before, where all selected bones were taken into account. ([https://projects.blender.org/blender/blender/commit/e41fbfd6e93 e41fbfd6e9], [https://projects.blender.org/blender/blender/commit/c230c526261 c230c52626], [https://projects.blender.org/blender/blender/commit/d5329eeea23 d5329eeea2], [https://projects.blender.org/blender/blender/commit/e59944dba4e e59944dba4], [https://projects.blender.org/blender/blender/commit/62639080bce 62639080bc])
* The Bone Layer operators (M, Shift+M) have been replaced by bone collection operators. ([https://projects.blender.org/blender/blender/commit/b48031df8e7 b48031df8e])
* Bone collections can be added to linked armatures via library overrides.
* The Armature property `show_group_colors` has been renamed to `show_bone_colors`. ([https://projects.blender.org/blender/blender/commit/f78ed7b900e99302d3bd0dd828167ec7fc09f370 f78ed7b900])
* The "Skeleton" panel in the Armature properties has been renamed to "Pose". ([https://projects.blender.org/blender/blender/commit/007cea46537 007cea4653])

See [[Reference/Release_Notes/4.0/Animation_Rigging/Bone_Collections_%26_Colors:_Upgrading|Bone Collections & Colors: Upgrading]] for an overview of how to change Python code from armature layers & bone groups to bone collections.

== Bendy Bones ==

A new method of mapping vertices to B-Bone segments for deformation that takes the rest pose curvature into account has been added. ([https://projects.blender.org/blender/blender/commit/0055ae01abbf0784eb3a64a6c50be7a5183b9524 0055ae01ab], [[Source/Animation/B-Bone_Vertex_Mapping#Curved_Mapping|Wiki]])

The new mapping is slower to compute, but produces better deformation when the B-Bone is strongly curved in the rest pose, e.g. in a corner of the mouth:

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Straight mapping and Curved mapping, with comparison of deformation.
 |valign=top|[[File:Bbone-mapping-straight.png|256px|center]]
 |valign=top|[[File:Bbone-mapping-curved.png|256px|center]]
 |valign=top|[[File:Bbone-mapping-comparison.png|256px|center]]
 |}
</center>

== Preserve Volume ==

The long standing issue with Preserve Volume causing distortion when rotation is combined with scaling has been fixed. ([https://projects.blender.org/blender/blender/commit/f12e9f32b56ec7ca8cc3251dd46e1ffc56d9d102 f12e9f32b5], [https://projects.blender.org/blender/blender/commit/dee29f4e81d22f0d721e2d8010b06637ef7a5882 dee29f4e81])

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Deformation of a rotated and uniformly scaled down joint before and after the fix.
 |valign=top|[[File:Demo-preserve-volume-fix-before.png|384px|center]]
 |valign=top|[[File:Demo-preserve-volume-fix-after.png|384px|center]]
 |}
</center>

== NLA Editor ==
* NLA Strips can now be vertically re-ordered. Strips can vertically be dragged through existing strips, and locked tracks. ([https://projects.blender.org/blender/blender/commit/4268ac0ed96bfacf0ad093be2e593208d64c8fcb 4268ac0ed9])
* Vertically clamp scrolling to view. Preventing infinite scrolling. ([https://projects.blender.org/blender/blender/commit/71463719646d686ec1f9341f64e93f42b5158dc5 7146371964])

== Graph Editor ==

=== Editing ===

* Interactive sliders for Match Slope, Blend To Ease, Blend Offset, Shear Keys, Scale Average, Time Offset, Push/Pull. ([https://projects.blender.org/blender/blender/pulls/110567 PR #110567], [https://projects.blender.org/blender/blender/pulls/110566 PR #110566], [https://projects.blender.org/blender/blender/pulls/110544 PR #110544], [https://projects.blender.org/blender/blender/pulls/111735 PR #111735], [https://projects.blender.org/blender/blender/pulls/111744 PR #111744], [https://projects.blender.org/blender/blender/pulls/110540 PR #110540], [https://projects.blender.org/blender/blender/pulls/112388 PR #112388])

[[File:Animaide tools.mp4|720px|center|thumb|New graph editor operators]]

* Butterworth Smoothing filter. ([https://projects.blender.org/blender/blender/pulls/106952 PR #106952])

[[File:Butterworth Smoothing Filter.mp4|720px|center|thumb|Butterworth Smoothing Filter]]

* Select/deselect handles from current selection. ([https://projects.blender.org/blender/blender/pulls/111143 PR #111143])

=== Performance ===

The performance of the Graph Editor has been greatly enhanced for when it comes to dense key data. ([https://projects.blender.org/blender/blender/pulls/110301 PR #110301], [https://projects.blender.org/blender/blender/pulls/110764 PR #110764], [https://projects.blender.org/blender/blender/pulls/110788 PR #110788], [https://projects.blender.org/blender/blender/pulls/112126 PR #112126], 

In the following test setup with 6000 keys per channel on 62 bones, performance was improved between 5x and 10x depending on the zoom level.

[[File:Graph Editor speedup comparison.mp4|Performance comparison between 3.6 and 4.0|720px|center|thumb]]

Animation playback timing was improved, to avoid jitter between frames. ([https://projects.blender.org/blender/blender/issues/111579 #111579])

[[File:FPS_Jitter_Compare.png|720px|center|thumb|FPS Jitter comparison]]

== Weight Paint ==
* Add loop select for faces. ([https://projects.blender.org/blender/blender/pulls/107653 PR #107653])

== User Interface ==

* Channel list background colors have moved from covering the entire background (making text hard to read) to a small rectangle. The channel background color is also more subtle and blends in better. As the feature now no longer causes hard to read text, the "Channel Group Colors" option in the preferences has been enabled by default. ([https://projects.blender.org/blender/blender/commit/c524fbe62393bdaba6503e1317c009e66be0df66 c524fbe623])
* Improved drawing of locked FCurves. ([https://projects.blender.org/blender/blender/pulls/106052 PR #106052])
* Multi Editing for FCurve modifiers. ([https://projects.blender.org/blender/blender/pulls/112419 PR #112419])
* Support adjusting the number of samples used for FPS playback display in the preferences. ([https://projects.blender.org/blender/blender/commit/b150b477202dc6d2a595b51be8cc34f8d6269731 b150b47720])
* Rename "Bake Curve" to "Keys to Samples". ([https://projects.blender.org/blender/blender/pulls/111049 PR #111049])
* Rename "Sample Keyframes" to "Bake Keyframes". ([https://projects.blender.org/blender/blender/pulls/112148 PR #112148], [https://projects.blender.org/blender/blender/pulls/112151 PR #112151])
* Keep icons aligned when curves are baked. ([https://projects.blender.org/blender/blender/pulls/108518 PR #108518])

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Channel drawing in 3.6 and 4.0
 |valign=top|[[File:blender-4.0-channel-group-colors-before.png|275px|center]]
 |valign=top|[[File:blender-4.0-channel-group-colors-after.png|256px|center]]
 |}
</center>

== Breaking Changes ==

* 1-9 hotkeys for switching collection visibility have been removed from Pose Mode. ([https://projects.blender.org/blender/blender/pulls/105120 PR #105120])
* Certain Graph Editor Slider tools had their range changed from 0-1 to -1/1: Blend to Neighbor, Breakdown, Ease. ([https://projects.blender.org/blender/blender/pulls/107173 PR #107173])
* Making Make Vertex Weight Edit modifier Group Add / Remove threshold inclusive. [https://projects.blender.org/blender/blender/commit/1837bda030651582766ca5f8ebda216739285185 1837bda030]
* The "Push Rest Pose" and "Relax Rest Pose" operators have been merge into "Blend Pose with Rest Pose". ([https://projects.blender.org/blender/blender/pulls/108309 PR #108309])
* Update NLA "Duplicate hotkey" to be "Duplicate Linked" by default. ([https://projects.blender.org/blender/blender/commit/8183f21258cd15d3aab1dfc5b1344f42e9560f78 8183f21258])
* The "Tab" shortcut to enter tweak mode in the NLA now defaults to "full stack evaluation". ([https://projects.blender.org/blender/blender/commit/b95cf5ff0f3da57738303b653c2c01326d663e46 b95cf5ff0f])
* The Preserve Volume deformation fix may change deformation in existing rigs (see above).
* Snapping for the Graph Editor, NLA and Dope Sheet have moved to the scene. ([https://projects.blender.org/blender/blender/pulls/109015 PR #109015])
* Armature Layers and Bone Groups have been removed, and replaced with Bone Collections and per-bone colors (see above).
* Bones influenced by an Inverse Kinematics (IK) constraint can now be keyed visually. This means that when the 'Use Visual Keying' preferenced is enabled, visual keying is used on such bones (where in Blender 3.6 and older still would create regular keys). ([https://projects.blender.org/blender/blender/commit/eab95fa2aaa9d1b29b460ab5c52fe8daf1da2d1b eab95fa2aa])