= Python API =

== Text Editor ==

* Python exceptions from the text editor are now displayed in the info-space (as well as the console). ([https://projects.blender.org/blender/blender/commit/2d2baeaf04d481f284bc2f098fb6d7ee9268151f  rB2d2baeaf]).
* Soft keywords are now syntax highlighted. ([https://projects.blender.org/blender/blender/commit/c6ed879f9aa7cd863f146cc7104f16cfe8a73e8a c6ed879f9a])

== Additions ==

* [https://docs.blender.org/api/3.2/bpy.types.Context.html#bpy.types.Context.temp_override `Context.temp_override`] has been added to support temporarily setting the context which can be useful after loading a file (for example) where the context is cleared making many operators fail to run. ([https://projects.blender.org/blender/blender/commit/f438344cf243632e497772cf1f855b9c8856fd37 f438344cf2])
* `Context.path_resolve` now supports resolving attributes such as "active_object", "scene" ... etc. Expressions such as `context.path_resolve("active_object.modifiers[0].name")` now work as expected. ([https://projects.blender.org/blender/blender/commit/7bb8eeb3a871eb1e72ee129f8ff441f2752b37be 7bb8eeb3a8])
* `Text.region_from_string` & `Text.region_as_string` methods can be used to get and set the selection, optionally passing in the region to access. ([https://projects.blender.org/blender/blender/commit/f49a736ff4023231483c7e535ca2a7f2869d641d  rBf49a736f]).
* `mathutils.Color` gained methods to convert between the scene linear color space and sRGB, CIE XYZ, ACES and Linear Rec.709. Most values returned by Blender APIs are in scene linear color space, with the notable exception of user interface theming colors which are in sRGB. ([https://projects.blender.org/blender/blender/commit/469ee7f 469ee7f])
* Persistent handlers in `bpy.app.handlers` can now use methods from a class. ([https://projects.blender.org/blender/blender/commit/0f583d9d604f45606e238f88eb2300eb75e492bb 0f583d9d60])

== Breaking Changes ==

* `XrActionMapItem.user_paths` collection property replaces `XrActionMapItem.user_path0/user_path1`. Similarly, `XrActionMapBinding.component_paths` collection property replaces `XrActionMapBinding.component_path0/component_path1`. This allows XR actions to have a variable amount of bindings instead of being limited to 2 per action. ([https://projects.blender.org/blender/blender/commit/6a8709ba136e  rB6a8709ba])
* `gpu.shader.code_from_builtin` it is no longer compatible with Blender's internal gpu module and had to be removed. ([https://projects.blender.org/blender/blender/commit/3e98331a094b  rB3e98331a])
* The `MeshVertex.normal` property is now read-only, which prevents confusion due to the way vertex normals are calculated on-demand. ([https://projects.blender.org/blender/blender/commit/891268aa824f0f 891268aa82])
* Tweak events have been removed from `KeyMapItem.type`. (`EVT_TWEAK_L`, `EVT_TWEAK_M`, `EVT_TWEAK_R`)
: Instead of special types, regular mouse button types should be used (`LEFTMOUSE`, `MIDDLEMOUSE`, `RIGHTMOUSE`) with the `KeyMapItem.value` set to `CLICK_DRAG`. Directional dragging can be specified by setting `KeyMapItem.direction`. ([https://projects.blender.org/blender/blender/commit/4986f718482b061082936f1f6aa13929741093a2 4986f71848], [https://projects.blender.org/blender/blender/commit/7e4c0313283304bd8f020eaedb94b35e75b50068 7e4c031328])
* The value of motion events (`MOUSEMOVE`, `INBETWEEN_MOUSEMOVE`) is now always `NOTHING`, instead of the previous `PRESS` / `RELEASE` event's value [https://projects.blender.org/blender/blender/commit/52af3b20d45ea525a0ce66b2613ac132c9032a3f 52af3b20d4].
* Sequence properties `frame_still_start` and `frame_still_end` have been removed. Same functionality can be now achieved by setting negative `frame_offset_start` or `frame_offset_end` ([https://projects.blender.org/blender/blender/commit/8ca9ce09865e6a617d6c2f78f3483ba1fd5d6aef  rB8ca9ce09])
* Adding a new Geometry Nodes modifier doesn't automatically populate it with a new node tree anymore ([https://projects.blender.org/blender/blender/commit/08b4b657b64f 08b4b657b6]).


== Deprecation ==

* The primitive drawing types `'LINE_LOOP'` and `'TRI_FAN'` used in `GPUBatch` have been deprecated as they will be removed in future releases. ([https://projects.blender.org/blender/blender/commit/e2d8b6dc06  rBe2d8b6dc])

* Passing the context to operators as a dictionary) has been deprecated as part of the inclusion of `Context.temp_override` which should be used instead. ([https://projects.blender.org/blender/blender/commit/f438344cf243632e497772cf1f855b9c8856fd37 f438344cf2])

: <source lang="python"># Deprecated API
bpy.ops.object.delete({"selected_objects": objects_to_delete})

# New API
with context.temp_override(selected_objects=objects_to_delete):
    bpy.ops.object.delete()
</source>

* `Mesh.vertex_colors` is deprecated and will be removed in a future release. The new sculpt paint tools can now create color attributes, found in `Mesh.attributes` with type `BYTE_COLOR` and `FLOAT_COLOR`. Importers and exporters are recommended to support both types of color attributes, and attributes in general.<br/>Different from the old vertex colors, color attribute values are in scene linear color space instead of sRGB color space. For file formats that require sRGB colors, `mathutils.Color.from_scene_linear_to_srgb` can perform the conversion.