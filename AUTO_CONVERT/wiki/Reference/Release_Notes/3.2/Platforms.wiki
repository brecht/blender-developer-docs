== Windows ==
* Improved error message when monitors are connected to multiple adapters. ([https://projects.blender.org/blender/blender/commit/fd1078e1055e fd1078e105]).
* Faster blender launch when there are disconnected network shares. ([https://projects.blender.org/blender/blender/commit/84fde382e43c 84fde382e4]).