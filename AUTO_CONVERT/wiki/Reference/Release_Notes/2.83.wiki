= Blender 2.83 Release Notes =

Blender 2.83 was released on June 3, 2020.

Check out the final [https://www.blender.org/download/releases/2-83/ release notes on blender.org].

This release includes long-term support, see the [https://www.blender.org/download/lts/ LTS page] for a list of bugfixes included in the latest version.

== [[Reference/Release Notes/2.83/Grease_Pencil|Grease Pencil]] ==

Completely rewritten from scratch, the new Grease Pencil is much faster and better integrated with Blender

== [[Reference/Release Notes/2.83/Volumes|Volumes]] ==

OpenVDB files can now be imported and rendered with the new volume object.

== [[Reference/Release Notes/2.83/Virtual_Reality|Virtual Reality]] ==

Initial native virtual reality, focused on scene inspection.

== [[Reference/Release Notes/2.83/Cycles|Cycles]] ==

Adaptive sampling, OptiX viewport denoising, improved shader nodes, and more.

== [[Reference/Release Notes/2.83/EEVEE|EEVEE]] ==

New render passes, improved light cache and high-quality hair transparency.

== [[Reference/Release Notes/2.83/Sculpt|Sculpt]] ==

Face sets to control visibility, cloth brush, and many more improvements.

== [[Reference/Release Notes/2.83/Modeling|Modeling]] ==

== [[Reference/Release Notes/2.83/Physics|Physics]] ==

== [[Reference/Release Notes/2.83/User_Interface|User Interface]] ==

== [[Reference/Release Notes/2.83/Animation|Animation & Rigging]] ==

== [[Reference/Release Notes/2.83/More_Features|More Features]] ==

== [[Reference/Release Notes/2.83/Python_API|Python API]] ==

== [[Reference/Release Notes/2.83/Add-ons|Add-ons]] ==

Major updates to Collection Manager.