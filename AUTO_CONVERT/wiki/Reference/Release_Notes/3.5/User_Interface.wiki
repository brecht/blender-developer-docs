= User Interface =

== General ==

* The icon preview shown in the material slots and material search menu will no longer be rendered for linked in materials. This would cause slowdowns and UI freezes every time the file is reloaded, since the preview is never stored in files. ([https://projects.blender.org/blender/blender/commit/571f373155cb  rB571f3731])
* More menus align their item labels when some items include an icon and some don't ([https://projects.blender.org/blender/blender/commit/58752ad93c 58752ad93c]).
* Font previews now differentiate better between Korean, Japanese, Simplified & Traditional Chinese  ([https://projects.blender.org/blender/blender/commit/485ab420757b  rB485ab420])
[[File:FontPreviewsCJK.png|800px|thumb|center]]

== Viewport ==

* Cancelling viewport orbit/pan/zoom & dolly is now supported (by pressing RMB), this allows orbiting out of camera or orthographic views temporarily.  ([https://projects.blender.org/blender/blender/commit/0b85d6a030a3fc5be10cce9b5df7a542a78b4503  rB0b85d6a0])

== macOS ==

* User notifications for finishing renders and compiling shaders were removed. ([https://projects.blender.org/blender/blender/commit/3590e263e09d 3590e263e0])