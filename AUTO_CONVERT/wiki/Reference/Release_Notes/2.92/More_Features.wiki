= Blender 2.92: More Features =

== Compositor ==

* Keying node in compositor ensures result is properly premuiltiplied ([https://projects.blender.org/blender/blender/commit/f68c3d557aa f68c3d557a]) In practice this means that there is no need in manually adding Alpha Convert node after the keying one. The existing files might need to be adjusted in this regard: the alpha convert node is to be removed if it present.

* The Save as Render option was added to the File Output node. This feature allows to disable the use of the scene's color management options for each input socket individually. ([https://projects.blender.org/blender/blender/commit/27b78c9c94ba 27b78c9c94])

* New compositor node to adjust the exposure of images. ([https://projects.blender.org/blender/blender/commit/6538f1e600ad 6538f1e600])

[[File:0001-1322.mp4|600px|center|thumb]]
''Demo by [http://www.monorender.com/ Carlo Bergonzini].''

== Compatibility ==

* Pre-2.50.9 multires code has been removed ([https://projects.blender.org/blender/blender/commit/d11e357824d d11e357824])
* Removed Simple multires subdivision type ([https://projects.blender.org/blender/blender/commit/17381c7b90e 17381c7b90])
* "Camera View Lock" is now in the object relations panel instead of the preferences ([https://projects.blender.org/blender/blender/commit/00374fbde2bd 00374fbde2])

== Motion tracking ==

* Reorganize menus to make them consistent with the rest of Blender and expose items previously only visible in panels ([https://projects.blender.org/blender/blender/commit/6c15b702796e 6c15b70279])
* Simplified configuration of intrinsics to refine ([https://projects.blender.org/blender/blender/commit/0269f0c5745 0269f0c574])
* Moved optical center to lens settings panel ([https://projects.blender.org/blender/blender/commit/24686351746 2468635174])
* Solved random crashes tracking multiple tracks ([https://projects.blender.org/blender/blender/commit/29401d38d11 29401d38d1])
* Huge speedup of tracking multiple tracks ([https://projects.blender.org/blender/blender/commit/5d130826221 5d13082622])

== Sequencer ==

* Media transform redesign ([https://projects.blender.org/blender/blender/commit/e1665c3d31 e1665c3d31])
* Background rectangle option for text strip ([https://projects.blender.org/blender/blender/commit/235c309e5f86  rB235c309e])
* Add Overlay popover panels ([https://projects.blender.org/blender/blender/commit/fad80a95fd08  rBfad80a95])
* Paste strips after playhead by default ([https://projects.blender.org/blender/blender/commit/dd9d12bf45ed  rBdd9d12bf])
* Move remove gaps operator logic to module code ([https://projects.blender.org/blender/blender/commit/9e4a4c2e996c  rB9e4a4c2e])
* Hide cache settings and adjust defaults ([https://projects.blender.org/blender/blender/commit/f448ff2afe7a  rBf448ff2a])

== macOS ==

* Follow system preference for natural trackpad scroll direction automatically, remove manual preference ([https://projects.blender.org/blender/blender/commit/055ed33 055ed33])