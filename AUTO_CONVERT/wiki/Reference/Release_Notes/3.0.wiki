= Blender 3.0 Release Notes =

Blender 3.0 was released on December 3, 2021.

Check out the final [https://www.blender.org/download/releases/3-0 release notes on blender.org].

== [[Reference/Release Notes/3.0/Animation_Rigging|Animation & Rigging]] ==

== [[Reference/Release Notes/3.0/Asset_Browser|Asset Browser]] ==

== [[Reference/Release Notes/3.0/Core|Core]] ==

== [[Reference/Release Notes/3.0/EEVEE|EEVEE & Viewport]] ==

== [[Reference/Release Notes/3.0/Grease_Pencil|Grease Pencil]] ==

== [[Reference/Release Notes/3.0/Modeling|Modeling]] ==

== [[Reference/Release Notes/3.0/Nodes_Physics|Nodes & Physics]] ==

== [[Reference/Release Notes/3.0/Pipeline_Assets_IO|Pipeline, Assets & I/O]] ==

== [[Reference/Release Notes/3.0/Python_API|Python API & Text Editor]] ==

== [[Reference/Release Notes/3.0/Cycles|Render & Cycles]] ==

== [[Reference/Release Notes/3.0/Sculpt|Sculpt, Paint, Texture]] ==

== [[Reference/Release Notes/3.0/User_Interface|User Interface]] ==

== [[Reference/Release Notes/3.0/VFX|VFX & Video]] ==

== [[Reference/Release Notes/3.0/Virtual_Reality|Virtual Reality]] ==

== [[Reference/Release Notes/3.0/Add-ons|Add-ons]] ==

== Compatibility ==

See the sections [[Reference/Release Notes/3.0/Nodes_Physics|Nodes]], [[Reference/Release_Notes/3.0/Add-ons|Add-ons]], [[Reference/Release Notes/3.0/Cycles|Cycles]], [[Reference/Release_Notes/3.0/Core|Core]], [[Reference/Release Notes/3.0/Animation_Rigging|Animation_Rigging]] and [[Reference/Release Notes/3.0/Python_API|Python API]].

== [[Reference/Release Notes/3.0/Corrective_Releases|Corrective Releases]] ==