
= Blender 3.1: Grease Pencil =

== Line Art ==

[[File:Line Art collection inverse.gif|400px]]

* New option to toggle collection inverse selection. This allows conveniently exclude a collection and select "the rest of" the scene. ([https://projects.blender.org/blender/blender/commit/40c8e23d481c 40c8e23d48])
**Differences between collection line art usage flag:
**# Collection line art usage is global, used to specify e.g. if a collection should appear in calculation at all.
**# Invert collection selection is used to easily select a collection and "everything else other than that collection", making it convenient to set up such cases.

[[File:Line Art Facemark Contour Preserving.png|400px]]

* New option to preserve contour lines when using face mark filtering. ([https://projects.blender.org/blender/blender/commit/dde997086ce2 dde997086c])

[[File:Line art noise tolerant chaining.png|400px]]

* Noise tolerant chaining. This feature takes advantage of original line art chain which is a continuous long chain, instead of splitting it at each occlusion change, this function tolerates short segments of "zig-zag" occlusion incoherence and don't split the chain at these points, thus creates a much smoother result. ([https://projects.blender.org/blender/blender/commit/5ae76fae90da 5ae76fae90])

* Back face culling option. ([https://projects.blender.org/blender/blender/commit/579e8ebe79a1 579e8ebe79])

== Operators ==
* New Merge All Layers option in Merge operator. ([https://projects.blender.org/blender/blender/commit/556c71a84ac1 556c71a84a])
* New option in PDF export to export full scene frame range. ([https://projects.blender.org/blender/blender/commit/e1c4e5df225d e1c4e5df22])

== Tools ==
* Now Fill tool allows to use a Dilate negative value to contract the filled area and create a gap between fill and stroke. ([https://projects.blender.org/blender/blender/commit/3b1422488195 3b14224881])

[[File:Fill Dilate-Contract sample.png|400px]]

== Modifiers and VFX ==
* New Shrinkwrap modifier. ([https://projects.blender.org/blender/blender/commit/459af75d1ed5 459af75d1e])
* New Randomize parameter in Length modifier. ([https://projects.blender.org/blender/blender/commit/a90c35646766 a90c356467])