= Blender 4.1 Release Notes =

Blender 4.1 is currently in '''Alpha'''. Phase '''Bcon1''' until January 3, 2024. [https://projects.blender.org/blender/blender/milestone/18 See schedule].

Under development in [https://projects.blender.org/blender/blender/src/branch/main `main`].

== [[Reference/Release Notes/4.1/Animation_Rigging|Animation & Rigging]] ==

== [[Reference/Release Notes/4.1/Core|Core]] ==

== [[Reference/Release Notes/4.1/EEVEE|EEVEE & Viewport]] ==

== [[Reference/Release Notes/4.1/Grease_Pencil|Grease Pencil]] ==

== [[Reference/Release Notes/4.1/Modeling|Modeling & UV]] ==

== [[Reference/Release_Notes/4.1/Nodes_Physics|Nodes & Physics]] ==

== [[Reference/Release Notes/4.1/Pipeline_Assets_IO|Pipeline, Assets & I/O]] ==

== [[Reference/Release Notes/4.1/Python_API|Python API & Text Editor]] ==

== [[Reference/Release Notes/4.1/Rendering|Rendering]] ==

== [[Reference/Release Notes/4.1/Cycles|Cycles]] ==

== [[Reference/Release Notes/4.1/Sculpt|Sculpt, Paint, Texture]] ==

== [[Reference/Release Notes/4.1/User_Interface|User Interface]] ==

== [[Reference/Release Notes/4.1/Keymap|Keymap]] ==

== [[Reference/Release Notes/4.1/VFX|VFX & Video]] ==

== [[Reference/Release Notes/4.1/Add-ons|Add-ons]] ==

== [[Reference/Release Notes/4.1/Asset_Bundles|Asset Bundles]] ==

== Compatibility ==