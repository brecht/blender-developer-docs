= Blender 2.79: Add-ons =

== New Add-ons ==

----

===Dynamic Sky===
[[File:Addons_dynamic_sky.jpg|right|600px]]
*This add-on is in the Toolshelf > Tools Tab
*Dynamic sky is an add-on to create Procedural Sun and sky with lots of additional options to tweak
*For Cycles Worlds
*Authors: Draguu (Pratik Solanki)
*Maintainer: Draguu (Pratik Solanki)
*[http://www.dragoneex.com/downloads/dynamic-skyadd-on Wiki Page] (dragoneex.com)

<br style="clear:both;">

----

===Archipack===
[[File:Addon_Archipack.png|right|600px]]
Provide a set of parametric objects made to boost CAD users productivity.<br/>
Author: Stephen Leger (stephen-l)<br/>
Maintainer: stephen-l<br/>
This addon is in the Toolshelf > Create Tab
====Available objects====
* Door
* Window
* Wall
* Stair
* Fence
* Slab
* Floor
* Truss
* Roof

====Main Features====
*Realtime on-screen manipulation
*Automatic interactive boolean
*Customizable presets graphical menu 
*Draw a wall / window / door create tools
[https://github.com/s-leger/archipack/wiki Wiki Page] (github.com)<br/>
[https://github.com/s-leger/archipack/issues Issue tracker] (github.com)<br/>
[https://blenderartists.org/forum/showthread.php?417856-Addon-Archipack Blender Artists Thread]
<br style="clear:both;">

----

=== Magic UV ===
[[File:Addon_MagicUV.png|right|250px]]

Magic UV (a.k.a. Copy/Paste UVs) was available only for the [https://builder.blender.org/download/ buildbot builds] until 2.78.<br/>From 2.79, Magic UV is included to the '''official release'''.

Author: Nutti, Mifth, Jace Priester, kgeogeo, mem, Keith (Wahooney) Boshoff, McBuff, MaxRobinot<br/>Task Tracker: https://github.com/nutti/Magic-UV/issues<br/>Wiki Page: https://github.com/nutti/Magic-UV/wiki<br/>GitHub Page(For Development):https://github.com/nutti/Magic-UV<br/>[https://blenderartists.org/forum/showthread.php?391573-Add-on-Magic-UV-v4-1-(Copy-Paste-UV-Preserve-UVs-UV-Bounding-Box-and-so-on) Blender Artist Thread]

====New Features:====

Newly added features since [https://wiki.blender.org/index.php/Dev:Ref/Release_Notes/2.76/Add-ons 2.76]) are as follows.

[https://www.youtube.com/playlist?list=PLxU3dbBpsS3flku3NlSigLVltSokGBg-R Tutorial Video List (All features)]<br/>
[https://www.youtube.com/playlist?list=PLxU3dbBpsS3c_jY3Fd7Ogqr9Lr7w-0h0U Tutorial Video List (New features since 2.76)]

*'''Bounding-Box Manipulator in Image/UV Editor'''<br/>Edit UV with the Bounding-Box Manipulator.<br/>Translation, rotation and scaling operations are supported.
*'''Move UV from 3D View'''<br/>Edit UV with the mouse from 3D View.
*'''Texture Projection'''<br/>Project the texture to the mesh from 3D View while displaying the texture.
*'''Pack UV Extenstion (with same UV island packing)'''<br/>"Pack UV" and merge the similar UV islands.
*'''Texture Lock'''<br/>Preserve UV coordinates while editing the meshes.
*'''Mirror UV'''<br/>Copy/Paste UV with Mirror-inverted
*'''World Scale UV'''<br/>Scale UV along to the mesh's size.
*'''Unwrap Constraint'''<br/>Keep U or V coordinate during "Unwrap UV" operation.
*'''Preserve UV Aspect'''<br/>Restrain changing UV coordinate when the different sized texture will be attached to the mesh.

====Bug fixes:====

*User reported bugs are fixed
*Refactor and Adopt style guide.<br style="clear:both;">

----

===Mesh Edit Tools===
[[File:Addon_multiextrude_glam.jpg|right|600px]]
[[File:Extrude_Reshape.jpg|right|600px]]
*   '''Face Inset Fillet:''' based completely on add-on by zmj100
*   '''Vertex Align:''' by zmj100
*   '''Edge Fillet Plus:''' by Gert De Roost - original by zmj100
*   '''Split Solidify:''' by zmj100, updated by zeffii to BMesh
*   '''Pen Tool:''' by zmj100
*   '''Mesh Cut Faces:''' by Stanislav Blinov (Stan Pancakes)
*   '''V/E/F Context Menu:''' by Stanislav Blinov (Stan Pancakes)
*   '''Edge Roundifier:''' by Piotr Komisarczyk (komi3D), PKHG
*   '''PKHG Face Extrude:''' by PKHG, based of geodesic domes add-on "faces function"
*   '''Set Edges Length:''' by "Giuseppe De Marco [BlenderLab] inspired by NirenYang"
*   '''Edge Tools:''' by Paul Marshall (brikbot)
*   '''Extrude and Reshape:''' by Germano Cavalcante (mano-wii)
*   '''Fast Loop:''' by Andy Davies (metalliandy)
*   '''Multi Extrude:''' by Liero, Jimmy Hazevoet
*   '''Offset Edges:''' by Hidesato Ikeya
*   '''Edge Floor Plan:''' by lijenstina
*   '''Vertex Chamfer:''' by Andrew Hale (TrumanBlending)
*   '''Random Vertices:''' by Oscurart
*   '''Select Tools:''' by dustractor
*   Thanks to Macouno & CoDEmanX 

*   Mesh Edit Tool adds several tools to Blender that are not available in the built in tools or provide different methods for similar tasks
*   Each Menu: Vert, Edge, Face & Utils is a sub panel that is closed by default in the Toolshelf Edit Mode Tools
*   The Icons in the closed panel headers contain some new/different selection tools & edit mode v/e/f Selection Modes for quick access to some tools, before opening the sub panels
*   In the ui there's a variety of tools in each category, most tools have a small box to the right to provide some usage information to help users understand each tools basic requirements.
*   '''Hotkeys:''' most significant is in edit mode double right click brings up the Blender Vert/Edge/Face menus depending on v/e/f selection mode
*   Menu Integration is main tools into the 'w' key edit mode specials, selection tools into the edit mode select menu 
*Maintainer: meta-androcto, lijenstina
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Modeling/Extra_Tools Wiki Page] (blender.org)
<br style="clear:both;">

----

===Skinify===
[[File:Addon_skinify_rigify2.jpg|left|520px]]
[[File:Addon_skinify_ui.jpg|right|425px]]
<br style="clear:both;">

*This add-on Skins an armature with a mesh
*Supports rigify with hands
*Supports BVH
*Easy to use & set up quick proxy mesh for animation testing
*Fast results for testing poses
*Quad mesh created, sculpting is possible.
*Author: karab44
*Maintainer: karab44
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Object/Skinify Wiki Page] (blender.org)
<br style="clear:both;">

----

===Display Tools===
[[File:Addon_display_tools.jpg|right|250px]]
*This Addon is in the Toolshelf > Display Tab
*Display Tools is designed to provide several tools to speed your workflow in viewport display related tasks and interaction with multiple objects.
*Mostly concerned with wire overlays & viewport display
*Including new tools Fast Nav, (unselected objects are displayed in wireframe when moving objects
*The add-on works in two ways:
*Apply on selected objects.
*Apply on all objects if none are selected. 
*Authors: Jordi Vall-llovera Medina, Jhon Wallace, Jasperge, Pixaal, Meta-androcto
*Maintainer: meta-androcto, lijenstina
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/3D_interaction/Display_Tools Wiki Page] (blender.org)
<br style="clear:both;">

----

===Brush Menus===
[[File:Addon_brush_tools.jpg|right]]
*Based off Advanced UI Menus from Imaginer
*'''Hotkey''' Alt/V
*This addon provides fast access to the Brush Menus & settings in the 3dview.
*Featuring Small or Large Brush Icons
*This menu is not designed to set up your entire sculpt session,
*The aim of the addons in to make fast changes to settings whilst you sculpt without opening the toolshelf.
*Supports Custom Brushes
*Authors: Ryan Inch (Imaginer), meta-androcto
*Maintainer: meta-androcto, Ryan Inch (Imaginer),
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/3D_interaction/Advanced_UI_Menus Wiki Page] (blender.org)
<br style="clear:both;">

----

===Btrace===
[[File:Addons_btrace_1_1.jpg|right]]
*This addon is in the Toolshelf > Create Tab 
*Features Object & Particle Trace & Animated curve follow
*Authors: liero, crazycourier, Atom, Meta-Androcto, MacKracken
*Maintainer: meta-androcto, crazycourier
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Curve/Btrace Wiki Page] (blender.org)
<br style="clear:both;">

----

===Is Key Free===
*This addon is in the Text Editor Toolshelf
*Primarily a tool for addons developers to check the availability & mapping of selected hotkeys also showing editor types they apply to
*Secondary use for end users who set up their own keymaps & would like to check again the availability of hotkeys
*Author @Antonio Vazquez (antoniov)
*Maintainer: Antoniov
<br style="clear:both;">

----

===Turnaround Camera===
[[File:Addon_turnaround_camera_UI.jpg|thumb|300px|right|Camera Turnaround User Inteface]]
*This add-on is in the Toolshelf > Animation Tab
*Quite Simply, it creates a 360 degree animation around the selected Object
*Useful for a product turnaround animation setup
*Author @Antonio Vazquez (antoniov)
*Maintainer: Antoniov
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Animation/TurnaroundCamera Wiki Page] (blender.org)
<br style="clear:both;">

----

===Auto Mirror===
[[File:Addon_auto_mirror_suzanne.jpg|450px|thumb|right|Automatically mirror an object on the Z axis]]
*This add-on is in the Toolshelf > Tools Tab
*Automatically add the mirror modifier on the selected axis and optionally enter edit mode ready for editing
*Author @Lapineige
*Maintainer: @Lapineige
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Modeling/AutoMirror Wiki Page] (blender.org)
<br style="clear:both;">

----

===Camera Rigs===
[[File:Addon_camera_rigs.jpg|right]]
*This Addon is in the 3d View header > Add menu > Camera menu
*Creating either a Dolly or Crane rig
*'''Note:''' You need to enable Auto Run Python Scripts in the User Preferences > File Menu
*Authors: Wayne Dixon, Kris Wittig
*Maintainer: Wayne Dixon, Kris Wittig
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Rigging/Add_Camera_Rigs Wiki Page] (blender.org)
<br style="clear:both;">

----

===Snap Utils Line===
*Advanced vertex snap to object & create faces tool
*Author: Mano-Wii
*Maintainer: Mano-Wii
*[https://github.com/Mano-Wii/Addon-Snap-Utilities-Line/wiki Wiki Page] (github.com)
<br style="clear:both;">

----

===Add Advanced Objects===
[[File:Addons_cubester.jpg|right|500px]]
*Advanced Scene & Object creation suite
*'''Delaunay Voronoi:''' by Domlysz, Points cloud Delaunay triangulation in 2.5D & cross-sections
*'''Constellation:''' by Oscurart, Point to Point Constellation pattern generation
*'''Add Light Template:''' by Rebellion, 3 point light set
*'''Aggregate mesh:''' by Liero, updated by Jimmy Hazevoet, Object particle arrays
*'''Arrange on Curve:''' by Mano-Wii, Array objects along a curve
*'''Circle Array:''' by Antonis Karvelas, Array controlled by an empty
*'''Copy2 vertices:''' edges or faces: by Eleanor Howick, Copy Any object to the selected verts/edges/faces of a mesh
*'''CubeSter:''' by Jacob Morris, Takes image, image sequence, or audio file and converts it into a height map based on pixel color and alpha values
*'''Drop to Ground1:''' by Unnikrishnan(kodemax), Florian Meyer(testscreenings), drop selected or all objects to 'Ground' plane
*'''Strut Generator:''' by Bill Currie (taniwha), Add struts meshes based on selected truss meshes
*'''Easy Lattice Object:''' by Kursad Karatas, Quick Lattice for shape editing
*'''Object Add Chain:''' by Brian Hinton (Nichod), Add Chain Link to & curve path
*'''Laplacian Lightning:''' by teldredge, Creates Laplace Lightning strikes between objects
*'''Mangle Tools:''' by Phil Cote, displace the verts on your mesh
*'''Oscurart Chain Maker:''' by Oscurart, create chain links parented to existing armature
*'''3D Pixelate:''' by liero, Create a blocky version of your mesh (dupliverts)
*'''Add Random Box Structure:''' by Dannyboy, random blocks from mesh
*'''Rope Creator:''' by Jorge Hernandez - Melenedez, Dynamic rope (with cloth) creator
*'''Test Scenes:''' by meta-androcto, fast test scene creation
*'''Tri-Lighting Creator:''' by Daniel Schalla
*'''Unfold transition by liero, atom, Animated flipping faces or build

*Special mention Authors: Rebellion, lijenstina, Unnikrishnan(kodemax), Florian Meyer, Omar ahmed,
*Maintainer: meta-androcto, lijenstina
* [https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Object/Add_Advanced Wiki Page] (blender.org)

<br style="clear:both;">

----

===Export Paper Model===
[[File:Addons_paper_model.png|right|600px]]
*Export the active mesh into PDF or SVG, including baked textures
*Print on paper or cut from plywood, aluminium etc.
*Preview and customize the layout, all within Blender
*Maintainer: Adam Dominec (emu)
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Import-Export/Paper_Model Wiki Page] (blender.org)
<br style="clear:both;">

----

===Kinoraw Tools===
*Extensive VSE toolkit
*Author: Carlos Padial
*Maintainer: Carlos Padial
*[https://github.com/kinoraw/kinoraw_tools/blob/master/README.md Wiki Page] (github.com)
<br style="clear:both;">

----

===Stored Views===
[[File:Addon_stored_views.jpg|right|250px]]
*Save & store Views & cameras, create cameras to views, saves view settings per .blend
*Add camera markers for animated camera switching
*Authors: nfloyd, fsiddi, meta-androcto
*Maintainer: meta-androcto
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/3D_interaction/stored_views Wiki Page] (blender.org)
<br style="clear:both;">

----

===Render Clay===
[[File:Addons_clay_bmw.jpg|right|500px]]
*Simple checkbox to overwrite materials in BI or Cycles to Render clay with choice of diffuse color.
*Author: ruesp83 (Fabio Russo) 
*Maintainer: meta-androcto
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Render/Clay_Render Wiki Page] (blender.org)
<br style="clear:both;">

----

===Auto Tracker===
[[File:Addons_autotracker.jpeg|right|350px]]
*addon which introduces autotracking for Blender motion tracking.
*Author: Miika Puustinen, Matti Kaihola, Stephen Leger
*Maintainer: Miika Puustinen, Stephen Leger
*[https://github.com/miikapuustinen/blender_autotracker Wiki Page] (github.com)
<br style="clear:both;">

----

===Refine Tracking Solution===
*automatically set weight of all tracks according error in a single click.
*Author: Stephen Leger
*Maintainer: Stephen Leger
*[https://github.com/s-leger/blenderTracking/wiki Wiki Page] (github.com)
<br style="clear:both;">

----

===Materials Library VX===
[[File:Addon_matlibvx1.jpg|right|250px]]
*materials library for blender 2.79
*Author: Alfonso Serra "Mackraken"
*Maintainer: Alfonso Serra "Mackraken"
*[https://sites.google.com/site/aleonserra/home/scripts/matlib-vx-5-6 Wiki Page] (google.com)
<br style="clear:both;">

----

===Mesh Tissue===
<br\>
[[File:Tissue_v02_cover.jpg|center|900px]]
<br style="clear:both;">

*Object Tesselation, Duel Mesh, auto weights
*Author: Alessandro-Zomparelli
*Maintainer: Alessandro-Zomparelli
*[https://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Mesh/Tissue Wiki Page] (blender.org)
'''Tessellate'''

Tessellate tool allows you to copy a selected object (Component) on the faces of the active object (Generator), adapting its bounding box to the shape of quad-faces. Once the Tessellate button was pressed, then more options will appear in the Operator parameters. Update allows you to update the active the Tessellated mesh according to Component and Generator changes. You can also change the parameters of tessellation.

'''Colors-Weight Exchanger'''

Colors from Weight create a new Vertex Colors channel reading data from Vertex Weight Weight from Colors create a new Vertex Weight channel reading data from Vertex Colors

'''Dual Mesh'''

Dual Mesh modify the selected meshes creating dual meshes.
<br style="clear:both;">

----

===Cell Fracture Crack It===
[[File:Addon_crackit.jpg|right|420px]]
*Helper addon for Cell Fracture Displace & materials
*Author: Nobuyuki Hirakata
*Maintainer: Nobuyuki Hirakata
*[http://gappyfacets.com/2016/08/11/blender-crack-addon-basic-tutorial/ Wiki Page] (gappyfacets.com)
* Image by YAFU
<br style="clear:both;">

----

== Updated Add-ons ==

=== Collada ===
*'''New features'''

*'''Keep Bind Info'''<br/>[[File:Ref_release_notes_2.79_add-ons_collada_keep_bindpose.png|right]]The Collada Importer replaces the original Restpose of imported armatures by their Bindpose. This actually destroys information in the bone matrices. This works when all you want is to get an armature into Blender but never export it again.<p> However there is one scenario where you want to use Blender in the middle of a pipeline. In that case it may be important to keep the original Bindpose and Restpose information. See https://developer.blender.org/T50412 for more detailed info. This information is now preserved</p>The Collada module now has a new Import/Export option:
**'''Import:''' ''Keep Bind Info'' : Store the original Restpose and the original Bindpose into the bone custom properties ''bind_mat'' and ''rest_mat'' (both are 4*4 matrices)
**'''Export:''' ''Keep Bind Info'' : When the matrices ''bind_mat'' and ''rest_mat'' are found in the bone custom properties then those matrices are exported instead of the matrices used in Blender.<p>Important: When you edit the Armature after import, then you might need to adjust bind_mat and rest_mat, or disable ''Keep Bind Info'' during export.</p>
*'''Limit Precision''' (only available for export)<br/>When exporting then especially the Matrix data often contains floating point values which are exposed in badly readable format like 0.9999999 or even worse like -5.45049e-9 While this is not harmful at all it becomes much harder to spot issues in the data during debugging and comparing collada files. The ''limit precision'' option takes care that the exported data is sanitized by reducing the precision to 6 digits.

*'''Bug Fixes'''

*'''Display of connected Bones'''<br/>Connected Bones have been given wrong locations for the bone tails. The imported Armatures look odd until you edit them. Then the connected bones snap into place. There was no report for this bug.

*'''Export of multiple Armatures'''<br/>When multiple armatures are exported which use same bone names, the exported Collada file became invalid. This fix is related to https://developer.blender.org/T50807

*'''Blender Crash during export'''<br/>When exporting while editing an armature, blender crashes. The fix is related to https://developer.blender.org/D2489

*'''Export of UV Face Textures'''<br/>In previous releases UV Face textures have been exported only when the mesh object also used Materials. Then the textures had been added silently to the material diffuse channel. To be clear: the exported materials have been modified!. But this resulted in total mix up when a material is reused by other objects. In that case the UV face textures would erroneously be also used in all objects using the same material. Of course the error only appears after you import back the Collada file into Blender or any other application.<br/>In Blender 2.79 we have changed the behavior significantly. Now you can decide to either export textures that are related to materials, or you can export UV Face textures. In the latter case there is no longer the need to have materials defined in the objects. However, we then generate materials on the fly. So when you import back your objects, you see they now have got materials (one material per UV texture).<br/>This topic is related to https://developer.blender.org/T51288

*'''Exporting triangulated Meshes'''<br/>Blender now uses <triangle> lists when meshes are triangulated upon export, or when they only contain triangles. Previously meshes have always been exported as <polylist>. This topic is related to https://developer.blender.org/T51622

*'''Export of Meshes with multiple UV Maps'''<br/>The export failed when more than 1 UV map was assigned to a mesh object. This bug is related to https://developer.blender.org/T51288
<br style="clear:both;">

----

=== POV-Ray ===

'''New features'''
[[File:povNormalPerturbation.png|right|300px]]
*Camera normal perturbation
*Photon map save/load
*charset option available to choose text encoding between:
**SYS
**ASCII
**UTF-8
<br style="clear:both;">
----
[[File:povTemplatesMenu.png|300px|right]]
*Some complete POV-Ray scenes are available to Templates menu from the text header.
<br style="clear:both;">
----
[[File:povSyntaxHighlight.png|300px|right]]
*Colored Syntax Highlight
Activating syntax highlights in the text editor now also supports POV-Ray files.
If you load an ini file, you will notice its syntax is different, but all the other three formats are pov sdl with different file names for different uses
* pov
* inc
* mcr
* ini
<br style="clear:both;">
----
[[File:povInsertMenu.png|300px|right]]
*An Insert Menu to add some pov code snippets at cursor's location has been added.

*added an operator to render from text editor 3 modes: 3dview, pov text only or both. this property replaces the custompov code (with "both" option) known problem is that some scenes get their rendered image passed to render results while others don't. Still useable for preview anyway.

*Added support of empty curves and metaballs represented by empty spheres of zero radius

*All Meta elements were no longer export to mesh but as POV-Ray blobs
*Added the following pov keywords to object properties tab: 
**hollow
**double_illuminate
**sturm (for Metaballs and lathe)
**no_shadow
**no_image
**no_reflection
**no_radiosity
**inverse
**hierarchy

[[File:BlenderToPovrayHair.png|300px|right]]
'''Bugfixes'''
*fixed hair location bug
*Fixed texture normal_factor regression
*Fixed lighting attenuation for a closer match with Blender Internal
*Merging different metaball objects is now possible
*Removed automatic activation of Add Mesh: extra objects used for 3d function surface (exported as pov parametric object) 
*Replaced by a message and easy menu for user to activate it himself to avoid conflicts with other scripts
*Use 2D curve for lathe
*Fixed an error accessing photon variables
*Depth Of Field focal point by specifying dof_object in properties ui input is now possible
*Converted some rgb keywords to srgb to better match Blender Internal shading 

<br style="clear:both;">
----

=== OBJ (Wavefront) ===
*'''New features'''
**Importer can now generate basic Cycles shaders in addition to classical BI materials, when Cycles render is active.

<br style="clear:both;">

----

=== FBX I/O .fbx ===
*'''New features'''
**new menu item: drop down selector titled: Apply Scalings.
**various bug fixes
**FBX exporter: Fix broken non-mesh geometry export. 
**Fix Blender 2.78a doesn't import user properties from FBX file. 


<br style="clear:both;">

----

=== Rigify ===

*'''Code Refactor'''<br/>Old rigify types and meta-rigs were merged with the pitchipoy ones. Due to this change all the "pitchipoy" names were removed.
<br style="clear:both;">
*'''Legacy Mode'''<br/>In order to keep backward compatibility during the transition to the new feature set a "Legacy Mode" option was added in the add-on preference pane. When enabled rigfy will run with the old 2.77 feature-set.
**'''Legacy Switcher''' <br/>When the system detects an old/unsupported Meta-Rig will ask the user to switch to legacy mode.
**'''Upgrade Meta-Rig'''<br/>When a old but supported Meta-Rig is detected the system will give the user an option to upgrade the Meta-Rig types to the current version. When this is done the user can upgrade the rig too by clicking on generate rig again.
<br style="clear:both;">
*'''Updated rig-types'''<br/>All rig types now use the formerly known as "pitchipoy.type" as base. New feature were ported from the original rigify code and merged or updated as listed below:
**'''super_limb''' <br/>Formerly known as 'pitchipoy.super_limb' is the new base type to create Arms, Legs and Paws. All the types now support standard 'pole vector' as an option in the ui after rig generation. All the FK/IK snapping functions were updated to work seamlessly indipendently from the pole type (standard or rotational). Bone rolls are automatically calculated during the rig generation process, so no more eyeballing on the metarig's limbs bone rolls!
**'''super_spine'''<br/>Formerly known as 'pitchipoy.super_torso_turbo' is the new base type to create spines. This new type support the creation of stretchy spines complete systems splitting "head/neck" and "Tail" generation as an user defined option. This sample can generate a full bendy-bones spine (from tail to head). If the distance from head to neck is more than 3 bones will automatically generate an ik-stretchy neck useful for animals rigs. The spine system features rotation isolation modes for head, neck and even the new tail.
<br style="clear:both;">
*'''Rigify Layers Redesign'''<br/> [[File:Addon_Rigify_0.5_BG.png|right|300px]][[File:Addon_Rigify_0.5_Layers.png|right|300px]] Rigify Layers - formerly used to define names in the UI - are now updated to support per-layer automatic creation of user defined bone groups and selection sets on the final rig.

<br style="clear:both;">
*'''Rig_UI automatic linking''' <br/>The generated rig_ui.py script is now automatically set as python controller for the armature rig forcing the script to be imported along with the armature when a rigify rig is linked into another blend file.
<br style="clear:both;">
*'''Advanced Generation Options'''<br/>[[File:Addon_Rigify_0.5_AdvGen.png|right|300px]] Rigify can now handle more than 1 rig generation per scene. Users can now select a specific target rig and python ui to be updated from the metarig generate panel. This feature also addresses the patch proposed in https://developer.blender.org/T51094.
<br style="clear:both;">
*'''Rigify Animation Tools'''<br/> When the add-on is enabled rigify rigs will benefit of a new animation tool panel that will allow the user to easily manage animation properties and switches on the whole animation or on a specific frame range. Rigify Animation Tools features are:<br/><br/>[[File:Addon_Rigify_0.5_AnimTools.png|right|300px]] 
**'''ik2fk pose''' <br/> Snaps the current IK pose to the FK pose and insert automatically a keyframe.
**'''ik2fk action''' <br/> Snaps the current IK pose to the FK pose and insert automatically a keyframe on each keyed frame within the selected frame range.
**'''fk2ik pose''' <br/> Snaps the current FK pose to the IK pose and insert automatically a keyframe.
**'''fk2ik action''' <br/> Snaps the current FK pose to the IK pose and insert automatically a keyframe on each keyed frame within the selected frame range.
**'''clear IK action''' <br/> Deletes all the inserted keyframes for the IK chain within the selected frame range.
**'''clear FK action''' <br/> Deletes all the inserted keyframes for the FK chain within the selected frame range.
**'''switch to pole''' <br/> Converts the keyframes animation from the rotational pole to the standard pole vector within the selected frame range.
**'''switch to rotation''' <br/> Converts the keyframes animation from the standard pole vector to the rotational pole within the selected frame range.
**'''Quaternion/Euler converter'''<br/> Converts any rotation order f-curves to the desired one (i.e. Quaternion > XYZ Euler).
<br style="clear:both;">


*'''New Metarigs'''<br/>Along with the 0.5 featureset a bunch of new metarigs are added to the armatures list:<br/><br/>[[File:Addon_Rigify_0.5_Animals.png|right|300px]]
**'''Basic Human''' <br/> A generic Human Meta-Rig, no fingers, no face.
**'''Basic Quadruped''' <br/> A generic Quadruped Meta-Rig including the new tail option in the spine.
**'''Cat''' <br/> A complete Cat Meta-Rig including claws, face bones and the new tail option in the spine.
**'''Wolf''' <br/> A complete Wolf/Dog Meta-Rig including claws, face bones and the new tail option in the spine.
**'''Horse''' <br/> A Horse Meta-Rig including the new tail option in the spine, hairs, tails and the bend&stretch ik neck option.
**'''Bird''' <br/> A basic Bird Meta-Rig including wings, feathers, claws, beak and the new tail option in the spine
**'''Shark''' <br/> A basic Shark Meta-Rig including fins, jaws, and the new tail option in the spine.<br/><br/>
[[File:Addon_Rigify_0.5_Armatures.png|center|300px]]
<br style="clear:both;">

*'''Bug Fixes'''
**fixed typo errors in various part of the old pitchipoy code
**fixed eyes bones/widgets size
**upgraded limb types to avoid the errors in legs generation when bones were not aligned https://developer.blender.org/T50500
**upgraded limb types to correctly show all animated properties in the graph editor as requested in https://developer.blender.org/T50511
**upgraded limb types to allow user defined foot orientation as requested in https://developer.blender.org/T50502
**All bone widgets shapes are now created as child of the 'WGTS' empty.
<br> <br>

----

===Ant Landscape===

[[File:Addon_ant_landscape.jpg|left|520px|thumb|Overview of the User Interface]]
[[File:Addon_ant_ui_presets.jpg|right|350px|thumb|Presets Overview]]
[[File:Addon_ant_screen.png|right|350px|thumb|Vertex Groups and Weights]]
<br style="clear:both;">
*'''New in A.N.T.Landscape v.1.8'''<br/>
**'''Landscape settings''' and refresh button are now available '''in the Properties shelf''' (View 3D > N).
**'''Presets''' for fast start creating various landscape/noise mesh.
**Use '''Blender Internal Texture Data Block - Texture Nodes''' as source for displacement.
**New '''Noise''' types/variations.

*'''Landscape Tools''' in Toolshelf (View 3D > T):<br/>
**'''Mesh Displace''': Displace selected mesh vertices along normal or x, y, z direction.
**'''Weight from Slope''': Generate vertex weight slope map from z normal value.
**'''Landscape Eroder''': Apply various kinds of erosion to a landscape mesh.
**Eroder also available in Weight Paint mode > Weights menu.
**ErosionR by: Michel Anders and Ian Huish

*Author: Jimmy Hazevoet
*Maintainer: Jimmy Hazevoet
<br style="clear:both;">
----

===Add Curve Extra Objects===
[[File:Addon_spline.jpg|right|600px]]
*'''New features'''
*Update Curves Galore > 2d Curve Profiles By Jimmy Hazevoet
*Redesign UI by meta-androcto
*Catenary by Jimmy Hazevoet
*Spirofit by Antonio Osprite, Jimmy Hazevoet
*Bounce Spline by Liero, Atom, Jimmy Hazevoet
*Celtic links by Adam Newgas
*Braid Knot by Jared Forsyth
*2d Curve objects by Spivak Vladimir
*Curly Curve by Cmomoney
*Bevel Taper Curve by Cmomoney


<br style="clear:both;">

----

===Viewport Pie Menus===
*'''New features'''
*General Clean up
*New split Snap/Origin pies to individual addons
*Fix for edit mode select menu usability
*Author: meta-androcto

<br style="clear:both;">

----
===Blender ID===

The Blender ID add-on was upgraded to version 1.3.0:

* Show a message after logging out.
* Store token expiry date in profile JSON.
* Show "validate" button when the token expiration is unknown.
* Urge the user to log out & back in again to refresh the auth token if it expires within 2 weeks.
* Added a method `validate_token()` to the public Blender ID Add-on API.
* Author: [[User:Sybren]]

<br style="clear:both;">

----

===Node Wrangler:===
[[File:Nodewrangler_principled_1.jpg|right|600px]]
[[File:Nw principled prefs.jpg |right|600px]]

The Node Wrangler add-on was updated to 3.35

An Operator for batch image import and auto texture setup for the new Principled BSDF was added.

Shortcut: ctrl-shift-T

===== Supported sockets =====

If the image files are sensibly named the following slots will be connected:
* Base Color
* Subsurface Color
* Metallic
* Specular
* Roughness
* Normal

For the roughness slot an invert node is added if the image is a glossy map (according to image name).
The normal slot will either add a normal node or a bump node according to image name.

===== Displacement maps =====
In addition if a displacement image is added a displacement setup is also added.

===== User Preferences =====
The tags for assigning textures are editable in the add-on preferences

<br style="clear:both;">

----

===MeasureIt===
[[File:MeasureIt_170_screenshot.png|right|250px]]

MeasureIt 1.70 update

* Redesign of Mesh Debug, Configuration, and N-panel settings layouts
* Added option to rotate text and measurements by angles specified in degrees
* Added option to horizontally align text to left, center, or right
* Added option to expand or collapse all Number panel measure settings
* Added options to Mesh Debug to display indexes of scene objects and mesh edges
* Added options to Mesh Debug to display object and vertex global locations
<br style="clear:both;">
----

===Themes===
[[File:Themes_north.jpg|right|800px]]

* new themes committed:
*North theme by TimoShch
*24x_blues by meta-androcto
*Dark Blue Gradient by olson
*Modern Minimalist by JonathanLampel
*SandySlate by Vicki
*True Blue Menu by meta-androcto
*Default++ by aermartin

*renamed softimage to Softblend (needs more work)
*removed ubuntu ambience to contrib & nightly builds.

*Thanks to participants here: 
* https://blenderartists.org/forum/showthread.php?416156-2-79-Final-Themes-competition!

* Result is 4 lightish themes, 4 darkish themes, 4 color themed themes & 4 loud or high contrast themes
Thanks. 
<br style="clear:both;">
----

==Addons Contrib==
*Addons contrib is the addons section in Blender nightly builds for developing addons.
*Most of the addons are very stable and several are being developed with the aim of including in Blender 2.8
*You can download a snapshot of all addons contrib addons here:
*[http://git.blender.org/gitweb/gitweb.cgi/blender-addons-contrib.git/snapshot/HEAD.tar.gz Download Snapshot (Add-ons Contrib)]
* You can manually install the addons by copy paste the addons_contrib folder to your Blender/2.79/scripts folder directory (the same folder as the Addons folder)
* This will install all of contrib at next startup.
* If you wish to install individual addons:
* The folder addons can be individually zipped & then use install from file button.
* The .py addons can use install from file button.
* there's some really good & new addons in addons contrib, it's well worth the effort of checking out.
<br style="clear:both;">

----

==New Behaviors==
*Panel & Tab Cleanup:
*Since 2.78 release, the Add-ons panels & Tabs have had a clean up
*Add-ons in 2.79 that use panels & create their own tabs now have better grouping & context sensitivity
*Panels are now only visible in the the correct contexts
*Add-ons that use Panels all have a function to change the panel location in the Add-ons preferences
*Add-ons that use Tabs have been restricted to use built in Tab names or to use similar ones as other add-ons
*A clear example of this is the "Display Tab" is activated by Measureit, Display Tools & 3d Navigation add-ons
*This has greatly reduced UI clutter with regards to Panels & Tabs with built in add-ons
<br style="clear:both;">

----

==Special Mentions==
*Several addons did not make it to Blender 2.79 release that deserve a special mention.
*These addons have been developed outside Blender addons but are significant and the developers have been active participants in Blender Addons Development.

*'''Ctools by Chromoly'''
** https://github.com/chromoly/ctools
** Includes Screencast keys addon
*'''Scripts by Dairin0d'''
** https://github.com/dairin0d/blender-scripts
*'''Sverchok'''
** https://github.com/nortikin/sverchok
*'''Animation Nodes'''
** https://github.com/JacquesLucke/animation_nodes
*'''Manuel Bastioni Lab'''
** https://blenderartists.org/forum/showthread.php?391401-Addon-Manuel-Bastioni-Lab-turns-Blender-in-a-laboratory-for-3d-humanoids-creation
*'''Mifth Tools'''
** https://github.com/mifth/mifthtools
*'''Asset Management''' by  pitiwazou: http://www.pitiwazou.com
** https://blenderartists.org/forum/showthread.php?391114-Addon-Asset-Management
*'''Hard Ops'''
** https://blenderartists.org/forum/showthread.php?387735-Hard-Ops-Thread
*These addons have been actively developed for several years & full credits to the authors for providing such inspirational works & tools.

*'''Special thanks to the Blender devs that made all this possible'''
** meta-androcto
** lijenstina
** mont29
** ideasman42
** italic
** the irc regulars
<br style="clear:both;">

----