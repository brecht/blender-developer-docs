= Blender 2.79: Mesh Modeling =

== Modifiers ==
[[File:Cycles2.79_surface_deformer_example.png|thumb|700px|center|Surface Deform example: Cloth simulation copied to an arbitrary mesh with rings as duplifaces]]
* New Surface Deform modifier: allow an arbitrary mesh surface to control the deformation of another, essentially transferring its motion/deformation. One great use for this is to have a proxy mesh for cloth simulation, which will in turn drive the motion of your final and more detailed mesh, which would otherwise not be suitable for simulation. See the [https://docs.blender.org/manual/en/dev/modeling/modifiers/deform/surface_deform.html surface deform documentation] for details.
[[File:Blender_Surface_Deform_Modifier.jpg|thumb|410px|center|Surface Deform Modifier User Interface]]
* Displace modifier with direction X/Y/Z/XYZ can now work in local or global space. ([https://projects.blender.org/blender/blender/commit/e0a34e963fff e0a34e963f])
* Displace modifier support for multi-threading.
* Mirror Modifier: add offsets for mirrored UVs.
<br style="clear:both;">

== Other Improvements ==

* Delete Unlocked Vertex Groups in vertex group list menu. ([https://projects.blender.org/blender/blender/commit/cf8f6d1dbcfc cf8f6d1dbc])
* Set custom normals from selected faces in the Normals section of Shading tools, Set Normals from Faces operator. ([https://projects.blender.org/blender/blender/commit/dd6fa94dc6 dd6fa94dc6])
* Improved center of mass calculation for mesh centers. ([https://projects.blender.org/blender/blender/commit/37bc3850cee 37bc3850ce])
* Sculpt dynamic topology constant detail is now a resolution value. ([https://projects.blender.org/blender/blender/commit/6271410 6271410])
* Mesh intersect has a new Cut separate mode, keeping each side of the intersection separate without splitting faces in half. ([https://projects.blender.org/blender/blender/commit/a461216885 a461216885])
* The Monkey primitive now has default UVs. ([https://projects.blender.org/blender/blender/commit/a070a5befa11 a070a5befa])
* Improved default UVs for UV Sphere and Icosphere. ([https://projects.blender.org/blender/blender/commit/a070a5befa11 a070a5befa])
* Screw Modifier now has remove doubles option (useful for closing off end-points) ([https://projects.blender.org/blender/blender/commit/584523e0adeb2663077602953f0d3288c4c60fe4 584523e0ad])