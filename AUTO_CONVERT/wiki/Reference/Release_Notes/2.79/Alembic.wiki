= Blender 2.79: Alembic =

The Alembic import & export has been improved, both in stability and compatibility (revisions [https://projects.blender.org/blender/blender/commit/56cfd3d5a7d 56cfd3d5a7]..[https://projects.blender.org/blender/blender/commit/c684fab8a6ea c684fab8a6])

==== Fixes ====

* Fixed errors in matrix conversion (most Alembic files use Y=up, while Blender uses the more sensible Z=up).
* Re-implemented part of the importer, so that it has to guess less, improving compatibility with non-Blender generated files (fixes [https://developer.blender.org/T50403 T50403])
* The importer is now more lenient to unknown types.
* Improved performance by removing some unnecessary computations.
* Prevented crash when cancelling import ([https://projects.blender.org/blender/blender/commit/3748ca432d2 3748ca432d] & [https://projects.blender.org/blender/blender/commit/43a910abce5 43a910abce]).
* Fixed "flat" import (i.e. all objects in world coordinates, no parenting) ([https://projects.blender.org/blender/blender/commit/642728b3395a 642728b339])
* Fixed [https://developer.blender.org/T50227 T50227]: Alembic uv export/load issue ([https://projects.blender.org/blender/blender/commit/699a3e2498 699a3e2498])
* Added simple unit tests ([https://projects.blender.org/blender/blender/commit/6af131fa5 6af131fa5])
* Greatly improved performance of curve/hair import ([https://projects.blender.org/blender/blender/commit/e12c3110024dc e12c311002])
* Changing cache modifier path no longer discards object paths ([https://projects.blender.org/blender/blender/commit/9c02990ac13a 9c02990ac1])
* Meshes with animation data are now saved on every frame ([https://projects.blender.org/blender/blender/commit/6715bfee92e72d 6715bfee92], [https://developer.blender.org/T51351 T51351]). This allows you to change a mesh procedurally, for example using a Python script, and have it exported every frame, simply by ensuring the mesh has some animation data on it.
* Support for sub-frame sampling ([https://projects.blender.org/blender/blender/commit/35f4abcf53dad 35f4abcf53] and [https://projects.blender.org/blender/blender/commit/24a0b332e2a72 24a0b332e2]).
* Fixed missing material assignment when an animated mesh changed vertex count ([https://projects.blender.org/blender/blender/commit/45d7513f84bd3f 45d7513f84]).
* Importing now takes the "inherit transform" flag into account correctly when constructing parent-child relationships ([https://projects.blender.org/blender/blender/commit/32edfd53d978b34 32edfd53d9] and [https://developer.blender.org/T52022 T52022]).
* Sequences of Alembic files are now handled properly ([https://projects.blender.org/blender/blender/commit/0900914d9644 0900914d96] and [https://developer.blender.org/T51820 T51820]).

==== New Features ====

* Exporting of linked-in dupli-groups is now possible. This supports the common character animation workflow used at the Blender Institute, where a character, its rig, and the custom bone shapes are all part of a group. This group is then linked into the scene, the rig is proxified and animated. Such a group can now be exported. Use "Renderable objects only" in the export options to prevent writing the custom bone shapes to the Alembic file. ([https://projects.blender.org/blender/blender/commit/ebb30451140 ebb3045114], [https://developer.blender.org/T50688 T50688])
* Exporting hair & particle systems is now optional. The export still takes more time with disabled hair than when there is no hair, as the particle systems themselves aren't disabled during the export. It's only the writing to the Alembic file that's skipped. ([https://projects.blender.org/blender/blender/commit/b148ac5cf77a869bc b148ac5cf7])
* The Import and Export operators now take a new flag <tt>as_background_job</tt>, which is <tt>True</tt> by default. When set to <tt>False</tt>, the operator will block Blender during the import. This allows Blender scripts to perform tasks after the import/export has completed. ([https://projects.blender.org/blender/blender/commit/2dac8b3ee043 2dac8b3ee0])
* Empties can now also be exported to Alembic (both static and animated) ([https://projects.blender.org/blender/blender/commit/9b3e3d4defc66 9b3e3d4def]).
* Exporting simple child hairs is now possible. ([https://projects.blender.org/blender/blender/commit/6ed15c5a411 6ed15c5a41])
* Added support for face-varying vertex colours (read: compatibility with Houdini) ([https://projects.blender.org/blender/blender/commit/7b25ffb618dd 7b25ffb618], [https://developer.blender.org/T51534 T51534])
* Support for empty meshes (which can get vertices later in their animation) ([https://projects.blender.org/blender/blender/commit/6715bfee92e72d 6715bfee92], [https://developer.blender.org/T51351 T51351])
* Loading an Alembic file stored in the obsolete HDF5 format will now give an error message telling you the format is not supported in Blender ([https://projects.blender.org/blender/blender/commit/9dadd5ff937 9dadd5ff93], [https://developer.blender.org/T51292 T51292]). Such files can be converted using [https://github.com/alembic/alembic/tree/master/bin/AbcConvert AbcConvert]. The current format, Ogawa, is [http://exocortex.com/blog/alembic_is_about_to_get_really_fast 4x to 25x faster than HDF5], and was [https://groups.google.com/forum/#!topic/alembic-discussion/FTG1HuuO_qA introduced almost four years ago].