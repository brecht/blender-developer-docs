= User Interface =

== General ==
* Input Placeholders to show a hint about the expected value of an input. ([https://projects.blender.org/blender/blender/commit/b0515e34f9 b0515e34f9]).
[[File:Placeholders.png|662px|thumb|center]]
* Cryptomatte picking can now occur between separate windows. ([https://projects.blender.org/blender/blender/commit/9ee5de05c0 9ee5de05c0]).
* The UI interface font can now be shown in any weight. ([https://projects.blender.org/blender/blender/commit/0bde01eab5 0bde01eab5]).
[[File:UIFontweight.png|662px|thumb|center]]
* Khmer font added to support a new translation of that Cambodian language. ([https://projects.blender.org/blender/blender/commit/3f5654b491 3f5654b491]).
* New icons added to represent area splitting, joining, and swapping. ([https://projects.blender.org/blender/blender/commit/153dd76d22 153dd76d22], [https://projects.blender.org/blender/blender/commit/8933284518 8933284518]).
* Wide Enum lists will now collapse to a single column if not enough space. ([https://projects.blender.org/blender/blender/commit/83ce3ef0db 83ce3ef0db]).
* Changing UI font in Preferences will now start in your OS Fonts folder. ([https://projects.blender.org/blender/blender/commit/d3a2673cb8 d3a2673cb8]).
* File Browser List View removes columns and reformats as width is decreased. ([https://projects.blender.org/blender/blender/commit/07820b0703 07820b0703]).
* Improved Color Picker cursor indication and feedback. ([https://projects.blender.org/blender/blender/commit/c11d5b4180 c11d5b4180]).
[[File:Colorpicker.png|532px|thumb|center]]
* Outliner Context Menu "Show Hierarchy" and "Expand/Collapse All". ([https://projects.blender.org/blender/blender/commit/4793b4592b 4793b4592b], [https://projects.blender.org/blender/blender/commit/f815484e7d f815484e7d]).
* Text Object fonts now look in the fallback stack when characters are not found. ([https://projects.blender.org/blender/blender/commit/604ee2d036 604ee2d036]).
[[File:Textobjectfallback.png|840px|thumb|center]]
* Animation marker drawing improvements. ([https://projects.blender.org/blender/blender/commit/0370feb1bf 0370feb1bf]).
* Improved corner rounding for menus and popup blocks. ([https://projects.blender.org/blender/blender/commit/42ddc13033 42ddc13033]).
[[File:Menurounding.png|495px|thumb|center]]
* Improved quality of menu and popup block shadows. ([https://projects.blender.org/blender/blender/commit/0335b6a3b7 0335b6a3b7]).
* Improved initial display of compositor node trees. ([https://projects.blender.org/blender/blender/commit/ff083c1595 ff083c1595]).
* New Text Objects will use translated "Text" as default. ([https://projects.blender.org/blender/blender/commit/5e38f7faf0 5e38f7faf0]).
[[File:TranslatedText.png|800px|thumb|center]]
* Eyedropper can now pick colors outside the Blender window on Mac. ([https://projects.blender.org/blender/blender/commit/639de68aaa 639de68aaa]).

== Viewport ==

* Walk mode now supports relative up/down (using R/F keys) ([https://projects.blender.org/blender/blender/commit/c62009a6ac961e199285dee0d7b5037132a067a7 c62009a6ac]).
* Improved Mesh Edge Highlighting. ([https://projects.blender.org/blender/blender/commit/dfd1b63cc7 dfd1b63cc7]).