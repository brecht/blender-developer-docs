= EEVEE & Viewport =

== EEVEE-Next ==

EEVEE has been fully rewritten to take advantage of modern GPU features, and to add new feature like screen space global illumination and displacement. Lighting and shading should match Cycles more closely now.

Release notes and manual still needs to be updated. See issue [https://projects.blender.org/blender/blender/issues/93220 #93220] for a technical overview of the changes.

'''Lights'''

A scene can now use many lights. It used to be limited to 8 sun lights and 128 non sun-lights. The maximum number is 65336 lights after culling. This includes all light types.

'''Light probes'''

Probe types have been renamed:
* Reflection Cubemap -> Sphere
* Reflection Plane -> Plane
* Irradiance Grid -> Volume

By changing the pool size more than 64 active irradiance volumes can be supported. Improved performance when using light probe planes.

Viewport Display options are now per-probe instead of per-scene.

'''Indirect lighting'''

Volumetrics receives light from irradiance volumes and world diffuse lighting.

'''Shadows'''

EEVEE uses SVM for shadow mapping allowing shadow to be cast by many lights.

'''Ambient Occlusion'''

Over- and under-shadowing can be controlled globally.

'''Raytracing'''

Screen space reflection and refractions are done as part of the new raytracing pipeline. Multiple BSDF can emit screen space reflections

'''Motion Blur'''

Motion blur is available in the viewport and viewport renders.

'''Materials'''

* Reflective materials and materials using ambient occlusion are now compatible with refractive surfaces.
* Correct location of refractions in reflections.
* Object volume shaders now consider the object mesh.
* Improved performance for shaders that use multiple BSDFs.
* Subsurface: Support for variable radius has been added.
* Light Path: Is Camera, Is Shadow, Is Diffuse and Is Glossy are supported outside a Volume Probe
* Displacement and vector displacement is now supported.
* Emission can be used as direct light source.

'''Emissive materials'''