= Blender 2.82: User Interface =

== Fallback Tools ==

* Blender now includes the concept of 'Fallback Tools', which has implications about how tools and gizmos work. Now, when you have a tool active, you can still easily box, lasso or circle select when you click and drag outside the gizmo. Choose what happens when you drag in the viewport using this new popover:

[[File:Fallback Tools.png|center|Fallback tools popover]]

== Many more tool gizmos ==

* Many more tools now have gizmos, so now it's easy to use gizmos for interacting with the tools, but click and drag outside the gizmo to perform selection actions.

[[File:Bevel gizmo.png|center|Bevel tool now has a gizmo]]

Modeling tools like Bevel, Inset, Smooth, Randomize, Extrude Individual, Shrink/Fatten, Rip and more all have gizmos now.

== UV Gizmos ==

* The UV Editor now supports gizmos for the transform tools, just like the 3D View. This is especially important for users of the Industry Compatible keymap
[[File:UV Gizmos.png|center|UV gizmos now appear in the UV Editor]]

== Custom Face Orientation colors ==
* We now support custom Face Orientation Colors, defined in the theme  

[[File:Screenshot 2020-01-23 at 12.37.04.png|center]]

([https://projects.blender.org/blender/blender/commit/9c1134015c3 9c1134015c])


== Brush Settings overhaul ==
Now, the brush settings in Blender are grouped differently, so that it's now possible to see which properties relate to the active ''brush'' vs items that are simply global options that affect the ''mode'' in entirety. 

Many changes to the layout and ordering were done to make the brush controls more easy to navigate.

Before:
[[File:Brushprops before.jpg|center|Before]]

After:
[[File:Brushprops after.jpg|center|After]]

([https://projects.blender.org/blender/blender/commit/fb74dcc5d69 fb74dcc5d6])


== Graph Editor selection and transform overhaul==
The way selecting and transforming keyframes & handles in the Graph Editor works has been overhauled to match how the Dopesheet, Node Editor, Sequencer and NLA work.

Now, when you have several keyframes selected, dragging on one of them will move all selected heys or handles. 

([https://projects.blender.org/blender/blender/commit/b037ba2665f4 b037ba2665])

== Industry Compatible keymap ==
The included Industry Compatible keymap was updated to include the following fixes and changes: 

* Support tool cycling for all tool shortcuts 
* Add support for the context menu PC keyboard key 
* Support 1-4 keys for switching selection modes in the UV Editor 
* Support 1-3 keys in the UV Editor when Sync Selection is enabled
* Fix Tweak tool in UV Editor not working well
* Fix some issues with UV editor Box Select tool
* Fix paint mode context menus  
* Fix conflict between Scale Cage and Insert Keyframe 
* Support scroll wheel zooming also while Alt is held 
* Use consistent shortcuts for sculpt mode masking  
* Support the recently added Drag option 
* Properly support Grease Pencil Edit Mode 
* Fix Polybuild tool not working with this keymap 
* Add Remesh shortcuts (Ctrl-R & Ctrl-Alt-R)


== Preferences ==

* When Developer Extras are enabled in the Interface tab, a new Preferences experimental tab is shown. It contains non-production ready features that need feedback and design iterations. ([https://projects.blender.org/blender/blender/commit/a7fcd78d2d6 a7fcd78d2d])

[[File:Preferences Virtual Reality Examples.png|center]]


== Miscellaneous ==

* Alt-Click and Copy To Selected are now supported for Python properties located inside a Property Group attached to a bone, e.g. Rigify metarig parameters. ([https://projects.blender.org/blender/blender/commit/b3f388dca9 b3f388dca9])

* Various small tweaks to how the Text Editor looks ([https://projects.blender.org/blender/blender/commit/8c6ce742391 8c6ce74239])

* Tooltips were added for all modifiers ([https://projects.blender.org/blender/blender/commit/b50853611e8 b50853611e])

* Rename 'Toggle Markers' to 'Show Markers' ([https://projects.blender.org/blender/blender/commit/8b260fec0c7 8b260fec0c])

* Updated icons, including new bespoke icons for Top Bar, Status Bar, CD/DVD drives, Home, Documents, Temp, Memory, Options and many others ([https://projects.blender.org/blender/blender/commit/cf95d7f10eb cf95d7f10e])

* The File Browser now uses special new icons for Volumes and System Lists ([https://projects.blender.org/blender/blender/commit/7c2217cd126 7c2217cd12])

* Tool cycling shortcuts in the toolbar are now visible in the tooltips ([https://projects.blender.org/blender/blender/commit/7f36db35cee 7f36db35ce])

* Scrollbars have a reduced width in UI-lists to match other scrollbars in Blender ([https://projects.blender.org/blender/blender/commit/J85cf56ecbc6 J85cf56ecb])

* Use property split layout for absolute shape keys ([https://projects.blender.org/blender/blender/commit/e653f8fbb12 e653f8fbb1])