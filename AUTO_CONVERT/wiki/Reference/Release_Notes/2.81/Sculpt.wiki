= Blender 2.81: Sculpt and Retopology =

== Sculpting ==

=== Tools ===

* Pose brush: pose a model simulating an armature-like deformation. The pivot point for rotation is calculated automatically based on the radius of the brush and the topology of the model.
* Translate, Rotate, Scale tools: transform around a pivot point, also taking into account symmetry.
* Elastic Deform brush: grab, biscale grab, triscale grab, scale and twist operations that preserve volume. All deformation modes are accessible under the same tool. Based on the "[https://graphics.pixar.com/library/Kelvinlets/paper.pdf Regularized Kelvinlets: Sculpting Brushes based on Fundamental Solutions of Elasticity]" paper.
* Draw Sharp brush: Similar to the draw brush, but it deforms the mesh from the original coordinates. When used with the sharper curve presets it has a much more pleasant crease/cut behavior than any of the other brushes. It is useful for creating cloth wrinkles, stylized hair or hard surface edges.
* Mesh Filter tool: applies a deformation to all vertices in the mesh at the same time. It includes multiple deformation modes and the option to lock the deformation axis.

=== Brush Settings ===

* Sculpt cursor follows the surface normal and displays symmetry.
* Normal radius setting for sculpt tools, to control how sensitive the sculpt normal direction is to the underlying surface.
* World spacing option for brush strokes, to keep an even distance between stroke points in 3D space.
* Automasking: assigns a factor to each vertex before starting the stroke. Topology automasking affects only vertices connected to the active vertex.
* Grab active vertex snaps the maximum strength of the grab brush to the highlighted active vertex, making it easier to manipulate low poly models or meshes with subdivision surfaces.
* Dynamic Mesh Preview displays the edges of the mesh inside the brush radius. This helps to visualize the real geometry the user is manipulating from sculpt mode when there are active modifiers.

=== Masking ===

* Mask Filter: modifies the whole paint mask. In includes multiple operations like smooth, grow or contrast accessible from a pie menu.
* Dirty Mask generator: similar to Dirty Vertex Colors, but it generates a paint mask. It can be used to mask cavities in the sculpt.
* Mask Expand: creates a mask from the active vertex to the vertex under the cursor based on topology or mesh curvature
* Mask Extract: extracts the paint mask to a new mesh object. It can extract the paint mask creating a boundary loop in the geometry, making it ready for adding a subdivision surface modifier.

== Poly Build ==

The poly build tool was improved for retopology.

* Click and drag from a boundary edge extrudes a new quad.
* Click and drag on vertices tweaks the position.
* Ctrl + click adds geometry, with geometry preview and automatic creation of quads.
* Shift + click deletes mesh elements (faces or vertices).

== Remeshing ==

Two remesh operators were added, to create a new quad based mesh from an existing mesh. This is especially useful for sculpting, to generate better topology after blocking out the initial shape. They are available from the mesh properties, and in the sculpt tool settings.

* Voxel Remesh: creates a mesh with even face size and resolves issues with intersections, by converting to a 3D volume and back. It can be used as an alternative to dynamic topology without the performance cost of continuous updates.
* QuadriFlow Remesh: creates a quad mesh with few poles and edge loops following the curvature of the surface. This method is relatively slow but generates higher quality for final topology.