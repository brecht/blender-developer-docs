= Core =


== Overriding Resource Directories ==

It is now possible to override all `USER` and `SYSTEM` paths using environment variables:
* `BLENDER_USER_RESOURCES` (accessed via `bpy.utils.resource_path('USER')`).
* `BLENDER_SYSTEM_RESOURCES` (accessed via `bpy.utils.resource_path('SYSTEM')`).

Previously, sub-directories could be overridden via environment variables such as `BLENDER_USER_CONFIG` and `BLENDER_SYSTEM_PYTHON` (see the `--help` message for a full list). However there was no way to override the top level directories which scripts could access via `bpy.utils.resource_path`. Now the top-level directories can be overridden so the default USER or SYSTEM resource paths aren't used by accident ([https://projects.blender.org/blender/blender/commit/bf4926b30c6fc7b9f98dde508b7b644feaf21022 bf4926b30c]). 

== Fonts ==

* New stack of fonts for improved language and symbol coverage ([https://projects.blender.org/blender/blender/commit/e9bd6abde37c e9bd6abde3]).
* FreeType caching to allow more simultaneous fonts with less resources and increased performance. ([https://projects.blender.org/blender/blender/commit/d39abb74a0a9 d39abb74a0])
* Avoid loading fonts that are not actually used ([https://projects.blender.org/blender/blender/commit/c0845abd897f c0845abd89]).
* Font sizing now always assumes 72 DPI, simplifying code. DPI argument to blf.size() is now optional and deprecated. ([https://projects.blender.org/blender/blender/commit/cd1631b17dd0 cd1631b17d])
* Gamma correction for text output for improved anti-aliasing. Result is also ''slightly'' fuller and brighter. ([https://projects.blender.org/blender/blender/commit/d772e11b5a1e d772e11b5a])

== Performance ==

* Create preview thumbnails of WebP images a little quicker while using much less RAM. ([https://projects.blender.org/blender/blender/commit/8851790dd733 8851790dd7])
* View layer sync operations are now postponed until the sync results are needed. This greatly speeds up scripts that create many objects in a single operation ([https://projects.blender.org/blender/blender/commit/68589a31eb  rB68589a31]).
* Depsgraph: Optimize evaluation of dependencies of disabled modifiers ([https://projects.blender.org/blender/blender/commit/f12f7800c29 f12f7800c2])

== Others ==

* Metaball objects are now evaluated as meshes. Render engines now only need to process the corresponding evaluated mesh object and can skip the evaluated metaball object. ([https://projects.blender.org/blender/blender/commit/eaa87101cd5a  rBeaa87101]).
* Support extracting frames from WebM videos that dynamically change resolution. ([https://projects.blender.org/blender/blender/commit/d5554cdc7c90 d5554cdc7c]).
* Video rendering: Support FFmpeg AV1 codec encoding. ([https://projects.blender.org/blender/blender/commit/59a0b49c100b8444a15f4713409004eade9fd321  rB59a0b49c]).
* libOverride: RNA API: Add option to make all overrides editable in `override_hierarchy_create` ([https://projects.blender.org/blender/blender/commit/a67b33acd04ea48  rBa67b33ac]).
* Support for the Wayland graphics system is now enabled for Linux ([https://projects.blender.org/blender/blender/commit/f9ab2214ae52c51c068ceff97e5264fd518d8f59 f9ab2214ae]).