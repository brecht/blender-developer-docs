= Modeling = 

== Modifiers ==
* The Subdivision Surface modifier and node's performance has been improved when many loose edges are subdivided. ([https://projects.blender.org/blender/blender/commit/12c235a1c515d4 12c235a1c5])

= UV Editing =

== Sculpt Tools ==
[[File:Geometry.png|thumb|Show Geometry driven relax option]]
The big new change in the UV editor for Blender 3.4 is the addition of a new geometry-based relax brush. This improves the quality of the UV mapping by making the UVs more closely follow the 3D geometry. As this is a brush, the user can drive the relaxation process. ([https://projects.blender.org/blender/blender/commit/74ea0bee9c0a 74ea0bee9c])

To support the new relax brush mode, there has been additional UV Sculpt and UV Relax tool improvements:
* Fix boundary edges for ''UV Relax'' tool. ([https://projects.blender.org/blender/blender/commit/3c351da89f70 3c351da89f])
* UV Sculpt tools now respect pinned vertices. ([https://projects.blender.org/blender/blender/commit/3c351da89f70 3c351da89f])
* UV Sculpt tools now work with ''constrain to bounds''. ([https://projects.blender.org/blender/blender/commit/66822319d3ee 66822319d3])
* UV Sculpt tools now ignore winding, preventing orphaned islands. ([https://projects.blender.org/blender/blender/commit/dcf50cf04668 dcf50cf046])
* UV Grab tool now supports ''Live Unwrap''. ([https://projects.blender.org/blender/blender/commit/836c07f29c76 836c07f29c])

== UV Packing ==
* Specify exact margin when packing. ([https://projects.blender.org/blender/blender/commit/c2256bf7f714 c2256bf7f7])
* Add option to use Blender 2.8 margin calculation. ([https://projects.blender.org/blender/blender/commit/c2256bf7f714 c2256bf7f7])
* Many UV Packing operations now work with non-manifold geometry. (many commits)

== UV Grid and Selection ==
* Allow non-uniform grids. ([https://projects.blender.org/blender/blender/commit/b7decab07ef8 b7decab07e])
* Add option to use pixel spacing for UV Grid. (Also [https://projects.blender.org/blender/blender/commit/b7decab07ef8 b7decab07e])
* Show UV grid over the top of the image. ([https://projects.blender.org/blender/blender/commit/c50335b359e0 c50335b359])
* Rename "UV Snap To Pixels" -> "UV Round To Pixels". ([https://projects.blender.org/blender/blender/commit/b5115ed80f19 b5115ed80f])

== New Operators ==
Additional new operators have been added affecting island rotation:
* Add new operator, ''Randomize Islands''. ([https://projects.blender.org/blender/blender/commit/de570dc87ed1 de570dc87e])
* Add new operator, ''UV Align Rotation''. ([https://projects.blender.org/blender/blender/commit/20daaeffce4c 20daaeffce])

== Other Improvements and Fixes ==
* Rotation operator supports ''constrain to bounds''. ([https://projects.blender.org/blender/blender/commit/d527aa4dd53d d527aa4dd5])
* Respect UV Selection in ''Spherical Project'', ''Cylindrical Project'', ''Cube Project'' and ''Smart UV Project''.([https://projects.blender.org/blender/blender/commit/a5c696a0c2b9 a5c696a0c2])
* Fix UV Island calculation with hidden faces. ([https://projects.blender.org/blender/blender/commit/8f543a73abc4 8f543a73ab])
* Fix bugs in UV Island calculation when in edge selection mode. ([https://projects.blender.org/blender/blender/commit/178868cf4259 178868cf42])
* More options for ''UV Select Similar'' operator, ''Face'', ''Area'' and ''Area UV''. ([https://projects.blender.org/blender/blender/commit/a5814607289a a581460728])
* Fix ''UV Unwrap'' with degenerate triangles. ([https://projects.blender.org/blender/blender/commit/94e211ced914 94e211ced9])