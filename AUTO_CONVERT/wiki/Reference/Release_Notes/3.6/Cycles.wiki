= Cycles =

== GPU Rendering ==

=== AMD ===

Experimental support for AMD hardware ray-tracing acceleration on Windows, using [https://gpuopen.com/hiprt/ HIP RT]. ([https://projects.blender.org/blender/blender/commit/557a245 557a245])

* This improves performance on GPUs with ray tracing acceleration - RX 6000 and RX 7000 series, as well as W6000 and W7000 series workstation GPUs.
* Driver version [https://www.amd.com/en/support/kb/release-notes/rn-rad-win-22-40-51-06-blender-3-6-beta 22.40.51.06 for Blender 3.6 Beta] needs to be installed, along with enabling HIP RT in Preferences > System.
* Known limitations:
** No Linux support, as HIP RT is Windows only still.
** Degenerate triangles may causes crashes or poor performance.
** Shadows in hair are not rendering accurately.

AMD GPUs now also support light trees. ([https://projects.blender.org/blender/blender/commit/d5757a0 d5757a0])

=== Intel ===

Hardware ray-tracing acceleration for Intel® Arc™ and Data Center GPUs, using [https://www.embree.org/ Embree 4]. ([https://projects.blender.org/blender/blender/commit/3f8c995109 3f8c995109])

{| class="transparent" style="text-align: center"
|[[File:Cycles 3.6 Intel A770.png|800px|Render time per sample on an Intel® Arc™ A770 GPU|thumb|center]]
||
{| class="wikitable"
|-
! Scene !! With HW RT || Without HW RT
|-
| barbershop_interior || 0.192991 || 0.223141
|-
| bmw27 || 0.013019 || 0.015585
|-
| classroom || 0.112879 || 0.139789
|-
| fishy_cat || 0.014602 || 0.024707
|-
| junkshop || 0.101691 || 0.119027
|-
| monster || 0.057748 || 0.067487
|-
| pabellon || 0.045524 || 0.066214
|-
| sponza || 0.023778 || 0.031841
|}

 |}

* Known limitations:
** During their first use with oneAPI device, Ambient Occlusion and Bevel nodes will trigger a GPU binaries recompilation that will use around 9GB of memory and take several minutes. Improvements on this may come from future GPU drivers.
** On Windows, when using drivers < 101.4644, embree on GPU may crash when rendering duration goes beyond 3 minutes.

=== Apple ===

Apple Silicon GPUs now support NanoVDB for Metal, reducing memory usage of volumes. ([https://projects.blender.org/blender/blender/commit/02c2970983 02c2970983])

== Performance ==

* Light trees now use less memory and are faster to build, through instancing and multi-threading. ([https://projects.blender.org/blender/blender/commit/bfd1836861 bfd1836861], [https://projects.blender.org/blender/blender/commit/23c5e0693253516a51906588a361bb637ffd391b 23c5e06932])
* Loading large geometries into Cycles is much faster, meaning rendering can start more quickly after geometry changes or switching to rendered view.
** Loading large meshes 4-6x faster, and mesh attributes are copied up to 10x faster (or even 60x faster for an extreme example with UV maps) ([https://projects.blender.org/blender/blender/commit/4bcd59d644c4cc50692e60899403e3f60ac409de 4bcd59d644], [https://projects.blender.org/blender/blender/commit/8d0920ec6dcd3fc7fc6b3d7dd76872801dcefa29 8d0920ec6d]).
** Loading point clouds can be 9x faster ([https://projects.blender.org/blender/blender/commit/aef0e72e5acccf5f86cabe17dc49d7af473901e9 aef0e72e5a]).
** Loading curves can be 10x faster ([https://projects.blender.org/blender/blender/commit/ae017b3ab7fb119d8a3327f880f74ce2c31a57f9 ae017b3ab7]).

== Other Improvements ==
* OSL: support for new standard microfacet closures from MaterialX (`dielectric_bsdf`, `conductor_bsdf`, `generalized_schlick_bsdf`).
* Byte color attributes are now supported for point clouds and curves ([https://projects.blender.org/blender/blender/commit/5a86c4cc886ef25bf6f1fd61b5b9c706bba93de0 5a86c4cc88]).
* Improved Fresnel handling of the Glass BSDF for better energy preservation and accuracy of results at high roughness. ([http://developer.blender.org/D17149 D17149])
* Bump mapping for diffuse surfaces was improved. ([https://projects.blender.org/blender/blender/pulls/105776 PR #105776])