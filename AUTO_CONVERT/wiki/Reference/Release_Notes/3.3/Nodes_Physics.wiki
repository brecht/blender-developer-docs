= Hair System =
* A new hair workflow is possible with the new curves object
** [https://code.blender.org/2022/07/the-future-of-hair-grooming The Future of Hair Grooming]
** [https://docs.blender.org/manual/en/3.3/modeling/curves/primitives.html#empty-hair User Manual: Hair Primitive]
** [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/introduction.html User Manual: Curves Sculpting]

[[File:Einar hair video thumbnail.jpg|700px|center|thumb]]

=== Sculpt Mode ===
[[File:Curves Sculpt Brush Icons 3.3.png|thumb|right]]
The new curves object type has a sculpt mode meant mostly for hair grooming
* Sculpting can be done on top of curves deformed with geometry nodes ([https://projects.blender.org/blender/blender/commit/1f94b56d774 1f94b56d77]).
* Curves sculpt mode supports selecting control points or curves to limit the effect of actions ([https://projects.blender.org/blender/blender/commit/a1830859fa9 a1830859fa]).
** An overlay dims any deselected curves or points ([https://projects.blender.org/blender/blender/commit/e3ef56ef914 e3ef56ef91]).
** A few new selection operators adjust the soft selection in sculpt mode: ''Select Random'', ''Select Endpoints'', and ''Grow/Shrink'' ([https://projects.blender.org/blender/blender/commit/e3ef56ef914 e3ef56ef91]).
** The spreadsheet editor supports filtering by selected curves or points in sculpt mode ([https://projects.blender.org/blender/blender/commit/396b7a6ec8f 396b7a6ec8]).
* Sculpt mode supports symmetry on the X, Y and Z axes ([https://projects.blender.org/blender/blender/commit/7dc94155f62 7dc94155f6]).
* Several sculpt brushes are available
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/selection_paint.html Selection Paint] brush edits the selection mask ([https://projects.blender.org/blender/blender/commit/a1830859fa9 a1830859fa]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/add_curves.html Add] brush adds new curves, interpolating the length, shape, and point count from neighbors ([https://projects.blender.org/blender/blender/commit/ac45540a348 ac45540a34], [https://projects.blender.org/blender/blender/commit/8852191b779 8852191b77], [https://projects.blender.org/blender/blender/commit/dbba5c4df97 dbba5c4df9]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/delete_curves.html Delete] brush removes curves ([https://projects.blender.org/blender/blender/commit/3f91e7d7792 3f91e7d779], [https://projects.blender.org/blender/blender/commit/f0f44fd92f1 f0f44fd92f]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/density_curves.html Density] brush adds or removes curves based on a density ([https://projects.blender.org/blender/blender/commit/416aef4e13c 416aef4e13]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/comb_curves.html Comb] brush moves points, preserving segment lengths ([https://projects.blender.org/blender/blender/commit/10c11bb8973 10c11bb897]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/snake_hook_curves.html Snake Hook] brush grabs points and moves them without preserving length ([https://projects.blender.org/blender/blender/commit/fced604acfc fced604acf]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/grow_shrink_curves.html Grow/Shrink] brush makes curves longer or shorter ([https://projects.blender.org/blender/blender/commit/190334b47df46f 190334b47d]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/pinch_curves.html Pinch] brush moves control points closer together ([https://projects.blender.org/blender/blender/commit/416aef4e13c 416aef4e13]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/puff_curves.html Puff] brush rotates curves to match the surface normal ([https://projects.blender.org/blender/blender/commit/416aef4e13c 416aef4e13]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/smooth_curves.html Smooth] brush makes curves straighter ([https://projects.blender.org/blender/blender/commit/416aef4e13c 416aef4e13]).
** The [https://docs.blender.org/manual/en/3.3/sculpt_paint/curves_sculpting/tools/slide_curves.html Slide] brush moves curves along the surface  ([https://projects.blender.org/blender/blender/commit/416aef4e13c 416aef4e13], [https://projects.blender.org/blender/blender/commit/1f94b56d774 1f94b56d77]).

=== Surface Attachment ===
* Curves can be attached to a mesh surface with UV coordinates and a UV map ([https://projects.blender.org/blender/blender/commit/899ec8b6b80 899ec8b6b8], [https://projects.blender.org/blender/blender/commit/12722bd3545 12722bd354]).
** Operators are available in sculpt mode to snap curves to the nearest or deformed mesh surface ([https://projects.blender.org/blender/blender/commit/50869b408b20f6 50869b408b]).
** ''Note: Many tools require a UV map to function. The quality of the map does not matter, but it must not have overlapping faces.''
* ''Add Empty Hair'' in the add menu creates curves with the active object as a surface and its active UV map for attachment ([https://projects.blender.org/blender/blender/commit/434521a9e2cc41 434521a9e2], [https://projects.blender.org/blender/blender/commit/2232855b50e 2232855b50]).

=== Nodes ===
* The curves object supports the geometry nodes modifier, which can be applied. All existing curve nodes are supported ([https://projects.blender.org/blender/blender/commit/96bdd65e740 96bdd65e74]).
** A new geometry node deforms curves based on the deformation of the surface mesh they are attached to ([https://projects.blender.org/blender/blender/commit/05b38ecc783 05b38ecc78]).
*** ''Note: The node will be generalized in the future by exposing more inputs or turning it into a node group.''

=== Rendering ===
* Curves are rendered like hair particles, with support for Workbench, EEVEE, and Cycles.
* The object mode selection outline works for curves objects ([https://projects.blender.org/blender/blender/commit/a8f7d41d389 a8f7d41d38]).
* Curves can be converted to and from a hair particle system, and from the old curve type ([https://projects.blender.org/blender/blender/commit/94a54ab5549 94a54ab554], [https://projects.blender.org/blender/blender/commit/3656e66ac24d1c 3656e66ac2], [https://projects.blender.org/blender/blender/commit/edc92f779ec102 edc92f779e]).
* ''Note: Rendering the new curves object is not fully supported yet. For example, curves are always rendered as Catmull Rom curves. More information can be found [https://developer.blender.org/T96455 here].''

= Geometry Nodes =

=== Performance Improvements ===

* Curve nodes have been ported to the new curves type, with performance improvements anywhere from 3x to over 10x.
** The ''Set Spline Type'' node can also skip processing when all curves have the correct type already ([https://projects.blender.org/blender/blender/commit/9e393fc2f125  rB9e393fc2]).
** ''Fillet Curve'' ([https://projects.blender.org/blender/blender/commit/2551cf908732ba 2551cf9087]).
** The ''Subdivide Curve'' node supports Catmull Rom curves now as well ([https://projects.blender.org/blender/blender/commit/9435ee8c65193e 9435ee8c65]).
* The ''Separate XYZ'' and ''Separate Color'' nodes can be over 20% faster when not all outputs are used ([https://projects.blender.org/blender/blender/commit/b8bd304bd453  rBb8bd304b], [https://projects.blender.org/blender/blender/commit/04734622415521 0473462241]).
* The ''UV Sphere'' primitive is parallelized, with a 3.6x improvement observed for high resolutions ([https://projects.blender.org/blender/blender/commit/7808ee9bd73 7808ee9bd7]).
* Evaluating the input field in the ''Capture Attribute'' is skipped if the output is not needed ([https://projects.blender.org/blender/blender/commit/c980ed27f09 c980ed27f0]).

=== General ===
* The '''Interpolate Domain''' node allows evaluating a field on a different domain ''inside'' of another field, avoiding the need for a separate "Capture Attribute" node ([https://projects.blender.org/blender/blender/commit/ba1e97f1c6cd0c ba1e97f1c6], [https://projects.blender.org/blender/blender/commit/ea4b1d027d97 ea4b1d027d]).

=== Meshes ===
* The '''UV Unwrap''' and '''Pack UV Islands''' nodes open the possibility of creating and adjusting UV maps directly inside of geometry nodes ([https://projects.blender.org/blender/blender/commit/4593fb52cf809d 4593fb52cf]).
* The '''Mesh To Volume''' node acts like the existing modifier of the same name ([https://projects.blender.org/blender/blender/commit/1516f7dcde34a4 1516f7dcde], [https://projects.blender.org/blender/blender/commit/0ea282f7462070 0ea282f746]).
* Three new nodes can be used for path-finding across mesh edges.
** The '''Shortest Edge Paths''' node finds the best path along edges from every vertex to a set of end vertices ([https://projects.blender.org/blender/blender/commit/c8ae1fce6024  rBc8ae1fce]). 
** The '''Edge Paths to Curves''' node generates a separate curve for every edge path from a set of start vertices. The path information usually generated by the "Shortest Edge Paths" node ([https://projects.blender.org/blender/blender/commit/c8ae1fce6024  rBc8ae1fce]).
** The '''Edge Paths to Selection''' node generates an edge selection that includes every edge that is part of an edge path ([https://projects.blender.org/blender/blender/commit/c8ae1fce6024  rBc8ae1fce]).
* The ''Mesh Boolean'' node now has an output field that gives just the intersecting edges ([https://projects.blender.org/blender/blender/commit/e2975cb70169  rBe2975cb7]).

=== Instances ===
* New '''Instance Scale''' and '''Instance Rotation''' nodes provide access to instance transformations ([https://projects.blender.org/blender/blender/commit/3a57f5a9cff5  rB3a57f5a9], [https://projects.blender.org/blender/blender/commit/14fc89f38f0e  rB14fc89f3]).

=== Primitive Nodes ===
* The '''Volume Cube''' primitive node allows sampling a field in a dense bounding box to create an arbitrary volume grid ([https://projects.blender.org/blender/blender/commit/838c4a97f  rB838c4a97]).
* The '''Points''' node creates any number of point cloud points, with position and radius defined by fields ([https://projects.blender.org/blender/blender/commit/9a0a4b0c0d452d 9a0a4b0c0d]).


= Physics =

* Effector forces on rigid bodies are independent of simulation substeps again ([https://projects.blender.org/blender/blender/commit/f4d8382c86ba760c5fccf7096ca50d6572b208bd  rBf4d8382c]).
: This has been a regression since 2.91, so the fix may affect user files with combinations of effectors and rigid bodies. Forces can be scaled down inversely to substeps to maintain the effect. If the scene uses 10 substeps for rigid body simulation (the default), effector force should be scaled by 0.1.