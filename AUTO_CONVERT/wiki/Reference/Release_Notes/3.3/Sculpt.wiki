= Sculpt, Paint, Texture = 

== UDIM ==
Packing UDIM texture sets into .blend files is now supported. ([https://projects.blender.org/blender/blender/commit/578771ae4dc  rB578771ae])

== UI ==

* Use constant size for voxel remesh size edit by default ([https://projects.blender.org/blender/blender/commit/5946ea938a0  rB5946ea93]).
* Add operator to duplicate the active color attribute layer ([https://projects.blender.org/blender/blender/commit/9c28f0eb37a  rB9c28f0eb])

* There are new icons for the trimming tools ([https://projects.blender.org/blender/blender/commit/17fc8db104f 17fc8db104]).

[[File:Trim icons temp.png|center|frameless]]
''The old icon was too similar to existing selection tool icons. We updated the icon to avoid confusion, especially with planned future features.''

== New Features ==

* Add elastic mode to transform tools ([https://projects.blender.org/blender/blender/commit/e90ba74d3eb826abab4ec65b82fe0176ce3b7d1b  rBe90ba74d]).

== Performance ==

* Drastically improve drawing performance when sculpting with EEVEE enabled ([https://projects.blender.org/blender/blender/commit/285a68b7bbf  rB285a68b7]).