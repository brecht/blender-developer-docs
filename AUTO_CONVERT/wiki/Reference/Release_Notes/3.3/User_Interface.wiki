= User Interface =

== General ==
* Windows: support for precision touchpad gestures. Navigation is now consistent with middle mouse button, and different from the previous default behavior. Panning with two fingers orbits the 3D viewport and pans in 2D viewports. Hold down shift to pan, and control to zoom. The pinch gesture can also be used to zoom in or out. ([https://projects.blender.org/blender/blender/commit/e58b18888c0e e58b18888c])<br/>Check if your touchpad supports precision gestures by going to Windows Settings > Devices > Touchpad and looking for the "Your PC has a precision touchpad" label.<br/>Trackpad input can be customized in the keymap editor, by filtering by Key-Binding "trackpad".
[[File:Windows Precision touchpad gestures demo.mp4|800px|thumb|center]]

* Windows: title Bar will respect operating system "Dark Mode" setting. ([https://projects.blender.org/blender/blender/commit/ddbac88c08ef ddbac88c08])

* Support for text fields to show candidates in more situations including operator, menu & panel identifiers in the key-map editor and context data-paths for operators that use data-paths ([https://projects.blender.org/blender/blender/commit/3f3d82cfe9cefe4bfd9da3d283dec4a1923ec22d 3f3d82cfe9])

[[File:UI Text Field Search.png||600px|thumb|center]]

* Scrollbars always visible, but gain prominence as your mouse approaches. ([https://projects.blender.org/blender/blender/commit/624397231928 6243972319])
* Avoid redundant words in checkbox labels for custom properties: ''Is Library Overridable'' -> ''Library Overridable'', ''Use Soft Limits'' -> ''Soft Limits'' ([https://projects.blender.org/blender/blender/commit/18960c08fde7 18960c08fd], [https://projects.blender.org/blender/blender/commit/18960c08fde7 18960c08fd])
* A ''Pin Scene'' option was introduced to workspaces. Workspaces with the scene pinned will remember the last used scene, and restore that when the workspace gets re-enabled ([https://projects.blender.org/blender/blender/commit/e0cc86978c0f e0cc86978c]). An icon in the scene switcher allows toggling it ([https://projects.blender.org/blender/blender/commit/b8605ee458e3 b8605ee458]).
* Improved layout of File Browser UI settings in the Preferences: ''Show Recent Locations'' and ''Show System Locations'' now show up as ''Recent'' and ''System'' under a ''Show Locations'' heading. ([https://projects.blender.org/blender/blender/commit/1ef686bd26c  rB1ef686bd])

== Outliner ==

* In the Library Overrides view mode for Properties, display overridden properties in hierarchy with proper UI names and icons, instead of as a list of RNA paths to the properties. This is a more user friendly way of visualizing this data. 
{| class="wikitable"
|-
! Old !! Blender 3.3
|-
| [[File:Outliner-library-overrides-properties-pre-3.3.png|thumb|center]] || [[File:Outliner-library-overrides-properties-post-3.3.png|thumb|center]]
|}


* Ability to directly edit library paths in the Outliner has been removed. This could lead to corruptions of advanced library data like overrides. Modern process using relocate operation should be used instead. Direct library file path editing remains possible from direct RNA access (python console or Data API view of the Outliner for example).
* Improved performance of ''View Layer'' and ''Library Overrides'' display mode with complex object parent/child hierarchies. ([https://projects.blender.org/blender/blender/commit/afde12e066e2  rBafde12e0])

== Asset Browser ==

* Don't show catalog path in the tooltip when dragging assets over root level catalogs. The path and catalog name would match then, so the name would just be repeated twice in the tooltip. ([https://projects.blender.org/blender/blender/commit/2e33172719c9  rB2e331727])

== File Browser ==

* <span class="hotkeybg"><span class="hotkey">Ctrl l</span></span> (<span class="hotkeybg"><span class="hotkey">Cmd l</span></span> on macOS) to edit the directory path button. ([https://projects.blender.org/blender/blender/commit/209bf7780e7c  rB209bf778])
* Theme settings: Use ''Widget'' font style instead of ''Widget Label'' for File Browser text. ([https://projects.blender.org/blender/blender/commit/6f7171525b4a  rB6f717152])

== Gizmos ==

* Transform gizmos now display while being dragged ([https://projects.blender.org/blender/blender/commit/648350e456490f8d6258e7de9bf94d3a6a34dbb2 648350e456]).