= Sprint 7 =

* '''Start''': 2021/02/01
* '''End''': 2021/02/05

* '''Goal''': Wrap up the requirements by the studio, as well as polishing.

== Time ==

* Jacques: 4 days
* Pablo: 1 day
* Hans: 4 days
* Simon: 1 day
* Sebastian: 0.5 days

== Impediments ==

== Sprint Review ==

=== What went well ===

* The internal code review is working very well.
* The quality of the meetings was fun, for example the "Is Viewport" label discussion.
* The first big patches from the community got in.
* Time-boxing the design tasks.

=== What didn't go well ===

* Some tasks are bigger than expected and are dragging.
* Pablo didn't manage to do the things they wanted to, and prioritized.
* Simon spent too much time in the project.
* The design tasks are still taking longer than we can afford.

=== Improvements ===

* Find the balance between the time allocated to the project and the other responsibilities.
* Reach out even further to 1:1 with the community.