= Sprint 6 =

* '''Start''': 2021/01/12
* '''End''': 2021/01/22

== Impediments ==

== Sprint Review ==

=== What went well ===

* Simon insisting on a robust demo file.
* Got great feedback on the collection info node.
* Seeing the progress is great. very active channel
* More community contribution.
* People getting involved (implementing features, design tasks)
* More time in testing patches and giving feedback.
* Opening up the channel for development. After a few moderations the community acts appropriate.

=== What didn't go well ===

* Lacking on planning effort results in little progress in the design tasks. Time to spend more time to the project.
** Proposal to have a 1 week sprint to only focus on design tasks (also technical).
* Did a lot of back and forth. Thought that something was working, but didn't think of all the test scenarios.
** There was a discussion about adding test scenarios in the design task (scoping what should work) and adding test cases.
* Time management (over promising, getting distracting)
* Beginning of the sprint was very hectic. 
* Sharing stuff to late.
** Early on sharing on a public branch or with screen shots in the task or on the channel.

=== Improvements ===

* Be more in touch with the users receive the project in the product owner role.
* Work with community improvements on scattering.
** If we want the community involved we have the spent time on it.
** Within the sprint we want to get more experienced first.
** Involving the community should not be the responsibility of the team. The development manager/community manager should.
** More team members mention this issue. Discussion went from the differences between the scattering example and distribute example.
** Make the process of giving feedback of community designs part of our own design discussions.
* Sharing earlier
** Ask more. Have a place where we can ask/discuss the project in an earlier stage. Like in the first sprints.
* Be more aware of the bcon phases for the upcoming sprints. Have a moment during the sprint planning for this.
* Should the end user testing be part of the development task.
* Add nice to have improvements for the product backlog. File it under (unaddressed feedback) on the product backlog. Already assigned to Dalai.
* Update the sprint log template.
** Remove unneeded parts
** Promised part, not mention specific tasks, but add the user story/use case.
** Goal.
* Make someone responsible at the beginning of the sprint for the demo.