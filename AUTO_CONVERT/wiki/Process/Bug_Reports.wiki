=Bug Reports=


<div class="bd-lead">
Best practices, tips and tricks for effective bug reporting.
</div>

All reports must contain all the required information to reproduce the issue, publicly available to everyone. In almost all cases, this includes:
* Reproducible steps leading to the issue.
* A simple .blend file to demonstrate the issue.
These issues are [[Reference/Not a bug|not considered bugs]]:
* Functionality which works as intended but could be improved.
* Crashes running out of memory.
* Crashes from loading corrupted Blend files.


==How and where to report bugs==

Bugs in Blender or Add-ons included with Blender can be reported at [https://projects.blender.org projects.blender.org].
You will need an account, so that developers can follow up with questions and notify you when the bug is fixed. You will receive an email each time there is an update to the bug report.

These so-called "Issues" are created per repository (there is a list of blenders repositories [https://projects.blender.org/blender here]), each has an Issues tab at the top:

[[File:Issues-tab.png|class=img-fluid]]

Here are direct links to the most relevant Issues pages (note is is really important to report the issue in the right repository, since issues cannot be moved between repositories later):
* [https://projects.blender.org/blender/blender/issues blender]
* [https://projects.blender.org/blender/blender-addons/issues blender-addons]

The recommended way to report a bug (since this will take you directly to the submission form and auto-fill some relevant [e.g. System and blender version] information) is to use `Help` > `Report a Bug` from within blender:

[[File:BugReport-blender.png|class=img-fluid]]

Or, if this is a bug in an Addon, use the corresponding button in the `User Preferences` for that Addon:

[[File:BugReport-blender-addon.png|class=img-fluid]]

Alternatively, you can also do this from within gitea (without the relevant information pre-filled though)

Click the `New Issue` button in the corresponding repository:

[[File:New-Issue.png|class=img-fluid]]

Now click the `Get Started` on the Bug Report row

[[File:New-BugReport.png|class=img-fluid]]

The [https://projects.blender.org/blender/blender/issues/new?template=.gitea%2fissue_template%2fbug.yaml bug report submit form] has concise instructions and a template for creating reports. More advice on how to create good reports is listed below. Following these steps will make life easier for you, the user, and us, the developers.

==Avoid Walls of Text==
Please, avoid copy/pasting the content of text files like ''system-info, debug/console/command line'' output, ''scripts'' directly in the report's Description field. It makes the report difficult to read and breaks up the formatting. Include them as files instead (.txt). On how to include files, look at the '''Appending files to the report''' section.

==Avoid Duplicates==
Before you think of posting any bug report, first consult the Issue search for an existing report on the bug you have spotted. Make sure you search in all bug reports, including closed ones.

[[File:Issues-search.png|class=img-fluid]]

Keep in mind the issues are '''per repository''' (so for example bugs in blender and bugs in Addons are on separate lists), so as a reminder, here are the links for the two most relevant repositories again:
* [https://projects.blender.org/blender/blender/issues blender]
* [https://projects.blender.org/blender/blender-addons/issues blender-addons]

It may very well be that the bug has already been reported/ fixed.

==Simplify==
First try to simplify the process that triggers the bug, so that it is reproducible in the least amount of steps. The less steps there are, the easier it is for us to spot the problem in the code. Typically if you have a complex .blend file, it is possible to keep removing objects, modifiers, nodes, and other data until the problem has been isolated to a simple setup.

Very important is, that the bug is reproducible, that is, not occurring on a random basis. It is fine if the bug randomly happens for example 1 out of 10 times you perform some operation. However if the bug randomly happens without any clear steps leading up to it, then it is unlikely developers will be able to fix the bug.

==Provide Backup==
A .blend or screenshot illustrating the bug will help to speed up the process. As in the previous paragraph, make them as simple as possible, so that the developers can spot the problem without too much effort.

You can even provide a backtrace which lead to possible crash. That may help developers as well. A backtrace is not necessary and producing a useful one requires special debug build and a debugger. See [https://docs.blender.org/manual/en/latest/troubleshooting/ troubleshooting docs] for ways to narrow down the issue.

===How to Append Files to the Report===
Files like '''.blend''', '''.txt''', '''images''' can be included into the report by dragging and dropping them into the Description field. At the bottom of the bug report page, there is a Preview of how the report will look like when posted. If everything is OK, a link will show up with the file name. In the Description field itself, the file is represented by a set of letters and numbers in brackets. To move the link to an appropriate place if needed, cut and paste it and be sure that there are spaces between it and the rest of the text.

If you include a screencast of a problem make sure it is as short and clear as possible, edit it if necessary. A video is not the main way to convey information, but can be used as support.

==Be Complete==
Apart from the actual bug report it is always good (if not necessary) to include operating system, software (drivers) and hardware specifications of your configuration. Some bugs can be related to either area and including this information can help the developers to pinpoint problems better. And let's not forget mentioning what Blender version you are using, and in which version of Blender things still worked properly. Please look in the splash top right corner for all build details and include that as well.

===How to Provide System Information===
Including the <code>system-info.txt</code> generated from the '''Help > Save System Info''' Menu will give information about the OS, hardware, drivers, Blender version and add-ons enabled.

==Publicly Available Data==
Blender is an open source projects. As such, all bug reports must be fully reproducible based on publicly accessible data. As such:

; Proposal to sign a NDA (Non-Disclosure Agreement) as part of handling a bug will be systematically declined.
: If such level of privacy is required, then the bug handling should be done first by investing in-house time on it, and/or hiring external resources.
: There is often little work needed to turn a complete production file that cannot be shared publicly, into a much simpler 'safe to share' one for the purpose of a bug report. This is actually part of the reporting process, see [[#Simplify|the Simplify section]] above.

; Privately sharing information (through mails e.g.) is highly discouraged.
: There are some rare cases however where this can be the easiest/most efficient way to tackle an issue. In such case:
:* This can only happen if a developer explicitly accepts to handle the issue that way beforehand.
:* The developer then becomes directly the owner of the bug report, and is responsible to manage it (in other words, the triaging step is skipped).
:* The developer is responsible to ensure that the bug report is fully valid, especially that it has all the necessary information to reproduce the issue. This often implies creating a simple .blend file based on what they found while investigating the privately shared data.

==Triaging==

Understanding how the team of triagers will now handle your bug report might also be interesting (since it might give further information on how to report as best as possible).
For this (or for anyone willing to help with bug triaging), reading the following documents is useful:

* [[Process/Help Triaging Bugs|Help Triaging Bugs]]
* [[Process/A Bugs Life|A Bugs Life]]
* [[Process/Bug_Reports/Triaging_Playbook|Bug Triaging Playbook]]