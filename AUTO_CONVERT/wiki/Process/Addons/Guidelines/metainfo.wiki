= Script Meta Info =

You must include a python dictionary named "bl_info" at the top of your addon .py file or __init__.py if your Addon is a Python module.

<source lang="python">
bl_info = {
    "name": "My Script",
    "description": "Single line explaining what this script exactly does.",
    "author": "John Doe, Jane Doe",
    "version": (1, 0),
    "blender": (2, 65, 0),
    "location": "View3D > Add > Mesh",
    "warning": "", # used for warning icon and text in addons panel
    "doc_url": "http://wiki.blender.org/index.php/Extensions:2.6/Py/"
                "Scripts/My_Script",
    "tracker_url": "https://developer.blender.org/maniphest/task/edit/form/2/",
    "support": "COMMUNITY",
    "category": "Add Mesh",
}
</source>

Below you find a brief description of the meta data elements:
----
===== name =====

: ''(string)''
: Name of the script. This will be displayed in the add-ons menu as the main entry.

----
===== description =====

: ''(string)''
: This short text helps the user to decide if he needs the addon when he reads the addons list. Any further help text should be put in the help page (see [[#doc_url]]).

----
===== author =====

: ''(string)''
: Author name(s).

----
===== version =====

: ''(tuple of integers)''
: Script version. Please put any important notes into the warning field, don't use text in the version number.

----
===== blender =====

: ''(tuple of 3 integers)''
: The minimum Blender version required to run the script. In the add-ons panel this is used to check if the user has a Blender version that is new enough to enable the script. eg.<br>'''Blender2.65''' -> <tt>(2, 65, 0)</tt>, note that the last value should normally be kept to zero, this is blenders sub-versions which scripts wont normally need to use.
: When the version number ends in zero, you need to supply that 0 too, as in <tt>(2, 50, 0)</tt> and not <tt>(2, 5, 0)</tt>.

----
===== location =====

: ''(string)''
: Explains where the new functionality can be found. For example: "View3D > Properties > Measure"

----
===== warning =====

: ''(string)''
: Used for warning icon and text in addons panel. If this is empty you will see nothing; if it is non-empty, you will see a warning icon and the text you put here alerts the user about a bug or a problem to be aware of.

----
===== support =====

: ''(string)''
: Display support level (''COMMUNITY'' by default)
* ''OFFICIAL'' - Officially supported
* ''COMMUNITY'' - Maintained by community developers
* ''TESTING'' - Newly contributed scripts (excluded from release builds)

----
===== doc_url =====

: ''(string)''
: Link to the documentation page of the script: here you must put the script manual.

: This url is mandatory for the add-on to be accepted in the Blender Addon repositories (see [[#Publishing_a_script_in_Blender|below]])'''.
<br style="clear:both;">

----

===== tracker_url =====

: ''(string)''
: Optional field to specify a bug tracker other than the default https://developer.blender.org

----
===== category =====

: ''(string)''
: Defines the group to which the script belongs. Only one value is allowed in this field, multiple categories are not supported. The category is used for filtering in the add-ons panel.
: Below is the list of categories currently in use by official add-ons. Please pay attention to their correct spelling and capitalization, as you can also type your own custom categories and they will show in the add-ons panel filtering options.

{| width=98% align=right
|+'''List of categories'''
|-
|
* 3D View
* Add Mesh
* Add Curve
* Animation
* Compositing
* Development
|
* Game Engine
* Import-Export
* Lighting
* Material
* Mesh
* Node
|
* Object
* Paint
* Physics
* Render
* Rigging
* Scene
|
* Sequencer
* System
* Text Editor
* UV
* User Interface
|}<br style="clear:both;">