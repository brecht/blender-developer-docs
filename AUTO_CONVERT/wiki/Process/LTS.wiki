= Blender LTS =

With the release of Blender 2.83, Blender Foundation will start a LTS (Long Term Support) pilot program. The program is aimed at ensuring that long-lasting projects can be executed using a stable Blender version, which will provide critical fixes throughout a 2-year time span.

== Criteria to determine what to be ported ==

Currently, for non-LTS releases, Blender only has a corrective release if severity 1 issues (high priority bugs) are found. When the corrective release is agreed on, however, severity 2 (high priority and normal bugs) fixes are ported along.

For the LTS releases, a more limited policy would apply (only porting severity 1 issues after the next stable release), on a fixed schedule (e.g., every 3 months) after the fix was tested in master for some time (e.g., 1 week).

== Bug-fix flow diagram ==

Currently, at the beginning of bcon3, master is forked into a release branch. Any bug fix relevant to the upcoming release happens in the release branch, and is merged to master.

[[File:Lts-branch-management-pre.png|1024px|center]]

For the LTS versions, their branches work as usual, but once the release happens they keep receiving critical updates. Such updates can be ported from master or be performed on the branch itself.

[[File:Lts-branch-management-post.png|1024px|center]]

== Distribution ==

The LTS release will be added to the current roster of Blender releases available during the development cycle.

* [http://blender.org/download blender.org/download] offers latest stable release
* [http://blender.org/download/lts blender.org/download/lts] offers the latest LTS releases (up to 2 in parallel)
* [http://builder.blender.org builder.blender.org] offers alpha builds of the upcoming latest version

== Version Communication ==

In order to clearly communicate how the growing number of Blender versions relate to each other, an updated version naming convention will be adopted:

* Sub-version as corrective releases. Use 2.83.1 instead of 2.83a
Never expose to the users the internal sub-subversion numbering (e.g., 2.83.9). The complete version and the hash is enough for bug reporting.
* Rename the development builds (currently 2.90 alpha) to allow better focus on the upcoming release (2.83). Instead of calling it "2.90 alpha", we call it ''development / master / daily / next / ...'' build.
* [http://builder.blender.org builder.blender.org] should feature exclusively (or distinctly) the master build

== Addenda ==

* [[Process/LTS/Addenda#Timeline|Timeline]]
* [[Process/LTS/Addenda#Issue_Severity_Levels|Issue Severity Levels]]
* [[Process/LTS/Addenda#Open_Issues|Open Issues]]