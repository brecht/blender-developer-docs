= How to help triaging bugs? =

There are many ways in which users can help developers to improve Blender.
Making bug reports or feature requests are examples (and they are always welcome), but unfortunately, these also take some time to triage.
Since the number of users making such reports is much larger than the number of developers, this can be overwhelming.

This document will explain how every Blender user can help reduce the workload of the existing developers by making sure a report is in best shape to be processed/fixed with minimal effort by developers.

For additional information, reading the [[Process/Bug Reports/Triaging_Playbook|Triaging Playbook]] and [[Process/A Bugs Life|A Bugs Life]] is encouraged.

== Prerequisites ==

* Use the newest blender version from [https://builder.blender.org/download/ builder.blender.org].
* Create an account on [https://projects.blender.org// projects.blender.org].

== Where are the bug reports? ==

Issues are created per repository (there is a list of blenders repositories [https://projects.blender.org/blender here]), each has an Issues tab at the top:

[[File:Issues-tab.png|class=img-fluid]]

Here are direct links to the most relevant Issues pages:
* [https://projects.blender.org/blender/blender/issues blender]
* [https://projects.blender.org/blender/blender-addons/issues blender-addons]

If you are part of organizations in gitea (e.g. you have commit rights for blender, add-ons, docs, as well as moderators), the issues will also show up in a "unified" manner on your personal Issues tab for all repositories you are part of:

[[File:Issues-personal.png|class=img-fluid]]

A list of issues can then be further refined by filtering in the UI either by clicking on labels or using the dropdowns:

[[File:Issues-filter-label.png|class=img-fluid]]

You can also create custom query strings in URLs to do this filtering, a couple of useful combinations will be provided as links in [https://projects.blender.org/blender/blender/wiki/Module:%20Triaging Triaging wiki page].

For example: this one shows only reports that still need to be triaged in the blender repository : https://projects.blender.org/blender/blender/issues?q=&type=all&state=open&labels=300,294

Same for the Addons repository: https://projects.blender.org/blender/blender-addons/issues?q=&type=all&state=open&labels=175,169


== I've found the reports, what next? ==

Every report has a specific status, which is indicated by the color of the `Status > label next to the report.
The most important status are these:
* Needs Triage: Initial state of every report.
* Needs Information from User: The bug cannot be reproduced with the given amount of information.
* Needs Information from Developers: The report is valid and reproducible, but the input of the module team is need to decide how the ticket should be handled. This status hands over the responsibility from the triaging team to the module and should therefore be used sparingly. For instance this could be necessary when unusual behavior can be reproduced by the triaging team, but it is unclear whether this is a known limitation or bug. 
* Confirmed: The report has been reproduced by triagers, assigned a priority and module label and the bug should be fixed at some point.

Depending on the current status, you can do different things.

=== Needs Triage ===

* Check that the report contains the relevant system information (click on ''Help -> Report a Bug'' in Blender to see what is necessary).
* Check that the used Blender version is somewhat recent. Ideally, the most recent version.
* Check if you can easily understand the bug, based on the report.
* Check if you can reproduce the bug yourself, given the steps explained in the report.

* If any information is missing, change the `Status >` label to `Status > '''Needs Information from User'''` and explain what information is missing. Also encourage that the original report should be updated, instead of just adding more and more comments.
* If there is nearly no information at all in the report (e.g. only the title), the report can be closed (use the `Close` button, a bot ensures the `Status >` label to be `Status > '''Archived'''`, visible only after a manual refresh of the page). Post a comment like "Please create a new report that contains all the information the template asks for."
* If the information is all there, but you don't understand the bug, you can leave the status as is. Furthermore, you should ask if the person, who created the report, can give a more concise explanation (short, well formatted). Also ask for additional screenshots, a .blend file or a .gif file showing the issue.
* If the bug is easily understandable, but you cannot reproduce it yourself, either ask for better reproduction steps, or leave the report for a developer to triage.
* If you can reproduce the bug in the newest version, post the OS, hardware and driver versions you use, if they haven't been mentioned before.
* If you can reproduce the bug, but it is quite difficult or the file is large and complex, try to find an easier way to reproduce it.
* If you can reproduce the bug, check if there have been previous reports of the same issue. Merge the ticket into the older report if such a duplicate exists. If there current report is more informative and the older report has not received any attention yet, you may also merge the older into the newer ticket. To merge, mention the duplicate issue in a comment (reference its issue number including the dash character, e.g. `#12345` so this correctly shows up in the report this will get merged to) Close the report. Change the `Status >` label to `Status > '''Duplicate'''`, and ask the reporter to subscribe in the report this is merged to (this will not happen automatically).
* '''Don't''' confirm a ticket unless you are part of the triaging team. 
* If you are part of the triaging team, the issue is reproducible, the ticket contains all relevant information and you are sure that it is a bug:
** Tag the module the bug report belongs to (add the corresponding `Module >` label). The `Module >` labels are scoped meaning only one can be chosen (this ensures responsibility by a single module). If in doubt, choose the module where the issue is most visible to the user or CC the @blender/Module team which consists of the module owners. Later in the process, the modules may decide to hand over responsibility themselves (by changing the `Module >` label). To help discoverability in the case the issue touches multiple areas in blender, (multiple) `Interest >` tags can still be added optionally. 
** Assign an initial priority based on the severity of the bug (add the corresponding `Priority >` label), also see [[Process/Bug_Reports/Triaging_Playbook|Bug Triaging Playbook]]).
** Change the `Status >` label to `Status > '''Confirmed'''`.
* If you are part of the triaging team, the issue is reproducible, the ticket contains all relevant information but you are unsure if this is a bug:
** Ask other triagers about it in the [https://blender.chat/channel/blender-triagers blender-triagers] channel first.
** If it requires a decision by a core developer of the affected module, Change the `Status >` label to `Status > '''Needs Information from Developers'''`. This should only be used when it unclear whether the issue at hand is a bug.

=== Needs Information from User ===

If you think you experience the same bug, post the missing information and change the `Status >` label back to `Status > '''Needs Triage'''`.

=== Needs Information from Developers ===

If you are a module member the ticket is tagged for this report needs your input on how to proceed. It is your decision whether the problem is a bug and to set the appropriate status and subtype accordingly.

=== Confirmed ===

The report/issue  should already be in the best possible state at this point, however if you are familiar with the issue you can:
* still add easier reproduction steps.
* check if there have been previous reports of the same issue. Merge the ticket into the older report if such a duplicate exists. If there current report is more informative and the older report has not received any attention yet, you may also merge the older into the newer ticket. To merge, mention the duplicate issue in a comment (reference its issue number including the dash character, e.g. `#12345` so this correctly shows up in the report this will get merged to) Close the report. Change the `Status >` label to `Status > '''Duplicate'''`, ask the user to subscribe in the report this is merged to (this will not happen automatically).

== The Perfect Bug Report ==

The steps explained above hopefully end up in bug reports that developers can easily work with.
[[Process/A Bugs Life|A Bugs Life]] also explains this in the context of the whole process of fixing a bug.

An ideal bug report should check the following boxes:
* The title describes the problem in a short and concise way.
* All necessary system information is given.
* The broken (and optionally working) Blender version is given.
* After looking 10 seconds at the report, it should be obvious that the bug is real (without opening Blender, even when opening the report on a phone). Images or short screen casts can help a lot.
* The bug can be reproduced easily with a given .blend file (ideally, the developer only has to do zero or one clicks once the .blend file is open).
* A .blend file with the bug can easily be build from the default startup file.
* All relevant information is given in the top most post.

If any of these boxes is not checked when a developer tries to understand the report, some time will be wasted.