= Release on Windows Store =

{|class="note"
|-
|<div class="note_title">'''NOTE'''</div>
<div class="note_content">This procedure has changed to use Buildbot. Needs updating.</div>
|}


work-in-progress playbook for release on Windows Store

== Requirements ==

* As clean Windows host as possible.
* MSIX packaging tool. Install from [https://www.microsoft.com/en-us/p/msix-packaging-tool/9n5lw3jbcxkf its own Windows Store page]
* Official Blender installer (.msi)
* Microsoft Live login credentials for Blender Foundation to its store page
* No pending reboots

=== In the future ===

* Have a virtual machine with a clean Windows install to create the package on

== Steps ==
# Use the create_msix_package.py script ([https://developer.blender.org/D8310 D8310] until this has landed)
# For an LTS release that is also the current latest release run the packaging script '''twice''': once with the `--lts`, once without the `--lts` switch
# Login to Windows Store page
# Update the existing package (`Blender` for the latest release, `Blender X.YZ LTS` for the corresponding LTS)
## This will create a new submission
## Upload the newly created package
## Deselect the old version(s), leaving only the new package selected
## Submit
### Upload new package
### Edit the Store Listing - make sure the release notes section version number and info gets properly updated

Once the new package has been submitted it takes anywhere between several hours to several days for the submission to complete, after which it can take up to 24 hours for the update to be available on all Windows Store markets.

= What To Do When This Is A New LTS Release =
# Log in to the Microsoft Store dashboard (access to shared credentials needed)
# Create a new application
# Name it along the form of `Blender X.YZ LTS`, i.e. `Blender 2.83 LTS`
# When the first submission is created fill out all necessary information
## You can copy&paste most from the `Blender` application.
## Ensure the correct age badges are set, unfortunately one can't copy the ID from other applications
## For the listing I suggest exporting the one from `Blender`, take out the links for the images, tweak the text, and import it (en-US). Don't forget to upload the imagery for the listing
## Ensure that version mentioned throughout this new application is correct

= TODOs =

* Ensure all icon assets are created, see
** https://docs.microsoft.com/en-us/windows/uwp/design/style/app-icons-and-logos#more-about-app-icon-assets
** https://docs.microsoft.com/en-us/windows/uwp/design/style/app-icons-and-logos#icon-types-locations-and-scale-factors