= Animation & Rigging =

Notes and documentation about the Animation & Rigging module. 

'''Subpages:'''

* [[Modules/Animation-Rigging/Bigger Projects|Bigger Projects]]: broad ideas for the future.
* [[Source/Animation|Code Documentation]]
* [[Modules/Animation-Rigging/Code Review|Code Review]]
* [[Modules/Animation-Rigging/Weak Areas|Weak Areas]]: areas of the Animation & Rigging system that could use improvement.
* [[Modules/Animation-Rigging/Might Revisit|Might Revisit]]: design limitations that we have no intention of lifting at the moment, but that could be revisited in the future.
* [[Modules/Animation-Rigging/Character Animation Workshop 2022]]