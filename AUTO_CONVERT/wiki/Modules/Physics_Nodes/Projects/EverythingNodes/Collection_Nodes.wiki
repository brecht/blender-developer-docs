=Collection Nodes=

Collection nodes (or modifiers) allow for object operations.

This complements the [[Source/Nodes/Modifier_Nodes|geometry nodes]] functionality by letting users control data in the object level. For instance, to populate a collection dynamically and set different actions for different objects inside of it.

==Use Cases==

1. Crowd cheering inside a soccer stadium.

2. A tornado taking a house apart, piece by piece ([https://twitter.com/ZanQdo/status/1358533918006378498 reference]).

[[File:EverythingNodes-CollectionModifier-Tornado.png|center]]

==Unit==

While [[Source/Nodes/Modifier_Nodes|geometry nodes]] operate with geometry collections operate with objects.

==Nodes==

* Separate/Join objects
* Add/remove objects
* Set action
* Set material
* Objects constraints
* Set colliders (?)

==Tornado Example==

Node tree for the tornado use case. In this case the tornado is a node group with its complexity abstracted away.

[[File:EverythingNodes-CollectionNodes-Tornado-Nodes.png|thumb|center|1150px]]

==Stadium Example==

Node tree for the stadium cheering crowd. The points object is using geometry nodes. But no geometry can be passed in this nodetree.

[[File:EverythingNodes-CollectionNodes-Cheering-stadium.png|thumb|center|1564px]]