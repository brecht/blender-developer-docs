= Roles =

This page explains the roles and responsibilities of contributors that are part of the Blender.org project.

See [[Modules]] for some of the names of the people with those roles.

== BF Blender ==

This is the official Blender source code and release from the Blender Foundation. Decisions are generally made in consensus between contributors. However if no consensus can be found the admins make the decision.

== Admins ==

The admins are accountable for making sure the Blender.org project runs well as a whole.

This is only the project as a whole, the roadmap and strategy. Module and project owners are accountable for their own areas, and (can) consult with admins and go to them for support.

All admins have been involved in Blender development for considerable time.

====Responsibilities====
* Help to keep modules and projects running well.
* Appoint module owners.
* Propose and approve dev grants and development hires.
* Review and follow-up on projects.
* Make decisions when no consensus can be reached among contributors.

The admins report to the Blender Foundation chairman on these topics.

== Modules ==

Blender development is organized in multiple [[Modules|modules]].

=== Owners ===

Active developers who frequently contribute to parts of Blender can become module owners. Ownership means you have a lot of freedom to work on the modules, decide on implementation and design issues here, and approve or reject patches and feature requests. This based on aligning well with Blender's general design decisions, release planning and roadmap.

Owners should keep track of other module teams to make sure overlapping work is being agreed on, and make sure their own module teams agree on work that's being done.

Responsibilities:
* Be available for regular bug fixing in the module (at least for every release)
* Make sure the module is well tested for a release
* Organize review of pull requests as submitted for the module
* Ensure new features or changes in modules are well documented in wiki.

Module owner and members mostly self organize. When needed, the module owner acts as the main point of contact and coordinates efforts.

Project admins approve module ownership.

=== Members ===

Module members are involved in and contribute to the module. This includes developers, designers, writers, artists, users, and more.

Anyone actively contributing to a module can be listed as a member, there is no formal process to join. Members are listed in their corresponding areas if their involvement is more sporadic, or as a module member if they want to be more active and involved.

Whenever possible, members are available for bug fixing, reviews and documentation. However, it's more of a loose commitment compared to owners, based on 'being involved' more than on 'being available'.

==== '''Developers''' ====

Developers typically contribute in the following ways:
* Design and implement features
* Review pull requests
* Handle bugs reports
* Listen to user feedback
* Answer questions from contributors

Once a developer has contributed good quality code, they can be given access to commit directly to the Blender repository. They can then commit code approved code reviews themselves, or merge approved contributions from others.

When making changes in other modules, the developers should get approval from the relevant members of those modules. Any areas not clearly covered by a module are handled by the project admins.

==== '''Artists and Users''' ====

Users can also be a member and participate in module discussions and decision making. They are usually artists who act in the quality assurance and stakeholder role of the module.

Users typically contribute in the following ways: 
* Make sure the module is well tested for a release
* Help documenting and writing release notes
* Participate in design tasks, reviews, meetings
* Gather user feedback and respond to user questions

The module team ensure that stakeholders endorse and agree with features and development plans, while the stakeholders test new features and changes as they are developed.

==== '''Designers, Writers, Organizers, ...''' ====

There are many valuable ways to contribute and members often contribute in multiple ways. There are no strict roles for each. The module team and members can fill this in themselves.

== Projects ==

[[Process/Projects|Projects]] gather multiple developers and contributors to work on specific topics. They have a start and end date, while module work is always ongoing.

Admins review and approve proposals for projects and fit them in the overall planning. Once started, a project owner is accountable for the project organization and outcome. One go-to admin will be consulted about the project progress.

After the project completes the outcome becomes part of regular module maintenance.

The roles of owners and members are the same as in modules.