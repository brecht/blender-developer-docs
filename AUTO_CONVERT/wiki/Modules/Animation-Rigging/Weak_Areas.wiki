= Animation & Rigging: Weak Areas =

Here module members can keep track of weaker areas in the animation system.

'''NOTE:''' This is not meant to be an exhaustive list of all known issues. We have [https://developer.blender.org/maniphest/query/hmUqAeH_CAE8/ the issue tracker] for that.

== Auto-keying ==

The auto-keying system appears to have been designed for transforms only, and as a result doesn't work well for other properties.

* Transform operations can cause keying of non-transform properties, and non-transform operations often don't even auto-key ([http://developer.blender.org/T88066 T88066]: Auto Keying with "Only Active Keying Set" creates keyframes it shouldn't)
* Custom keying sets are always absolute (in terms of which properties of which objects are keyed), instead of the often-expected "the named property of the active/selected objects".
* [http://developer.blender.org/T73524 T73524]: Autokey keys disabled driver values
* [http://developer.blender.org/T73773 T73773]: "Only Insert Needed" option in conjunction with keying set causes "Unable to Keyframe" warning message
* [http://developer.blender.org/T91134 T91134]: Change autokey preferences "Only insert available" to "Only insert keyframes on existing F-curves"
* [http://developer.blender.org/T91135 T91135]: Change autokey preferences "Only insert needed" to "Only insert keyframes on properties that have changed."
* [http://developer.blender.org/T91941 T91941]: "Whole Character" keying set not working properly with autokey enabled
* [http://developer.blender.org/T95364 T95364]: Autokey function hinders execution of drivers
* [http://developer.blender.org/T95866 T95866]: Changing the Driver while Auto Keying is on, causes new Keys on it's F-Curve

== Mirroring and Symmetry ==

* [http://developer.blender.org/T61386 T61386]: Mirror Paste for pose bones also copies custom properties
* [http://developer.blender.org/T65671 T65671]: Armature X-Mirror inconsistencies
* [http://developer.blender.org/T74032 T74032]: Pose Mode X-Axis Mirror is not working properly with IK when skeleton is moved
* [http://developer.blender.org/T76915 T76915]: Pose bone: clearing transformations in pose mode does not affect the mirrored bones even when the X-axis mirror is activated.
* [http://developer.blender.org/T83657 T83657]: In pose mode: Mirror X doesnt work with certain operations


== Selection synchronisation between pose bones and animation channels ==

* [https://developer.blender.org/T48145 T48145: All bone drivers selected when reselecting bone]
* [https://developer.blender.org/T58718 T58718: Clicking on Dope Sheet deselects all bones]
* [https://developer.blender.org/T62463 T62463: Skeleton rig with keyframes prevents selection of Shader Nodetree channels in Dope Sheet and Graph Editor]
* [https://developer.blender.org/T71615 T71615: Select key in dopesheet deselect bone in the viewport]
* [https://developer.blender.org/T73215 T73215: Blender autokeying deselects objects channels but not Armatures]

In short, there is space for improvement regarding the synchronisation between the selection state of bones (in the viewport) and channel rows (in the dope sheet/action editor/etc). Hjalti mentioned that it's probably better to synchronise less, that is, have the selections more separate. This new behaviour would need to be properly designed & discussed, though.