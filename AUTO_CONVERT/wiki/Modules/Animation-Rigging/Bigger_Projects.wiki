= Animation & Rigging: Bigger Projects =

This page lists the broad ideas for the future of the module. It is the intention that these eventually become design tasks on [https://developer.blender.org/tag/animation_rigging/ Phabricator].

== Face Maps ==

The ability to adjust bones without directly working with the bones, but rather by interacting with groups mesh faces.

== Animation Layers ==

Like the NLA editor, but better.

Historical link: This used to be tracked in [https://developer.blender.org/T68902 T68902]

== Node-based Rigging ==

Constraints are quite complex, and can likely benefit from separating them into a network of simpler nodes.

Historical link: This used to be tracked in [https://developer.blender.org/T68907 T68907]

== Optimize VSE Playback for Animators ==

Animators often use the VSE to play reference footage during their work. This task is a placeholder to look at improvements that can be done in these specific setups to speed up the playback performance.

Historical link: This used to be tracked in [https://developer.blender.org/T75719 T75719].

== Decouple physics and animation clock ==

This permits an interactive scene (e.g., VR) where physic simulation is happening in realtime, without the animation playing together.

Historical link: This used to be tracked in [https://developer.blender.org/T68979 T68979]

== Animation fine-tuning sculpting ==

Animation sculpting on top of finalized animation for fine tuned animation details. This could even include sculpting on top of cached geometry (like from USD or Alembic).

Historical link: This used to be tracked in [https://developer.blender.org/T68906 T68906]

== Motion path viewport editing ==

Historical link: This used to be tracked in [https://developer.blender.org/T68901 T68901]

For reference:
* [https://blenderartists.org/t/editable-motion-trails-3d/1197906 Editable Motion Trails 3D]
* [https://blenderartists.org/t/so-i-tried-to-implement-tangent-space-optimization-for-interactive-animation-control/1230828/ So I tried to implement “Tangent-Space Optimization for Interactive Animation Control”]