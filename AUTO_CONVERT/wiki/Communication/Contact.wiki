=Communication=

<div class="bd-lead">
Blender is a community project. It relies on its users to support each other, spread news, and continuously improve Blender.

</div>

With millions of downloads, Blender has relatively few contributors.
It is essential that information flows in both directions between users and developers in a useful manner.

===Copyright and Guidelines===

Blender is an open source project, but developers and contributors need to take copyrights of proprietary software into account. Please read the [[Contact/CopyrightRules|Copyright Rules]] carefully before you start contemplating a contribution to Blender / submit a proposal to any of the Blender communication platforms.

General guidelines for communication have been outlined in our [[Contact/CodeOfConduct|Code of Conduct]].

==Contact Developers==

===Developer Forum===

The official development discussion platform is [https://devtalk.blender.org/ DevTalk]. A few useful sections:

* [https://devtalk.blender.org/c/contributing-to-blender/11 Building Blender]
* [https://devtalk.blender.org/c/contributing-to-blender/29 Contributing to Blender]
* [https://devtalk.blender.org/c/contributing-to-blender/12 Documentation and Translations]

Until 2023, mailing lists were used to coordinate development, and were progressively replaced with the DevTalk forum. The lists archives are available at [https://archive.blender.org/lists/ archive.blender.org].

===Chat===
Most developers are active on the [https://blender.chat/channel/blender-coders #blender-coders channel on blender.chat].

See here for a full [[Communication/Contact/Chat|overview of popular chat channels]].

===Vulnerability Reporting===

While most bug reporting should be done via Blender's bug tracker, sensitive vulnerabilities may be reported privately, see: [[Process/Vulnerability_Reports|Vulnerability Reporting]].

==Decisions and Planning==

Blender's code is organized in modules; libraries or directories with files sharing a certain functionality. For each module, the module owners are responsible for maintenance and bug fixes. They also define - together with their team - roadmaps or todos for the module, aligned with the overall release cycles and roadmaps.

===Weekly Development Updates===

Every Monday an update report is published to [https://devtalk.blender.org/c/announcements/weekly-updates/14 devtalk]. This is the one document to direct anyone interested on finding about what is happening in Blender.

This report is compiled by multiple hands:
* Announcements: Everyone.
* Modules and Projects: Modules and project members.
* New Features and Changes:
** The list of commits is organized manually, but cleanup and fix commits are filtered out automatically with this script: `git log --no-merges --since 7.days.ago --pretty=format:"%h%x09%ar%x09%s ([commit](https://projects.blender.org/blender/blender/commit/%h)) (_%an_)" | grep -v "Fix " | grep -v "Cleanup:"`
* Welcomes, final edit and publishing: Development Coordinator (''Thomas Dinges'').

===Modules Meetings===

Most modules have regular meetings. Their announcements and final notes are in [https://devtalk.blender.org/tag/meeting devtalk].

==User Feedback and Requests==

Blender has magnitudes of more users than people who are involved with making it. That makes handling user feedback and feature requests - or even tracking them sanely - nearly impossible.

Instead we try to organize ourselves in ways that development and users can cooperate closely. For each module in Blender, there's a small and competent team working on it - including users helping out. You can find this information listed above. Feel welcome to use our channels and get involved with making Blender better.

As an alternatives we like to mention:

* Add ideas on the community-run [https://rightclickselect.com/p/ Right-Click Select] website
* Make sure your proposal is documented and published in public. Post this on the appropriate public lists, or on public forums (such as Blenderartists.org). It doesn't really matter where, a lot of users out there probably can tell you whether it's already there, already planned, a great new idea, or simply not possible.