= Optimized IDE Setup on Linux =

Some of the steps in this guide are not immediately obvious even for experienced linux users/developers so I have written up this document for others who are interested in developing blender on linux.


[[Image:Qtc_blender_25.png|640px]]


== Who Is This For? ==
Linux developers who want to setup an efficient build environment.

There are a number of aspects that help to achieve this:
* Using a minimal CMake  configuration.
* QtCreator has a fast C/C++ source code indexer (much faster than eclipse/netbeans/kdevelop)
* Avoid executing the install target (no copying files on rebuild), instead reference the files in-place to run blender.

== Starting out ==
This document assumes you have built blender using the [[Building_Blender/Linux/Ubuntu/CMake|Linux/CMake Quickstart]].

* blender source in '''~/blender-git/blender'''
* blender build in '''~/blender-git/build'''

This was tested on Ubuntu but should work on any recent Linux distribution.

== Installing QtCreator ==
QtCreator is an IDE which can be used for general C/C++ development, not just QT Applications, it has an editor, debugger, auto-completion as well as git and cmake integration.

Download and install from QT's site: https://download.qt.io/official_releases/qtcreator/

Click 'View All Downloads', then scroll down to the Qt Creator standalone packages and download the most current package for your system.

== Configure QtCreator ==
Launch QtCreator.

Before loading a blender project make some minor changes to QtCreators defaults.

<div class="tableNormal">
Open the Options dialog '''Tools -> Options'''

'''Build and Run'''
* Always build project before deploying it (disable)
</div>

As of December 10, 2019, the clang code model plug-in in newer versions of Qt Creator makes highlighting and code suggestions in Blender code in Qt Creator unusable, so it is suggested to disable it.

'''Open Help->About Plugins and disable the Clang code model plugin.''' You will have to restart Qt Creator for this to take effect.


For instructions on configuring QtCreator for Blender's code style

see: [[Source/Code_Style/Configuration#QtCreator]]

== Making a new QtCreator Project ==
=== Create the Project ===
''Note, for now its recommended to use the custom project file generator included with blender until QtCreator's has support for reading CMakes defines & includes!''
==== Using Blender's Custom Project File Generator ====
<div class="tableNormal">
From your cmake directory, run Blender's project file generator.
''You may want to make some short cut to this - a shell script, bash alias or QtCreator external command all work fine.''
<source lang="bash">
cd ~/blender-git/build
python3 ../blender/build_files/cmake/cmake_qtcreator_project.py
</source>

From QtCreator go to File -> Open and select ~/blender-git/build/blender.creator

==== Custom Project Names ====
If you want to use a custom name for the QtCreator project, you can use the '''--name''' or '''-n''' argument when calling the cmake_qtcreator_project.py script. This is useful if you want to manage multiple projects for multiple builds. The files created by this will then be called accordingly, e.g. some_name.creator instead of blender.creator

==== Configuring the Project ====
Now you should see the blender project open, start by pressing the build button(Ctrl+B), this should be very quick since blender has been built before.

To run blender you'll need to point to the blender executable: Projects (side toolbar) -> Run Settings (Tab), then see the "Executable" field, browse for the executable.

QtCreator stores its build directory in '''~/blender-git/blender/blender.creator.user''', if you have some configuration problem and wish to start over be sure to remove this file first.
</div>

==== Using QtCreators CMake Project Support (deprecated for now) ====
<div id="qt_creator_cmake_project">
  <div class="card">
    <div class="card-header" id="heading_qt_creator_cmake_project">
        <div class="btn btn-link" data-toggle="collapse" data-target="#collapse_qt_creator_cmake_project" aria-expanded="true" aria-controls="collapse_qt_creator_cmake_project">QtCreators CMake Project Support (deprecated)</div>
    </div>

    <div id="collapse_qt_creator_cmake_project" class="collapse" aria-labelledby="heading_qt_creator_cmake_project" data-parent="#qt_creator_cmake_project">
      <div class="card-body">

* File -> Open File or Project, Select ~/blender-git/blender/CMakeLists.txt

This invokes the '''CMake Wizard'''
* '''Build Location''' change the build directory to '''~/blender-git/build''' and press Next
* '''Run CMake''', Press the "Run CMake" button, then finish.<br>''note: this will use the configuration selected when first building blender from the command line.''

===== Configuring the Project =====
Now you should see the blender project open, start by pressing the build button, this should be very quick since blender has been built before.

To run blender you need to select the binary, on the left hand side there is a "Projects" section, Select '''Run Settings''',  Change the run configuration from makesdna to blender and press "Make blender Active".

QtCreator stores its build directory in '''~/blender-git/blender/CMakeLists.txt.user''', if you have some configuration problem and wish to start over be sure to remove this file first.
</div>
    </div>
  </div>
</div>__NOEDITSECTION__


To re-open the project after re-starting QtCreator, select this project from '''File -> Recent Projects'''

== Debugging ==
In '''~/blender-git/build/CMakeCache.txt''', change '''CMAKE_BUILD_TYPE''' from '''Release''' to '''Debug'''. This will create a debug executable instead the next time you build Blender.

QtCreator requires an independent debugger program be installed on your system (e.g. GDB). If your distro does not automatically ship with one, you may need to manually install and set it up with QtCreator (see [https://doc.qt.io/qtcreator/creator-debugger-engines.html here] for details).

The video bellow 'Navigate Code / Tips' has [http://youtu.be/_kniMTbcCko?t=8m19s a part] on how to debug using QtCreator.

== Using QtCreator for Python ==
This section is optional and unnecessary if you are only interested in editing the C/C++ code.

The [[Dev:Doc/IDE_Configuration/Linux_CMake_QtCreator#Using_Blenders_Custom_Project_File_Generator | project generator]] also creates a project for python code.
Go to '''File -> Open''' and select ''~/blender-git/build/blender.creator'' QtCreator supports having multiple projects open.

== Git Integration ==

While there is reasonably good git integration in QtCreator, if you're comfortable using the git command line, then you can keep doing so.

* Blender can be updated from git within QtCreator<br>'''Tools -> Git -> Remote Repository -> Pull"'''
* QtCreator's '''git blame''' integration is quite good, so you can see who last edited each line of a source-file.

== QtCreator Videos ==
* [http://www.youtube.com/watch?v=5Ymoav0nNWQ&feature Navigate Code + Simple Fix]
* [http://www.youtube.com/watch?v=_kniMTbcCko Navigate Code / Tips]
Here are the [http://wiki.blender.org/index.php/User:Ideasman42#crep crep] and [http://wiki.blender.org/index.php/User:Ideasman42#prep prep] tools mentioned in the videos.

== Trouble Shooting ==
=== Ubuntu / Ptrace Error ===

On ubuntu there is a reported error running a debug binary within QtCreator.

Ptrace is 'process trance'.  By using ptrace one process can control another, enabling the controller to inspect and manipulate the internal state of its target.  Potentially a security problem, which is why this is switched on.  There are therefore two solutions, open ptrace up permanently or temporarily.

Temporary solution is:

 echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope

After reboot the ptrace will be active again so you may want to add this to a script file and run when using Qt Creator.

Permanent solution is:

1: Set 'ptrace_scope' to 0 in /etc/sysctl.d/10-ptrace.conf 
2: Restart the service using sudo sysctl -p

=== Build button disabled in QtCreator ===
If the build button is disabled in QtCreator, it probably means there are no kits defined. Go to '''Tools -> Options''' then '''Build & Run -> Kits''' to add a kit for Desktop QT with GCC.

== See also ==

Comparison [http://lists.blender.org/pipermail/bf-committers/2010-May/027444.html QtCreator/KDevelop4] by Xat