= Google Season of Docs 2019 - Ideas =

This page collects potential ideas and mentors for documentation projects in 2019.


== Contacting us ==

You can contact us via our [https://devtalk.blender.org/c/blender developers forum] or [http://projects.blender.org/mailman/listinfo/bf-docboard bf-docboard mailing list].<br>
Also on IRC: irc.freenode.net [irc://irc.freenode.net/blendercoders #blendercoders]

For matters you prefer to discuss in private, mail Ton Roosendaal: ton at blender.org.

== General information for writers and (potential) mentors ==

The list below provides ideas for projects that:

* A person can complete in 12 weeks (not too easy and not too hard);
* Offers documentation that would be a great improvement for Blender.

Choosing an idea from this list is ''not'' mandatory. 

== Inspiration ==

To get an idea of what sort of documentation can be done for Blender and to see the current development direction:

* [http://docs.blender.org The Blender Documentation project]
* [http://www.blender.org/2.8/ The Blender 2.8 project]
* [https://wiki.blender.org/wiki/Reference/Release_Notes Release logs]
* [https://code.blender.org/ Code Blog]

== Ideas ==

==== Blender 2.8 Migration Guide ====

* ''Description'': The next big Blender release has many changes in the UI and in how tools work. This document should allow an experienced Blender to understand all changes, and provide a quick reference to look up how things work now.
* ''Requirements'': Proficient with Blender, knowing the previous and next release
* ''Target audience'': Experienced Blender end-users
* ''Difficulty'': medium
* ''Possible mentors'': Bastien Montagne, Ton Roosendaal, Pablo Vazquez

==== Blender 2.8 Beginners Guide ====

* ''Description'': Provide a well balanced educational introduction to Blender 2.80. The purpose is not to explain how 3D works, but how to use Blender for basic 3D modeling and animation purposes.
* ''Requirements'': Proficient with Blender, knowing the current release well.
* ''Target audience'': People who understand the concepts of 3D (for example from other software), and who want to learn Blender.
* ''Difficulty'': easy to medium
* ''Possible mentors'': Ton Roosendaal, Aaron Carlisle, Campbell Barton



----