= Point Cloud Object =

The point cloud object is aimed at motion graphics, granular materials like sand, 3D scanning and as intermediate representation in geometry nodes and physics simulation.

* [https://projects.blender.org/blender/blender/issues/75717 #75717 on developer.blender.org]

== Data Structures ==

* `Object` can be of type `OB_POINTCLOUD`, and then uses a `PointCloud` datablock as object data.
* `PointCloud` is datablock that contains an array of points. Points have a position, radius, and arbitrary set of attributes.

== Rendering ==

Cycles and EEVEE can render point clouds and their attributes natively, which is more memory efficient than creating instances for each point.

* Cycles supports rendering points as spheres. Extending this to support disks or potentially other shapes is planned.
* EEVEE supports rendering points as instances of a low poly sphere mesh. We want to improve this to render them as perfect spheres, but doing this efficiently may require a higher minimum GPU requirement.

== Future Work ==

Point clouds can currently only be procedurally generated through geometry nodes, and can't be created or edited directly by users.

To make them useful for more use cases like 3D scanning, editing tools, import/export and other integration with the rest of Blender should be added.