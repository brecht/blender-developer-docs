<source lang="text">
NOTE: These docs are based on code comments in `BKE_geometry_set.hh`. When making changes, please keep that up to date too.
</source>

== Geometry Set ==

A geometry set is a container for multiple kinds of geometry. It does not own geometry directly
itself, instead geometry is owned by multiple Geometry Components, and the geometry set
increases the user count of each component, so they avoid losing the data. This means
individual components might be shared between multiple geometries and other code. Shared
components are copied automatically when write access is requested.

The components usually do not store data by themself, but keep a reference to a data structure
defined elsewhere in Blender. There is at most one component of each type:
* `MeshComponent`
* `CurveComponent`
* `PointCloudComponent`
* `InstancesComponent`
* `VolumeComponent`

Copying a geometry set is a relatively cheap operation, because it does not copy the referenced
geometry components, so `GeometrySet` can often be passed or moved by value.

== Geometry Components ==

=== Mesh Component ===
A geometry component that can store a mesh, using the `Mesh` data-block.

Attributes are stored, on any of the four attribute domains. Generic attributes are stored in
contiguous arrays, but often built-in attributes are stored in an array of structs fashion for
historical reasons, requiring more complex attribute access.

=== Point Cloud Component ===
A geometry component that stores a point cloud, corresponding to the `PointCloud` data structure.
While a point cloud is technically a subset of a mesh in some respects, it is useful because of
its simplicity, partly on a conceptual level for the user, but also in the code, though partly
for historical reasons. Point clouds can also be rendered in special ways, based on the built-in
`radius` attribute.

Attributes on point clouds are all stored in contiguous arrays in its `CustomData`,
which makes them efficient to process, relative to some legacy built-in mesh attributes.

=== Curve Component ===
A geometry component that stores a group of curves, corresponding the `Curves` data-block type
and the `CurvesGeometry` data structure. Attributes are are stored on the control point domain and the
curve domain.

=== Instances Component ===
A geometry component that stores instances. The instance data can be any type described by
`InstanceReference`. Geometry instances can even contain instances themselves, for nested
instancing. Each instance has an index into an array of unique instance data, and a transform.
The component can also store generic attributes for each instance.

The component works differently from other geometry components in that it stores
data about instancing directly, rather than owning a pointer to a separate data structure.

This component is not responsible for handling the interface to a render engine, or other
areas that work with all visible geometry, that is handled by the dependency graph iterator
(see `DEG_depsgraph_query.h`).

==== Instance Reference ====
An instance reference holds a reference to conceptually unique geometry or a pointer to
object/collection data that is is instanced with a transform in `InstancesComponent`.

=== Volume Component ===
A geometry component that stores volume grids, corresponding to the #Volume data structure.
This component does not implement an attribute API, partly because storage of sparse volume
information in grids is much more complicated than it is for other types