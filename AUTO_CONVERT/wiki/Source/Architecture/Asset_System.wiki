= Asset System =

Assets in Blender are data-blocks that are packaged for sharing/reuse. The asset system is the technical foundation that makes Blender able to deal with assets, and includes features like asset libraries, asset library loading, asset catalogs, asset metadata, etc. Assets can be displayed in the UI in various ways, including in the Asset Browser and asset views ("mini" Asset Browsers optimized for using, not organizing). ''Longer term, the asset system seeks to bring a future-proof understanding of assets for sharing and reuse as first class citizens to Blender.''

Currently assets have to represent ID data-blocks (Blender objects, collections, materials, etc.). But the asset system design foresees assets that can represent arbitrary entities, including files, or entities stored in some data-base.


{|class="note"
|-
|<div class="note_title">'''Note'''</div>
<div class="note_content">The asset system is still in heavy development.</div>
|}


== Subpages ==

[[Source/Architecture/Asset_System/FAQ|Frequently Asked Questions]]


'''Functional design:'''
* [[Source/Architecture/Asset_System/Asset_Bundles|Asset Bundles]]
* [[Source/Architecture/Asset_System/Brush_Assets|Brush Assets]]

'''Technical design:'''
* [[Source/Architecture/Asset_System/Back_End|Asset System Backend]]
* [[Source/Architecture/Asset_System/Catalogs|Asset Catalogs]]
* [[Source/Architecture/Asset_System/UI| User Interface]]
* [[Source/Architecture/Asset_System/Asset_Indexing|Asset Indexing]]

