= Materials Bundle =

The material bundle has procedural materials which don't need any texture files. Their functionality is encapsulated inside a node group per material. This way only high-level parameters are exposed in the properties editor.

[[File:Materials asset bundle.jpg|800px|thumb|center]]

== Catalogs ==

* Metal
* Wood
* Bricks
* Tiles
* Organic
* Fabric
* Concrete
* Glass
* Water
* Misc
* NPR