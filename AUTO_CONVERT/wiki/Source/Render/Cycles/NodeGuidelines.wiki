= Cycles Node Design =

== Guidelines ==

When adding or modifying nodes, keep these guidelines in mind:

* If possible, inputs should be in the 0.0..1.0 range. This is predictable, makes it easier to explore the range of possibilities. It also means it's easier to texture the inputs. If such a range is not possible or desirable, use physical units if possible.
* Similarly, the preferred range for outputs is 0.0..1.0. For example textures should in most cases output values in this range, to ensure energy conservation. An example of an exception is a HDR image texture, used for emission.
* Node input and output names should be short but not abbreviated. Node names may be longer, and some thought should be put into naming conflicts with possible future nodes.
* Inputs that have no reasonable values outside their range should be clamped. Nodes can't expect this to be ensured by the specified min/max range, as textures and other arbitrary node setups may result in values outside of the range.
* Closure color inputs are 0.8, 0.8, 0.8 by default. Full white rarely exists in the real world and may converge poorly.
* All nodes except for output nodes should have no side effects. That means they should not modify any shader data, and only output values into the output sockets.