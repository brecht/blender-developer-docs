= Cycles Render Scheduling =

== Overview ==

The main steps involved in rendering are:
* Allocate and clear render buffers
* Render N samples on devices
* Rebalance work between devices
* Adaptive sampling error redistribution and stopping
* Render buffer postprocessing
* Denoising
* Display texture update

To support interactive rendering and progressive display of offline renders, these steps are not executed once but dynamically scheduled. For example, adaptive sampling requires recomputing the error every few samples, and progressive rendering may denoise and re-display the render as more samples are added.

For interactive renders, scheduling aims for stable and low latency updates at the start, by rendering one or a few samples at a time. Resolution is automatically lowered if full resolution can't be achieved at high enough FPS. For offline rendering or interactive rendering after the first few seconds, we schedule larger batches of work to improve GPU occupancy and coherence.

== Display Driver ==

Cycles can be run inside a host application, that does its own CPU or GPU operations in parallel. To reduce one blocking the other, the render scheduler will update a [[Source/Render/Cycles/Driver|display driver]] in parallel to the host application. This happens periodically, and the render scheduler asks the host application to re-display the texture after each update.

For best performance, a dedicated OpenGL/Vulkan/.. context separate from the host application context should be used.