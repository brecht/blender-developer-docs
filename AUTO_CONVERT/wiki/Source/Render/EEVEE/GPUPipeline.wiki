__TOC__

== EEVEE GPU Pipeline (WIP) ==

This section describes how EEVEE uses the GPU to come to the final image. In this page we will use the [https://download.blender.org/demo/eevee/tree_creature/tree_creature.blend Tree Creature] demo file from Daniel Bystedt. As it uses many areas of the render pipeline.

Note that EEVEE is continuously in development and you should be aware that this document does not reflect the latest changes. Use this information to get a head start at understanding how EEVEE works from a developers point of view.



=== Update Light & Shadows ===

At the beginning of each sample EEVEE will make sure that the light probes, shadow maps and reflection planes are updated. When they need to be updated they will.


The first step is to update the shadow. During this step the textures that will contain the shadow maps are allocated and marked to be updated when a light source request to be updated.

* `eevee_shadows.c`#`EEVEE_shadows_update`
* `eevee_lightprobes.c`#`EEVEE_lightprobes_refresh`
* `eevee_lightprobes.c`#`EEVEE_lightprobes_refresh_planar`
* `eevee_shadows.c`#`EEVEE_shadows_draw`

=== Render the Depth Pass ===

The depth textures are updated. EEVEE has two depth texture (with and without culling)


Calculate a cubemap from the world.

=== Update the light probes and reflection planes ===

=== Draw Shadows ===

This step will update the shadow maps


=== Bloom ===

Bloom is a screen space effect that happens after the samples have been merged. It consist out of several screen space kernels.

* Down-sample the color buffer doing a small blur during each step
* Accumulate bloom color using previously down-samples color buffers and do an up-sample blur for each new accumulated layer
* Finally add accumulation buffer onto the source color buffer.

==== Important Files ====
* `eevee_bloom.c`
* `effect_bloom_frag.glsl`