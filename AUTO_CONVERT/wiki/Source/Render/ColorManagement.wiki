= Color Management =

Blender integrates OpenColorIO, but still misses some functionality to make it fully possible to use for example the ACES configuration.

* Cycles and EEVEE shader nodes need to support arbitrary working color space.
** Blackbody
** Wavelength
** Sky Texture
* Render file output and file saving needs to have a configurable color space to write to.
** By default it would use the scene display and view transform.
** This could then be changed to a color space, or a display + view transform + look.
* OpenEXR reading and writing should use chromaticity metadata to determine the color space of the image, and convert it to and from the working color space.
* The compositor should get a compositor node to convert between two color spaces defined in the OpenColorIO configuration.

* The bundled configuration can be improved:
** Add support for the latest ACES view transform, the current one is outdated.
* To add support for ACEScg as working space out of the box, a second configuration would need to be added that can be chosen per project.
** Such a configuration requires materials and lights to be set up with different values, and so is not compatible with most existing .blend files.
** Projects using such a configuration need to use it from the start so all assets can be created to work with it.

* To support 10 bit wide gamut monitors, Blender drawing code needs to be improved to output 10 bit instead of 8 bit when displaying the 3D viewport and images/renders.