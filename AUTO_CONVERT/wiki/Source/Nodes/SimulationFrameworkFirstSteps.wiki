= Simulation Framework - First Steps =

We want to have a new particle system.
Unfortunately, they way Blender currently handles simulations has problems explained in [[Source/Nodes/SimulationArchitectureProposal|this]] document.
Before actually implementing a new particle system, we need a more future-proof foundation for all kinds of simulations.
I like to call it "making simulations first-class-citizens in Blender".
This document proposes the next steps to achieve this goal.

I just want to say beforehand that we do not have to move every existing simulation type to the new framework before starting to work on a new particle system.
In fact, we don't have to move any of them in the beginning.
However, in the long term we should.

== Simulation World ==

The fundamental idea is to introduce the concept of a Simulation World in Blender.
A simulation world describes how a predefined set of objects interact.
For more details on how objects relate to simulation worlds, checkout the proposal linked above.

Part of the problem is to decide, who owns the simulation world and how it relates to appending/linking.
I do not have a definite answer for that yet, but I can propose two possible solutions.

=== Simulation World as ID ===

# Introduce a new "SimulationWorld" ID block (can't be more first-class than that).
# A scene has an unordered set of simulation worlds.
# A simulation world has references to all objects that are active or passive in it.
# Objects may only have weak/run-time references to simulation worlds.

This would allow e.g. appending a simulation world to a new file, and all relevant objects follow.
Furthermore, you can still link/append objects to another file, without the entire simulation world.
Open questions are whether a simulation world can owned by multiple scenes and how a linked in simulation world would work.

=== Simulation Worlds as part of the Scene ===

In this scenario we do not need a new ID type.
Instead the simulation world would just become a new struct in DNA.
A scene still owns a set of simulation worlds.
However, a simulation world cannot live on its own and is always owned by exactly one scene.

A consequence of that is that a simulation cannot be appended to another file alone.
Nevertheless, we want to be able to reuse e.g. particle system descriptions.
This is still possible by making lower level components of simulation worlds reusable (most notably node groups).

== Caching ==

For the moment I just assume that a simulation world can own and maintain it's own cache.
More details regarding the storage and file formats have to be the topic of a future document.

== Simulation Description ==

Abstractly speaking, a simulation world describes a function with the following signature:

* Inputs: Last state, elapsed time, final evaluated passive objects, pre-sim evaluated active objects.
* Outputs: New state, post-sim evaluated active objects mapped to pre-sim objects.

What's contained in the ''state'' depends on the specific kind of simulation.

The main question is: How to create an artist friendly user interface that allows defining this function and which objects should be passed into it?
I have an idea for how to do this for particle systems (using nodes), but the problem is much broader than that.

If (technical) artists want to help with that, please try to do the following:
# Find a video of a simulation that you would like to create in Blender.
# Try to think of a concise step by step guide on how to create this specific simulation in an ideal imaginary program.
# Send me or publish the video and the steps.

== Next Steps ==

First, I'll have to check with other developers if this "Simulation World" thing is actually something we want to do.
Assuming the answer is yes, these steps will follow:

# Decide how the simulation world is stored. Above are two possibilities, but I'm sure there are more.
# Implement the selected solution (I don't expect this to be a large amount of code).
# Implement a very simple simulation for testing (e.g. moving an object one step along the x axis per time unit). That also means that we have to hook it up into the dependency graph correctly. To do that, we also have to introduce the pre-sim and post-sim state of objects. Based on my current knowledge I can't really estimate the complexity of this step.
# Then we can start actually implementing a new node based particle system.