= Blender source code directories explained =

This page lists the directories in Blender's code and their use.

* [https://download.blender.org/ftp/ideasman42/pics/code_layout.webp Diagram showing code layout of modules].
* [https://projects.blender.org/blender/blender Browse the code online].

'''Note:''' this documents paths can be validated against Blender's source using: [https://projects.blender.org/blender/blender/src/branch/main/tools/check_wiki/check_wiki_file_structure.py check_wiki_file_structure].

== Directory Layout ==

=== Support Files ===

{| class="wikitable"
|-
! scope="col" width="43%"| DIRECTORY / FILE
! scope="col" | USE
|-
| /'''doc/'''
| Document generation (doxygen and sphinx), licenses, man page and file format info.<br>
Note that documentation may be comments in the code, or on our developer wiki. So this isn't the best place to look for introductory materials.
|-
| /'''locale/'''
| Translation files for Blender's interface and scripts.
|-
| /'''release/'''
| Contains data-files included with Blender releases, such as icons, fonts and platform specific files.

This also includes all Python scripts: add-ons, user-interface layout, utility modules.

''Even though in a sense this _is_ source-code, its simply copied into a directory for releases.''
|-
| /release/'''bin/'''
| Contains the "`.blender`" folder, with fonts and such files. These are copied in place the installation dir.
|-
| /release/'''darwin'''
| Files to be included for macOS installation.
|-
| /release/'''datafiles'''
| Fonts, icons, mat-cap images and other files included in Blender builds.
|-
| /release/'''freedesktop'''
| Desktop and icon files for the Linux desktop (X11/Wayland).
|-
| /release/'''license'''
| The full text for all licenses used by Blender and the libraries it's distributed with.
|-
| /release/'''lts'''
| Utilities to assist with the long term support (LTS) releases.
|-
| /release/'''text'''
| Readme and license files and text included with Blender installations.
|-
| /release/'''windows'''
| Files for MS-Windows installation.
|-
| /'''scripts/'''
| Python scripts that will be placed in Blender's installation directories
|-
| /'''tests/'''
| Files for testing.
|-
| /tests/'''blender_as_python_module/'''
| Tests for Blender when compiled as a Python module.
See: [https://docs.blender.org/api/current/info_advanced_blender_as_bpy.html Blender as a Python Module].
|-
| /tests/'''gtests/'''
| C & C++ tests using the gtest framework.
See: [http://wiki.blender.org/index.php/Dev:Doc/Tools/Tests/GTest Blender's gtest docs].
|-
| /tests/'''performance/'''
| Benchmarks.
|-
| /tests/'''python/'''
| Various tests for Blender's Python API, as well as other tests written in Python.
See: [http://wiki.blender.org/index.php/Dev:Doc/Tools/Tests/Python Blender's gtest docs].
|-
| /'''tools/'''
| Tools and utilities to assist with development (not required for building).
See: [https://wiki.blender.org/index.php/Dev:Doc/Tools/Blender_Tools_Repo Blender's Developer Tools docs].
|}

=== Build System ===

{| class="wikitable"
|-
! scope="col" width="43%"| DIRECTORY / FILE
! scope="col" | USE
|-
| /'''build_files/'''
| Files needed when building the source
|-
| /build_files/'''build_environment/'''
| Scripts for setting up a build environment (currently only used for Linux).
|-
| /build_files/'''buildbot/'''
| Configuration for automated builds.
Used for automated builds on [http://builder.blender.org builder.blender.org]

See: [http://buildbot.net home page].
|-
| /build_files/'''cmake/'''
| CMake build-system modules.
|-
| /build_files/'''config/'''
| Buildbot configuration, used to create daily builds on the [https://builder.blender.org builder.blender.org].
|-
| /build_files/'''package_spec/'''
| Specifications for Blender packages (currently supporting various popular Linux package formats).
Note: these aren't currently used for official releases which use a portable archive, building package files can be useful still.
|-
| /build_files/'''utils/'''
| Utility scripts to help with releases & building.
|-
| /build_files/'''windows/'''
| Files associated with convenience targets on MS-Windows.
Commands such as `make update` and `make icons` are implemented here.
|-
| /'''CMakeLists.txt'''
| CMake build-system file.
See: [http://cmake.org home page].
|-
| /'''GNUmakefile'''
| Make-file convenience wrapper for the CMake build-system, to quickly setup CMake builds without manual (configure, build, install) steps.
Used on Linux/BSD/maxOS.<br>There are various useful targets unrelated to building, see: `make help` for details.
|-
| /'''make.bat'''
| Batch-file convenience wrapper for the CMake build-system, to quickly setup CMake builds without manual (configure, build, install) steps.
Used on MS-Windows.<br>There are various useful targets unrelated to building, see: `make help` for details.
|}

=== Application Code (source/) ===

{| class="wikitable"
|-
! scope="col" width="43%"| DIRECTORY / FILE
! scope="col" | USE
|-
| /'''source/'''
| Main source code directory for code maintained by Blender developers.
|-
| /source/'''blender/'''
| Source code directory for Blender's application code.
|-
| /source/blender/'''asset_system/'''
| [[Source/Architecture/Asset_System/Back_End|Asset system back-end]] (asset representations, asset libraries, catalogs, etc.).
|-
| /source/blender/'''blendthumb/'''
| Stand-alone utility for extracting preview thumbnails from `.blend` files so thumbnails can be shown by file-managers.
|-
| /source/blender/'''blenfont/'''
| Blender's internal font system
|-
| /source/blender/'''blenkernel/'''
| Kernel functions (data structure manipulation, allocation, free. No tools or UI stuff, very low level).
|-
| /source/blender/'''blenlib/'''
| Internal misc libraries: math functions, lists, random, noise, memory pools, file operations (platform agnostic).
|-
| /source/blender/'''blenloader/'''
| Blend file loading and writing as well as in memory undo file system.
|-
| /source/blender/'''blentranslation/'''
| Internal support for non-English translations (uses [http://www.gnu.org/software/gettext gettext]).
Used by the user interface for displaying text labels and tool-tips.

See: [[Dev:Doc/Process/Translate_Blender|Blender's translations docs]].
|-
| /source/blender/'''bmesh/'''
| Mesh editing API, with support for n-gons and comprehensive editing operations.
Used in ''Edit Mode'' and by modifiers that perform advanced editing operations (such as bevel).

See: [[Source/Modeling/BMesh/Design|BMesh Design]].
|-
| /source/blender/'''compositor/'''
| Compositor nodes, operations and execution logic.
|-
| /source/blender/'''datatoc/'''
| Utility to convert data files (images, fonts & shaders) into C source code files containing that data.
Used by the build system to convert data into symbols that can be compiled into the executable.
|-
| /source/blender/'''depsgraph/'''
| Dependency graph API. Used to track relations between various pieces of data in a Blender file.
The evaluation engine is responsible for updating and scheduling the operation nodes according to their constraints and relations.
|-
| /source/blender/'''io/'''
| Input/output libraries for various format (including 2D & 3D file formats).
|-
| /source/blender/io/'''avi/'''
| AVI loading and writing code (Only supporting primitive AVI raw, AVI jpeg and windows codec, not ffmpeg)
|-
| /source/blender/io/'''collada/'''
| Collada interface between blender and the external collada library
|-
| /source/blender/'''draw/'''
| Viewport drawing and EEVEE rendering engine.
|-
| /source/blender/'''editors/'''
| Graphical editor code, tools, UI ... (most of the interesting code is in there)
|-
| /source/blender/editors/'''animation/'''
| F-Curves, keyframes utilities and UI drawing for NLA, Grease Pencil and Graph Editor.
|-
| /source/blender/editors/'''armature/'''
| API and related functions and operations for bones, poses and armatures in edit mode.
|-
| /source/blender/editors/'''asset/'''
| Asset editor: operators and asset manager implementation (uses the `asset_system`).
|-
| /source/blender/editors/'''curve/'''
| Curves and fonts edit operators (selection, copy, extrude...).
Note that this is considered legacy, new development is happening in `/source/blender/editors/curves/`.
|-
| /source/blender/editors/'''curves/'''
| Curves editing functions and operators (editing, selection, conversion & undo).
|-
| /source/blender/editors/'''datafiles/'''
| Single `CMakeLists.txt` that aggregates various editor related datafiles (brushes, blend files, images...).
|-
| /source/blender/editors/'''geometry/'''
| Generic operators for managing geometry attributes which may apply to meshes, curves & point clouds.
|-
| /source/blender/editors/'''gizmo_library/'''
| 3D and 2D shapes such as arrows &  that can be used to drag gizmos (the transform gizmo in the 3D view-port for example). 
|-
| /source/blender/editors/'''gpencil/'''
| Grease pencil editing functions and operators (drawing, armature, selection...).
|-
| /source/blender/editors/'''include/'''
| Include directory for editor related header files.
|-
| /source/blender/editors/'''interface/'''
| Widget drawing, widget handling, widget layout generation, 2D editor scrolling, interface operators, icon API, theme management.
|-
| /source/blender/editors/'''io/'''
| input/Output operators for Alembic, Collada, USD and Grease Pencil.
Note that the implementation is located under `/source/blender/io/`.
|-
| /source/blender/editors/'''lattice/'''
| Lattice related editor functions (selection, duplicate, undo...).
|-
| /source/blender/editors/'''mask/'''
| 2D mask editing & operators for creating, selecting, modifying & animating masks which can used by the compositor and sequencer.
|-
| /source/blender/editors/'''mesh/'''
| Mesh edit operators (bevel, extrude, knife...) 
|-
| /source/blender/editors/'''metaball/'''
| Meta-Ball related editor functions (selection, duplicate, undo...)
|-
| /source/blender/editors/'''object/'''
| Object level operators including Add, Transform, Modifiers, Selection, Bake...
|-
| /source/blender/editors/'''physics/'''
| Physics related operators for editing particles, fluids or dynamic paint.
|-
| /source/blender/editors/'''render/'''
| 3D viewport and render engine operators in the editor (view layers, freestyle, material slots, render...)
|-
| /source/blender/editors/'''scene/'''
| Scene operators (new & delete).
|-
| /source/blender/editors/'''screen/'''
| Screen layout operators for modifying and editing panels, menus and windows.
|-
| /source/blender/editors/'''sculpt_paint/'''
| Sculpt and paint operators (weight paint, texture paint and sculpt).
|-
| /source/blender/editors/'''sound/'''
| Sound operators (open, bake animation, pack, unpack...)
|-
| /source/blender/editors/'''space_action/'''
| Action editor: between the low-level F-Curves, and the high-level NLA editor.
|-
| /source/blender/editors/'''space_api/'''
| Registering space types, space macros and keymaps. Includes a templace for implementing a custom space editor.
|-
| /source/blender/editors/'''space_buttons/'''
| Buttons utilities (property search, context menu, file browse...)
|-
| /source/blender/editors/'''space_clip/'''
| Movie clip and tracking operators in the editor (solving, tracking, clip graph...)
|-
| /source/blender/editors/'''space_console/'''
| Python console UI and operators.
|-
| /source/blender/editors/'''space_file/'''
| File browser UI and operators (selection, folders, bookmarks... )
|-
| /source/blender/editors/'''space_graph/'''
| Graph related UI and operators for dealing with keyframes and FCurve modifiers.
|-
| /source/blender/editors/'''space_image/'''
| Image viewer UI and operators (zooming, panning, render slots...)
|-
| /source/blender/editors/'''space_info/'''
| Scene statistics and info.
|-
| /source/blender/editors/'''space_nla/'''
| Non Linear Action editor UI and operators.
|-
| /source/blender/editors/'''space_node/'''
| Node editor drawing and operators (hide, selection, link...)
|-
| /source/blender/editors/'''space_outliner/'''
| Outliner UI and operators for moving, displaying and editing the tree.
|-
| /source/blender/editors/'''space_script/'''
| Python script editor (deprecated)
|-
| /source/blender/editors/'''space_sequencer/'''
| Sequencer UI and operators for interacting with strips, modifiers and proxies.  
|-
| /source/blender/editors/'''space_spreadsheet/'''
| Spread-sheet UI to explore geometry data in a table.
|-
| /source/blender/editors/'''space_statusbar/'''
| Interface code for the status-bar (shown at the bottom of the window).
|-
| /source/blender/editors/'''space_text/'''
| Text editor UI and operators. Used as a basic text editor for raw text and Python scripts.
|-
| /source/blender/editors/'''space_topbar/'''
| Interface code for the top-bar (shown at the top of the window) including the file menu and workspace tabs.
|-
| /source/blender/editors/'''space_userpref/'''
| User preference operators (auto execution, asset library)
|-
| /source/blender/editors/'''space_view3d/'''
| 3D view operators (rotate, pan, move, select...) and gizmos (lights, forcefields, ruler, camera...)
|-
| /source/blender/editors/'''transform/'''
| Transform operators across different spaces including object, graph, curve or greasepencil.
|-
| /source/blender/editors/'''undo/'''
| High level undo logic (undo, redo operators & access to the undo history). The undo system is defined in `blenkernel`.
|-
| /source/blender/editors/'''util/'''
| Utility operators for UI data or for the UI to use.
|-
| /source/blender/editors/'''uvedit/'''
| UV editor operators (selection, align, pack islands...)
|-
| /source/blender/'''functions/'''
| Run-time type system that implements lazy functions, multi-function network & generic functions.
Used by geometry nodes.
|-
| /source/blender/'''geometry/'''
| Geometry (Mesh) functions such as merging vertices, converting mesh to curves and other geometry manipulation operations.

Used by modifiers and geometry nodes.
|-
| /source/blender/'''gpencil_modifiers/'''
| Grease pencil modifiers.
|-
| /source/blender/'''gpu/'''
| GLSL shaders, buffers and other [[Dev:Doc/Blender_Source/Files_Structure#GPU|GPU]] related functions
|-
| /source/blender/io/'''alembic/'''
| Support for alembic, a computer graphics interchange framework.
See: [http://www.alembic.io home page].
|-
| /source/blender/io/'''common/'''
| Shared functionality for importers and exporters.
|-
| /source/blender/'''freestyle/'''
| Freestyle NPR rendering engine (scene graph, Python code, stroke, geometry.. etc).
|-
| /source/blender/io/'''gpencil/'''
| Grease-pencil support for 2D PDF & SVG export and SVG import.
|-
| /source/blender/io/'''stl/'''
| Import/export support for the STL 3D file format.
See: [https://en.wikipedia.org/wiki/STL_(file_format) Wikipedia].
|-
| /source/blender/io/'''usd/'''
| Support for Universal Scene Description (USD), an open and extensible framework and ecosystem for describing, composing, simulating and collaborating within 3D worlds.
See: [https://graphics.pixar.com/usd home page].
|-
| /source/blender/io/'''wavefront_obj/'''
| Import/export support for the Wavefront OBJ file format.
See: [https://en.wikipedia.org/wiki/Wavefront_.obj_file Wikipedia].
|-
| /source/blender/'''ikplugin/'''
| Inverse kinematics API, abstracting the itasc and iksolver modules.<br>Note that while this system supports multiple inverse-kinematics solvers there is no ''plug-in'' system exposed from a user perspective.
|-
| /source/blender/'''imbuf/'''
| Image buffer API and functions (loading, writing and manipulating different image formats); seq just has it's own buffer structure, but the loading is still done by imbuf; imbuf also has code for loading textures into openGL for display and all that
|-
| /source/blender/'''makesdna/'''
| DNA structures definition: All data structures that are saved into files are here
|-
| /source/blender/'''makesrna/'''
| RNA definitions and functions. Sits on top of DNA to provide a low level data access and definition API
|-
| /source/blender/'''modifiers/'''
| Object modifiers (used for meshes, curves and lattices).
|-
| /source/blender/'''nodes/'''
| Nodes code: `CMP`: composite, `GEO`: geometry, `SHD`: material, `TEX`: texture.
|-
| /source/blender/'''python/'''
| Python API, source files that embed the [http://www.python.org Python] scripting language into Blender, as well as the API's Blender exposes which are written in C.
See: [http://www.blender.org/api/blender_python_api_current Python API Reference].
|-
| /source/blender/python/'''bmesh/'''
| The Python API for editing mesh data.
|-
| /source/blender/python/'''generic/'''
| The Python API generic functions (font, opengl, noise, ...)
|-
| /source/blender/python/'''gpu/'''
| The Python API for drawing using the GPU (geometry, shaders, frame-buffers & selection).
|-
| /source/blender/python/'''intern/'''
| The primary parts of the API (bpy module, wraps ''Operators'' and ''RNA'').
|-
| /source/blender/python/'''mathutils/'''
| The Python API 3D math module (access to Euler, Vector, Matrix, Quaternion, Color types).
|-
| /source/blender/'''render/'''
| The rendering API and pipeline
|-
| /source/blender/'''sequencer/'''
| Blender's video sequencer editor (sequencer).
|-
| /source/blender/'''shader_fx/'''
| Real-time effects (currently used for grease-pencil).
|-
| /source/blender/'''simulation/'''
| Solver for cloth simulation, used by the `BKE_cloth` API.
|-
| /source/blender/'''windowmanager/'''
| Blender's internal window manager, event system, operators, thread jobs, ...
|-
| /source/'''creator/'''
| Contains the `main(...)` entry point function for the Blender executable. Handles initialization and command line argument parsing.
|}

=== Internal Library Code (intern/) ===

{| class="wikitable"
|-
! scope="col" width="43%"| DIRECTORY / FILE
! scope="col" | USE
|-
| /'''intern/'''
| Abbreviation for ''internal''. This directory contains source-code maintained by Blender developers but kept as isolated modules/libraries.
Some of these modules are C-API's to libraries written in C++, since much of Blender's core is written in C and can't call into C++ code directly.
|-
| /intern/'''atomic/'''
| Low level atomic operations, for thread-safe atomic updates of variables.

See jemalloc's [http://www.canonware.com/jemalloc home page] for the original source.
|-
| /intern/'''audaspace/'''
| An internal C API for ``audaspace``, see: ``/extern/audaspace``.
|-
| /intern/'''clog/'''
| C-Logging library, API (similar to glog).

Use for logging output in Blender's C code.
|-
| /intern/'''cycles/'''
| Cycles rendering engine.
|-
| /intern/'''dualcon/'''
| This library is used to re-create geometry.
Used by the remesh modifier.
|-
| /intern/'''eigen/'''
| Library written in C used to wrap the C++ Eigen3 library.
|-
| /intern/'''ffmpeg/'''
| Library used for video encoding and decoding.
Used for reading and writing many different video formats (movie clip editor, sequencer, render output... etc).
|-
| /intern/'''ghost/'''
| Stands for ''General Handy Operating System Toolkit''.

This library abstracts platform specific operations.
So we can avoid using macOS/Wayland/WIN32/X11 API calls in the rest of Blender's code.

It handles window management actions, the OpenGL context and reading events from the mouse and keyboard,
it also supports less common input devices such as a tablet or [[#NDOF|NDOF]] device.
|-
| /intern/'''guardedalloc/'''
| This Blender primary memory allocator which is used in place of the system allocator. It has facilities for (optionally) guarding against common memory errors. Enabled when running with the `--debug` command-line argument.
Used throuought nearly all of Blender's own code.
|-
| /intern/'''iksolver/'''
| Inverse kinematics solver, to constraint a chain of bone to a target.
Used by the IK armature constraint.
|-
| /intern/'''itasc/'''
| A library used for the [[#IK|IK]] constraint solver ''iTaSC'', an alternate solver useful for robotics.
Used by the ''IK plug-in system''.
|-
| /intern/'''libc_compat/'''
| Improved GLIBC compatibility on Linux.
|-
| /intern/'''libmv/'''
| A motion tracking library.
Used for motion-tracking in the movie-clip editor.
|-
| /intern/'''locale/'''
| Wrapper for `gettext` translation library, not intended for standalone use.
Used by `blentranslation`.
|-
| /intern/'''mantaflow/'''
| Fluid & smoke simulation library (internal API to wrap `extern/mantaflow`).
|-
| /intern/'''memutil/'''
| Utility library with C++ memory management classes.
Generic cache-limiter and reference-counted implimentations and their C-API's.
|-
| /intern/'''mikktspace/'''
| Utility module for calculating tangents from mesh geometry (triangles and quads) and their UV coordinates.
Used by the DerivedMesh system and Blender Internal Render Engine.
|-
| /intern/'''opencolorio/'''
| A C-API to the OpenColorIO Library (often abbreviated to OCIO).
Used by image loading and saving code to support color-management, as well as rendering.

See [http://opencolorio.org home page].
|-
| /intern/'''opensubdiv/'''
| Subdivision surface library.
Used by Blender's subdivision surface modifier.

See: [http://graphics.pixar.com/opensubdiv home page].
|-
| /intern/'''openvdb/'''
| Library used for efficiently store volumetric data.
Used for storing smoke simulation cache.
|-
| /intern/'''quadriflow/'''
| A C-API for the quadreflow remeshing library (see `extern/quadriflow`).
|-
| /intern/'''renderdoc_dynload/'''
| RenderDoc is a graphic debugger for multiple platforms and output systems.
|-
| /intern/'''rigidbody/'''
| A C-API to the Bullet physics library.
Used by the rigid-body physics system.
|-
| /intern/'''sky/'''
| Parametric sky texture (used by Cycles and EEVEE).
|-
| /intern/'''utfconv/'''
| Utility functions for converting between unicode `utf-8` and `utf-16` (`wchar_t`).
Used for file operations on MS-Windows, which require `utf-16`.
|-
| /intern/'''wayland_dynload/'''
| Support for dynamically loading WAYLAND and related libraries on Linux. Allowing WAYLAND to be supported on Linux without it being a requirement.
|}

=== External Library Code (extern/) ===

{| class="wikitable"
|-
! scope="col" width="43%"| DIRECTORY / FILE
! scope="col" | USE
|-
| /'''extern/'''
| Abbreviation for ''external'',
This directory contains source-code imported from other projects and not apart of Blender (strictly speaking).

Each library is stand-alone and typically included
because they aren't common system libraries we can rely on the system providing
(such as `libjpeg`, `zlib` and `libc`).
|-
| /extern/'''audaspace/'''
| An audio library which abstracts playback across different back ends, as well as handling 3D audio.
Used for audio playback in the sequencer, speaker objects and by the ''Game Engine''.

See: [http://github.com/neXyon/audaspace home page].
|-
| /extern/'''binreloc/'''
| Utility to find the absolute path of the currently running executable on Linux.
Used for launching animation playback and some scripts which launch a Blender sub-process.

See: [http://web.archive.org/web/20090125113506/http://autopackage.org/docs/binreloc archived home page].
|-
| /extern/'''bullet2/'''
| A real-time physics library.
Used for animating rigid-body physics and in the ''Game Engine''.

See: [http://bulletphysics.org home page].
|-
| /extern/'''ceres/'''
| Ceres Solver is a C++ library for modeling and solving large, complicated optimization problems.
Used for motion tracking.

See: [http://ceres-solver.org home page].
|-
| /extern/'''clew/'''
| An [[#extension wrangler|extension wrangler]] library for OpenCL, a library used for executing code both on the CPU and GPU with the ability to take advantage of many [[#GPU|GPU]] cores.
|-
| /extern/'''cuew/'''
| CUDA Extension Wrangler.

See: [http://github.com/CudaWrangler/cuew home page].
|-
| /extern/'''curve_fit_nd/'''
| Library for calculating bezier (cubic) curve paths from input points.
Used for freehand curve drawing.

See: [https://github.com/ideasman42/curve-fit-nd home page]
|-
| /extern/'''draco/'''
| An open-source library for compressing and decompressing 3D geometric meshes and point clouds.
Used by the Python `draco` module which is used by the `io_scene_gltf2` add-ons (GLTF support).

See: [https://google.github.io/draco home page].

|-
| /extern/'''Eigen3/'''
| A linear algebra header-only library to perform vector, matrix math operations and a sparse matrix solver.
Used by motion-tracking, ITASC [[#IK|IK]] solver and BLI math library.

See: [http://eigen.tuxfamily.org/index.php home page].
|-
| /extern/'''fast_float/'''
| Fast conversion of text to floating point values.
Used by the OBJ and STL importers.
|-
| /extern/'''fmtlib/'''
| Formatting library providing a fast and safe alternative to C stdio and C++ iostreams.
Used by the wavefront OBJ exporter.

See: [https://github.com/fmtlib/fmt home page].
|-
| /extern/'''gflags/'''
| A C++ command line flag parsing library.
Not used directly, only included as a dependency for glog.
|-
| /extern/'''glew-es/'''
| An [[#extension wrangler|extension wrangler]] library for OpenGL-ES. A limited version of OpenGL, supported on mobile devices.
Note that OpenGL-ES is currently not supported.
|-
| /extern/'''glog/'''
| A C++ logging framework.
Used by `/extern/ceres`, `/intern/libmv` & `/tests/gtests`.

See: [http://github.com/google/glog home page].
|-
| /extern/'''gmock/'''
| A mocking library for C++.
Used by `gtest`.

See: [https://google.github.io/googletest/gmock_for_dummies.html home page].
|-
| /extern/'''gtest/'''
| A C++ testing framework.
Used for testing C and C++ code in ``tests/``

See: [http://github.com/google/googletest home page].
|-
| /extern/'''hipew/'''
| The HIP Extension Wrangler Library (HIPEW) is a cross-platform open-source C/C++ library to dynamically load the HIP library. HIP (Heterogeneous-Compute Interface for Portability) is an API for C++ programming on AMD GPUs.
Used by cycles.
|-
| /extern/'''json/'''
| JavaScript Object Notation (JSON) library for modern C++.
Used by `tinygltf` and `BLI_serialize`.

See: [https://github.com/nlohmann/json home page].
|-
| /extern/'''lzma/'''
| A compression library focusing on best compression at the expense of processing time.
Used for compressing physics cache.

See: [http://www.7-zip.org/sdk.html home page].
|-
| /extern/'''lzo/'''
| A compression library focusing on fast compression at the expense of final file-size.
Used for compressing physics cache.

See: [http://www.oberhumer.com/opensource/lzo home page].
|-
| /extern/'''mantaflow/'''
| Fluid & smoke simulation library.
See: [http://mantaflow.com home page].
|-
| /extern/'''nanosvg/'''
| Support for reading scalable-vector-graphics (SVG), used by grease-pencil SVG import.
See: [https://github.com/memononen/nanosvg home page].
|-
| /extern/'''quadriflow/'''
| Quad re-meshing library, used for sculpt-mode remeshing.
See: [https://github.com/hjwdzh/QuadriFlow home page].
|-
| /extern/'''rangetree/'''
| Basic library for storing non-overlapping scalar ranges.
Used by dynamic-topology sculpting, to store unique ID's for undo data.
See: [https://github.com/ideasman42/rangetree-c home page].

|-
| /extern/'''renderdoc/'''
| Graphics debugger
|-
| /extern/'''sdlew/'''
| An [[#extension wrangler|extension wrangler]] library for the SDL library.
Used for audio and joystick support in the ''Game Engine''.

See: [http://github.com/SDLWrangler/sdlew home page].
|-
| /extern/'''smaa_areatex/'''
| A C++ implementation of Enhanced Subpixel Morphological Antialiasing (SMAA).
Used by the viewport and GPU compositor.

See: [https://github.com/iRi-E/smaa-cpp home page.]
|-
| /extern/'''tinygltf/'''
| A C++ library for loading 3D models in the GLTF file format.
Used by Blender's virtual reality support (`GHOST_XrControllerModel`).

See: [https://github.com/syoyo/tinygltf home page].
|-
| /extern/'''vulkan_memory_allocator/'''
| Easy to integrate Vulkan memory allocation library.
Used by Blender's Vulkan GPU back-end.

See: [https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html home page].
|-
| /extern/'''wcwidth/'''
| Support for fixed width unicode characters, where some characters use multiple ''cells''.
Used for the text editor and Python console.

See: [http://www.cl.cam.ac.uk/~mgk25/ucs/wcwidth.c online source].
|-
| /extern/'''xdnd/'''
| Utility to support drag and drop on Unix/X11.
Used to support dropping files onto Blender's window.
|}

__TOC__